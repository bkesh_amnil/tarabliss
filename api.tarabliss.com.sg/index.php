<?php

/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Script that serves the API method requested.
 * - Routes a HTTP request to an endpoint.
 * 
 * Invoked by front end applications (possibly AJAX) through the world wide web.
 *
 * @input: $_GET['api', 'method']
 ***/
try {
           
	require __DIR__ . '/init.php';

	// Include classes that will definitely be used during serving of API functions
	require API_ROOT.'/classes/HTTPResponse.php';
	require API_ROOT.'/classes/persistence/Session.php';

	// Set CORS domains - should not be necessary under normal circumstance
	HTTPResponse::addCORSAllowedOrigin(('http://tara.front.dev'));
	HTTPResponse::addCORSAllowedOrigin(('http://tara.staff.dev'));
	//HTTPResponse::addCORSAllowedOrigin('https://www.some-other-host-2.com');

	//DEBUG
	Log::debug('index.php from '.$_SERVER['HTTP_ORIGIN'].' Host: '.$_SERVER['HTTP_HOST'].' '.$_GET['method']);
	
	// Handle CORS Pre-flight: simply return immediately
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		$response = new HTTPResponse();
		$response->deliver();
		exit(0);
	}
	
	// Argument checks
	if (!isset($_GET['api'])) {
		$response = new HTTPResponse(400);
		$response->addData('error', 'Missing argument(s).');
		$response->deliver();
		exit(0);
	}
	if (!isset($_GET['method'])) {
		$response = new HTTPResponse(400);
		$response->addData('error', 'Missing argument: method');
		$response->deliver();
		exit(0);
	}

	// Check valid API class
	$api_class = formatPascalCase($_GET['api']) . 'API';
	if (!is_readable(API_ROOT . '/classes/apis/' . $api_class . '.php')) {
		$response = new HTTPResponse(400);
		$response->addData('error', 'No such API: ' . $_GET['api']);
		$response->deliver();
		exit(0);
	}
	$api = new $api_class();
	unset($_GET['api']);

	// Check that action is a callable function
	$fn = formatCamelCase($_GET['method']);
	if (!is_callable(array($api, $fn))) {
		$response = new HTTPResponse(400);
		$response->addData('error', 'Requested method "' . $fn . '" is not callable in ' . $api_class);
		$response->deliver();
		exit(0);
	}
	unset($_GET['method']);
	
	Session::start();
	
	// Call fn and return response. Result will be stored in $response->data
	$response = new HTTPResponse();
	$response_code = $api->$fn($response);
	$response->setResponseCode($response_code);
	$response->deliver();
	exit(0);
} //try
catch (Exception $e) {
	Log::fatal('serve-api.php error - ' . $e->getMessage() . "\n" . $e->getTraceAsString());

	// Return error and terminate
	$response = new HTTPResponse(500);
	$response->setData(array('error' => $e->getMessage()));
	$response->deliver();
	exit(1);
} //catch
