/***
 * CQL Schema
 *
 * Note:
 * - CQL data types: https://docs.datastax.com/en/cql/3.1/cql/cql_reference/cql_data_types_c.html
 * - CQL table/column names are case-insensitive
 *
 * In order for installer to parse queries correctly:
 * - Ensure only CREATE TABLE or CREATE INDEX ON statements.
 * - Ensure each statement ends with a ; (semi-colon).
 * - The installer will render the following templates:
 *     {{table-prefix}}                        e.g. "TB_"
 */

/* Setting */
CREATE TABLE {{table-prefix}}Setting (
	"key" varchar,
	"value" varchar,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY("key")
);

/* DataList */
CREATE TABLE {{table-prefix}}DataList (
	"name" varchar,
	"json" varchar,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY("name")
);

/* Page */
CREATE TABLE {{table-prefix}}Page (
	"domainLabel" varchar,
	"name" varchar,
	"template" varchar,
	"data" varchar,
	"title" varchar,
	"description" varchar,
	"html" varchar,
	"css" varchar,
	"js" varchar,
	"img" varchar,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY("domainLabel", "name")
);

/* Document */
CREATE TABLE {{table-prefix}}Document (
	"domainLabel" varchar,
	"fileName" varchar,
	"blob" blob,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY("domainLabel", "fileName")
);

/* Image */
CREATE TABLE {{table-prefix}}Image (
	"domainLabel" varchar,
	"fileName" varchar,
	"blob" blob,
	"width" int,
	"height" int,
	"title" varchar,
	"description" varchar,
	"copyright" varchar,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY("domainLabel", "fileName")
);

/* PrivateImage */
CREATE TABLE {{table-prefix}}PrivateImage (
	"userID" int,
	"userType" varchar,
	"fileName" varchar,	
	"blob" blob,
	"width" int,
	"height" int,
	"title" varchar,
	"description" varchar,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY(("userID", "userType"), "fileName")
);

/* Activity */
CREATE TABLE {{table-prefix}}Activity (
	"userID" int,
	"userType" varchar,
	"action" varchar,
	"subjectID" int,
	"subjectType" varchar,
	"remarks" varchar,
	"createdTime" bigint,
	PRIMARY KEY(("userID", "userType"), "createdTime")
);
CREATE INDEX on {{table-prefix}}Activity (action);

/* Tag */
CREATE TABLE {{table-prefix}}Tag (
	"type" varchar,
	"value" varchar,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY("type", "value")
);

/* TherapyImage */
CREATE TABLE {{table-prefix}}TherapyImage (
	"therapyID" int,
	"fileName" varchar,
	"blob" blob,
	"width" int,
	"height" int,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY("therapyID", "fileName")
);

/* CategoryImage */
CREATE TABLE {{table-prefix}}CategoryImage (
	"categoryID" int,
	"fileName" varchar,
	"blob" blob,
	"width" int,
	"height" int,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY("categoryID", "fileName")
);

/* ArticleImage */
CREATE TABLE {{table-prefix}}ArticleImage (
	"articleID" int,
	"fileName" varchar,
	"blob" blob,
	"width" int,
	"height" int,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY("articleID", "fileName")
);

/* AccoladeImage */
CREATE TABLE {{table-prefix}}AccoladeImage (
	"accoladeID" int,
	"fileName" varchar,
	"blob" blob,
	"width" int,
	"height" int,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY("accoladeID", "fileName")
);

/* AccoladeDocument */
CREATE TABLE {{table-prefix}}AccoladeDocument (
	"accoladeID" int,
	"fileName" varchar,
	"blob" blob,
	"createdTime" bigint,
	"lastUpdateTime" bigint,
	PRIMARY KEY("accoladeID", "fileName")
);