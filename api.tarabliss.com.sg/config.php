<?php
/***
 * Framework v2.1.0
 * =================
 *
 * System settings that deteremine behaviour of the server.
 * - Note: should not be exposed to end users. For configuration that administrator users can control, use user-config.json
  ***/

define('PROJECT_DOMAIN', 'tarabliss.com.sg');
define('PROJECT_NAME', 'Tara Bliss');

/*****************************
 *  Pathing / DB constants   *
 *****************************/
define('PROTOCOL', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'])?'https://':'http://'));
define('API_ROOT', __DIR__);

///* Development
define('WWW_ROOT', dirname(API_ROOT).'/www.tarabliss.com.sg');
define('STAFF_ROOT', dirname(API_ROOT).'/staff.tarabliss.com.sg');
define('API_URL', PROTOCOL.'tara.front.dev');
define('WWW_URL', PROTOCOL.'tara.api.dev');
define('PROMOS_URL', PROTOCOL.'localhost:8052');
define('STAFF_URL', PROTOCOL.'localhost:8053');
define('MYSQL_HOST', '127.0.0.1');
define('MYSQL_DB_NAME', 'tarabliss');
define('MYSQL_USER', 'root');
define('MYSQL_PASS', '');
define('REDIS_HOST', '127.0.0.1');
define('REDIS_PORT', 6379);
define('CASSANDRA_HOST', '127.0.0.1');
define('CASSANDRA_PORT', 9042);
define('CASSANDRA_KEYSPACE', 'tarabliss');
//*/

/* Staging
define('WWW_ROOT', dirname(API_ROOT).'/staging.www.tarabliss.com.sg');
define('STAFF_ROOT', dirname(API_ROOT).'/staging.staff.tarabliss.com.sg');
define('API_URL', PROTOCOL.'staging.api.tarabliss.com.sg');
define('WWW_URL', PROTOCOL.'staging.www.tarabliss.com.sg');
define('PROMOS_URL', PROTOCOL.'staging.promos.tarabliss.com.sg');
define('STAFF_URL', PROTOCOL.'staging.staff.tarabliss.com.sg');
define('MYSQL_HOST', '10.240.44.227');
define('MYSQL_DB_NAME', 'staging_tarabliss');
define('MYSQL_USER', 'tarabliss_user');
define('MYSQL_PASS', 'vL2dSupZB5B');
define('REDIS_HOST', '127.0.0.1');
define('REDIS_PORT', 6379);
define('CASSANDRA_HOST', '10.240.44.227');
define('CASSANDRA_PORT', 9042);
define('CASSANDRA_KEYSPACE', 'staging_tarabliss');
//*/

/* Production
define('WWW_ROOT', dirname(API_ROOT).'/www.tarabliss.com.sg');
define('STAFF_ROOT', dirname(API_ROOT).'/staff.tarabliss.com.sg');
define('API_URL', PROTOCOL.'api.tarabliss.com.sg');
define('WWW_URL', PROTOCOL.'www.tarabliss.com.sg');
define('PROMOS_URL', PROTOCOL.'promos.tarabliss.com.sg');
define('STAFF_URL', PROTOCOL.'staff.tarabliss.com.sg');
define('MYSQL_HOST', '10.240.44.227');
define('MYSQL_DB_NAME', 'tarabliss');
define('MYSQL_USER', 'tarabliss_user');
define('MYSQL_PASS', 'vL2dSupZB5B');
define('REDIS_HOST', '127.0.0.1');
define('REDIS_PORT', 6379);
define('CASSANDRA_HOST', '10.240.44.227');
define('CASSANDRA_PORT', 9042);
define('CASSANDRA_KEYSPACE', 'tarabliss');
//*/

define('MYSQL_ENGINE', 'InnoDB');
define('MYSQL_TABLE_PREFIX', 'TB_');
define('CASSANDRA_TABLE_PREFIX', 'TB_');

/******************************
 *          Sessions          *
 ******************************/
define('SESSION_NAME', 'SESSIONID');
define('SESSION_DURATION', 1800); // seconds

define('HTML_PAGES_REFRESH_PERIOD', 60); // seconds
define('IMAGES_REFRESH_PERIOD', 60); // seconds
define('DOCUMENTS_REFRESH_PERIOD', 60); // seconds
define('DATALISTS_REFRESH_PERIOD', 60); // seconds
define('SETTINGS_CACHE_PERIOD', 60); // seconds
define('THERAPY_IMAGES_REFRESH_PERIOD', 60); // seconds
define('CATEGORY_IMAGES_REFRESH_PERIOD', 60); // seconds
define('ARTICLE_IMAGES_REFRESH_PERIOD', 60); // seconds
define('ACCOLADE_IMAGES_REFRESH_PERIOD', 60); // seconds
define('ACCOLADE_DOCUMENTS_REFRESH_PERIOD', 60); // seconds

/*******************************
 *      External Systems       *
 *******************************/
define('NO_REPLY_EMAIL', 'noreply@'.PROJECT_DOMAIN);

define('FACABOOK_APP_ID', '535408059994792');
define('FACABOOK_APP_SECRET', 'a08500b69d34bb33e306f46e15b8a9d2');
define('CREATESEND_API_HOST', 'https://api.createsend.com/api/v3.1');
define('CREATESEND_API_KEY', '7646fedb197ae83b8d4b2cbf3b9f1399f6ef818d5345e044');

///* Development / Staging
define('WEBMASTER_EMAIL', 'chuan@techplusart.com');              // email bounces default to this email
define('PAYPAL_HOST', 'https://www.sandbox.paypal.com');
define('PAYMENT_CURRENCY', 'USD');
define('CREATESEND_API_LIST_ID', 'f71b72a32a59325dda0f7ee2fb78f6e3'); // Test list
//*/

/* Production
define('WEBMASTER_EMAIL', 'chuan@techplusart.com');              // email bounces default to this email
define('PAYPAL_HOST', 'https://www.paypal.com');
define('PAYMENT_CURRENCY', 'SGD');
define('CREATESEND_API_LIST_ID', '5911fb580b50ea21bfb84857077b5d13'); // TaraBliss Web Signup 
//*/

/*******************************
 *    Accounts and Passwords   *
 *******************************/
define('CSRF_TOKEN_LENGTH', 32);
define('PERSISTENT_LOGIN_TOKEN_LENGTH', 32);
define('PERSISTENT_LOGIN_HASHES_LENGTH', 1024); // 1024 chars can contain up to 13 hashes of 77 chars each
define('PERSISTENT_LOGIN_DURATION', 5184000000); // 60 days == 5184,000,000 milliseconds
define('OTP_LENGTH', 6);
define('OTP_EXPIRY', 300000); // 5 mins == 300,000 milliseconds
define('MAX_OTP_ATTEMPTS', 5);
define('MAX_PASSWORD_ATTEMPTS', 10);

/**************************
 *    Input Validation    *
 **************************/
define('MIN_PASSWORD_LENGTH', 6);
define('MIN_NAME_LENGTH', 2);
define('MIN_EMAIL_SUBJECT_LENGTH', 3);
define('MIN_EMAIL_MESSAGE_LENGTH', 10);
define('MIN_ADDRESS_LENGTH', 8);
define('MIN_CITY_LENGTH', 2);

/*******************************
 *       Uploaded Images       *
 *******************************/
define('IMAGE_THUMBNAIL_MIN_WIDTH', 200);
define('IMAGE_THUMBNAIL_MIN_HEIGHT', 200);

/*******************************
 *     User Access Levels      *
 *******************************/ 
define('GENERAL_STAFF_ACCESS_LEVEL', 20);
define('SUPERVISOR_ACCESS_LEVEL', 51);
define('ADMIN_ACCESS_LEVEL', 90);
define('SUPER_ADMIN_ACCESS_LEVEL', 99);
