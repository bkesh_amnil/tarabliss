<?php
class EventGallery {
	private $id;					// uint (DEFAULT: 0)
	private $name;                  // string
	private $description;           // string
	private $displayPriority;       // uint - (DEFAULT: 1)
	private $createdTime;			// uint - UNIX timestamp
	private $lastUpdateTime; 		// uint - UNIX timestamp

	/********** Boilerplate functions (Framework 2.1.0) **************/
	// Default function: checks if this class has a property
	static function hasProperty($a) {
		return property_exists($this, $a);
	} //hasProperty()
	
	// Default function: make invalid method calls throw Exceptions
	function __call($name, $arg) {
		throw new Exception ('Error in '. get_class($this) . ' class - method '.$name.'() does not exist');
	} //call()
	
	// Default function: make this a printable string
	function __toString() {
		return get_class($this).' '.$this->id;
	} //toString()
	
	// Default function: converts ALL data members into array
	function toArray() {
		return get_object_vars($this);
	} //toArray()

	/**
	 * Constructor takes an array of args, and maps each arg into a data member
	 * Note: Only data members defined by this class will be processed; unrecognised args will be ignored.
	 * @param: {array} of data
	 */
	function __construct($arg) {
		$this->update($arg);
	} //construct()
	
	/**
	 * Updates with an array of new data (e.g. from $_POST)
	 * Note: only data members defined by the class will be processed; unrecognised args will be ignored.
	 *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
	 * @param: {array} of new data
	 */
	function update($arg) {
		if (!is_array($arg)) {
			throw new Exception('Error updating '. get_class($this) . ' - arg is not array');
		}
		
		// Parse each arg in the array
		foreach ($arg as $key => $value) {
			if (!property_exists($this, $key)) {
				continue;
			}

			$this->$key = $value;
		} //foreach arg
		
		if (get_parent_class($this)) {
			parent::checkData();
		}
		$this->checkData();
	} //update()

	// Checks a string field is set correctly. Throws exception if not set.
	protected function checkStrictStringField($f) {
		if (!isset($this->$f) ||  strlen($this->$f) === 0) {
			throw new Exception('Error in '. get_class($this) . ' data: llegal ' . $f . '.');
		}
	}

	// Checks an uint field is set correctly. Throws exception if not set.
	protected function checkStrictUIntField($f) {
		if (!isset($this->$f) || !isUnsignedInt($this->$f)) {
			throw new Exception('Error in '. get_class($this) . ' data: llegal ' . $f . '.');
		}
		$this->$f = (int) $this->$f;
	}

	// Checks an int field is set correctly. Throws exception if not set.
	protected function checkStrictIntField($f) {
		if (!isset($this->$f) || !isInt($this->$f)) {
			throw new Exception('Error in '. get_class($this) . ' data: llegal ' . $f . '.');
		}
		$this->$f = (int) $this->$f;
	}

	// Check a bool field.
	// - Note: only strings and ints["1", "0", 1, 0]) are considered, all other values throw exception.
	protected function checkStrictBoolField($f) {
		if (!isset($this->$f) || ($this->$f !== TRUE && $this->$f !== FALSE && $this->$f !== 1 && $this->$f !== 0 && $this->$f !== '1' && $this->$f !== '0') ) { 
			throw new Exception('Error in '. get_class($this) . ' data: llegal ' . $f . '.');
		}
		$this->$f = (bool) $this->$f;
	}

	// Check a string field and set any NULL/empty fields to a default value
	protected function checkStringField($f, $default=NULL) {
		if (!isset($this->$f) ||  strlen($this->$f) === 0) {
			$this->$f = $default;
		}
	}

	// Check an uint field and set any non-int fields to a default value
	protected function checkUIntField($f, $default=NULL) {
		if (!isset($this->$f) || !isUnsignedInt($this->$f)) {
			$this->$f = $default;
		}
		else {
			$this->$f = (int) $this->$f;
		}
	}

	// Checks an int field and set any non-int fields to a default value
	protected function checkIntField($f, $default=NULL) {
		if (!isset($this->$f) || !isInt($this->$f)) {
			$this->$f = $default;
		}
		else {
			$this->$f = (int) $this->$f;
		}
	}

	// Check a bool field and set any non-bool fields to a default value.
	// - Note: only strings and ints["1", "0", 1, 0]) are considered, all other values evaluate to default value.
	protected function checkBoolField($f, $default=NULL) {
		if (!isset($this->$f) || ($this->$f !== TRUE && $this->$f !== FALSE && $this->$f !== 1 && $this->$f !== 0 && $this->$f !== '1' && $this->$f !== '0') ) { 
			$this->$f = $default;
		}
		else {
			$this->$f = (bool) $this->$f;
		}
	}

	// For DB to set ID after creation
	function setID($input) {
		if (!isset($input) || !isUnsignedInt($input)) {
			throw new Exception ('Error in '. get_class($this).' setID: invalid id');
		}
		$this->id = (int) $input;
	} //setID()

	/********** End of Boilerplate functions (Framework 2.1.0) **************/

	/*
	 * Checks that each data member is valid:
	 * - throws Exceptions when critical data is invalid
	 * - sets other compulsory fields with invalid data to their default values
	 * - sets optional fields with invalid data to NULL
	 */
	protected function checkData() {
		$this->checkUIntField('id', 0);

		$this->checkStrictStringField('name');
//		$this->checkStrictStringField('description');
		$this->checkUIntField('displayPriority', 1);

//		$this->checkStrictUIntField('createdTime');
//		$this->checkStrictUIntField('lastUpdateTime');
	} // checkData()

	/***************
	 *   GETTERS   *
	 ***************/
	function getID() {
		return $this->id;
	}
	function getName() {
		return $this->name;
	}
	function getCreatedTime() {
		return $this->createdTime;
	}
	function getLastUpdateTime() {
		return $this->lastUpdateTime;
	}
} //class eventGallery
