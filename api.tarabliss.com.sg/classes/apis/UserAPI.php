<?php
/***
 * Backend Framework v2.1.0 (Edited)
 * ========================
 *
 * Functions called by a Public User
 ***/
class UserAPI {
	/***
	 * Gets the current session user
	 * @input: none
	 * @output: user - staff/member/etc. object
	 ***/
	function getSessionUser($response) {
		$response->addData('user', $_SESSION['user']->getSanitizedArray());
		return 200;
	} //getSessionUser()
	
	/**
	 * Appends an error to log file.
	 * @input: $_POST['error']
	 * @output: -
	 */
	function logError($response) {
		Log::error($_POST['error']);
		return 200;
	}
	
	/***
	 * Sends email to website contact person
	 * @input: $_POST['name', 'email', 'mobile', 'message']
	 * @output: message
	 ***/
	function sendContactMessage($response) {
		// Data check: name
		if (!isset($_POST['name']) || !Validator::isName($_POST['name'])) {
			$response->addData('error', 'Invalid name: '.$_POST['name']);
			return 400;
		}
		else {
			$name = trim($_POST['name']);
		}
		
		// Data check: email
		if (!isset($_POST['email'])) {
			$response->addData('error', 'Email is required.');
			return 400;
		}
		if (!Validator::isEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email: '.$_POST['email']);
			return 400;
		}
		else {
			$email = strtolower($_POST['email']);
		}
		
		// Data check: mobile
		if (!isset($_POST['mobile']) || !Validator::isMobile($_POST['mobile'])) {
			$response->addData('error', 'Illegal Mobile.');
			return 400;
		}
		else {
			$mobile = trim($_POST['mobile']);
		}
		
		// Data check: message
		if (!isset($_POST['message']) || !Validator::isContactMessage($_POST['message'])) {
			$response->addData('error', 'Message must be at least '.MIN_EMAIL_MESSAGE_LENGTH.' characters.');
			return 400;
		}
		else {
			$message = trim($_POST['message']);
		}
		
		// Create email
		$contact_email = InternalAPI::getSetting('contact-email');
		$mail = new Mail();
		$mail->setSubject('Website Enquiry');
		$mail->setTo($contact_email);
		$mail->setFrom(NO_REPLY_EMAIL);
		$mail->setReplyTo($email);
		$mail->addHTMLMail(API_ROOT.'/mails/contact-message.html');
		$mail->addTextMail(API_ROOT.'/mails/contact-message.txt');
		$mail->setVariable(array (
			'name' => $name,
			'email' => $email,
			'mobile' => $mobile,
			'message' => $message
		));
		
		// Send email
		if (!$mail->send()) {
			Log::fatal(__METHOD__.'() - unable to send out email to '.$contact_email);
			$response->addData('error', 'Server email error!');
			return 500;
		}
		
		// YAY
		return 200;
	} //sendContactMessage()

	/**
	 * Gets a list of user's application settings.
	 * - Note: keys is a JSON string representing an array of keys, e.g. ["contact-email", "paypal-email"]
	 *         or a CSV of keys, e.g. "contact-email, paypal-email"
	 * - Note: if keys are not specified, ALL settings are returned
 	 * @input: $_GET['keys', 'force']
 	 * @output: settings - Array of key-value pairs
 	 */
	function getSettings($response) {
		// Get all settings
		if (isset($_GET['forceRefresh']) && $_GET['forceRefresh']) {
			$settings = InternalAPI::readSettings(TRUE);
		}
		else {
			$settings = InternalAPI::readSettings(FALSE);
		}

		// Parse names of settings we need
		if (!isset($_GET['keys'])) {
			$keys = array();
		} // no keys specified
		else {
			$keys = json_decode($_GET['keys'], FALSE); // format to array 
			if (!is_array($keys)) {
				$keys = csv2array($_GET['keys']);
			}
		} // keys specified

		// Return
		if (count($keys) > 0) {
			$to_return = array();
			foreach ($keys as $key) {
				$key = trim($key);
				$to_return[$key] = $settings[$key];
			}

			$response->addData('settings', $to_return);
		}
		else {
			$response->addData('settings', $settings);			
		}
		return 200;
	} //getSettings()

	/************ End of Boilerplate for v2.1.0 ************/
	/*
	 * Edited:
	 * - sendContactMessage()
	 *
	 *******************************************************/

	/**
	 * Subscribes to mail list (managed in createsend)
	 * @input: $_POST['name, email, listID, any other data... ']
	 * @output: -
	 */
	function subscribeMailList($response) {
		// Data check: name
		if (!isset($_POST['name']) || strlen($_POST['name']) === 0) {
			$_POST['Name'] = 'Anonymous';
		}
		else {
			$name = $_POST['name'];
		}
		unset($_POST['name']);

		// Data check: email
		if (!isset($_POST['email'])) {
			$response->addData('error', 'Email is required.');
			return 400;
		}
		if (!Validator::isEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email: '.$_POST['email']);
			return 400;
		}
		else {
			$email = strtolower($_POST['email']);
		}
		unset($_POST['email']);

		// Data check: listID
		if (!isset($_POST['listID'])) {
			$response->addData('error', 'List ID is required.');
			return 400;
		}
		$url = CREATESEND_API_HOST.'/subscribers/'. $_POST['listID'] . '.json';
		unset($_POST['listID']);
		
		// Copy over all custom fields
		$to_post = array(
			'Name' => $name,
			'EmailAddress' => $email,
			'Resubscribe' => TRUE,
			'CustomFields' => array()
		);
		foreach ($_POST as $key => $value) {
			array_push($to_post['CustomFields'], array(
				'Key' => $key,
				'Value' => $value
			));
		}
		//Log::debug(__METHOD__.'() - Posting to: ' . $url);

		// cURL to createsend
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERPWD, CREATESEND_API_KEY.':x');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_HEADER, 0); // Don't show header in results
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($to_post));

		// Get cURL response
		$raw_data = curl_exec($ch);
		$error = curl_error($ch);
		$data = json_decode($raw_data, TRUE);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		// Handle cURL errors
		if (!$httpcode) {
			Log::error(__METHOD__.'() - cURL HTTP Response 0. Error: ' . $error);
			$response->addData('error', 'Unable to send API call');
			return $httpcode || 500;
		}
		if ($error) {
			Log::error(__METHOD__.'() - cURL error: '.$error);
			$response->addData('error', 'Error during API call');
			return $httpcode;
		}

		// Check good response
		if ($httpcode >= 400) {
			$response->addData('error', $data['Message']);
			return $httpcode;
		}
		if ($data !== $to_post['EmailAddress']) {
			Log::warning(__METHOD__.'() - Expecting "'. $to_post['EmailAddress'].'" got: "'. $data .'"');
		}

		// YAY
		//$response->addData('raw', $raw_data); // DEBUG
		return $httpcode;
	} //subscribeMailList()

	/**
	 * Unsubscribes from mail list
	 * @input: $_POST['email, listID']
	 * @output: -
	 */
	function unsubscribeMailList($response) {
		// Data check: email
		if (!isset($_POST['email'])) {
			$response->addData('error', 'Email is required.');
			return 400;
		}
		if (!Validator::isEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email: '.$_POST['email']);
			return 400;
		}
		else {
			$email = strtolower($_POST['email']);
		}

		// Data check: listID
		if (!isset($_POST['listID'])) {
			$response->addData('error', 'List ID is required.');
			return 400;
		}

		// cURL to createsend
		$ch = curl_init(CREATESEND_API_HOST.'/subscribers/'. $_POST['listID'] . '/unsubscribe.json');
		curl_setopt($ch, CURLOPT_USERPWD, CREATESEND_API_KEY.':x');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_HEADER, 0); // Don't show header in results
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
			'EmailAddress' => $email
		)));
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

		// Get cURL response
		$raw_data = curl_exec($ch);
		$error = curl_error($ch);
		$data = json_decode($raw_data, TRUE);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		// Handle cURL errors
		if (!$httpcode) {
			Log::error(__METHOD__.'() - HTTP Response 0. Error: ' . $error);
			$response->addData('error', 'Unable to send API call');
			return $httpcode || 500;
		}
		if ($error) {
			Log::error(__METHOD__.'() - '.$error);
			$response->addData('error', 'Error during API call');
			return $httpcode;
		}

		// Check good response
		if ($data !== '') {
			Log::warning(__METHOD__.'() - Expecting "" got: "'. $data .'"');
		}

		//$response->addData('raw', $raw_data); // DEBUG
		return $httpcode;
	} //unsubscribeMailList()

	/**
	 * @input: -
	 * @output: categories
	 */
	function getFeaturedCategories($response) {
		$models = Database::readObjects('Category', array(
			array(
				array('displayPriority','>',0)
			)
		));

		// Make into data array
		$data = array();
		foreach ($models as $model) {
			$data[] = $model->toArray();
		}

		// YAY
		$response->addData('categories', $data);
		return 200;
	} //getFeaturedCategories()
	/**
	 * @input: -
	 * @output: categories
	 */
	function getEventGalleries($response) {
		$models = Database::readObjects('EventGallery', array(
			array(
				array('displayPriority','>',0)
			)
		));

		// Make into data array
		$data = array();
		foreach ($models as $model) {
			$data[] = $model->toArray();
		}

		// YAY
		$response->addData('eventGalleries', $data);
		return 200;
	} //getEventGalleries()
        
	/**
	 * @input: -
	 * @output: categories
	 */
	function getEvents($response) {
		$models = Database::readObjects('Event', array(
			array(
				array('displayPriority','>',0),
				array('eventGalleryID','=',$_GET['eventGalleryID']),
			)
		));

		// Make into data array
		$data = array();
		foreach ($models as $model) {
			$data[] = $model->toArray();
		}

		// YAY
		$response->addData('events', $data);
		return 200;
	} //getEvents()

	/**
	 * @input: -
	 * @output: subCategories
	 */
	function getFeaturedSubCategories($response) {
		$models = Database::readObjects('SubCategory', array(
			array(
				array('displayPriority','>',0)
			)
		));

		// Make into data array
		$data = array();
		foreach ($models as $model) {
			$data[] = $model->toArray();
		}

		// YAY
		$response->addData('subCategories', $data);
		return 200;
	} //getFeaturedSubCategories()

	/**
	 * @input: -
	 * @output: therapies
	 */
	function getFeaturedTherapies($response) {
		$models = Database::readObjects('Therapy', array(
			array(
				array('displayPriority','>',0)
			)
		));

		// Make into data array
		$data = array();
		foreach ($models as $model) {
			$data[] = $model->toArray();
		}

		// YAY
		$response->addData('therapies', $data);
		return 200;
	} //getFeaturedTherapies()

	function getFeaturedArticles($response) {
		$models = Database::readObjects('Article', array(
			array(
				array('displayPriority','>',0)
			)
		));

		// Make into data array
		$data = array();
		foreach ($models as $model) {
			$data[] = $model->toArray();
		}

		// YAY
		$response->addData('articles', $data);
		return 200;
	} //getFeaturedArticles()

	function getFeaturedAccolades($response) {
		$models = Database::readObjects('Accolade', array(
			array(
				array('displayPriority','>',0)
			)
		));

		// Make into data array
		$data = array();
		foreach ($models as $model) {
			$data[] = $model->toArray();
		}

		// YAY
		$response->addData('accolades', $data);
		return 200;
	} // getFeaturedAccolades()

	function getFeaturedJobListings($response) {
		$models = Database::readObjects('JobListing', array(
			array(
				array('displayPriority','>',0)
			)
		));

		// Make into data array
		$data = array();
		foreach ($models as $model) {
			$data[] = $model->toArray();
		}

		// YAY
		$response->addData('job-listings', $data);
		return 200;
	} // getFeaturedJobListings()
} //class UserAPI
