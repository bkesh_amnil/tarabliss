<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Provides connectivity to an SQL DB (e.g. MySQL)
 * - Note: global constants MYSQL_HOST, MYSQL_DB_NAME, MYSQL_USER, MYSQL_PASS, MYSQL_TABLE_PREFIX have to be defined (in configuration file).
 *
 * Uses prepared statements (slightly worse performance for simple direct queries, but better for security).
 * Able to handle both objects and arrays as data
 * - Note: all data must have surrogate key "id" (int)
 * - Note: Objects must implement iModel, specifically toArray() and setID().
 *         Objects must also perform their own data checks, typcasting strings returned by this driver into the appropriate data types.
 *
 * Usage:
 * - For array-based CRUD operations:
 *     Database::create($class, $data);
 *     Database::read($class, $or_conditions, $options);
 *     Database::readOne($class, $or_conditions. $options);
 *     Database::update($class, $data);
 *     Database::delete($class, $id);
 *
 * - For object-based CRUD operations:
 *     Database::create($obj)
 *     Database::readObject($class, $or_conditions, $options);
 *     Database::readObjects($class, $or_conditions, $options);
 *     Database::update($obj)
 *     Database::delete($obj)
 *
 * - For complicated queries:
 *     Database::query('SELECT Customers.CustomerName, Orders.OrderID FROM Customers INNER JOIN Orders ON Customers.CustomerID=Orders.CustomerID ORDER BY Customers.CustomerName;');
 *
 * - For transactions:
 *     Database::beginTransaction();
 *     Database::endTransaction();
 *
 * - Locks (DON'T USE UNLESS ABSOLUTELY NECESSARY):
 *     Database::lockTables()
 *     Database::unlockTables()
 *
 ***/
class Database {
	const EXECUTE_ATTEMPTS = 3;              // how many times we should try a query if it fails (note: may deadlock)

	private static $statements;	                    // associative array of prepared statements, e.g. ['INSERT INTO transactions'] => PDOStatement,
	private static $hasActiveTransaction = FALSE;   // flag whether there is an active transaction
	private static $dbh;                            // handler for DB, i.e. PDO object
	
	private static function init() {
		self::$dbh = new PDO('mysql:host='.MYSQL_HOST.';dbname='.MYSQL_DB_NAME,
			MYSQL_USER,
			MYSQL_PASS,
			array( // set error mode, set charset
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"
			)
		);
	} //init()
	
	//Default function: make invalid method calls throw Exceptions
	static function __callStatic($name, $arguments) {
		throw new Exception ('Error in Database class: method '.$name.'() does not exist');
	} //call()
	
	/***************************
	 *  Database TRANSACTIONS  *
	 ***************************/
	/**
	 * Starts a transaction (suspends auto-commit)
	 * All CRUDs to the DB will not be committed until commit() is called.
	 */
	static function beginTransaction() {
		if (is_null(self::$dbh)) {
			self::init();
		}
		
		if (self::$hasActiveTransaction) {
			Log::error(__METHOD__.'() - already has active transaction.');
			throw new Exception('Database error - unable to begin transaction.');
		}
		
		try {
			if (self::$dbh->beginTransaction()) {
				self::$hasActiveTransaction = TRUE;
				return;
			}
			Log::error(__METHOD__.'() failed.');
			throw new Exception('Database error - unable to begin transaction.');
		} //try
		catch (PDOException $e) {
			$error_info = $e->errorInfo;
			Log::error(__METHOD__.'() -'. "\n" .
				' [0] SQLSTATE error code:' . $error_info[0] . "\n" .
				' [1] Driver error code: ' . $error_info[1] . "\n" .
				' [2] Driver error message: '.$error_info[2] . "\n" .
				 $e->getTraceAsString()
			);
			throw new Exception('Database error - unable to begin transaction.');
		} //catch
	} //beginTransaction()

	/**
	 * Commits a transaction, or rollback if errors are encountered.
	 * @return: {bool} TRUE on successful commit, FALSE if rolled back
	 */
	static function endTransaction() {
		if (is_null(self::$dbh)) {
			self::init();
		}

		if (!self::$hasActiveTransaction) {
			Log::error(__METHOD__.'() - no active transaction.');
			throw new Exception('Database error - unable to end transaction.');
		}

		try {
			if (self::$dbh->commit()) {
				self::$hasActiveTransaction = FALSE;
				return TRUE;
			}
			else {
				throw new Exception('Database error - unable to commit transaction.');
			}
		}
		catch (PDOException $e) {
			$error_info = $e->errorInfo;
			Log::error(__METHOD__.'() -'. "\n" .
				' [0] SQLSTATE error code:' . $error_info[0] . "\n" .
				' [1] Driver error code: ' . $error_info[1] . "\n" .
				' [2] Driver error message: '.$error_info[2] . "\n" .
				 $e->getTraceAsString()
			);
			if ($error_info[0] !== 40001) { // 40001: (ISO/ANSI) Serialization failure, e.g. timeout or deadlock
				throw $e;
			}
			if (self::$dbh->rollback()) {
				self::$hasActiveTransaction = FALSE;
				return FALSE;
			}
			else {
				throw new Exception('Database error - unable to rollback transaction.');
			}
		}
	} //endTransaction()
	
	/**
	 * Use only when absolutely necessary: very very very unlikely
	 * @param: {string | array} tables - CSV or array of names of tables to lock.
	 * @return: {bool} success
	 */
	static function lockTables($tables) {
		if (is_null(self::$dbh)) {
			self::init();
		}

		if (is_string($tables)) {
			$tables = csv2array($tables);
		}
		else if (!is_array($tables)) {
			throw new Exception(__METHOD__.'() - expect either string or array param');
		}
		
		// Form SQL
		$q = 'LOCK TABLES ';
		foreach ($tables as $table) {
			$q .= MYSQL_TABLE_PREFIX.$table.' WRITE,';
		}
		$q = rtrim($q, ',');
		
		// Execute
		$result = self::$dbh->query($q); // result is a PDOStatement
		if ($result === FALSE) {
			return FALSE;
		}
		else {
			return TRUE;
		}
	} //lockTable()

	/**
	 * Use only when absolutely necessary: very very very unlikely
	 * @return: {bool} success
	 */
	static function unlockTables() {
		if (is_null(self::$dbh)) {
			self::init();
		}
		
		$result = self::$dbh->query('UNLOCK TABLES'); // result is a PDOStatement
		if ($result === FALSE) {
			return FALSE;
		}
		else {
			return TRUE;
		}
	} //unlockTable()
	
	/**
	 * Direct query (SQL) to database
	 * - Note: not protected from injections!
	 * @param: {string} SQL query
	 * @return: {PDOStatement}
	 */
	static function query($q) {
		if (is_null(self::$dbh)) {
			self::init();
		}
		
		return self::$dbh->query($q);
	} //query()
		
	/**********************
	 *   CRUD FUNCTIONS   *
	 **********************/
	/**
	 * Inserts a row of data into the database
	 * Will also set the data's global ID upon success
	 * @param: {object} model to be inserted,
     *            OR
	 *         {string} name of table to insert to
     *         {array} data to be inserted
	 * @return: {object | array} 
	 */
	static function create($class, $data=NULL) {
		if (is_null(self::$dbh)) {
			self::init();
		}

		// Data checks
		if (!isset($class)) {
			Log::error(__METHOD__.'(): class/object not set');
			throw new Exception('Error inserting to Database - class name / object not set.');
		}
		if (is_object($class)) {
			$data = $class->toArray();
			$sql = 'INSERT INTO '.MYSQL_TABLE_PREFIX.get_class($class);
		}
		else if (is_string($class)) {
			$sql = 'INSERT INTO '.MYSQL_TABLE_PREFIX.$class;
			if (!is_array($data)) {
				Log::error(__METHOD__.'() data is not array ' . gettype($data));
				throw new Exception('Error inserting to Database - illegal data type.');
			}
		}
		else {
			Log::error(__METHOD__.'(): class is not object / string: ' . gettype($class));
			throw new Exception('Error inserting to Database - illegal class name / object type.');
		}
		
		// Prepare
		$sql_params = ' (';
		$sql_values = 'VALUES (';
		foreach ($data as $key => $value) {
			if ($key != 'id') {
				$sql_params .= $key.',';
				$sql_values .= ':'.$key.',';
			}
		} //foreach data member
		$sql_params = trim($sql_params, ',') . ') ';
		$sql_values = trim($sql_values, ',') . ')';
		$sql = $sql.$sql_params.$sql_values;
		if (isset(self::$statements[$sql])) {
			$q = self::$statements[$sql];
		}
		else {
			$q = self::$dbh->prepare($sql);
			self::$statements[$sql] = $q;
		}

		// Bind
		foreach ($data as $key => $value) {
			if ($key === 'id') {
				continue;
			}

			if ($value === FALSE) {
				$value = 0;	// cast FALSE to an int of value 0, else MySQL will auto-cast it into an empty string
			}
			else if (is_array($value)) {
				$value = array2csv($value);	//stringify arrays
			}
			$q->bindValue(':'.$key, $value);
		} //foreach data value

		// Execute
		self::executePreparedStatement($q);
		
		// Process result
		$id = (int) self::$dbh->lastInsertId();
		if ($id > 0) {
			if (is_object($class)) {
				$class->setID($id);
				return $class;
			}
			else {
				$data['id'] = $id;
				return $data;
			}
		}
		else {
			throw new Exception('Database error - unable to retrieve ID of last inserted data.');
		}
	} //create()
	
	/**
	 * Finds rows of data from the database based on some condition(s)
	 * @param: {string} table to read from
	 *         {array} a non-associative 3D array of OR conditions
	 *          - each 2D array is non-associative set of AND conditions.
	 *          - i.e. each 2D array of conditions are OR-ed, each condition is AND-ed.
	 *			- e.g.
	 *            array(       // the following 3 sets of conditions are bounded by OR
	 *                array(array('id', '=', 1), array('lastUpdateTime', '>', 0))      // these conditions are bounded by AND
	 *                array(array('id', '=', 5), array('lastUpdateTime', '>', 0))      // these conditions are bounded by AND
	 *                array(array('id', '=', 5), array('passwordHash', '<>', 'NULL'))  // these conditions are bounded by AND
	 *            )
	 *         {array} associative array of options
	 *          - {int} offset - which row to start the search from                DEFAULT: 0
	 *          - {int} count - how many rows to return. 0 means unlimited         DEFAULT: 0
	 *          - {string} sort - which field to sort the results by               DEFAULT: NULL
	 *          - {string} order - ASC or DESC                                     DEFAULT: 'ASC'
	 *          - {bool} update - whether this read is for update (will lock row)  DEFAULT: FALSE
	 *          - {array | csv} return - fields to return                          DEFAULT: '*'
	 *          - {bool}  typecast - whether to typecast results                   DEFAULT: TRUE
	 * @return: {array} 2D array of data - empty array if no rows found
	 */
	static function read($class, $or_conditions=array(), $options=array()) {
		if (is_null(self::$dbh)) {
			self::init();
		}

		// Data checks - class
		if (!isset($class)) {
			Log::error(__METHOD__.'(): class not set');
			throw new Exception('Error reading from Database - class name not set.');
		}
		if (!is_string($class)) {
			Log::error(__METHOD__.'() class is not string ' . gettype($class));
			throw new Exception('Error reading from Database - illegal class name.');
		}

		// Data check - OR conditions
		if (!is_array($or_conditions)) {
			throw new Exception(__METHOD__.'(): $or_conditions is not array');
		}
		foreach ($or_conditions as $or_index => $and_conditions) {
			if (!is_array($and_conditions)) {
				throw new Exception(__METHOD__.'(): $or_conditions ['. $or_index .'] is not array');
			}

			$conditions_mapped = FALSE; // if we map any condition within the AND condition array, we need to replace the entire 2D array
			foreach ($and_conditions as $and_index => $condition) {
				if (!is_array($condition)) {
					Log::error(__METHOD__.'(): $and_conditions[' . $and_index . '] is not a valid condition (array type)');
					throw new Exception('Error in read conditions [' . $or_index . ', ' . $and_index . ']');
				}
				if (count($condition) !== 3) {
					Log::error(__METHOD__.'(): $and_conditions[' . $and_index . '] is not a valid condition (array of size 3)');
					throw new Exception('Error in read conditions [' . $or_index . ', ' . $and_index . ']');
				}

				// Ensure each condition is associative
				if (!isset($condition['key']) || !isset($condition['operand']) || !isset($condition['value'])) {
					$conditions_mapped = TRUE;
					$and_conditions[$and_index] = array(
						'key' => $condition[0],
						'operand' => $condition[1],
						'value' => $condition[2]
					);
				}
			}

			// Replace any mapped AND conditions
			if ($conditions_mapped) {
				$or_conditions[$or_index] = $and_conditions;
			}
		}

		// Default Options
		if (!is_array($options)) {
			$options = array();
		}
		if (!isset($options['offset']) || !isUnsignedInt($options['offset'])) {
			$options['offset'] = 0;
		}
		if (!isset($options['count']) || !isUnsignedInt($options['count'])) {
			$options['count'] = 0;
		}
		if (!isset($options['sort']) || !is_string($options['sort'])) {
			$options['sort'] = NULL;
		}
		if (!isset($options['order']) || !is_string($options['order'])) {
			$options['order'] = 'ASC';
		}
		if (!isset($options['sort1']) || !is_string($options['sort1'])) {
			$options['sort1'] = NULL;
		}
		if (!isset($options['order1']) || !is_string($options['order1'])) {
			$options['order1'] = 'ASC';
		}
		if (!isset($options['sort2']) || !is_string($options['sort2'])) {
			$options['sort2'] = NULL;
		}
		if (!isset($options['order2']) || !is_string($options['order2'])) {
			$options['order2'] = 'ASC';
		}
		if (!isset($options['update']) || !is_bool($options['update'])) {
			$options['update'] = FALSE;
		}
		if (!isset($options['fields'])) {
			$options['fields'] = '*';
		}
		if (!isset($options['typecast']) || !is_bool($options['typecast'])) {
			$options['typecast'] = TRUE;
		}

		// Prepare query
		$sql = 'SELECT ';
		if (is_array($options['fields'])) {
			$sql .= array2csv($options['fields']) . ' FROM ' . MYSQL_TABLE_PREFIX.$class;
		}
		else if (is_string($options['fields'])) {
			$sql .= $options['fields'] . ' FROM ' . MYSQL_TABLE_PREFIX.$class;
		}
		else {
			$sql .= '* FROM ' . MYSQL_TABLE_PREFIX.$class;
		}
		foreach ($or_conditions as $or_index => $and_conditions) {
			if ($or_index == 0) {
				$sql .= ' WHERE ';
			}
			else {
				$sql .= ' OR ';
			}

			foreach ($and_conditions as $and_index => $condition) {
				$suffix = ($or_index + 1) * ($and_index + 1) - 1;
				if ($and_index > 0) {
					$sql .= ' AND ';
				}
				$sql .= $condition['key'].' '.$condition['operand'].' :'.$condition['key'].$suffix;
			}
		}
		if (isset($options['sort'])) {
			$sql .= ' ORDER BY `' . $options['sort'] . '` ' . $options['order'];
			if (isset($options['sort1'])) {
				$sql .= ', `' . $options['sort1'] . '` ' . $options['order1'];
			}
			if (isset($options['sort2'])) {
				$sql .= ', `' . $options['sort2'] . '` ' . $options['order2'];
			}
		}
		else if (isset($options['sort1'])) {
			$sql .= ' ORDER BY `' . $options['sort1'] . '` ' . $options['order1'];
			if (isset($options['sort2'])) {
				$sql .= ', `' . $options['sort2'] . '` ' . $options['order2'];
			}
		}
		else if (isset($options['sort2'])) {
			$sql .= ' ORDER BY `' . $options['sort2'] . '` ' . $options['order2'];
		}
		if ($options['count'] > 0) {
			$sql .= ' LIMIT ' . $options['count'] . ' OFFSET '. $options['offset'];
		}
		if ($options['update']) {
			$sql .= " FOR UPDATE";
		}
		else {
			$sql .= " LOCK IN SHARE MODE";
		}
		//Log::debug(__METHOD__.'() - ' . $sql);
		if (isset(self::$statements[$sql])) {
			$q = self::$statements[$sql];
		}
		else {
			$q = self::$dbh->prepare($sql);
			self::$statements[$sql] = $q;
		}

		// Bind query
		foreach ($or_conditions as $or_index => $and_conditions) {
			foreach ($and_conditions as $and_index => $condition) {
				$suffix = ($or_index + 1) * ($and_index + 1) - 1;
				$q->bindValue(':'.$condition['key'].$suffix, $condition['value']);
			}
		}

		// Execute
		self::executePreparedStatement($q);

		// Process result
		if (!$options['typecast']) {
			return $q->fetchAll(PDO::FETCH_ASSOC);
		}
		$rows = $q->fetchAll(PDO::FETCH_ASSOC);
		$row_count = count($rows);
		for ($i = 0; $columnMeta = $q->getColumnMeta($i); $i++) {
			$type = $columnMeta['native_type'];
			$name = $columnMeta['name'];
			switch($type) {
				case 'DECIMAL':
				case 'TINY':
				case 'SHORT':
				case 'LONG':
				case 'LONGLONG':
				case 'INT24':
					for ($j=0; $j<$row_count; $j++) {
						$rows[$j][$name] = (int) $rows[$j][$name];
					}
					break;
				case 'DATETIME':
				case 'DATE':
				case 'TIMESTAMP':
					for ($j=0; $j<$row_count; $j++) {
						$rows[$j][$name] = strtotime($rows[$j][$name]);
					}
					break;
				// default: keep as string
			}
		} // for each column
		return $rows;
	} // read()

	/**
	 * Finds rows of data from the database based on some condition(s)
	 * @param: {string} table to read from
	 *         {array} a non-associative 3D array of OR conditions
	 *          - each 2D array is non-associative set of AND conditions.
	 *          - i.e. each 2D array of conditions are OR-ed, each condition is AND-ed.
	 *			- e.g.
	 *            array(       // the following 3 sets of conditions are bounded by OR
	 *                array(array('id', '=', 1), array('lastUpdateTime', '>', 0))      // these conditions are bounded by AND
	 *                array(array('id', '=', 5), array('lastUpdateTime', '>', 0))      // these conditions are bounded by AND
	 *                array(array('id', '=', 5), array('passwordHash', '<>', 'NULL'))  // these conditions are bounded by AND
	 *            )
	 *         {array} associative array of options
	 *          - {int} offset - which row to start the search from
	 *          - {string} sort - which field to sort the results by
	 *          - {string} order - ASC or DESC
	 *          - {bool} update - whether this read is for update (will lock row)
	 *          - {array | csv} return - fields to return
	 * @return: {array | NULL} array of row data, NULL if not found
	 */
	static function readOne($class, $or_conditions=array(), $options=array()) {
		if (!is_array($options)) {
			$options = array();
		}
		$options['count'] = 1;

		$rows = self::read($class, $or_conditions, $options);

		// Retrieve row
		if (count($rows) !== 1) {
			return NULL;
		}
		else {
			return $rows[0];
		}
	} // readOne()
	
	/**
	 * Updates a row of data in the database
	 * @param: {object} model to be updated,
     *            OR
	 *         {string} name of table to update to
     *         {array} data to be updated
	 * @return: {bool} any changes made
	 */
	static function update($class, $data=NULL) {
		if (is_null(self::$dbh)) {
			self::init();
		}

		// Data checks
		if (!isset($class)) {
			Log::error(__METHOD__.'(): class/object not set');
			throw new Exception('Error updating Database - class name / object not set.');
		}
		if (is_object($class)) {
			$data = $class->toArray();
			$sql = 'UPDATE '.MYSQL_TABLE_PREFIX.get_class($class);
		}
		else if (is_string($class)) {
			$sql = 'UPDATE '.MYSQL_TABLE_PREFIX.$class;
			if (!is_array($data)) {
				Log::error(__METHOD__.'() data is not array ' . gettype($data));
				throw new Exception('Error updating Database - illegal data type.');
			}
		}
		else {
			Log::error(__METHOD__.'(): class is not object / string' . gettype($class));
			throw new Exception('Error updating Database - illegal class name / object type.');
		}
		
		// Prepare
		$sql .= ' SET ';
		foreach ($data as $key => $value) {
			if ($key != 'id') {
				$sql .= $key.'=:'.$key.',';
			}
		} //foreach data member
		$sql = trim($sql, ',') . ' WHERE id=:id LIMIT 1';
		if (isset(self::$statements[$sql])) {
			$q = self::$statements[$sql];
		}
		else {
			$q = self::$dbh->prepare($sql);
			self::$statements[$sql] = $q;
		}
		
		// Bind
		foreach ($data as $key => $value) {
			if ($value === FALSE) {
				$value = 0;	//cast FALSE to an int of value 0, else it will be auto-casted into an empty string
			}
			else if (is_array($value )) {
				$value = array2csv($value);	//stringify arrays
			}
			$q->bindValue(':'.$key, $value);
		} //foreach data value

		// Execute
		self::executePreparedStatement($q);

		//Process result
		$rowsAffected = $q->rowCount();
		if ($rowsAffected > 0) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	} //update()
	
	/**
	 * Deletes a row from the database
	  * @param: {object} model to be updated,
     *            OR
	 *         {string} name of table to update to
     *         {int} id of row to delete
	 * @return: {bool} success
	 */
	static function delete($class, $id=NULL) {
		if (is_null(self::$dbh)) {
			self::init();
		}

		// Data checks
		if (!isset($class)) {
			Log::error(__METHOD__.'(): class/object not set');
			throw new Exception('Error deleting from Database - class name / object not set.');
		}
		if (is_object($class)) {
			$id = $class->getID();
			$sql = 'DELETE FROM '.MYSQL_TABLE_PREFIX.get_class($class) . ' WHERE id=:id LIMIT 1';
		}
		else if (is_string($class)) {
			$sql = 'DELETE FROM '.MYSQL_TABLE_PREFIX.$class . ' WHERE id=:id LIMIT 1';
			if (!is_int($id)) {
				Log::error(__METHOD__.'() id is not int ' . gettype($id));
				throw new Exception('Error deleting from Database - illegal id type.');
			}
		}
		else {
			Log::error(__METHOD__.'(): class is not object / string' . gettype($class));
			throw new Exception('Error deleting from Database - illegal class name / object type.');
		}
		
		// Prepare query
		if (isset(self::$statements[$sql])) {
			$q = self::$statements[$sql];
		}
		else {
			$q = self::$dbh->prepare($sql);
			self::$statements[$sql] = $q;
		}

		// Bind query
		$q->bindValue(':id', $id, PDO::PARAM_INT);

		// Execute euqery
		self::executePreparedStatement($q);
		
		// Process result
		$rowsAffected = $q->rowCount();
		if ($rowsAffected > 0) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	} //delete()
	
	/**
	 * Counts the number of this model in the database
	 * @param: {string} class name
	 *         {array} non-associative 3D array of OR conditions
	 * @return: {int}
	 */
	static function count($class, $or_conditions=array()) {
		if (is_null(self::$dbh)) {
			self::init();
		}

		// Data checks - class
		if (!isset($class)) {
			Log::error(__METHOD__.'(): class not set');
			throw new Exception('Error reading from Database - class name not set.');
		}
		if (!is_string($class)) {
			Log::error(__METHOD__.'() class is not string ' . gettype($class));
			throw new Exception('Error reading from Database - illegal class name.');
		}

		// Data check - OR conditions
		if (!is_array($or_conditions)) {
			throw new Exception(__METHOD__.'(): $or_conditions is not array');
		}
		foreach ($or_conditions as $or_index => $and_conditions) {
			if (!is_array($and_conditions)) {
				throw new Exception(__METHOD__.'(): $or_conditions ['. $or_index .'] is not array');
			}

			$conditions_mapped = FALSE; // if we map any condition within the AND condition array, we need to replace the entire 2D array
			foreach ($and_conditions as $and_index => $condition) {
				if (!is_array($condition)) {
					Log::error(__METHOD__.'(): $and_conditions[' . $and_index . '] is not a valid condition (array type)');
					throw new Exception('Error in read conditions [' . $or_index . ', ' . $and_index . ']');
				}
				if (count($condition) !== 3) {
					Log::error(__METHOD__.'(): $and_conditions[' . $and_index . '] is not a valid condition (array of size 3)');
					throw new Exception('Error in read conditions [' . $or_index . ', ' . $and_index . ']');
				}

				// Ensure each condition is associative
				if (!isset($condition['key']) || !isset($condition['operand']) || !isset($condition['value'])) {
					$conditions_mapped = TRUE;
					$and_conditions[$and_index] = array(
						'key' => $condition[0],
						'operand' => $condition[1],
						'value' => $condition[2]
					);
				}
			}

			// Replace any mapped AND conditions
			if ($conditions_mapped) {
				$or_conditions[$or_index] = $and_conditions;
			}
		}
		
		// Prepare query
		$sql = 'SELECT COUNT(*) as count FROM ' . MYSQL_TABLE_PREFIX.$class;
		foreach ($or_conditions as $or_index => $and_conditions) {
			if ($or_index == 0) {
				$sql .= ' WHERE ';
			}
			else {
				$sql .= ' OR ';
			}

			foreach ($and_conditions as $and_index => $condition) {
				$suffix = ($or_index + 1) * ($and_index + 1) - 1;
				if ($and_index > 0) {
					$sql .= ' AND ';
				}
				$sql .= $condition['key'].' '.$condition['operand'].' :'.$condition['key'].$suffix;
			}
		}
		if (isset(self::$statements[$sql])) {
			$q = self::$statements[$sql];
		}
		else {
			$q = self::$dbh->prepare($sql);
			self::$statements[$sql] = $q;
		}
		
		// Bind query
		foreach ($or_conditions as $or_index => $and_conditions) {
			foreach ($and_conditions as $and_index => $condition) {
				$suffix = ($or_index + 1) * ($and_index + 1) - 1;
				$q->bindValue(':'.$condition['key'].$suffix, $condition['value']);
			}
		}

		// Execute euqery
		self::executePreparedStatement($q);

		// Process result
		$row = $q->fetch(PDO::FETCH_ASSOC);
		return (int) $row['count'];
	} //count()

	/******************************
	 *      Object Operations     *
	 ******************************/
	/**
	 * Finds an object from the database based on some AND condition(s).
	 * @param: {string} class name
	 *         {array} a non-associative 3D array of OR conditions
	 *          - each 2D array is non-associative set of AND conditions.
	 *          - i.e. each 2D array of conditions are OR-ed, each condition is AND-ed.
	 *			- e.g.
	 *            array(       // the following 3 sets of conditions are bounded by OR
	 *                array(array('id', '=', 1), array('lastUpdateTime', '>', 0))      // these conditions are bounded by AND
	 *                array(array('id', '=', 5), array('lastUpdateTime', '>', 0))      // these conditions are bounded by AND
	 *                array(array('id', '=', 5), array('passwordHash', '<>', 'NULL'))  // these conditions are bounded by AND
	 *            )
	 *         {array} associative array of options
	 *          - {int} offset - which row to start the search from
	 *          - {int} count - how many rows to return. 0 means unlimited
	 *          - {string} sort - which field to sort the results by
	 *          - {string} order - ASC or DESC
	 *          - {bool} update - whether this read is for update (will lock row)
	 *          - {array | csv} return - fields to return
	 * @return: {object | NULL} the model object if success, NULL otherwise
	 */
	static function readObject($class, $or_conditions=array(), $options=array()) {
		if (!is_array($options)) {
			$options = array();
		}
		$options['fields'] = '*'; // we need all fields to construct object
		$options['typecast'] = FALSE; // objects will perform own type checking
		$rows = self::read($class, $or_conditions, $options);

		// Transform into object
		if (count($rows) == 0) {
			return NULL;
		}
		else {
			$model = new $class($rows[0]);
			return $model;
		}
	} //readObject()
	
	/**
	 * Finds objects from the database based on some AND condition(s)
	 * @param: {string} class name
	 *         {array} a non-associative 3D array of OR conditions
	 *          - each 2D array is non-associative set of AND conditions.
	 *          - i.e. each 2D array of conditions are OR-ed, each condition is AND-ed.
	 *			- e.g.
	 *            array(       // the following 3 sets of conditions are bounded by OR
	 *                array(array('id', '=', 1), array('lastUpdateTime', '>', 0))      // these conditions are bounded by AND
	 *                array(array('id', '=', 5), array('lastUpdateTime', '>', 0))      // these conditions are bounded by AND
	 *                array(array('id', '=', 5), array('passwordHash', '<>', 'NULL'))  // these conditions are bounded by AND
	 *            )
	 *         {array} associative array of options
	 *          - {int} offset - which row to start the search from
	 *          - {int} count - how many rows to return. 0 means unlimited
	 *          - {string} sort - which field to sort the results by
	 *          - {string} order - ASC or DESC
	 *          - {bool} update - whether this read is for update (will lock row)
	 *          - {array | csv} return - fields to return
	 * @return: {array} of objects - empty array if not found
	 */
	static function readObjects($class, $or_conditions=array(), $options=array()) {
		if (!is_array($options)) {
			$options = array();
		}
		$options['fields'] = '*'; // we need all fields to construct object
		$options['typecast'] = FALSE; // objects will perform own type checking
		$rows = self::read($class, $or_conditions, $options);

		// Transform into objects
		$models = array();
		foreach ($rows as $row) {
			$models[] = new $class($row);
		}
		return $models;
	} //readObjects()

	/**
	 * Finds an object by its ID
	 * @param: {string} class name
	 *         {int} id
	 *         {array} associative array of options
	 *          - {bool} update - whether this read is for update (will lock row)
	 */
	static function readObjectByID($class, $id, $options=array()) {
		$or_conditions = array(
			array(
				array('id', '=', $id)
			)
		);

		// Execute with simple options
		if (!is_array($options)) {
			$options = array();
		}
		$options['offset'] = 0;
		$options['count'] = 1;
		$options['sort'] = NULL;
		$options['order'] = 'ASC';
		$options['fields'] = '*'; // we need all fields to construct object
		$options['typecast'] = FALSE; // objects will perform own type checking
		$rows = self::read($class, $or_conditions, $options);

		// Transform into object
		if (count($rows) == 0) {
			return NULL;
		}
		else {
			$model = new $class($rows[0]);
			return $model;
		}
	} //readObjectByID()

	/***********************
	 *  Common Procedures  *
	 ***********************/
	/**
	 * Procedure to execute a bound prepared statement
	 * @param {PDOStatement} $q
	 */
	private static function executePreparedStatement(PDOStatement $q) {
		// Execute query
		$attempts_left = self::EXECUTE_ATTEMPTS;
		$success = FALSE;
		while (!$success && $attempts_left > 0) {
			try {
				$attempts_left--;
				$success = $q->execute();
			}
			catch (PDOException $e) {
				$error_info = $e->errorInfo;
				Log::error(__METHOD__.'() -'. "\n" .
					' [0] SQLSTATE error code:' . $error_info[0] . "\n" .
					' [1] Driver error code: ' . $error_info[1] . "\n" .
					' [2] Driver error message: '.$error_info[2] . "\n" .
					'Query String: '.$q->queryString . "\n" .
					 $e->getTraceAsString()
				);
				if ($error_info[0] !== 40001) { // 40001: (ISO/ANSI) Serialization failure, e.g. timeout or deadlock
					throw $e;
				}
			}
		} //while not completed
		if (!$success) {
			$attempt_count = (self::EXECUTE_ATTEMPTS - $attempts_left);
			Log::error(__METHOD__.'(): statement execute failure after ' . $attempt_count . ' attempts.');
			throw new Exception('Error in Database - Could not execute query to database. Attempts made: ' . $attempt_count);
		}
	} //executePreparedStatement()
} //class Database
