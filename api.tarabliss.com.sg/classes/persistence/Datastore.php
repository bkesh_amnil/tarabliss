<?php
/***
 * Backend Framework v2.1.0
 * ========================
 * 
 * Using Cassandra as a simple document/columnar store with fixed schema.
  * - Note: global constants CASSANDRA_HOST, CASSANDRA_PORT, CASSANDRA_KEYSPACE, CASSANDRA_TABLE_PREFIX have to be defined (in configuration file).
 *
 * - Notes:
 *    - Auto-converts data types to/from native to Cassandra types
 *    - Because blobs are simply strings in PHP, any column storing blob must be named "blob"
 * 
 * * Usage:
 * - For simple CRUDs operations:
 *    Datastore::set($table, $model)
 *    Datastore::get($table, $conditions, $options)
 *    Datastore::getOne($table, $conditions, $options)
 *    Datastore::has($table, $conditions)
 *    Datastore::remove($table, $conditions)
 *
 * - For other queries:
 *     Datastore::query($cql) 
 */
class Datastore {
	private static $statements; 
	private static $session;

	private static function init() {
		$cluster = Cassandra::cluster()
			->withContactPoints(CASSANDRA_HOST)
			->withPort(CASSANDRA_PORT)
			->build();
		self::$session = $cluster->connect(CASSANDRA_KEYSPACE);
	} //init()

	//Default function: make invalid method calls throw Exceptions
	static function __callStatic($name, $arguments) {
		throw new Exception ('Error in Datastore class: method '.$name.'() does not exist');
	} //call()

	/**
	 * Direct query (CQL) to database
	 * - Note: not protected from injections!
	 * - Note: INSERT / UPDATE / DELETE returns empty array
	 * @param: {string} CQL query
	 * @return: {array}
	 */
	static function query($q) {
		if (is_null(self::$session)) {
			self::init();
		}
		
		$result = self::$session->execute(new Cassandra\SimpleStatement($q));

		// Format as array of primitive values
		$result_array = array();
		foreach ($result as $row) {
			// Ensure each value is primitive
			//Log::debug(__METHOD__.'() - Before: ' . array2csv($row));
			foreach ($row as $col => $value) {
				if (is_object($value)) {
					//Log::debug(__METHOD__.'() - class of '.$value.' is '.get_class($value));
					if (get_class($value) === 'Cassandra\Blob') {
						$row[$col] = $value->toBinaryString();
					}
					if (get_class($value) === 'Cassandra\Bigint') {
						$row[$col] = (int) $value->toDouble();
					}
					else if (get_class($value) === 'Cassandra\Float') {
						$row[$col] = $value->toDouble();
					}
				}
			}
			//Log::debug(__METHOD__.'() - After: ' . array2csv($row));
			$result_array[] = $row;
		} // for each row

		return $result_array;
	} //query()

	/***************
	 *    CRUDs    *
	 ***************/
	/**
	 * Note: the data model object must fit the schema of the table it is trying to insert into.
	 * @param: {string} $table - table name (no prefix)
	 *         {array | object} $model
	 */
	static function set($table, $model) {
		if (!isset($model)) {
			throw new Exception('Datastore error - model not set');
		}
		
		// Init
		if (is_null(self::$session)) {
			self::init();
		}

		// Generate query and execute
		try {
			// Ensure data is array
			if (is_array($model)) {
				$data = $model;
			}
			else {
				$data = $model->toArray();
			}

			// Generate the query to be prepared
			$cql_insert = 'INSERT INTO '.CASSANDRA_TABLE_PREFIX.$table; // Note: in CQL, INSERT and UPDATE are the same thing.
			$cql_params = ' (';
			$cql_values = 'VALUES (';
			$arguments = array();
			foreach ($data as $col => $value) {
				$cql_params .= '"'.$col.'",';
				$cql_values .= '?,';
				if ($col === 'blob') { // cannot find any other way to detect blob :(
					$value = new Cassandra\Blob($value);
				}
				else if (is_int($value) && $value > 2147483647) {
					$value = new Cassandra\BigInt($value);
				} // dealing with 64-bit integers here
				else if (is_float($value)) {
					$value = new Cassandra\Float($value);
				}
				else if (is_array($value) || is_object($value)) {
					throw new Exception('Error storing data. Unable to process col ' . $col . '(' . gettype($value). ')');
				}

				$arguments[] = $value;
			}
			$cql_params = trim($cql_params, ',') . ') ';
			$cql_values = trim($cql_values, ',') . ')';
			$cql_insert = $cql_insert.$cql_params.$cql_values;
			//Log::debug(__METHOD__.'() - '.$cql_insert);
			//Log::debug(__METHOD__.'() - '.array2csv($arguments));

			// Get prepared statement
			if (isset(self::$statements[$cql_insert])) {
				$prepared = self::$statements[$cql_insert];
			}
			else {
				$prepared = self::$session->prepare($cql_insert);
				self::$statements[$cql_insert] = $prepared;
			}
			// Execute
			$future = self::$session->executeAsync($prepared, new Cassandra\ExecutionOptions(array(
				'arguments' => $arguments
			)));
			$result = $future->get();
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - "'. $e->getMessage(). '" for CQL: '.$cql_insert);
			throw new Exception('Error storing to '.$table);
		}

		if (count($result) <= 0) {
			Log::error(__METHOD__.'() - No result for CQL: '.$cql_insert);
			throw new Exception('Error storing to '.$table);
		}
	} //set()

	/**
	 * - Refer: http://www.datastax.com/dev/blog/a-deep-look-to-the-cql-where-clause
	 * - Note: Cassandra does not support "OR syntax" in SELECT
	 * - Note: all conditions must be on partition columns, clustering columns or an INDEXED column, unless filtering is allowed.
	 *        - either all or none of the partition key columns should be restricted, unless secondary indices are used.
	 *        - all conditions on partition columns (first part of PRIMARY KEY) must be [=, IN]
	 *        - all conditions on clustering columns (subsequent parts of PRIMARY KEY) must be [=, >, >=, <= and <, CONTAINS or CONTAINS KEY]
	 *        - all conditions on secondary indices must use one of these operands: [=, >, >=, <=, <, CONTAINS, CONTAINS KEY]. IN is not supported.
	 * - Note: Empty conditions means get all rows.
	 * - Note: Empty fields means get all columns.
	 * - Note: Sort field must be a clustering column, i.e. the 2nd/3rd/4th/5th column of the PRIMARY KEY
	 * @param: {string} $table - name of table
	 *         {array} $key_conditions - 2D array of AND conditions
	 *         {array} $options - array of options
	 *          - {array / string} fields - array of strings, or a csv describing what fields to select (DEFAULT: '')
	 *          - {int} count - how many rows to return. 0 means unlimited (DEFAULT: 0)
	 *          - {string} sort - which field to sort the results by       (DEFAULT: NULL)
	 *          - {string} order - ASC or DESC                             (DEFAULT: ASC)
	 *          - {bool} filtering - whether or not to allow fetch then filter of all rows (inefficient) (DEFAULT: FALSE)
	 * @return: {array} 2D array of results - empty array for no results
	 */
	static function get($table, $key_conditions=array(), $options=array()) {
		// Data check: conditions
		if (!is_array($key_conditions)) {
			$key_conditions = array();
		}
		foreach ($key_conditions as $and_index => $condition) {
			if (!is_array($condition)) {
				Log::error(__METHOD__.'() - $key_conditions[' . $and_index . '] is not a valid condition (array type)');
				throw new Exception('Error in Datastore - invalid conditions');
			}
			if (count($condition) !== 3) {
				Log::error(__METHOD__.'() - $key_conditions[' . $and_index . '] is not a valid condition (array of size 3)');
				throw new Exception('Error in Datastore - invalid conditions');
			}

			// Ensure each condition is associative
			if (!isset($condition['key']) || !isset($condition['operand']) || !isset($condition['value'])) {
				$key_conditions[$and_index] = array(
					'key' => $condition[0],
					'operand' => $condition[1],
					'value' => $condition[2]
				);
			}
		}

		// Data check: options
		if (!is_array($options)) {
			$options = array();
		}
		if (!isset($options['count']) || !is_int($options['count'])) {
			$options['count'] = 0;
		}
		if (!isset($options['sort']) || !is_string($options['sort'])) {
			$options['sort'] = NULL;
		}
		if (!isset($options['order']) || !is_string($options['order'])) {
			$options['order'] = 'ASC';
		}
		if (!isset($options['filtering']) || !is_bool($options['filtering'])) {
			$options['filtering'] = FALSE;
		}
		if (!isset($options['fields'])) {
			$options['fields'] = array();
		}
		else if (is_string($options['fields'])) {
			$options['fields'] = csv2array($options['fields']);
		}
		else if (!is_array($options['fields'])) {
			$options['fields'] = array();
		}

		// Go through each field and ensure it's quoted with no stray spaces
		foreach ($options['fields'] as $idx => $field) {
			$field = trim($field);
			if (!startsWith($field, '"')) {
				$field = '"' . $field;
			}
			if (!endsWith($field, '"')){
				$field = $field . '"';
			}

			$options['fields'][$idx] = $field;
		}
		$fields = join(',', $options['fields']); // don't use csv2array to prevent quotes from being escaped.
		if ($fields === '') {
			$fields = '*';
		}
		//Log::debug(__METHOD__.'() fields - ' . $fields);

		// Init
		if (is_null(self::$session)) {
			self::init();
		}

		// Generate query and execute
		try {
			//Construct query
			$cql_select = 'SELECT '. $fields . ' FROM '.CASSANDRA_TABLE_PREFIX.$table;
			$arguments = array();
			foreach ($key_conditions as $and_index => $condition) {
				if ($and_index === 0) {
					$cql_select .= ' WHERE ';
				}
				else {
					$cql_select .= ' AND ';
				}
				$cql_select .= '"'.$condition['key'].'" '.$condition['operand'].' ?';
				$arguments[] = $condition['value'];
			}
			if (isset($options['sort'])) {
				$cql_select .= ' ORDER BY "' . $options['sort'] . '" ' . $options['order'];
			}
			if ($options['count'] > 0) {
				$cql_select .= ' LIMIT ' . $options['count'];
			}
			if ($options['filtering']) {
				$cql_select .= ' ALLOW FILTERING';
			}

			// Prepare statement
			//Log::debug(__METHOD__.'() - '.$cql_select);
			//Log::debug(__METHOD__.'() - '.$arguments[0]);
			if (isset(self::$statements[$cql_select])) {
				$prepared = self::$statements[$cql_select];
			}
			else {
				$prepared = self::$session->prepare($cql_select);
				self::$statements[$cql_select] = $prepared;
			}

			// Execute
			$future = self::$session->executeAsync($prepared, new Cassandra\ExecutionOptions(array(
				'arguments' => $arguments
			)));
			$result = $future->get();

			// Format as array of primitive values
			$result_array = array();
			foreach ($result as $row) {
				// Ensure each value is primitive
				//Log::debug(__METHOD__.'() - before: ' . array2csv($row));
				foreach ($row as $col => $value) {
					if (is_object($value)) {
						//Log::debug(__METHOD__.'() - class of '.$value.' is '.get_class($value));
						if (get_class($value) === 'Cassandra\Blob') {
							$row[$col] = $value->toBinaryString();
						}
						if (get_class($value) === 'Cassandra\Bigint') {
							$row[$col] = (int) $value->toDouble();
						}
						else if (get_class($value) === 'Cassandra\Float') {
							$row[$col] = $value->toDouble();
						}
					}
				}
				//Log::debug(__METHOD__.'() - after: ' . array2csv($row));
				$result_array[] = $row;
			} // for each row

			return $result_array;
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - "'. $e->getMessage(). '" for CQL: ' . $cql_select);
			throw new Exception('Error getting data from ' . $table);
		}
	} //get()

	/**
	 * @param: {string} table - name of table
	 *         {array} key_conditions - 2D array of AND conditions
	 *         {array} options - array of options
	 *          - {array / string} fields - array of strings, or a csv describing what fields to select (DEFAULT: '')
	 *          - {int} count - how many rows to return. 0 means unlimited (DEFAULT: 0)
	 *          - {string} sort - which field to sort the results by       (DEFAULT: NULL)
	 *          - {string} order - ASC or DESC                             (DEFAULT: ASC)
	 *          - {bool} filtering - whether or not to allow fetch then filter of all rows (inefficient) (DEFAULT: FALSE)
	 * @return: {array | NULL} result as an associative array, or NULL if not found
	 */
	static function getOne($table, $key_conditions=array(), $options=array()) {
		// Data check: options
		if (!is_array($options)) {
			$options = array();
		}
		$options['count'] = 1;

		$result = self::get($table, $key_conditions, $options);
		if (count($result) === 0)  {
			return NULL;
		}

		return $result[0];
	} // getOne()

	/**
	 * - Refer: http://www.datastax.com/dev/blog/a-deep-look-to-the-cql-where-clause
	 * - Note: Cassandra does not support "OR syntax" in SELECT
	 * - Note: all conditions must be on partition columns, clustering columns or an INDEXED column, unless filtering is allowed.
	 *        - either all or none of the partition key columns should be restricted, unless secondary indices are used.
	 *        - all conditions on partition columns (first part of PRIMARY KEY) must be [=, IN]
	 *        - all conditions on clustering columns (subsequent parts of PRIMARY KEY) must be [=, >, >=, <= and <, CONTAINS or CONTAINS KEY]
	 *        - all conditions on secondary indices must use one of these operands: [=, >, >=, <=, <, CONTAINS, CONTAINS KEY]. IN is not supported.
	 * - Note: Empty conditions means get all rows.
	 * - Note: Empty fields means get all columns.
	 * - Note: Sort field must be a clustering column, i.e. the 2nd/3rd/4th/5th column of the PRIMARY KEY
	 * @param: {string} table - name of table
	 *         {array} key_conditions - 2D array of AND conditions
	 *         {array} options - array of options
	 *          - {int} limit - max rows to scan. DEFAULT: 10,000
	 * @return: {int}
	 */
	static function count($table, $key_conditions=array(), $options=array()) {
		// Data check: conditions
		if (!is_array($key_conditions)) {
			$key_conditions = array();
		}
		foreach ($key_conditions as $and_index => $condition) {
			if (!is_array($condition)) {
				Log::error(__METHOD__.'(): $key_conditions[' . $and_index . '] is not a valid condition (array type)');
				throw new Exception('Error in Datastore - invalid conditions');
			}
			if (count($condition) !== 3) {
				Log::error(__METHOD__.'(): $key_conditions[' . $and_index . '] is not a valid condition (array of size 3)');
				throw new Exception('Error in Datastore - invalid conditions');
			}

			// Ensure each condition is associative
			if (!isset($condition['key']) || !isset($condition['operand']) || !isset($condition['value'])) {
				$key_conditions[$and_index] = array(
					'key' => $condition[0],
					'operand' => $condition[1],
					'value' => $condition[2]
				);
			}
		}

		// Data check: options
		if (!is_array($options)) {
			$options = array();
		}
		if (!isset($options['limit'])) {
			$options['limit'] = 10000;
		}

		// Init
		if (is_null(self::$session)) {
			self::init();
		}

		// Generate query and execute
		try {
			//Construct query
			$cql_select = 'SELECT COUNT(*) FROM '.CASSANDRA_TABLE_PREFIX.$table;
			$arguments = array();
			foreach ($key_conditions as $and_index => $condition) {
				if ($and_index === 0) {
					$cql_select .= ' WHERE ';
				}
				else {
					$cql_select .= ' AND ';
				}
				$cql_select .= '"'.$condition['key'].'" '.$condition['operand'].' ?';
				$arguments[] = $condition['value'];
			}
			if ($options['limit'] > 0) {
				$cql_select .= ' LIMIT ' . $options['limit'];
			}

			// Prepare statement
			//Log::debug(__METHOD__.'() - '.$cql_select);
			//Log::debug(__METHOD__.'() - '.$arguments[0]);
			if (isset(self::$statements[$cql_select])) {
				$prepared = self::$statements[$cql_select];
			}
			else {
				$prepared = self::$session->prepare($cql_select);
				self::$statements[$cql_select] = $prepared;
			}

			// Execute
			$future = self::$session->executeAsync($prepared, new Cassandra\ExecutionOptions(array(
				'arguments' => $arguments
			)));
			$result = $future->get();

			// Format as int
			//Log::debug(__METHOD__.'() count '.$result->count());
			if ($result->count() !== 1) {
				Log::error(__METHOD__.'() - Expecting only one row of result, got: '.$result->count());
				throw new Exception('Datastore error - invalid results size.');
			}
			//Log::debug(__METHOD__.'() result0count: ' . $result[0]['count']);

			return (int) $result[0]['count']; // Note: count is returned as a Cassandra\BigInt
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - "'. $e->getMessage(). '" for CQL: '.$cql_select);
			return 0;
		}
	} //count()

	/**
	 * @param: {string} table - name of table
	 *         {array} key_conditions - 2D array of AND conditions
	 * @return: {bool}
	 */
	static function has($table, $key_conditions=array()) {
		return self::count($table, $key_conditions) > 0;
	} //has()

	/**
	 * Note: Does not check that key no longer exists in the table. Operation stops upon issuing CQL command. Actual successful DELETE is assumed.
	 * @param: {string} table - name of table
	 *         {array} key_conditions - 2D array of AND conditions
	 */
	static function remove($table, $key_conditions=array())  {
		// Data check: conditions
		if (!is_array($key_conditions)) {
			$key_conditions = array();
		}
		foreach ($key_conditions as $and_index => $condition) {
			if (!is_array($condition)) {
				Log::error(__METHOD__.'(): $key_conditions[' . $and_index . '] is not a valid condition (array type)');
				throw new Exception('Error in Datastore - invalid conditions');
			}
			if (count($condition) !== 3) {
				Log::error(__METHOD__.'(): $key_conditions[' . $and_index . '] is not a valid condition (array of size 3)');
				throw new Exception('Error in Datastore - invalid conditions');
			}

			// Ensure each condition is associative
			if (!isset($condition['key']) || !isset($condition['operand']) || !isset($condition['value'])) {
				$key_conditions[$and_index] = array(
					'key' => $condition[0],
					'operand' => $condition[1],
					'value' => $condition[2]
				);
			}
		}

		// Init
		if (is_null(self::$session)) {
			self::init();
		}

		// Generate query and execute
		try {
			//Construct query
			$cql_delete = 'DELETE FROM '.CASSANDRA_TABLE_PREFIX.$table;
			$arguments = array();
			foreach ($key_conditions as $and_index => $condition) {
				if ($and_index === 0) {
					$cql_delete .= ' WHERE ';
				}
				else {
					$cql_delete .= ' AND ';
				}
				$cql_delete .= '"'.$condition['key'].'" '.$condition['operand'].' ?';
				$arguments[] = $condition['value'];
			}

			// Prepare statement
			//Log::debug(__METHOD__.'() - '.$cql_select);
			//Log::debug(__METHOD__.'() - '.$arguments[0]);
			if (isset(self::$statements[$cql_delete])) {
				$prepared = self::$statements[$cql_delete];
			}
			else {
				$prepared = self::$session->prepare($cql_delete);
				self::$statements[$cql_delete] = $prepared;
			}

			// Execute
			$future = self::$session->executeAsync($prepared, new Cassandra\ExecutionOptions(array(
				'arguments' => $arguments
			)));
			$result = $future->get();
		}
		catch (Exception $e) {
			Log::error(__METHOD__.'() - "'. $e->getMessage(). '" for CQL: '.$cql_delete);
			throw new Exception('Error removing from '.$table);
		}
	} //remove()
} //class Datastore
