<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Collection of functions for communications with external sites using HTTP
 * - currently only supports POST and GET
 */
class HTTP {
	const DEFAULT_TIMEOUT = 1000; // in ms

	/**
	 * @param: {string} url
	 *         {array} data - associative array of data
	 *         {array} options
	 *             - {int} timeout        (DEFAULT: 1000)
	 *             - {bool} forbidReuse   (DEFAULT: FALSE)
	 * @return: {array} indexed array of
	 *             - {int} status
	 *             - {string} return data
	 */
	static function post($url, $data=NULL, $options=NULL) {
		// Data checks
		if (!isset($url) || !is_string($url)) {
			throw new Exception('URL is not a string.');
		}
		if (!isset($data)) {
			$data = array();
		}
		if (!isset($options)) {
			$options = array();
		}
		
		$url_params = self::processURLParams($data);
		$options = self::processOptions($options);

		// Set cURL
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // return as string instead of outputting directly
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $url_params);
		if ($options['timeout']) {
			curl_setopt($ch, CURLOPT_TIMEOUT_MS, $options['timeout']);
		}
		if ($options['forbidReuse']) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close')); // Tell remote host to close connection after use
			curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);                     // connection cannot be pooled for reuse
		}

		// Post
		$result = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if ($result === FALSE) {
			$result = curl_error($ch);
		}
		return array($code, $result);
	} // post()

	/**
	 * @param: {string} url
	 *         {array} data - associative array of data
	 *         {array} options
	 *             - {int} timeout        (DEFAULT: 1000)
	 *             - {bool} forbidReuse   (DEFAULT: FALSE)
	 * @return: {array} indexed array of
	 *             - {int} status
	 *             - {string} return data
	 */
	static function get($url, $data=NULL, $options=NULL) {
		// Data checks
		if (!isset($url) || !is_string($url)) {
			throw new Exception('URL is not a string.');
		}
		if (!isset($data)) {
			$data = array();
		}
		if (!isset($options)) {
			$options = array();
		}

		$url_params = self::processURLParams($data);
		$options = self::processOptions($options);

		// Set cURL
		if ($url_params !== '') {
			$url = $url .'?'.$url_params;
		}
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // return as string instead of outputting directly
		curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
		if ($options['timeout']) {
			curl_setopt($ch, CURLOPT_TIMEOUT_MS, $options['timeout']);
		}
		if ($options['forbidReuse']) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close')); // Tell remote host to close connection after use
			curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);                     // connection cannot be pooled for reuse
		}

		// Get
		$result = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if ($result === FALSE) {
			$result = curl_error($ch);
		}
		return array($code, $result);
	} // get()

	/**
	 * @param: {array}
	 * @return: {array}
	 */
	private static function processOptions($options) {
		if (!isset($options['timeout'])) {
			$options['timeout'] = self::DEFAULT_TIMEOUT;
		}
		if (!isset($options['forbidReuse'])) {
			$options['forbidReuse'] = FALSE;
		}

		return $options;
	}

	/**
	 * @param: {array}
	 * @return: {string}
	 */
	private static function processURLParams($data) {
		$url_params = '';
		foreach ($data as $key => $value) {
			$url_params .= '&' . urlencode($key) . '=' . urlencode($value);
		}
		if ($url_params !== '') {
			$url_params = substr($url_params, 1);
		}

		return $url_params;
	}
} // class HTTP
