<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Collection of functions for validating user inputs
 * - Most functions expect string inputs
 * - Should operate at a semantic level, and not check for data types
 *
 * Semantic types and the respective config settings used:
 *   Type                Config
 *   ----                ------
 * - password            MIN_PASSWORD_LENGTH
 * - otp                 OTP_LENGTH
 * - name                MIN_NAME_LENGTH
 * - email subject       MIN_EMAIL_SUBJECT_LENGTH
 * - email message       MIN_EMAIL_MESSAGE_LENGTH
 * - address             MIN_ADDRESS_LENGTH
 * - city                MIN_CITY_LENGTH
 */
class Validator {
	// Default function: make invalid method calls throw Exceptions
	static function __callStatic($name, $arguments) {
		throw new Exception ('Error in Validator class: method '.$name.'() does not exist');
	} //call()

	/**
	 * Checks if a string is a valid key of a list of options
	 * @param: {string}
	 *         {array} 2D array of options - each option is a key-value pair (associative array)
	 * @return: {bool}
	 */
	static function isValidOptionKey($key, $options) {
		if (!is_array(($options))) {
			//Log::warning(__METHOD__.'() - checking ' . $key . ' against non-array: '. $options);
			return FALSE;
		}
		foreach ($options as $index => $option) {
			if (isset($option[$key])) {
				return TRUE;
			}
		}

		return FALSE;
	} //isValidOptionKey()

	static function isStrongPassword($password) {
		return mb_strlen($password) >= MIN_PASSWORD_LENGTH;
	} //isStrongPassword()

	static function isOTP($code) {
		return strlen($code) === OTP_LENGTH && ctype_digit($code);
	}

	static function isName($name) {
		return mb_strlen(trim($name)) >= MIN_NAME_LENGTH;
	} //isName()

	static function isContactSubject($a) {
		return mb_strlen(trim($a)) >= MIN_EMAIL_SUBJECT_LENGTH;
	}

	static function isContactMessage($a) {
		return mb_strlen(trim($a)) >= MIN_EMAIL_MESSAGE_LENGTH;
	}

	static function isDate($yyyy, $mm, $dd) {
		if (isUnsignedInt($yyyy) && isUnsignedInt($mm) && isUnsignedInt($dd)) {
			return checkdate((int) $mm, (int) $dd, (int) $yyyy);
		}
	}

	static function isAddress($x) {
		return mb_strlen(trim($x)) >= MIN_ADDRESS_LENGTH; 
	}

	static function isCity($x) {
		return mb_strlen(trim($x)) >= MIN_CITY_LENGTH; 	
	}

	/**
	 * @param: {string} phone number
	 *         {string} locale
	 * @return: {bool}
	 */
	static function isPhone($a, $locale='SG') {
		if (!is_string($a)) return FALSE;
		
		if ($locale =='SG') {
			// Process based on length of number
			if (strlen($a) == 11) {
				if (!ctype_digit($a)) return FALSE;
				
				// Ensure number starts with 1800
				return ($first4char = substr($a, 0, 4) == '1800');
			} 
			else if (strlen($a) == 8) {
				if (!ctype_digit($a)) return FALSE;
				
				// Ensure number starts with 3/6/8/9
				if ($a[0] != '3' && $a[0] != '6' && $a[0] != '8' && $a[0] != '9') return FALSE;
			
				// Ensure not 993, 995, 999
				$first3char = substr($a, 0, 3);	
				if ($first3char == '993' || $first3char == '995' || $first3char == '999') return FALSE;
				
				return TRUE;
			} // Check 3,6,8,9 numbers
			else {
				return FALSE;
			}
		} // SG numbers

		throw new Exception ('isPhone() not yet implemented for locale: '.$locale);
	} //isPhone()
	
	/**
	 * @param: {string} phone number
	 *         {string} locale
	 * @return: {bool}
	 ***/
	static function isMobile($a, $locale='SG') {
		if (!is_string($a)) return FALSE;
		
		if ($locale =='SG') {
			if (!ctype_digit($a)) return FALSE;
			
			// Ensure 8 digits long starting with 8/9
			if (strlen($a) != 8) return FALSE;
			if ($a[0] != '8' && $a[0] != '9') return FALSE;
			
			// Ensure not 9893, 995, 999
			$first3char = substr($a, 0, 3);
			if ($first3char == '993' || $first3char == '995' || $first3char == '999') return FALSE;
			
			return TRUE;
		} // SG numbers

		throw new Exception ('isMobile() not yet implemented for locale: '.$locale);
	} //isMobile()
	
	/**
	 * Checks that email is valid (using PHP validation)
	 * @param: {string}
	 * @return: {bool}
	 */
	static function isEmail($a) {
		if (!is_string($a)) {
			return FALSE;
		}
		
		return filter_var($a, FILTER_VALIDATE_EMAIL);
	} //isEmail()
	
	/***
	 * @param: {string} guid
	 * @return: {bool}
	 ***/
	static function isHexGUID($a) {
		if (!is_string($a)) {
			return FALSE;
		}
		
		$guid = strtoupper($a);
		return preg_match('/^\{?[A-F0-9]{8}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{12}\}?$/', $guid);
	} //isHexGUID()
	
	/**
	 * @param: {string} postal code
	 *         {string} locale
	 * @return: {bool}
	 */
	static function isPostalCode($a, $locale='SG') {
		if (!is_string($a)) {
			return FALSE;
		}
		
		if ($locale =='SG') {
			if (strlen($a) != 6) {
				return FALSE;
			}
			return ctype_digit($a);
		} // SG postal code

		throw new Exception ('isPostalCode() not yet implemented for locale: '.$locale);
	} //isPostalCode()
	
	/**
	 * Determines if string is a valid IP address on the Internet
	 * - private IPs return FALSE: 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16
	 * - reserved IPs return FALSE: 0.0.0.0/8, 169.254.0.0/16, 192.0.2.0/24, 224.0.0.0/4
	 * @param: {string} ip address
	 * @return: {bool}
	 */
	static function isPublicIPAddress($a) {
		if (!is_string($a)) {
			return FALSE;
		}
		
		return filter_var($a, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE);
	} //isPublicIPAddress()
	
	/**
	 * @param: {string}
	 * @return: {bool}
	 */
	static function isPrivateIPAddress($a) {
		if (!is_string($a)) {
			return FALSE;
		}
		
		// Check that it is an IP address
		if (!filter_var($a, FILTER_VALIDATE_IP)) {
			return FALSE;
		}
		
		// Should fail the IP check excluding private ranges
		if (filter_var($a, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE)) {
			return FALSE;
		}
		
		return TRUE;
	} //isPrivateIPAddress()
	
	/**
	 * @param: {string}
	 * @return: {bool}
	 */
	static function isReservedIPAddress($a) {
		if (!is_string($a)) return FALSE;
		
		// Check that it is an IP address
		if (!filter_var($a, FILTER_VALIDATE_IP)) {
			return FALSE;
		}
		
		// Should fail the IP check excluding reserved ranges
		if (filter_var($a, FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE)) {
			return FALSE;
		}
		
		return TRUE;
	} //isReservedIPAddress()
	
	/**
	 * Checks that URL is valid
	 * - Note: input must be in "escaped" form, e.g. %20 instead of spaces
	 * @param: {string}
	 * @return: {bool}
	 */
	static function isURL($a) {
		if (!is_string($a)) {
			return FALSE;
		}
		
		$url_format = '/^'.
			'(https?:\/\/)'.												// protocol (mandatory)
			'('.															// START: domain + TLD (mandatory)
				'([a-z0-9]\.|[a-z0-9][a-z0-9-]*[a-z0-9]\.)*'.				// - domain
				'[a-z][a-z0-9-]*[a-z0-9]'.									// - TLD
			')'.															// END: domain + TLD (mandatory)
			'('.															// START: path + query string (optional)
				'(\/+([\?\/a-z0-9$_\.\+!\*\'\(\),;:@&=-]|%[0-9a-f]{2})*)*'. // less rigid, but works
			')?'.															// END: path + query string (optional)
			'(#([a-z0-9$_\.\+!\*\'\(\),;:@&=-]|%[0-9a-f]{2})*)?'.			// fragment (optional)
			'$/i';															// insensitive matching
	
		return preg_match($url_format, $a);	
	} //isURL()
	
	/**
	 * Checks if an input is a valid Singapore NRIC / FIN
	 * - Note: first and last letters should be capital letters
	 * @param: {string}
	 * @return: {bool}
	 */
	static function isNRIC($a) {
		if (!is_string($a) || strlen($a) !== 9) {
			return FALSE;
		}
	
		$c = str_split($a);
		$digits = (substr($a, 1, 7));
		
		// Check that nric starts with valid letter
		if ($c[0] !== 'S' && $c[0] !== 'F' && $c[0] !== 'T' && $c[0] !== 'G') {
			return FALSE;
		}

		// Check that nric ends with valid letter
		if ($c[8] < 'A' || $c[8] > 'Z' || $c[8] === 'S' || $c[8] === 'O' || $c[8] === 'V' || $c[8] === 'Y') {
			return FALSE;
		}

		// Check that only digits in the middle
		if (!ctype_digit($digits)) {
			return FALSE;
		}
		
		/**
		 * The math goes like this:
	 	 *  1) Multiply first digit by 2, second multiply by 7, third by 6, fourth by 5, fifth by 4, sixth by 3, seventh by 2.
		 *     Sum the totals, e.g. NRIC number S1234567. Sum = 1×2 + 2×7 + 3×6 + 4×5 + 5×4 + 6×3 + 7×2 = 106.
		 *
		 *  2) If the first letter of the NRIC starts with T or G, add 4 to the total.
		 *
		 *  3) Divide the number by 11 and get the remainder. 106/11 = 9r7
		 *
		 *  4) Last letter depends on the first letter in the IC, using the code below:
		 *     If the IC starts with S or T: 0=J, 1=Z, 2=I, 3=H, 4=G, 5=F, 6=E, 7=D, 8=C, 9=B, 10=A
		 *     If the IC starts with F or G: 0=X, 1=W, 2=U, 3=T, 4=R, 5=Q, 6=P, 7=N, 8=M, 9=L, 10=K
		 */
		$sum = $c[1]*2 + $c[2]*7 + $c[3]*6 + $c[4]*5 + $c[5]*4 + $c[6]*3 + $c[7]*2;
		if ($c[0] == 'T' || $c[0] == 'G') {
			$sum +=4;
		}
		$remainder = $sum % 11;
		
		switch ($remainder) {
			case '0':
				if ($c[0] == 'S' || $c[0] == 'T') return $c[8] == 'J';
				if ($c[0] == 'F' || $c[0] == 'G') return $c[8] == 'X';
				break;
			case '1';
				if ($c[0] == 'S' || $c[0] == 'T') return $c[8] == 'Z';
				if ($c[0] == 'F' || $c[0] == 'G') return $c[8] == 'W';
				break;
			case '2':
				if ($c[0] == 'S' || $c[0] == 'T') return $c[8] == 'I';
				if ($c[0] == 'F' || $c[0] == 'G') return $c[8] == 'U';
				break;
			case '3':
				if ($c[0] == 'S' || $c[0] == 'T') return $c[8] == 'H';
				if ($c[0] == 'F' || $c[0] == 'G') return $c[8] == 'T';
				break;
			case '4':
				if ($c[0] == 'S' || $c[0] == 'T') return $c[8] == 'G';
				if ($c[0] == 'F' || $c[0] == 'G') return $c[8] == 'R';
				break;
			case '5':
				if ($c[0] == 'S' || $c[0] == 'T') return $c[8] == 'F';
				if ($c[0] == 'F' || $c[0] == 'G') return $c[8] == 'Q';
				break;
			case '6':
				if ($c[0] == 'S' || $c[0] == 'T') return $c[8] == 'E';
				if ($c[0] == 'F' || $c[0] == 'G') return $c[8] == 'P';
				break;
			case '7':
				if ($c[0] == 'S' || $c[0] == 'T') return $c[8] == 'D';
				if ($c[0] == 'F' || $c[0] == 'G') return $c[8] == 'N';
				break;
			case '8':
				if ($c[0] == 'S' || $c[0] == 'T') return $c[8] == 'C';
				if ($c[0] == 'F' || $c[0] == 'G') return $c[8] == 'M';
				break;
			case '9':
				if ($c[0] == 'S' || $c[0] == 'T') return $c[8] == 'B';
				if ($c[0] == 'F' || $c[0] == 'G') return $c[8] == 'L';
				break;
			case '10':
				if ($c[0] == 'S' || $c[0] == 'T') return $c[8] == 'A';
				if ($c[0] == 'F' || $c[0] == 'G') return $c[8] == 'K';
				break;
			default:
				return FALSE;
		}
		
		return FALSE;
	} //isNRIC()

	/**
	 * Checks if an input is a valid USA social security number
	 * @param: {string}
	 * @return: {bool}
	 */
	static function isSSN($ssn) {
		$ssn = preg_replace('~\D~', '', $ssn); // replace all non-digits
		return (preg_match('~^(?!000|666|9\d\d)\d{3}(?!00)\d{2}(?!0000)\d{4}$~', $ssn));
	} //isSSN()
} // class Validator
