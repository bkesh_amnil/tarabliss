<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Provides unit testing functionality for test suites. To be extended by test suite
 ***/
class UnitTester {
	/**
	 * Helper to check if two variables are deeply equal
	 * - Note: cannot simply use === as the result may contain objects, in which case we want to check only equality == not identity ===
	 * @param: {varies, varies}
	 * @return: {bool}
	 */
	static function deepEqual($a, $b) {
		$type_a = gettype($a);
		$type_b = gettype($b);

		// Check same type
		if ($type_a !== $type_b) {
			return FALSE;
		}

		switch ($type_a) {
			case 'array':
				// Go through array to check that all values in a are equal to b
				foreach ($a as $key => $val) {
					if (!isset($b[$key])) {
						return FALSE;
					}
					if (!self::deepEqual($val, $b[$key])) {
						return FALSE;
					}
				}

				return count($a) === count($b);
				break;
			case 'object':
				return deepEqual((array) $a, (array) $b); // objects can be casted to arrays
				break;

			default:
				return $a === $b;
				break;
		}
	} // deepEqual()

	/**
	 * Checks if an array contains all key-values of another array
	 * @param: {array} super array
	 *         {array} sub array
	 * @return: {bool}
	 */
	static function isSubset($a, $b) {
		$type_a = gettype($a);
		$type_b = gettype($b);

		if ($type_a === 'object') {
			$a = (array) $a;
			$type_a = 'array';
		}
		if ($type_b === 'object') {
			$b = (array) $b;
			$type_b = 'array';
		}

		// Check same type
		if ($type_a !== $type_b) {
			return FALSE;
		}

		// Handle non-arrays
		if ($type_a !== 'array') {
			return deepEqual($a, $b);
		}
		
		// Go through the sub-array
		foreach ($b as $key => $value) {
			if (!array_key_exists($key, $a)) {
				//Log::debug(__METHOD__.'() - '.$key. ' does not exist in a');
				return FALSE;
			}
			if ($a[$key] !== $value) {
				//Log::debug(__METHOD__.'() - '.$key. ' does not equal in a');
				return FALSE;
			}
		}

		return TRUE;
	} // iSubset()
	
	/**
	 * Checks if passing a set of params to a function produces expected results.
	 * - Note: if exceptions are expected, set "expected" => new Excception()
	 * - Note: params will be auto-wrapped in an array if it's not an array.
	 *         To pass an array as a param, set param as a 2D array. 
	 * @param: {array} subject - associative array containing
	 *                 - {string} description
	 *                 - {varies} params
     *                 - {varies} expected
     *         {string | array} callable function as a string, e.g. "str2int" or "MyClass::myFunction"
	 * @return: {bool}
	 */
	static function runTest($subject, $fn) {
		try {
			// Ensure single-param is wrapped in array
			if (gettype($subject['params']) !== 'array') {
				$subject['params'] = array($subject['params']);
			}

			$result = call_user_func_array($fn, $subject['params']);
			if (!self::deepEqual($result, $subject['expected'])) {
				$params_info = dump($subject['params']);
				$result_info = dump($result);
				$expect_info = dump($subject['expected']);

				// Get printable string denoting function name
				if (is_array($fn)) {
					list($obj, $method) = $fn;
					if (is_object($obj)) {
						$fn = get_class($obj) . '->' . $method;
					}
					else {
						$fn = $obj . '->' . $method;
					}
				}

				Log::error(__METHOD__.'() - Test "'. $fn .'() - ' . $subject['description'] . '" failed.'."\n".'Params: ' . $params_info . 'Produced: ' . $result_info . "\nExpected: " . $expect_info);
				return FALSE;
			}

			return TRUE;
		}
		catch (Exception $e) {
			return gettype($subject['expected']) === 'object' && is_a($subject['expected'], 'Exception');
		}
	} // runTest()
} // class UnitTester
