<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Collection of functions for generating, encrypting and checking passwords
 *
 ***/
class Encryptor {
	// Default function: make invalid method calls throw Exceptions
	static function __callStatic($name, $arguments) {
		throw new Exception ('Error in Encryptor class: method '.$name.'() does not exist');
	} //call()
	
	/**
	 * Create a base64-encoded SHA256 hash for a password
	 * - Note: the return salt and password components of the hash are base64-encoded.
	 * - Note: to hash a time-sensitive code, use the start time as salt.
	 * @param: {string} password (plain),
	 *          {string} salt (plain)
	 * @return: {string} hash in the format of "ALGORITHM:ITERATIONS:SALT:HASH".
	 */
	static function getHash($password, $salt=NULL) {
		// Settings
		$hash_algo = 'sha256';
		$salt_bytes = 24;
		$iterations = 1000;
		$hash_bytes = 24;
		
		// Generate Salt
		if (is_null($salt)) {
			$salt = base64_encode (mcrypt_create_iv ($salt_bytes, MCRYPT_DEV_URANDOM));
		}
		else {
			$salt = str_pad($salt, $salt_bytes, '0', STR_PAD_LEFT);
			$salt = base64_encode (substr($salt, 0, $salt_bytes));
		} // custom salt: make sure it's raw 24bytes.

		// Generate hash
		$hash = base64_encode (Encryptor::pbkdf2 (
			$hash_algo,
			$iterations,
			$salt,
			$password,
			$hash_bytes
		));

		return $hash_algo.':'.$iterations.':'.$salt.':'.$hash;
	} //getHash()
	
	/**
	 * Checks that a time-sensitive code is correct by comparing its hash with a known good hash
	 * - the UNIX timestamp of the good_hash is stored as the salt, so we can compare the time to know if this code has expired.
	 * - Compares the hashes in length-constant time.
	 * @param: {string} code, e.g. "96818637123456"
	 *         {string} hash, e.g. "sha256:1000:MRmvW6PJzU1flQwwbQTpBncwdPaL1hS2:UZOErhJE4sRYqgbTaPCEadcnJUNo2m8d"
	 *         {int} earliest_time - , e.g. 300 (for 5mins)
	 * @return: {bool} TRUE on correct, FALSE otherwise
	 */
	static function checkTimeSensitiveCode($code, $good_hash, $earliest_time) {
		// Check params for errors
		$params = explode(':', $good_hash);
		if (count($params) !== 4) {
			return FALSE;
		}
		
		$good_salt =  base64_decode($params[2]);
		$good_pbkdf2 = base64_decode($params[3]);
		
		// Check for expiry
		if ($good_salt < $earliest_time) {
			return FALSE;
		}

		// Check hash
		$unkn_pbkdf2 = Encryptor::pbkdf2 (
			$params[0],			//algo
			(int)$params[1],	//count
			$params[2],			//salt
			$code,			//password
			strlen($good_pbkdf2) //length
		);
		
		//Run time-constant equal check (slow equals)
		$diff = strlen($good_pbkdf2) ^ strlen($unkn_pbkdf2);
		for($i = 0; $i < strlen($good_pbkdf2) && $i < strlen($unkn_pbkdf2); $i++) {
			$diff |= ord($good_pbkdf2[$i]) ^ ord($unkn_pbkdf2[$i]);
		}
		return $diff === 0;
	} //checkTimeSensitiveCode()
	
	/**
	 * Checks that a password is correct by comparing its hash with a known good hash
	 * - Compares the hashes in length-constant time.
	 * @param: {string} ASCII password (plain)
	 *         {string} good hash to check against
	 * @return: {bool} TRUE on correct, FALSE otherwise
	 */
	static function checkPassword($password, $good_hash) {
		//Check params for errors
		$params = explode(':', $good_hash);
		if(count($params) !== 4) return FALSE;
			
		$good_pbkdf2 = base64_decode($params[3]);
		$unkn_pbkdf2 = Encryptor::pbkdf2 (
			$params[0],			//algo
			(int)$params[1],	//count
			$params[2],			//salt
			$password,			//password
			strlen($good_pbkdf2) //length
		);
		
		//Run time-constant equal check (slow equals)
		$diff = strlen($good_pbkdf2) ^ strlen($unkn_pbkdf2);
		for($i = 0; $i < strlen($good_pbkdf2) && $i < strlen($unkn_pbkdf2); $i++) {
			$diff |= ord($good_pbkdf2[$i]) ^ ord($unkn_pbkdf2[$i]);
		}
		return $diff === 0;
	} //checkPassword()
	
	/**
	 * PBKDF2 (Password-Based Key Derivation Function 2) function as defined by RSA's PKCS #5: https://www.ietf.org/rfc/rfc2898.txt
	 * Test vectors can be found at: https://www.ietf.org/rfc/rfc6070.txt
	 *
	 * This implementation of PBKDF2 was originally created by https://defuse.ca
	 * With improvements by http://www.variations-of-shadow.com
	 *
	 * @param: {string} - hash algorithm to use. Recommended: SHA256
	 *         {int} - iteration count. Higher is better, but slower. Recommended: At least 1000.
	 *         {string} - A base64-encoded salt
	 *         {string} - plain password.
	 *         {int} - The length of the derived key in bytes.
	 * @return: {string} hash derived from the password and salt.
	 */
	private static function pbkdf2($algorithm, $count, $salt, $password, $key_length) {
		//Check params for errors
		if(!in_array($algorithm, hash_algos(), true)) {
			throw new Exception('Error in Encryptor pbkdf2(): Invalid hash algorithm '.$algorithm);
		}
		if($count <= 0 || $key_length <= 0) {
			throw new Exception('Error in Encryptor pbkdf2(): Invalid parameters:'.$algorithm.' , '.$count.' , '.$salt.' , '.$password.', '.$key_length.' , '.$raw_output);
		}
		
		$hash_length = strlen(hash($algorithm, '', true));
		$block_count = ceil($key_length / $hash_length);
			
		$output = "";
		for($i = 1; $i <= $block_count; $i++) {
			//$i encoded as 4 bytes, big endian.
			$last = $salt . pack("N", $i);
			
			//first iteration
			$last = $xorsum = hash_hmac($algorithm, $last, $password, true);
			
			//perform the other iterations
			for ($j = 1; $j < $count; $j++) {
				$xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
			}
			$output .= $xorsum;
		}
			
		return substr($output, 0, $key_length);
	} //pbkdf2()
	
	/**
	 * Generates a random numeric code, e.g. '481067'
	 * @param: {int} length (optional). default: 6
	 * @return: {string}
	 */
	static function generateRandomNumericCode($length=6){
		$alphabet = "0123456789";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomNumericCode()
	
	/**
	 * Generates a random alphanumeric code, e.g. "Fgh4Yu9Q"
	 * @param: int} length (optional). default: 8
	 * @return: {string}
	 */
	static function generateRandomAlphanumericCode($length=8){
		$alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuwxyz0123456789";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomAlphanumericCode()
	
	/**
	 * Generates a random uppercase alphanumeric code, e.g. "FG3TY74Q"
	 * @param: {int} length (optional). default: 8
	 * @return: {string}
	 */
	static function generateRandomUpperCaseAlphanumericCode($length=8){
		$alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomUpperCaseAlphanumericCode()
	
	/**
	 * Generates a random password using non-capital alphabetical letters and numbers
	 * Doesn't use digits 0 and 1 to prevent confusion with I and O.
	 * @param: {int} length (optional). default: 8
	 * @return: {string}, e.g. 'j0de8zqr'
	 */
	static function generateRandomPassword($length=8) {
		$alphabet = "abcdefghijklmnopqrstuwxyz23456789";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		
		return implode($pass); // convert to array first
	} //generateRandomPassword()
	
	/**
	 * Generates a random UPPERCASE code, e.g. 'SRQSPGAS'
	 * @param: {int} length (optional).  default: 8
	 * @return: {string}
	 */
	static function generateRandomUpperCaseCode($length=8) {
		$alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomUpperCaseCode()
	
	/**
	 * Generates a random lowercase code, e.g. "bhwcdqas"
	 * @param: {int} length (optional). default: 8
	 * @return: {string}
	 */
	static function generateRandomLowerCaseCode($length=6) {
		$alphabet = "abcdefghijklmnopqrstuwxyz";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomLowerCaseCode()
	
	/**
	 * Generates a random mix-case (alphabetic) code, e.g. "FghTYuaQ"
	 * @param: {int} length (optional). default: 8
	 * @return: {string}
	 */
	static function generateRandomMixCaseCode($length=8) {
		$alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuwxyz";
		$code = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $length; $i++) {
		   $n = rand(0, $alphaLength);
		   $code[] = $alphabet[$n];
		}
		
		return implode($code); // convert to array first
	} //generateRandomMixCaseCode()

	/**
	 * @return: {string}
	 */
	static function generateUUID() {
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
	        // 32 bits for "time_low"
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

	        // 16 bits for "time_mid"
	        mt_rand( 0, 0xffff ),

	        // 16 bits for "time_hi_and_version",
	        // four most significant bits holds version number 4
	        mt_rand( 0, 0x0fff ) | 0x4000,

	        // 16 bits, 8 bits for "clk_seq_hi_res",
	        // 8 bits for "clk_seq_low",
	        // two most significant bits holds zero and one for variant DCE1.1
	        mt_rand( 0, 0x3fff ) | 0x8000,

	        // 48 bits for "node"
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
	    );
	} //generateUUID()

	/**
	 * Removes expired tokens from string of hashes
	 * @param:  {string} CSV of hashes
	 *          {int} earliest_time - hashes created before this time will be expired (DEFAULT: 0)
	 * @return: {string} CSV of new hashes
	 */
	static function purgeExpiredHashes($hashes_str, $earliest_time=NULL) {
		if (!isset($hashes_str) || !is_string($hashes_str)) {
			throw new Exception('Error purging expired hashes: param is not a string.');
		}
		if (strlen($hashes_str) === 0) {
			return '';
		}
		if (!is_int($earliest_time)) {
			$earliest_time = 0;
		}

		// Check each hash
		$hashes = csv2array($hashes_str);
		foreach ($hashes as $idx => $hash) {
			// Check hash consistency: remove inconsistent hashes
			$params = explode(':', $hash);
			if(count($params) !== 4) {
				unset($hashes[$idx]);
				Log::warning(__METHOD__.'() - corrupted hash found: '.$hash);
				continue;
			}
			
			// Check expiry: remove expired hashes
			$salt = base64_decode($params[2]); // salt represents start time 
			if ($salt < $earliest_time) {
				unset($hashes[$idx]);
			}
		}
		
		return array2csv($hashes);
	} //purgeExpiredHashes()

	/**
	 * Tries to find and replace a persistent login token
	 * @param: {string} CSV of existing hashes
	 *         {string} old token - whose hash needs to be replaced
	 *         {string} new token - whose hash will be included in the resultant CSV of hashes
	 *         {int} earliest_time - hashes created before this time will be to have expired.  (DEFAULT: 0)
	 *         {int} new_hash_time - time to generate new hash time on.                        (DEFAULT: now)
	 * @return: {string | NULL} CSV of new hashes; NULL on failure
	 */
	static function renewTimeSensitiveHash($hashes_str, $old_token, $new_token, $earliest_time=NULL, $new_hash_time=NULL) {
		if (!isset($hashes_str) || !is_string($hashes_str)) {
			throw new Exception('Error renewing token: hashes_str is not a string.');
		}
		if (strlen($hashes_str) === 0) {
			return NULL;
		}
		if (!isset($old_token) || !is_string($old_token)) {
			throw new Exception('Error renewing token: old_token is not a string.');
		}
		if (!isset($new_token) || !is_string($new_token)) {
			throw new Exception('Error renewing token: new_token is not a string.');
		}
		if (!is_int($earliest_time)) {
			$earliest_time = 0;
		}
		if (!is_int($new_hash_time)) {
			$new_hash_time = getTimeInMs();
		}
		
		// Check each hash
		$hashes = csv2array($hashes_str); // TOOD: can we use csv2array() instead?
		foreach ($hashes as $idx => $hash) {
			// Check hash consistency: remove inconsistent hashes
			$params = explode(':', $hash);
			if(count($params) !== 4) {
				unset($hashes[$idx]);
				Log::warning(__METHOD__.'() - corrupted hash found: '.$hash);
				continue;
			}
			
			// Check correctness
			if (!self::checkTimeSensitiveCode($old_token, $hash, $earliest_time)) {
				continue;
			}

			$hashes[$idx] = Encryptor::getHash($new_token, $new_hash_time);
			return array2csv($hashes);
		} //for each hash
		
		return NULL;
	} //renewTimeSensitiveHash()
} // class Encryptor
