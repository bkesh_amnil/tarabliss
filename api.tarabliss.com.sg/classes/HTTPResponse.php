<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Object that outputs HTTP header/body. Passed to API functions to populate data to be returned to frontend.
 */
class HTTPResponse {
	protected $response_code = 200;
	protected $data;		//array of data in this response
	protected static $http_response = array(
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		303 => 'See Other',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		409 => 'Conflict',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		503 => 'Service Unavailable',
		505 => 'HTTP Version Not Supported'
	);
	
	protected static $cors_allowed_origins = array();
	
	/***
	 * Adds a domain to CORS allowed origins
	 ***/
	static function addCORSAllowedOrigin($origin) {
		self::$cors_allowed_origins[] = $origin;
	}

	// Default function: make invalid method calls throw Exceptions
	function __call($name, $arguments) {
		throw new Exception ('Error in HTTPResponse class: method '.$name.'() does not exist');
	} //call()
	
	/**
	 * Construct with return code
	 * @param: {int}
	 */
	function __construct($a=200) {
		if (isUnsignedInt($a)) {
			$this->response_code = $a;
		}
		$this->data = array();
	} //construct()
	
	/**
	 * Deliver the HTTP Response
	 * Will return format according to client "HTTP Accept" header.
	 * Priority:
	 * - JSON, Javascript, XML, HTML
	 */
	function deliver() {
		header('HTTP/1.1 '.$this->response_code.' '.self::$http_response [$this->response_code]);
		
		// Check for CORS
		if (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN'] !== PROTOCOL.$_SERVER['HTTP_HOST']) {
			if (in_array($_SERVER['HTTP_ORIGIN'], self::$cors_allowed_origins)) {	
				header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
				header('Access-Control-Allow-Methods: OPTIONS, GET, POST');
				header('Access-Control-Allow-Headers: CSRF-TOKEN');
				header('Access-Control-Allow-Credentials: true');
				header('Access-Control-Max-Age: 1728000');
			}
			else if ($_SERVER['HTTP_ORIGIN'] != API_URL) {
				Log::warning(__METHOD__.'() - CORS denied from '.$_SERVER['HTTP_ORIGIN'].'['.$_SERVER['REQUEST_METHOD'].']');
				exit(1);
			}
			if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
				exit(0);
			}
		} // Non-matchin Origin and Host headers suggests CORS
		
		// Process different return formats (MIME content-types)
		$format = 'text/html';
		if (isset($_SERVER['HTTP_ACCEPT'])) {
			if (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false) $format = 'application/json';
			else if (strpos($_SERVER['HTTP_ACCEPT'], 'application/javacsript') !== false) $format = 'application/javascript';
			else if (strpos($_SERVER['HTTP_ACCEPT'], 'text/xml') !== false) $format = 'text/xml';
		}
		
		if($format == 'application/json'){
			header('Content-Type: application/json; charset=utf-8');
 			echo json_encode($this->data);
	    } //JSON
		elseif ($format == 'application/javascript'){
			header('Content-Type: application/json; charset=utf-8');
			
	        echo $_GET['callback'].'('. json_encode($this->data) .');';
    	} //JSONP
		elseif($format == 'text/xml'){
			header('Content-Type: application/xml; charset=utf-8');
			$xml_response = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
			$xml_response .= '<response>';
			$xml_response .= array2xml($this->data);
			$xml_response .= '</response>';
		
			echo $xml_response;
		} //XML
		else{
			header('Content-Type: text/html; charset=utf-8');
			echo '<!DOCTYPE html>';
			echo '<html><head>';
			echo '<title>'.PROJECT_NAME.' - '. self::$http_response [$this->response_code].'</title></head><body>';
			echo '<h1>'.$this->response_code.' '.self::$http_response [$this->response_code].'</h1>';
			echo '<table border="1" bgcolor="#DDDDDD" cellpadding="5" cellspacing="0" >';
			foreach ($this->data as $k => $v) {
				echo '<tr>';
				echo '<td align="right" valign="top"><strong><em>'.$k.':</em></strong></td>';
				echo '<td width="300">'.print_r($v, TRUE).'</td>';
				echo '</tr>';
			}
			echo '</table>';
			echo '</body></html>';		
		} //HTML
		exit(0);
	} //deliver()
	
	/**
	 * Serves a file as download
	 * @param: {string} full file path
	 */
	function serveDownloadF($file_path) {
		if (!file_exists($file_path)) {
			$this->response_code = 404;
			$this->deliver();
		}
		
		//Serve the file
		$file_name = basename($file_path);
		header('HTTP/1.1 200 OK');
		header("Content-disposition: attachment; filename=$file_name");
		header("Content-type: application/php");
		readfile($file_path);
		exit(0);
	} //serveDownload()
	
	/**
	 * Serves a file as image
	 * @param: {string} full file path
	 */
	function serveImageFile($file_path) {
		if (!file_exists($file_path)) {
			$this->response_code = 404;
			$this->deliver();
		}

		$blob = file_get_contents($file_path);

		return $this->serveImage($blob);
	} //serveImageFile()

	/**
	 * Serves a blob image
	 * @param: {string} blob
	 */
	function serveImage($blob) {
		list($orig_width, $orig_height, $detected_type) = getimagesizefromstring($blob);

		if ($detected_type === IMAGETYPE_JPEG) {
			header('HTTP/1.1 200 OK');
			header("Content-type: image/jpeg");
		}
		else if ($detected_type === IMAGETYPE_PNG) {
			header('HTTP/1.1 200 OK');
			header("Content-type: image/png");
		}
		else if ($detected_type === IMAGETYPE_GIF) {
			header('HTTP/1.1 200 OK');
			header("Content-type: image/gif");
		}
		echo $blob;
		exit(0);
	} //serveImage()
	
	/**
	 * Sets response code
	 * @param: {int} of HTTP response
	 */
	function setResponseCode($c) {
		if (!isUnsignedInt($c)) {
			throw new Exception ('Error in HTTPResponse setResponseCode: expecting int, got '. dump($c));
		}
		$this->response_code = $c;
	} //setResponseCode()
	
	/**
	 * Note: sets the ENTIRE data
	 * @param: {array}
	 */
	function setData($d){
		if (!is_array($d)) {
			throw new Exception ('Error in HTTPResponse setData: expecting array, got ' . dump($d));
		}
		$this->data = $d;
	} //setData()
	
	/**
	 * Adds to response data (dynamic)
	 * - Note: will overwrite existing values!
	 * @param: {string} key
	 *         {varies} value
	 */
	function addData($key, $value) {
		$this->data[$key] = $value;
	} //addData()
	
	/**
	 * Note: returns all data if key not specified
	 * @param: {string} key (optional)
	 * @return: {varies | array}
	 */
	function getData($k=NULL) {
		if (isset($k)) {
			return $this->data[$k];
		}
		else {
			return $this->data;
		}
	} //getData()
} //class HTTPResponse
