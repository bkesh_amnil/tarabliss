<?php
/***
 * Framework v1.1.0
 * =================
 *
 * Helper functions that "shims" old PHP.
 */

/*
 * IMPORTANT for older PHP versions (<5.4.0)
 * Get all HTTP request headers
 * Note: only works with Apache
 ***/
if (!function_exists('apache_request_headers')) {
    function apache_request_headers() {
        $headers = array();
        $regex_http = '/\AHTTP_/';
        foreach($_SERVER as $key => $val) {
            // ignore keys not starting with HTTP
            if(!preg_match($regex_http, $key) ) {
                continue;
            }
            
            // restore the original letter case (should work in most cases)
            $header_key = preg_replace($regex_http, '', $key); // remove starting HTTP_
            $regex_matches = explode('_', $header_key);
            if (count($regex_matches) > 0 && strlen($header_key) > 2) {
                foreach($regex_matches as $ak_key => $ak_val) {
                    $regex_matches[$ak_key] = ucfirst($ak_val);
                }
                $header_key = implode('-', $regex_matches);
            }
            $headers[$header_key] = $val;
        }
        
        return $headers;
    }
} //apache_request_headers()

/*
 * IMPORTANT for older PHP versions (<5.3.0)
 * Make a string's first character lowercase if that character is alphabetic.
 * - Note that 'alphabetic' is determined by the current locale. e.g., in the default "C" locale characters such as umlaut-a (ä) will not be converted.
 * @param: {string}
 * @return: {string}
 */
if (!function_exists('lcfirst')) {
    function lcfirst($string) {
        return substr_replace($string, mb_strtolower(mb_substr($string, 0, 1)), 0, 1);
    }
} //lcfirst()

/**
 * IMPORTANT for older PHP versions (<5.3.0)
 * Turns a CSV string into an array
 * @param: {string} CSV,
 *         {string} separator (optional. DEFAULT: ,)
 *	       {string} enclousure (optional. DEFAULT: ")
 *         {string} escape (optional. DEFAULT: \\)
 * @return: {array}
 */
if(!function_exists('str_getcsv')) {
    function str_getcsv($input, $delimiter = ",", $enclosure = '"', $escape = "\\") {
        $fp = fopen("php://memory", 'r+');
        fputs($fp, $input);
        rewind($fp);
        $data = fgetcsv($fp, null, $delimiter, $enclosure); // $escape only got added in 5.3.0
        fclose($fp);
        return $data;
    }
} //str_getcsv()