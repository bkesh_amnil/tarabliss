<?php
/***
 * Backend Framework v2.1.0
 * ========================
 *
 * Helper functions to enhance core PHP
 */

/***
 * Gets current system time accurate to milliseconds
 * @return: {int}
 */
//function getTimeInMs() {
//	list($ms, $s) = explode(' ', microtime());
//
//	if (PHP_INT_SIZE !== 8) {
//		throw new Exception('getTimeInMs() - unable to return integer larger than '.PHP_INT_MAX);
//	} // 32-bit machines
//	else {
//		$ms = round($ms * 1000);
//		return (int) ($s*1000 + $ms);
//	} // 64-bit machines
//} //getTimeInMs()

//#added/commented by bkesh
function getTimeInMs() {
	list($ms, $s) = explode(' ',microtime());
	$ms = round($ms * 1000);
	return $s*1000 + $ms;
} //getTimeInMs()