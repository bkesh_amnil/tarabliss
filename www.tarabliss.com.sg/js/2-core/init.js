/***
 * Frontend Web App Framework v1.5.0 - Init Script
 * =================================
 *
 * Execution:
 * - Feature Detect
 * - External plugins (Facebook, Google)
 *
 * Before use:
 * - settings
 *   - paths
 *   - default page
 *   - googla analytics ID
 *   - facebook app ID / secret
 *
 * - compatibility checks 
 */
(function(app, Modernizr, dialog){
	'use strict';
	
	/*******************
	 * Config settings *
	 *******************/
	app.settings = {
		// Validation
		CODE_LENGTH: 6,
		MIN_PASSWORD_LENGTH: 8,
		MIN_NAME_LENGTH: 2,
		PHONE_LENGTH: 8,

		// Pages
		DEFAULT_PAGE: 'home'
	};

	///* Development
	app.settings.DOMAIN_URL = 'http://tara.front.dev';
	app.settings.SERVER_URL = 'http://tara.api.dev/api';
	app.settings.GA_TRACKING_ID = 'UA-XXXXXXXX-Y';
	//*/
	
	/* Staging
	app.settings.DOMAIN_URL = 'http://staging.www.tarabliss.com.sg';
	app.settings.SERVER_URL = 'http://staging.www.tarabliss.com.sg/api';
	app.settings.GA_TRACKING_ID = 'UA-XXXXXXXX-Y';
	//*/
	
	/* Production
	app.settings.DOMAIN_URL = 'https://www.tarabliss.com.sg';
	app.settings.SERVER_URL = 'https://www.tarabliss.com.sg/api';
	app.settings.GA_TRACKING_ID = 'UA-74758007-1';
	//*/
	
	
	// Google Analytics

	/*****************
	 * Compatibility *
	 *****************/
	// Check critical features: halt on failure
	if (!Modernizr.cookies) {
		haltWithError('Your browser does not support cookies and will not work properly with our website. Please check your browser settings.');
		return;
	}
	if (!Modernizr.indexeddb && !Modernizr.localstorage && !Modernizr.websqldatabase) {
		haltWithError('Your browser does not support any local storage engines and will not work properly with our website. Please check your browser settings.');
		return;
	}
	
	// Check features: alert user of potential problems
	if (!Modernizr.history) {
		alertWithWarning('Your browser does not support browser history and may not work fully with our website.');
	}
	
	/**
	 * @private
	 */
	function alertWithWarning(msg) {
		var warning_html = ''+
			'<h2><Warning</h2>'+
			'<p>' + msg + '</p>';
		dialog.alert(warning_html);
	}
	function haltWithError(msg) {
		var error_html = ''+
			'<h2 class="error">Incompatible Browser</h2>' +
			'<p>' + msg + '</p>' +
			'<p>If you are using an <strong>outdated</strong> browser, please consider <a href="http://browsehappy.com/">upgrading your browser</a> to improve your experience.</p>' +
			'<p>If you are using a computer with restricted access to application updates, you may wish to explore using <a href="http://portableapps.com/apps/internet">portable browsers</a>.</p>';
		dialog.halt(error_html);
	}
	
	/*****************
	 * External APIs *
	 *****************/
	// Google Analytics
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create',app.settings.GA_TRACKING_ID,'auto');
	ga('send', 'pageview');
})(this.app = this.app || {}, Modernizr, dialog);
