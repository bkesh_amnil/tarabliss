/**
 * Frontend Web App Framework v1.5.0 - App Extension: SessionDB
 * =================================
 *
 * Asynchronous API for client-side storage
 *   - getItem(key)
 *   - setItem(key, value)
 *   - removeItem(key)
 *   - getType()
 *
 * Features:
 *   - Will select the best underlying driver available [IndexedDB, WebSQL, LocalStorage].
 *   - Can store any object type (not just strings).
 *   - Can use EITHER callbacks OR ES6-compliant promises (preferred), depending on whether callback functions are passed in as parameters
 *     Note: if using callbacks, all callbacks are Node-style (error argument first).
 *     Note: if using promises, each function returns an ES6-compliant Promise that resolves with appropriate values.
 *
 * Dependencies (libraries):
 *   - sessionforage 1.0.0
 */
(function(extensions, sessionforage) {
	'use strict';
	
	/**
	 * Returns the underlying driver for storage, one of: [sessionStorage, windowName]
	 * @return: {string}
	 */
	function getType() {
		return sessionforage.driver();
	}
	
	/**
	 * @return: {Promise} resolves with value
	 */
	function getItem(key) {
		return sessionforage.getItem(key); // use Promise interface :)
	}
	
	/**
	 * @return: {Promise} resolves with value
	 */
	function setItem(key, value) {
		return sessionforage.setItem(key, value); 
	}
	
	/**
	 * @return: {Promise} resolves with null
	 */
	function removeItem(key) {
		return sessionforage.removeItem(key);
	}
	
	/* we really dont need to expose these to components
	function clear(callback) {
		return sessionforage.clear(callback);
	}
	
	function length(callback) {
		return sessionforage.length(callback);
	}
	
	function keys(callback) {
		return sessionforage.keys(callback);
	}
	
	function iterate(iterator, callback) {
		return sessionforage.iterate(iterator, callback);
	}
	*/
	
	extensions.sessionDB = {
		getType: getType,
		getItem: getItem, get: getItem,
		setItem: setItem, set: setItem,
		removeItem: removeItem, remove: removeItem/*,
		clear: clear,
		length: length,
		key: key,
		keys: keys,
		iterate: iterate
		*/
	};
})(app.extensions = app.extensions || {}, sessionforage);
