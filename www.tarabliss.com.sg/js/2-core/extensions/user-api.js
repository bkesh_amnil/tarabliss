/**
 * Frontend Web App Framework v1.5.0 - App Extension: Public User API functions
 * =================================
 *
 * Dependencies (libraries):
 *   - Ajax 1.1.1
 *   - Sessionforage 1.0.0

 * Returns an ES-6 Promise that resolves/rejects with an object {status, data}.
 * - Our server returns JSON, so all server responses will be converted to JSONs.
 */
(function(extensions, ajax, sessionforage, SERVER_URL) {
	'use strict';
	
	// Settings
	var API_URL_PREFIX = SERVER_URL + '/user';
	
	/**
	 * Sends contact message to website administrator
	 * @param: {FormData | object literal} e.g.
	 *             {name: 'John Doe', organisation: 'ABC Pte Ltd', email: 'test@exmample.com', message: 'Hello World!'}
	 */
	function sendContactMessage(data) {
		return ajaxRequest('POST', '/sendContactMessage', data);
	} //sendContactMessage()

	/**
	 * Gets the current session user
	 */
	function getSessionUser() {
		return ajaxRequest('GET', '/getSessionUser').then(function(response_data) {
			return storeSessionUser(response_data['user']).then(function() {
				return response_data;
			});
		}, function(response_error) {
			return clearSessionUser().then(function() {
				throw response_error;
			});
		});
	}

	/************
	 * Settings *
	 ************/
	/**
	 * Gets a list of user's application settings. Note: keys is either a CSV of keys, or a JSON string representing an array of keys. If keys are not specified, ALL settings are returned
	 * @param: {FormData | object literal} e.g.
	 *             {keys: '["contact-email", "happindess-index"]'}
	 */
	function getSettings(data) {
		return ajaxRequest('GET', '/getSettings', data);
	}

	/****************************************
	 * @private - local and session storage *
	 ****************************************/
	function storeSessionUser(user) {
		return sessionforage.setItem('session-user', user);
	}
	function clearSessionUser() {
		return sessionforage.removeItem('session-user');
	}
	
	/**
	 * @private
	 * Makes an AJAX request to specified end-point
	 * @param: method (string) - POST / GET,
	 *         endpoint (string) - e.g. '/login',
	 *         data (Object) - typically serialized from a form
	 * @return: (Promise) - resolves/rejects with {status, data}
	 */
	function ajaxRequest(method, endpoint, data) {
		if (!endpoint.startsWith('/')) {
			endpoint = '/' + endpoint;
		}
		
		// Prepare request settings
		var settings = {
			method: method,
			url: API_URL_PREFIX + endpoint,
			data: data,
			returnType: 'json'
		};
			
		// Make request and parse result into JSON
		return ajax.request(settings).then(function(result){
			try {
				/* Refactored to keep things simple. App does not need to know satus. Simply returning data will do.
				return {
					status: result.status,
					data: JSON.parse(result.responseText)
				};
				*/
				return JSON.parse(result.responseText);
			}
			catch (ex) {
				//throw {status: 500, data: {error: 'Unable to parse server response.'}};
				throw new Error('Unable to parse server response.');
			}
		}, function(result) {
			var result_json;
			try {
				result_json = JSON.parse(result.responseText);
			}
			catch (ex) {
				//throw {status: 500, data: {error: 'Unable to parse server response.'}};
				throw new Error('Unable to parse server response.');
			}
			//throw {status: result.status, data: result_json};
			throw new Error(result_json['error']);
		});
	} //ajaxRequest()
	/*********************************
	 * End of boilerplate for v1.5.0 *
	 *********************************/


	/*************
	 * Mail List *
	 *************/
	/**
	 * @param {FormData | object literal} e.g.
	 *             {email: "test@example.com", name: "John Doe"}
	 */
	function subscribeMailList(data) {
		return ajaxRequest('POST', '/subscribeMailList', data);
	}

	/*************
	 * Therapies *
	 *************/
	function getFeaturedCategories() {
		return ajaxRequest('GET', '/getFeaturedCategories').then(function(response_data) {
			//return sessionforage.setItem('categories', response_data['categories']);
			return response_data['categories'];
		});
	}
	function getFeaturedSubCategories() {
		return ajaxRequest('GET', '/getFeaturedSubCategories').then(function(response_data) {
			//return sessionforage.setItem('sub-categories', response_data['subCategories']);
			return response_data['subCategories'];
		});
	}
	function getEventGalleries() {
		return ajaxRequest('GET', '/getEventGalleries').then(function(response_data) {
			//return sessionforage.setItem('sub-categories', response_data['subCategories']);
			return response_data['eventGalleries'];
		});
	}
	function getEvents(data) {
		return ajaxRequest('GET', '/getEvents','eventGalleryID='+data).then(function(response_data) {
			//return sessionforage.setItem('sub-categories', response_data['subCategories']);
			return response_data['events'];
		});
	}
	function getFeaturedTherapies() {
		return ajaxRequest('GET', '/getFeaturedTherapies').then(function(response_data) {
			//return sessionforage.setItem('therapies', response_data['therapies']);
			return response_data['therapies'];
		});
	}

	/************
	 * Articles *
	 ************/
	function getFeaturedArticles() {
		return ajaxRequest('GET', '/getFeaturedArticles').then(function(response_data) {
			return response_data['articles'];
		});
	}

	/************
	 * Accolades *
	 ************/
	function getFeaturedAccolades() {
		return ajaxRequest('GET', '/getFeaturedAccolades').then(function(response_data) {
			return response_data['accolades'];
		});
	}

	/************
	 * Careers *
	 ************/
	function getFeaturedJobListings() {
		return ajaxRequest('GET', '/getFeaturedJobListings').then(function(response_data) {
			return response_data['job-listings'];
		});
	}

	extensions.userAPI = {
		sendContactMessage: sendContactMessage,
		getSessionUser: getSessionUser,
		getSettings: getSettings,
		subscribeMailList: subscribeMailList,
		getFeaturedCategories: getFeaturedCategories,
		getFeaturedSubCategories: getFeaturedSubCategories,
		getEventGalleries: getEventGalleries,
		getEvents: getEvents,
		getFeaturedTherapies: getFeaturedTherapies,
		getFeaturedArticles: getFeaturedArticles,
		getFeaturedAccolades: getFeaturedAccolades,
		getFeaturedJobListings: getFeaturedJobListings
	};
})(app.extensions = app.extensions || {}, ajax, sessionforage, app.settings.SERVER_URL);
