/**
 * Frontend Web App Framework v1.5.0 - App Extension: Dialog
 * =================================
 *
 * Asynchronous API for dialog functions
 *   - halt
 *   - alert
 *   - prompt
 *   - confirm
 *   - select
 *   - selectImage
 *
 * Base Libraries:
 *   - ajax 1.1.1
 *   - dialog v2.1.1 
 *
 * Returns an ES-6 Promise tresolves with appropriate values (mostly undefined, except prompt: resolves with input string, rejects with null).
 *   - Our backend returns JSON, so all server responses will be converted to JSONs.
 *
 * Before use:
 *   - create/update [html]/[templates]/error-internet.html
 *   - ensure other HTML templates exist and contain correct text
 */
(function(extensions, dialog, ajax) {
	'use strict';
	
	// Template messages
	var INTERNET_ERROR_MESSAGE;   // to be loaded from template
	
	/***
	 * Shows a message with no possibility to stop
	 * @param: {string} msg
	 */
	function halt(msg) {
		return dialog.halt(msg);
	}
	
	/***
	 * Shows an alert dialog with one button
	 * @param: {string} msg
	 *         {string} okButtonText (optional)
	 * @return: {Promise} - resolves without data
	 ***/
	function alert(msg, okButtonText) {
		return dialog.alert(msg, {okButtonText: okButtonText});
	}
	
	/***
	 * Shows an info dialog with one button
	 * @param: {string} msg
	 *         {string} okButtonText (optional)
	 * @return: {Promise} - resolves without data
	 ***/
	function info(msg, okButtonText) {
		return dialog.info(msg, {okButtonText: okButtonText});
	}
	
	/***
	 * Shows a confirm dialog with two buttons
	 * @param: {string} msg
	 *         {string} yesButtonText (optional)
	 *         {string} noButtonText (optional)
	 * @return: {Promise} - resolves/rejects without data
	 *
	 ***/
	function confirm(msg, yesButtonText, noButtonText) {
		return dialog.confirm(msg, {yesButtonText:yesButtonText, noButtonText:noButtonText});
	}
	
	/***
	 * Shows a prompt dialog with an input box and two buttons
	 * @param: {string}
	 *         {string} placeholder (optional)
	 *         {string} yesButtonText (optional)
	 *         {string} noButtonText (optional)
	 * @return: {Promise} - resolves with input value {string}, rejects with null
	 *
	 ***/
	function prompt(msg, placeholderText, yesButtonText, noButtonText) {
		return dialog.prompt(msg, {placeholderText:placeholderText, yesButtonText:yesButtonText, noButtonText:noButtonText});
	}

	/**
	 * @param: {string}
	 *         {array} of strings
	 *         {string} yesButtonText (optional)
	 *         {string} noButtonText (optional)
	 * @return: {Promise} - resolves with select value {string}, rejects with null
	 */
	function select(msg, selectOptions, yesButtonText, noButtonText) {
		return dialog.select(msg, selectOptions, {yesButtonText:yesButtonText, noButtonText:noButtonText});
	}

	/**
	 * @param: {array} of image objects
	 *         {string}
	 *         {number} selectedIndex (optional)
	 * @return: {Promise} - resolves with select value {string}, rejects with null
	 */
	function selectImage(images, basePath, selectedIndex) {

		// Generate "src" from basePath
		for (var i=0,len=images.length; i<len; i++) {
			images[i].src = basePath + images[i].fileName;
		}
		return dialog.selectImage(images, {selectedIndex: selectedIndex});
	}
	
	// Template Alerts
	function haltTemplate(file_path) {
		return ajaxLoad(file_path).then(function(html) {
			return halt(html);
		});
	}
	function alertTemplate(file_path) {
		return ajaxLoad(file_path).then(function(html) {
			return alert(html);
		});
	}
	function confirmTemplate(file_path, yesButtonText, noButtonText) {
		return ajaxLoad(file_path).then(function(html) {
			return confirm(html, yesButtonText, noButtonText);
		});
	}
	function alertInternetError() {
		return alert(INTERNET_ERROR_MESSAGE);
	}
	
	/**
	 * Laods a file via AJAX. The entire file content is returned as a string in data.
	 * @param: {string}
	 * @return: {{romisea} - resolves/rejects with an object {status, data}.
	 */
	function ajaxLoad(path) {
		return ajax.load(path).then(function(xhr) {
			return xhr.response;
		}, function() {
			throw new Error('cannot load: ' + path);
		});
	} // ajaxLoad()

	/********
	 * Init *
	 ********/
	ajaxLoad('/html/templates/error-internet.html').then(function(html) {
		INTERNET_ERROR_MESSAGE = html;
	});

	extensions.dialog = {
		/* Generic */
		halt: halt,
		alert: alert,
		info: info,
		confirm: confirm,
		prompt: prompt,
		select: select,
		selectImage: selectImage,
		
		/* Custom messages */
		haltTemplate: haltTemplate,
		alertTemplate: alertTemplate,
		alertInternetError: alertInternetError,
		confirmTemplate: confirmTemplate
	};
})(this.app.extensions = this.app.extensions || {}, this.dialog, ajax);
