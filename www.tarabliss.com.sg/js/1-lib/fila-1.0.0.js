/***
 * Library - Fila
 *
 * Useful methods for File Operations
 * 
 ***/
(function(global, window, document) {
	'use strict';

	/**
	 * Downloads a string as a file
	 */
	function downloadAsFile(str, file_name) {
		// Check params
		if (typeof str !== 'string') {
			str = '';
		}
		if (typeof file_name !== 'string') {
			file_name = 'download.txt';
		}

		// Serve download based on different browsers' API
		if (window.navigator.msSaveOrOpenBlob) {
			var blob = new Blob([str]);
			window.navigator.msSaveOrOpenBlob(blob, file_name);
		} // MS
		else {
			var url = "data:application/octet-stream;charset=utf-8," + encodeURIComponent(str);
			var dom_a = document.createElement('a');

			
			if ('download' in dom_a) {
				dom_a.href = url;
				dom_a.download = file_name;
				dom_a.dispatchEvent(new MouseEvent("click"));
			} // FF & Chrome
			else {
				window.open(url, '_blank');
			} // Safari
		}
	} //downloadAsFile()

	/**
	 * Uses File Reader API to read the contents of the file
	 * @param: {HTMLInput}
	 *         {object} options - optional.
	 *                  - {string} encoding [utf-8, base64] (defaults to 'base64')
	 *         {function} successCallback - optional,
	 *         {function} failureCallback - optional.
	 * @return: {Promise}
	 */
	function readFileInput(dom_input, options, successCallback, failureCallback) {
		// Check params
		if (!(dom_input instanceof HTMLInputElement)) {
			throw new TypeError('fila.readFileInput() - Expecting param1 to be HTMLInputElement. Got: ' + dom_input);
		}
		if (typeof options !== 'object' || options === null) {
			options = {
				encoding: 'base64'
			};
		}
		if (typeof options.encoding !== 'string' || (options.encoding !== 'base64' && options.encoding !== 'utf-8')) {
			options.encoding = 'base64';
		}
		if (typeof successCallback !== 'undefined') {
			if (typeof successCallback !== 'function') {
				throw new TypeError('fila.readFileInput() - Expecting param3 to be success callback function. Got: ' + (typeof successCallback) + '. Value: ' + successCallback);
			}
			if (typeof failureCallback !== 'function') {
				throw new TypeError('fila.readFileInput() - Expecting param4 to be failure callback function. Got: ' + (typeof failureCallback) + '. Value: ' + failureCallback);
			}
		} // using callbacks

		// Set up callbacks / promise
		if (typeof successCallback !== 'undefined') {
			return process(successCallback, failureCallback);
		}
		else {
			return new Promise(process);
		}

		function process(resolve, reject) {
			if (dom_input.files && dom_input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					resolve(reader.result);
				}
				if (options.encoding === 'base64') {
					reader.readAsDataURL(dom_input.files[0]);
				}
				else if (options.encoding === 'utf-8') {
					reader.readAsText(dom_input.files[0], 'UTF-8');
				}
			}
			else {
				reject();
			}
		}
	} //readFileInput()

	global.fila = {
		downloadAsFile: downloadAsFile,
		readFileInput: readFileInput
	};
})(this, window, document);