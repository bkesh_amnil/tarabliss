/**
 * Library - Dialog
 * 
 * JS Widget that adds CSS-customisable dialog boxes to app:
 *   - halt(msg)                                               (unclose-able, and halts the app as far as user is concerned)
 *   - alert(msg, okButtonText)                                (msg with OK button)
 *   - info(msg, okButtonText)                                 (msg with OK button) - functionally same as alert(), but semantically different (use for user-requested pop-ups)
 *   - confirm(msg, yesButtonText, noButtonText)               (msg with two buttons)
 *   - prompt(msg, placeholder, yesButtonText, noButtonText)   (msg with input box and two buttons)
 *   - select(msg, options, yesButtonText, noButtonText)       (msg with select box and two buttons)
 *   - selectImage(msg, images, yesButtonText, noButtonText)   (msg with a series of images and two buttons)
 *
 * Features:
 *   - Can use EITHER callbacks OR ES6-compliant promises, depending on whether callback functions are passed in as parameters
 *     Note: if using callbacks, the functions will not return any value. Instead, the callbacks will be triggered with appropriate values.
 *     Note: if using promises, each function returns an ES6-compliant Promise that resolves with appropriate values.
 *
 * Before use:
 *   - Ensure CSS styles for these elements are done:
 *      - #dialog-overlay             // typicallly covers entire screen with visibility hidden.
 *      - #dialog-overlay.shown       // "shown" class will be appended to make dialog overlay visible
 *      - #dialog-box                 // typically middle-center of screen.
 *      - #dialog-close-button        // typically top-right corner
 *
 *   - Optionally, customise CSS styles for:
 *      - #dialog-box.halt            // specific style for halt dialog boxes, e.g. cover entire screen
 *      - #dialog-box.alert           // specific style for halt dialog boxes, e.g. background image
 *      - #dialog-box.info
 *      - #dialog-box.confirm
 *      - #dialog-box.prompt
 *      - #dialog-box.select
 *      - #dialog-box.select-image
 *      - #dialog-box-message            // If message contains <h1...h6>, <p> or other elements, style them as well.
 *
 *   - Specific elements for dialog:
 *      - #dialog-box-alert-ok-button    // <button> of alert dialog
 *      - #dialog-box-info-ok-button     // <button> of info dialog
 *      - #dialog-box-prompt-input       // <input> of prompt dialog
 *      - #dialog-box-prompt-yes-button  // <button> of prompt dialog
 *      - #dialog-box-prompt-no-button   // <button> of prompt dialog
 *      - #dialog-box-select             // <select> of select dialog
 *      - #dialog-box-select-yes-button  // <button> of select dialog
 *      - #dialog-box-select-no-button   // <button> of select dialog
 *      - #dialog-box-image-list         // <div> container for images of selectImage dialog
 *      - .dialog-box-image              // <img> of selectImage dialog
 *      - .dialog-box-image-meta         // <div> of selectImage dialog
 *
 * Usage (examples):
 *   dialog.alert('Stop doing <strong>that</strong>!');
 *   dialog.confirm('<p>Are you sure you want to do this?</p>', 'Oh yea!', 'Nooooo...');
 *   dialog.prompt('Enter your name', 'Done', 'Cancel');
 */
(function(global) {
	'use strict';
	
	// DOM elements - remove overlay then add it back (look here for the HTML structure of the dialog boxes)
	var dom_dialog_overlay = document.getElementById('dialog-overlay');
	if (dom_dialog_overlay) {
		dom_dialog_overlay.parentNode.removeChild(dom_dialog_overlay);
	}
	dom_dialog_overlay = document.createElement('div');
	dom_dialog_overlay.id = 'dialog-overlay';
	dom_dialog_overlay.innerHTML = ''+
		'<div id="dialog-box"></div>'+
		'<div id="dialog-close-button"></div>';
	document.body.appendChild(dom_dialog_overlay);
	var dom_dialog_box = dom_dialog_overlay.querySelector('#dialog-box');
	var dom_dialog_close_button = dom_dialog_overlay.querySelector('#dialog-close-button');
	
	// Defaults
	var DEAFULT_HALT_MESSAGE      = '<h2>Halt!</h2>';
	var DEFAULT_ALERT_MESSAGE     = '<h2>Alert</h2>';
	var DEFAULT_ALERT_BUTTON_TEXT = 'OK';
	var DEFAULT_INFO_MESSAGE      = '<h2>Info</h2>';
	var DEFAULT_INFO_BUTTON_TEXT  = 'OK';
	var DEAFULT_CONFIRM_MESSAGE   = '<h2>Are You Sure?</h2>';
	var DEFAULT_CONFIRM_YES_BUTTON_TEXT = 'Yes';
	var DEFAULT_CONFIRM_NO_BUTTON_TEXT = 'No';
	var DEFAULT_PROMPT_MESSAGE = '<h2>Enter Value</h2>';
	var DEFAULT_PROMPT_PLACEHOLDER_TEXT = 'Click here to start typing';
	var DEFAULT_PROMPT_YES_BUTTON_TEXT = 'OK';
	var DEFAULT_PROMPT_NO_BUTTON_TEXT = 'Cancel';
	var DEFAULT_SELECT_MESSAGE = '<h2>Make Your Selection</h2>';
	var DEFAULT_SELECT_YES_BUTTON_TEXT = 'OK';
	var DEFAULT_SELECT_NO_BUTTON_TEXT = 'Cancel';
	var DEFAULT_SELECT_IMAGE_MESSAGE = '<h2>Choose an image</h2>';
	var DEFAULT_SELECT_IMAGE_YES_BUTTON_TEXT = 'OK';
	var DEFAULT_SELECT_IMAGE_NO_BUTTON_TEXT = 'Cancel';

	// Context
	var scrollLocked = false;
	var prevWindowOnScroll = window.onscroll; // used by lockScroll to remember any onscroll function on window when we swap it out for a noop
	
	/**
	 * Shows a message with no possibility to stop
	 * @param: {string} msg
	 */
	function halt(msg) {
		// Default values for params
		if (typeof msg !== 'string') {
			msg = DEAFULT_HALT_MESSAGE;
		}
		
		// Set texts
		dom_dialog_box.className = 'halt';
		dom_dialog_box.innerHTML = '<div id="dialog-box-message">' + msg + '</div>';
		
		// Display dialog box
		lockScroll();
		dom_dialog_overlay.classList.add('shown');
		dom_dialog_box.scrollTop = 0;
	} //halt()
	
	/**
	 * Shows an alert dialog with one button
	 * @param: {string, object, function}             - use callback. (Case 1) Params read as: (msg, options, callback)
	 *         {string, function}                     - use callback. (Case 2) Params read as: (msg, callback).
	 *         {function}                             - use callback. (Case 3) Params read as: (callback).
	 *         {string, object}                       - use promise. (Case 4) Params read as: (msg, options)
	 *         {string}                               - use promise. (Case 5) Params read as: (msg).
	 *         {object}                               - use promise. (Case 6) Params read as: (options).
	 *         {}                                     - use promise. (Case 7)
	 * @return: {Promise} - resolves without data
	 */
	function alert() {
		var msg, options, callback;
		
		// Parse params
		var arglen = arguments.length;
		if (arglen === 3) {
			msg = arguments[0];
			options = arguments[1];
			callback = arguments[2];
		} // Case 1
		else if (arglen === 2) {
			if (typeof arguments[1] === 'function') {
				msg = arguments[0];
				callback = arguments[1];
			} // Case 2
			else if (typeof arguments[1] === 'object') {
				msg = arguments[0];
				options = arguments[1];
			} // Case 4
		} // Case 2, 4
		else if (arglen === 1) {
			if (typeof arguments[0] === 'function') {
				callback = arguments[0];
			} // Case 3 - function
			else if (typeof arguments[0] === 'string') {
				msg = arguments[0];
			} // Case 5 - string
			else if (typeof arguments[0] === 'object') {
				options = arguments[0];
			} // Case 6 - object
		} // Case 3, 5, 6
		else if (arglen === 0) {
		} // Case 7
		else {
			throw new TypeError('dialog.alert() - unable to parse arguments (count: ' + arglen + ')');
		}

		// Default values for params
		if (typeof callback !== 'function') {
			callback = void 0;
		}
		if (typeof msg !== 'string') {
			msg = DEFAULT_ALERT_MESSAGE;
		}
		if (typeof options !== 'object' || options === null) {
			options = {
				'okButtonText': DEFAULT_ALERT_BUTTON_TEXT
			}
		}
		if (typeof options['okButtonText'] !== 'string') {
			options['okButtonText'] = DEFAULT_ALERT_BUTTON_TEXT;
		}

		// Set text - msg
		dom_dialog_box.className = 'alert';
		dom_dialog_box.innerHTML = '<div id="dialog-box-message">' + msg + '</div>';

		// set text - button
		var dom_dialog_box_alert_ok = document.createElement('button');
		dom_dialog_box_alert_ok.id = 'dialog-box-alert-ok-button';
		dom_dialog_box_alert_ok.innerHTML = options['okButtonText'];
		dom_dialog_box.appendChild(dom_dialog_box_alert_ok);

		// Display dialog box
		lockScroll();
		dom_dialog_overlay.classList.add('shown');
		dom_dialog_box.scrollTop = 0;
		dom_dialog_box_alert_ok.focus();
		
		// Set up Promise, or callbacks if supplied as params
		if (typeof callback === 'undefined') {
			return new Promise(setupHandlers);
		}
		else {
			return setupHandlers(callback);
		}
		
		function setupHandlers(callback) {
			function alertOKHandler(e) {
				e.preventDefault();
				e.stopPropagation();
				
				closeOverlay();
				callback();
			}
			
			function keydownHandler(e) {
				var key = (window.event) ? event.keyCode : e.keyCode;
				if (key === 27) { // ESC
					e.preventDefault();
					
					closeOverlay();
					callback();
				}
			}
			
			function focusHandler(e){
				if (!isElementInDialog(e.target)) {
					dom_dialog_box_alert_ok.focus();
				}
			}
			
			function closeOverlay() {
				// Clean up bindings
				dom_dialog_close_button.removeEventListener('click', alertOKHandler);
				dom_dialog_box_alert_ok.removeEventListener('click', alertOKHandler);
				document.removeEventListener('keydown', keydownHandler);
				document.removeEventListener('focus', focusHandler, true);
				
				// Hide overlay
				dom_dialog_overlay.classList.remove('shown');
				unlockScroll();
			}
			
			// Listen for OK clicks, ESC key
			dom_dialog_close_button.addEventListener('click', alertOKHandler);
			dom_dialog_box_alert_ok.addEventListener('click', alertOKHandler);
			document.addEventListener('keydown', keydownHandler);
			document.addEventListener('focus', focusHandler, true);
		} // setupHandlers()
	} //alert()
	
	/**
	 * Shows an info dialog with one button
	 * @param: {string, object, function}             - use callback. (Case 1) Params read as: (msg, options, callback)
	 *         {string, function}                     - use callback. (Case 2) Params read as: (msg, callback)
	 *         {function}                             - use callback. (Case 3) Params read as: (callback)
	 *         {string, object}                       - use promise. (Case 4) Params read as: (msg, options)
	 *         {string}                               - use promise. (Case 5) Params read as: (msg)
	 *         {object}                               - use promise. (Case 6) Params read as: (options)
	 *         {}                                     - use promise. (Case 7)
	 * @return: void or {Promise} - resolves without data
	 */
	function info() {
		var msg, options, callback;
		
		// Parse params
		var arglen = arguments.length;
		if (arglen === 3) {
			msg = arguments[0];
			options = arguments[1];
			callback = arguments[2];
		} // Case 1
		else if (arglen === 2) {
			if (typeof arguments[1] === 'function') {
				msg = arguments[0];
				callback = arguments[1];
			} // Case 2
			else if (typeof arguments[1] === 'object') {
				msg = arguments[0];
				options = arguments[1];
			} // Case 4
		} // Case 2, 4
		else if (arglen === 1) {
			if (typeof arguments[0] === 'function') {
				callback = arguments[0];
			} // Case 3 - function
			else if (typeof arguments[0] === 'string') {
				msg = arguments[0];
			} // Case 5 - string
			else if (typeof arguments[0] === 'object') {
				options = arguments[0];
			} // Case 6 - object
		} // Case 3, 5, 6
		else if (arglen === 0) {
		} // Case 7
		else {
			throw new TypeError('dialog.info() - unable to parse arguments (count: ' + arglen + ')');
		}

		// Default values for params
		if (typeof callback !== 'function') {
			callback = void 0;
		}
		if (typeof msg !== 'string') {
			msg = DEFAULT_INFO_MESSAGE;
		}
		if (typeof options !== 'object' || options === null) {
			options = {
				'okButtonText': DEFAULT_INFO_BUTTON_TEXT
			}
		}
		if (typeof options['okButtonText'] !== 'string') {
			options['okButtonText'] = DEFAULT_INFO_BUTTON_TEXT;
		}
		
		// Set text - msg
		dom_dialog_box.className = 'info';
		dom_dialog_box.innerHTML = '<div id="dialog-box-message">' + msg + '</div>';

		// Set text - button
		var dom_dialog_box_info_ok = document.createElement('button');
		dom_dialog_box_info_ok.id = 'dialog-box-info-ok-button';
		dom_dialog_box_info_ok.innerHTML = options['okButtonText'];
		dom_dialog_box.appendChild(dom_dialog_box_info_ok);

		// Display dialog box
		lockScroll();
		dom_dialog_overlay.classList.add('shown');
		dom_dialog_box.scrollTop = 0;
		dom_dialog_box_info_ok.focus();
		
		// Set up Promise, or callbacks if supplied as params
		if (typeof callback === 'undefined') {
			return new Promise(setupHandlers);
		}
		else {
			return setupHandlers(callback);
		}
		
		function setupHandlers(callback) {
			function infoOKHandler(e) {
				e.preventDefault();
				e.stopPropagation();
				
				closeOverlay();
				callback();
			}
			
			function keydownHandler(e) {
				var key = (window.event) ? event.keyCode : e.keyCode;
				if (key === 27) { // ESC
					e.preventDefault();
					
					closeOverlay();
					callback();
				}
			}
			
			function focusHandler(e){
				if (!isElementInDialog(e.target)) {
					dom_dialog_box_info_ok.focus();
				}
			}
			
			function closeOverlay() {
				// Clean up bindings
				dom_dialog_close_button.removeEventListener('click', infoOKHandler);
				dom_dialog_box_info_ok.removeEventListener('click', infoOKHandler);
				document.removeEventListener('keydown', keydownHandler);
				document.removeEventListener('focus', focusHandler, true);
				
				// Hide overlay
				dom_dialog_overlay.classList.remove('shown');
				unlockScroll();
			}
			
			// Listen for OK clicks, ESC key
			dom_dialog_close_button.addEventListener('click', infoOKHandler);
			dom_dialog_box_info_ok.addEventListener('click', infoOKHandler);
			document.addEventListener('keydown', keydownHandler);
			document.addEventListener('focus', focusHandler, true);
		} // setupHandlers()
	} //info()
	
	/**
	 * Shows a confirm dialog with two buttons
	 * @param: {string, object, function, function}        - use callbacks. (Case 1) Params read as: (msg, options, yesCallback, noCallback)
	 *         {string, function, function}                - use callbacks. (Case 2) Params read as: (msg, yesCallback, noCallback)
	 *         {object, function, function}                - use callbacks. (Case 3) Params read as: (options, yesCallback, noCallback)
	 *         {function, function}                        - use callbacks. (Case 4) Params read as: (yesCallback, noCallback)
	 *         {string, object, function}                  - use callbacks. (Case 5) Params read as: (msg, options, yesCallback)
	 *         (string, function)                          - use callbacks. (Case 6) Params read as: (msg, yesCallback)
	 *         {object, function}                          - use callbacks. (Case 7) Params read as: (options, yesCallback)
	 *         (function)                                  - use callbacks. (Case 8) Params read as: (yesCallback)
	 *         {string, object}                            - use promise. (Case 9) Params read as: (msg, options)
	 *         {string}                                    - use promise. (Case 10) Params read as: (msg).
	 *         {object}                                    - use promise. (Case 11) Params read as: (options).
	 *         {}                                          - use promise. (Case 12)
	 * @return: {Promise} - resolves without data
	 */
	function confirm() {
		var msg, options, yesCallback, noCallback;
		
		// Parse params
		var arglen = arguments.length;
		if (arglen === 4) {
			msg = arguments[0];
			options = arguments[1];
			yesCallback = arguments[2];
			noCallback = arguments[3];
		} // Case 1
		else if (arglen === 3) {
			if (typeof arguments[1] === 'function') {
				if (typeof arguments[0] === 'string') {
					msg = arguments[0];
					yesCallback = arguments[1];
					noCallback = arguments[2];
				} // Case 2 - {string, function, function}
				else if (typeof arguments[0] === 'object') {
					options = arguments[0];
					yesCallback = arguments[1];
					noCallback = arguments[2];
				} // Case 3 - {object, function, function}
			} // Case 2, 3
			else if (typeof arguments[1] === 'object') {
				msg = arguments[0];
				options = arguments[1];
				yesCallback = arguments[2];
			} // Case 5 - {string, object, function}
		} // Case 2, 3, 5
		else if (arglen === 2) {
			if (typeof arguments[1] === 'function') {
				if (typeof arguments[0] === 'function') {
					yesCallback = arguments[0];
					noCallback = arguments[1];
				} // Case 4 - {function, function}
				else if (typeof arguments[0] === 'string') {
					msg = arguments[0];
					yesCallback = arguments[1];
				} // Case 6 - (string, function) 
				else if (typeof arguments[0] === 'object') {
					options = arguments[0];
					yesCallback = arguments[1];
				} // Case 7 - {object, function}
			} // Case 4, 6, 7
			else if (typeof arguments[1] === 'object') {
				msg = arguments[0];
				options = arguments[1];
			} // Case 9 - {string, object}
		} // Case 4, 6, 7, 9
		else if (arglen === 1) {
			if (typeof arguments[0] === 'function') {
				yesCallback = arguments[0]
			} // Case 8 - function
			else if (typeof arguments[0] === 'string') {
				msg = arguments[0];
			} // Case 10 - string
			else if (typeof arguments[0] === 'object') {
				options = arguments[0];
			} // Case 11 - object
		} // Case 8, 10, 11
		else if (arglen === 0) {
		} // Case 12
		else {
			throw new TypeError('dialog.confirm() - unable to parse arguments (count: ' + arglen + ')');
		}

		// Default values for params
		if (typeof yesCallback !== 'function') {
			yesCallback = void 0;
		}
		if (typeof noCallback !== 'function') {
			noCallback = void 0;
		}
		if (typeof msg !== 'string') {
			msg = DEAFULT_CONFIRM_MESSAGE;
		}
		if (typeof options !== 'object' || options === null) {
			options = {
				'yesButtonText': DEFAULT_CONFIRM_YES_BUTTON_TEXT,
				'noButtonText': DEFAULT_CONFIRM_NO_BUTTON_TEXT
			}
		}
		if (typeof options['yesButtonText'] !== 'string') {
			options['yesButtonText'] = DEFAULT_CONFIRM_YES_BUTTON_TEXT;
		}
		if (typeof options['noButtonText'] !== 'string') {
			options['noButtonText'] = DEFAULT_CONFIRM_NO_BUTTON_TEXT;
		}
		
		// Set text - msg
		dom_dialog_box.className = 'confirm';
		dom_dialog_box.innerHTML = '<div id="dialog-box-message">' + msg + '</div>';

		// Set text - yes button
		var dom_dialog_box_confirm_yes = document.createElement('button');
		dom_dialog_box_confirm_yes.id = 'dialog-box-confirm-yes-button';
		dom_dialog_box_confirm_yes.innerHTML = options['yesButtonText'];
		dom_dialog_box.appendChild(dom_dialog_box_confirm_yes);

		// Set text - no button
		var dom_dialog_box_confirm_no = document.createElement('button');
		dom_dialog_box_confirm_no.id = 'dialog-box-confirm-no-button';
		dom_dialog_box_confirm_no.innerHTML = options['noButtonText'];
		dom_dialog_box.appendChild(dom_dialog_box_confirm_no);

		// Display dialog box
		lockScroll();
		dom_dialog_overlay.classList.add('shown');
		dom_dialog_box.scrollTop = 0;
		dom_dialog_box_confirm_yes.focus();
		
		// Set up Promise, or callbacks if supplied as params
		if (typeof yesCallback === 'undefined') {
			return new Promise(setupHandlers);
		}
		else {
			return setupHandlers(yesCallback, noCallback);
		}
		
		function setupHandlers(yesCallback, noCallback) {
			function confirmYesHandler(e) {
				e.preventDefault();
				e.stopPropagation();
				
				closeOverlay();
				yesCallback();
			} //confirmYesHandler()
			
			function confirmNoHandler(e) {
				e.preventDefault();
				e.stopPropagation();
				
				closeOverlay();
				if (noCallback) {
					noCallback();
				}
			} //confirmNoHandler()
			
			function keydownHandler(e) {
				var key = (window.event) ? event.keyCode : e.keyCode;
				if (key === 27) { //ESC key
					e.preventDefault();
					
					closeOverlay();
					if (noCallback) {
						noCallback();
					}
				}

			} //keydownHandler()
			
			function focusHandler(e){
				if (!isElementInDialog(e.target)) {
					//console.log('auto-focusing');
					dom_dialog_box_confirm_yes.focus();
				}
			} //focusHandler()
			
			function closeOverlay() {
				// Clean up bindings
				dom_dialog_close_button.removeEventListener('click', confirmNoHandler);
				dom_dialog_box_confirm_yes.removeEventListener('click', confirmYesHandler);
				dom_dialog_box_confirm_no.removeEventListener('click', confirmNoHandler);
				document.removeEventListener('keydown', keydownHandler);
				document.removeEventListener('focus', focusHandler, true);
				
				// Hide overlay
				dom_dialog_overlay.classList.remove('shown');
				unlockScroll();
			} //closeOverlay()
			
			// Listen for OK clicks, ESC key
			dom_dialog_close_button.addEventListener('click', confirmNoHandler);
			dom_dialog_box_confirm_yes.addEventListener('click', confirmYesHandler);
			dom_dialog_box_confirm_no.addEventListener('click', confirmNoHandler);
			document.addEventListener('keydown', keydownHandler);
			document.addEventListener('focus', focusHandler, true);
		} //setupHandlers()
	} //confirm()
	
	/**
	 * @param: {string, object, function, function}        - use callbacks. (Case 1) Params read as: (msg, options, yesCallback, noCallback)
	 *         {string, function, function}                - use callbacks. (Case 2) Params read as: (msg, yesCallback, noCallback)
	 *         {object, function, function}                - use callbacks. (Case 3) Params read as: (options, yesCallback, noCallback)
	 *         {function, function}                        - use callbacks. (Case 4) Params read as: (yesCallback, noCallback)
	 *         {string, object, function}                  - use callbacks. (Case 5) Params read as: (msg, options, yesCallback)
	 *         (string, function)                          - use callbacks. (Case 6) Params read as: (msg, yesCallback)
	 *         {object, function}                          - use callbacks. (Case 7) Params read as: (options, yesCallback)
	 *         (function)                                  - use callbacks. (Case 8) Params read as: (yesCallback)
	 *         {string, object}                            - use promise. (Case 9) Params read as: (msg, options)
	 *         {string}                                    - use promise. (Case 10) Params read as: (msg).
	 *         {object}                                    - use promise. (Case 11) Params read as: (options).
	 *         {}                                          - use promise. (Case 12)
	 * @return: {Promise} - resolves with input value {string}, rejects with null
	 */
	function prompt() {
		var msg, options, yesCallback, noCallback;
		
		// Parse params
		var arglen = arguments.length;
		if (arglen === 4) {
			msg = arguments[0];
			options = arguments[1];
			yesCallback = arguments[2];
			noCallback = arguments[3];
		} // Case 1
		else if (arglen === 3) {
			if (typeof arguments[1] === 'function') {
				if (typeof arguments[0] === 'string') {
					msg = arguments[0];
					yesCallback = arguments[1];
					noCallback = arguments[2];
				} // Case 2 - {string, function, function}
				else if (typeof arguments[0] === 'object') {
					options = arguments[0];
					yesCallback = arguments[1];
					noCallback = arguments[2];
				} // Case 3 - {object, function, function}
			} // Case 2, 3
			else if (typeof arguments[1] === 'object') {
				msg = arguments[0];
				options = arguments[1];
				yesCallback = arguments[2];
			} // Case 5 - {string, object, function}
		} // Case 2, 3, 5
		else if (arglen === 2) {
			if (typeof arguments[1] === 'function') {
				if (typeof arguments[0] === 'function') {
					yesCallback = arguments[0];
					noCallback = arguments[1];
				} // Case 4 - {function, function}
				else if (typeof arguments[0] === 'string') {
					msg = arguments[0];
					yesCallback = arguments[1];
				} // Case 6 - (string, function) 
				else if (typeof arguments[0] === 'object') {
					options = arguments[0];
					yesCallback = arguments[1];
				} // Case 7 - {object, function}
			} // Case 4, 6, 7
			else if (typeof arguments[1] === 'object') {
				msg = arguments[0];
				options = arguments[1];
			} // Case 9 - {string, object}
		} // Case 4, 6, 7, 9
		else if (arglen === 1) {
			if (typeof arguments[0] === 'function') {
				yesCallback = arguments[0]
			} // Case 8 - function
			else if (typeof arguments[0] === 'string') {
				msg = arguments[0];
			} // Case 10 - string
			else if (typeof arguments[0] === 'object') {
				options = arguments[0];
			} // Case 11 - object
		} // Case 8, 10, 11
		else if (arglen === 0) {
		} // Case 12
		else {
			throw new TypeError('dialog.prompt() - unable to parse arguments (count: ' + arglen + ')');
		}

		// Default values for params
		if (typeof yesCallback !== 'function') {
			yesCallback = void 0;
		}
		if (typeof noCallback !== 'function') {
			noCallback = void 0;
		}
		if (typeof msg !== 'string') {
			msg = DEAFULT_CONFIRM_MESSAGE;
		}
		if (typeof options !== 'object' || options === null) {
			options = {
				'placeholderText': DEFAULT_PROMPT_PLACEHOLDER_TEXT,
				'yesButtonText': DEFAULT_PROMPT_YES_BUTTON_TEXT,
				'noButtonText': DEFAULT_PROMPT_NO_BUTTON_TEXT
			}
		}
		if (typeof options['placeholderText'] !== 'string') {
			options['placeholderText'] = DEFAULT_PROMPT_PLACEHOLDER_TEXT;
		}
		if (typeof options['yesButtonText'] !== 'string') {
			options['yesButtonText'] = DEFAULT_PROMPT_YES_BUTTON_TEXT;
		}
		if (typeof options['noButtonText'] !== 'string') {
			options['noButtonText'] = DEFAULT_PROMPT_NO_BUTTON_TEXT;
		}

		// Set texts
		dom_dialog_box.className = 'prompt';
		dom_dialog_box.innerHTML = '<div id="dialog-box-message">' + msg + '</div>';

		// Set text: input		
		var dom_dialog_box_prompt_input = document.createElement('input');
		dom_dialog_box_prompt_input.id = 'dialog-box-prompt-input';
		dom_dialog_box_prompt_input.setAttribute('placeholder', options['placeholderText']);
		dom_dialog_box.appendChild(dom_dialog_box_prompt_input);

		// Set text: yes button
		var dom_dialog_box_prompt_yes = document.createElement('button');
		dom_dialog_box_prompt_yes.id = 'dialog-box-prompt-yes-button';
		dom_dialog_box_prompt_yes.innerHTML = options['yesButtonText'];
		dom_dialog_box.appendChild(dom_dialog_box_prompt_yes);

		// Set text: no button
		var dom_dialog_box_prompt_no = document.createElement('button');
		dom_dialog_box_prompt_no.id = 'dialog-box-prompt-no-button';
		dom_dialog_box_prompt_no.innerHTML = options['noButtonText'];
		dom_dialog_box.appendChild(dom_dialog_box_prompt_no);

		// Display dialog box
		lockScroll();
		dom_dialog_overlay.classList.add('shown');
		dom_dialog_box.scrollTop = 0;
		dom_dialog_box_prompt_yes.focus();
		
		// Set up Promise, or callbacks if supplied as params
		if (typeof yesCallback === 'undefined') {
			return new Promise(setupHandlers);
		}
		else {
			return setupHandlers(yesCallback, noCallback);
		}

		function setupHandlers(yesCallback, noCallback) {
			function confirmYesHandler(e) {
				e.preventDefault();
				e.stopPropagation();
				
				closeOverlay();
				yesCallback(dom_dialog_box_prompt_input.value);
			} //confirmYesHandler()
			
			function confirmNoHandler(e) {
				e.preventDefault();
				e.stopPropagation();
				
				closeOverlay();
				if (noCallback) {
					noCallback(null);
				}
			} //confirmNoHandler()
			
			function keydownHandler(e) {
				var key = (window.event) ? event.keyCode : e.keyCode;
				if (key === 27) { //ESC key
					e.preventDefault();
					
					closeOverlay();
					if (noCallback) {
						noCallback(null);
					}
				}
				if(key === 13) { // ENTER key
					// Ignore ENTER key if not done in input box
					if (e.target !== dom_dialog_box_prompt_input) {
						return;
					}
					
					closeOverlay();
					yesCallback(dom_dialog_box_prompt_input.value);
				}
			} //keydownHandler()
			
			function focusHandler(e){
				if (!isElementInDialog(e.target)) {
					dom_dialog_box_prompt_input.focus();
				}
			} //focusHandler()
			
			function closeOverlay() {
				// Clean up bindings
				dom_dialog_close_button.removeEventListener('click', confirmNoHandler);
				dom_dialog_box_prompt_yes.removeEventListener('click', confirmYesHandler);
				dom_dialog_box_prompt_no.removeEventListener('click', confirmNoHandler);
				document.removeEventListener('keydown', keydownHandler);
				document.removeEventListener('focus', focusHandler, true);
				
				// Hide overlay
				dom_dialog_overlay.classList.remove('shown');
				unlockScroll();
			} //closeOverlay()
			
			// Listen for OK clicks, ESC, ENTER key
			dom_dialog_close_button.addEventListener('click', confirmNoHandler);
			dom_dialog_box_prompt_yes.addEventListener('click', confirmYesHandler);
			dom_dialog_box_prompt_no.addEventListener('click', confirmNoHandler);
			document.addEventListener('keydown', keydownHandler);
			document.addEventListener('focus', focusHandler, true);
		} //setupHandlers()
	} //prompt

	/**
	 * @param: {string, array, object, function, function}         - use callbacks. (Case 1) Params read as: (msg, selectOptions, options, yesCallback, noCallback)
	 *         {array, object, function, function}                 - use callbacks. (Case 2) Params read as: (selectOptions, options, yesCallback, noCallback)
	 *         {string, array, function, function}                 - use callbacks. (Case 3) Params read as: (msg, selectOptions, yesCallback, noCallback)
	 *         {array, function, function}                         - use callbacks. (Case 4) Params read as: (selectOptions, yesCallback, noCallback)
	 *         {string, array, object, function}                   - use callbacks. (Case 5) Params read as: (msg, selectOptions, options, yesCallback)
	 *         {array, object, function}                           - use callbacks. (Case 6) Params read as: (msg, selectOptions, options, yesCallback)
	 *         {string, array, function}                           - use callbacks. (Case 7) Params read as: (msg, selectOptions, yesCallback)
	 *         {array, function}                                   - use callbacks. (Case 8) Params read as: (selectOptions, yesCallback)
	 *         (string, array, object}                             - use promises. (Case 9) Params read as: (msg, selectOptions, options)
	 *         {array, object}                                     - use promises. (Case 10) Params read as: (selectOptions, options)
	 *         {string, array}                                     - use promises. (Case 11) Params read as: (msg, selectOptions)
	 *         {array}                                             - use promises. (Case 12) Params read as: (selectOptions)
	 *         - Note: selectOptions is always intepreted as an array of strings
	 * @return: {Promise} - resolves with select value {string}, rejects with null
	 */
	function select() {
		var msg, selectOptions, options, yesCallback, noCallback;

		// Parse params
		var arglen = arguments.length;
		if (arglen === 5) {
			msg = arguments[0];
			selectOptions = arguments[1];
			options = arguments[2];
			yesCallback = arguments[3];
			noCallback = arguments[4];
		} // Case 1 - (string, array, object, function, function)
		else if (arglen === 4) {
			if (typeof arguments[2] === 'function') {
				if (arguments[0] instanceof Array) {
					selectOptions = arguments[0];
					options = arguments[1];
					yesCallback = arguments[2];
					noCallback = arguments[3];
				} // Case 2 - {array, object, function, function}
				else if (arguments[1] instanceof Array) {
					msg = arguments[0];
					selectOptions = arguments[1];
					yesCallback = arguments[2];
					noCallback = arguments[3];
				} // Case 3 - (string, array, function, function)
			}  // Case 2, 3
			else if (typeof arguments[2] === 'object') {
				msg = arguments[0];
				selectOptions = arguments[1];
				options = arguments[2];
				yesCallback = arguments[3];
			} // Case 5 - (string, array, object, function)
		} // Case 2, 3, 5
		else if (arglen === 3) {
			if (typeof arguments[2] === 'function') {
				if (typeof arguments[1] === 'function') {
					selectOptions = arguments[0];
					yesCallback = arguments[1];
					noCallback = arguments[2];
				} // Case 4 - {array, function, function} 
				else if (typeof arguments[1] === 'object') {
					if (arguments[1] instanceof Array) {
						msg = arguments[0];
						selectOptions = arguments[1];
						yesCallback = arguments[2];
					} // Case 7 - (string, array, function)
					else {
						selectOptions = arguments[0];
						options = arguments[1];
						yesCallback = arguments[2];
					} // Case 6 - {array, object, function}
				} // Case 6, 7
			} // Case 4, 6, 7
			else if (typeof arguments[2] === 'object') {
				msg = arguments[0];
				selectOptions = arguments[1];
				options = arguments[2];
			} // Case 9 - (string, array, object)
		} // Case 4, 6, 7. 9
		else if (arglen === 2) {
			if (typeof arguments[1] === 'function') {
				selectOptions = arguments[0];
				yesCallback = arguments[1];
			} // Case 8  - {array, function}
			else if (typeof arguments[1] === 'object') {
				if (arguments[1] instanceof Array) {
					msg = arguments[0];
					selectOptions = arguments[1];
				} // Case 11 - {string, array}
				else {
					selectOptions = arguments[0];
					options = arguments[1];
				} // Case 10 - {array, object}
			} // Case 10, 11
		} // Case 8, 10, 11
		else if (arglen === 1) {
			selectOptions = arguments[0];
		} // Case 12 - {array}
		else {
			throw new TypeError('dialog.select() - unable to parse arguments (argument count: ' + arglen + ')');
		}

		// Default values for params
		if (typeof yesCallback !== 'function') {
			yesCallback = void 0;
		}
		if (typeof noCallback !== 'function') {
			noCallback = void 0;
		}
		if (typeof msg !== 'string') {
			msg = DEFAULT_SELECT_MESSAGE;
		}
		if (selectOptions instanceof Array) {
			if (selectOptions.length < 2) {
				throw new TypeError('dialog.select() - selectOptions must be an Array of 2 or more values.');
			}
		}
		else {
			throw new TypeError('dialog.select() - selectOptions must be an Array.');
		}

		// Options
		if (typeof options !== 'object' || options === null) {
			options = {
				'yesButtonText': DEFAULT_SELECT_YES_BUTTON_TEXT,
				'noButtonText': DEFAULT_SELECT_NO_BUTTON_TEXT
			}
		}
		if (typeof options['yesButtonText'] !== 'string') {
			options['yesButtonText'] = DEFAULT_SELECT_YES_BUTTON_TEXT;
		}
		if (typeof options['noButtonText'] !== 'string') {
			options['noButtonText'] = DEFAULT_SELECT_NO_BUTTON_TEXT;
		}

		// Set texts
		dom_dialog_box.className = 'select';
		dom_dialog_box.innerHTML = '<div id="dialog-box-message">' + msg + '</div>';

		// Set text: select		
		var dom_dialog_box_select = document.createElement('select');
		dom_dialog_box_select.id = 'dialog-box-select';
		dom_dialog_box_select.innerHTML = '';
		for (var i=0, len=selectOptions.length; i<len; i++) {
			dom_dialog_box_select.innerHTML += '<option value="' + selectOptions[i] + '">' + selectOptions[i] + '</option>';
		}
		dom_dialog_box.appendChild(dom_dialog_box_select);

		// Set text: yes button
		var dom_dialog_box_select_yes = document.createElement('button');
		dom_dialog_box_select_yes.id = 'dialog-box-select-yes-button';
		dom_dialog_box_select_yes.innerHTML = options['yesButtonText'];
		dom_dialog_box.appendChild(dom_dialog_box_select_yes);

		// Set text: no button
		var dom_dialog_box_select_no = document.createElement('button');
		dom_dialog_box_select_no.id = 'dialog-box-select-no-button';
		dom_dialog_box_select_no.innerHTML =  options['noButtonText'];
		dom_dialog_box.appendChild(dom_dialog_box_select_no);

		// Display dialog box
		lockScroll();
		dom_dialog_overlay.classList.add('shown');
		dom_dialog_box.scrollTop = 0;
		dom_dialog_box_select.focus();

		// Set up Promise, or callbacks if supplied as params
		if (typeof yesCallback === 'undefined') {
			return new Promise(setupHandlers);
		}
		else {
			return setupHandlers(yesCallback, noCallback);
		}

		function setupHandlers(yesCallback, noCallback) {
			function confirmYesHandler(e) {
				e.preventDefault();
				e.stopPropagation();
				
				closeOverlay();
				yesCallback(dom_dialog_box_select.value);
			} //confirmYesHandler()
			
			function confirmNoHandler(e) {
				e.preventDefault();
				e.stopPropagation();
				
				closeOverlay();
				if (noCallback) {
					noCallback(null);
				}
			} //confirmNoHandler()
			
			function keydownHandler(e) {
				var key = (window.event) ? event.keyCode : e.keyCode;
				if (key === 27) { //ESC key
					e.preventDefault();
					
					closeOverlay();
					if (noCallback) {
						noCallback(null);
					}
				}
			} //keydownHandler()
			
			function focusHandler(e){
				if (!isElementInDialog(e.target)) {
					dom_dialog_box_select.focus();
				}
			} //focusHandler()
			
			function closeOverlay() {
				// Clean up bindings
				dom_dialog_close_button.removeEventListener('click', confirmNoHandler);
				dom_dialog_box_select_yes.removeEventListener('click', confirmYesHandler);
				dom_dialog_box_select_no.removeEventListener('click', confirmNoHandler);
				document.removeEventListener('keydown', keydownHandler);
				document.removeEventListener('focus', focusHandler, true);
				
				// Hide overlay
				dom_dialog_overlay.classList.remove('shown');
				unlockScroll();
			} //closeOverlay()
			
			// Listen for OK clicks, ESC key
			dom_dialog_close_button.addEventListener('click', confirmNoHandler);
			dom_dialog_box_select_yes.addEventListener('click', confirmYesHandler);
			dom_dialog_box_select_no.addEventListener('click', confirmNoHandler);
			document.addEventListener('keydown', keydownHandler);
			document.addEventListener('focus', focusHandler, true);
		} //setupHandlers()
	} // select()

	/**
	 * @param: {string, array, object, function, function}         - use callbacks. (Case 1) Params read as: (msg, images, options, yesCallback, noCallback)
	 *         {array, object, function, function}                 - use callbacks. (Case 2) Params read as: (images, options, yesCallback, noCallback)
	 *         {string, array, function, function}                 - use callbacks. (Case 3) Params read as: (msg, images, yesCallback, noCallback)
	 *         {array, function, function}                         - use callbacks. (Case 4) Params read as: (images, yesCallback, noCallback)
	 *         {string, array, object, function}                   - use callbacks. (Case 5) Params read as: (msg, images, options, yesCallback)
	 *         {array, object, function}                           - use callbacks. (Case 6) Params read as: (msg, images, options, yesCallback)
	 *         {string, array, function}                           - use callbacks. (Case 7) Params read as: (msg, images, yesCallback)
	 *         {array, function}                                   - use callbacks. (Case 8) Params read as: (images, yesCallback)
	 *         (string, array, object}                             - use promises. (Case 9) Params read as: (msg, images, yesButtonText, noButtonText)
	 *         {array, object}                                     - use promises. (Case 10) Params read as: (images, yesButtonText, noButtonText)
	 *         {string, array}                                     - use promises. (Case 11) Params read as: (msg, images)
	 *         {array}                                             - use promises. (Case 12) Params read as: (images)
	 *         - Note: images is always intepreted as an array of objects
	 *           {
	 *               title:    {string}
	 *               src:      {string}
	 *               width:    {number}
	 *               height:   {number}
	 *           }
	 * @return: {Promise} - resolves with select image {object}, rejects with null
	 */
	function selectImage() {
		var msg, images, options, yesCallback, noCallback;

		// Parse params
		var arglen = arguments.length;
		if (arglen === 5) {
			msg = arguments[0];
			images = arguments[1];
			options = arguments[2];
			yesCallback = arguments[3];
			noCallback = arguments[4];
		} // Case 1 - (string, array, object, function, function)
		else if (arglen === 4) {
			if (typeof arguments[2] === 'function') {
				if (arguments[0] instanceof Array) {
					images = arguments[0];
					options = arguments[1];
					yesCallback = arguments[2];
					noCallback = arguments[3];
				} // Case 2 - {array, object, function, function}
				else if (arguments[1] instanceof Array) {
					msg = arguments[0];
					images = arguments[1];
					yesCallback = arguments[2];
					noCallback = arguments[3];
				} // Case 3 - (string, array, function, function)
			}  // Case 2, 3
			else if (typeof arguments[2] === 'object') {
				msg = arguments[0];
				images = arguments[1];
				options = arguments[2];
				yesCallback = arguments[3];
			} // Case 5 - (string, array, object, function)
		} // Case 2, 3, 5
		else if (arglen === 3) {
			if (typeof arguments[2] === 'function') {
				if (typeof arguments[1] === 'function') {
					images = arguments[0];
					yesCallback = arguments[1];
					noCallback = arguments[2];
				} // Case 4 - {array, function, function} 
				else if (typeof arguments[1] === 'object') {
					if (arguments[1] instanceof Array) {
						msg = arguments[0];
						images = arguments[1];
						yesCallback = arguments[2];
					} // Case 7 - (string, array, function)
					else {
						images = arguments[0];
						options = arguments[1];
						yesCallback = arguments[2];
					} // Case 6 - {array, object, function}
				} // Case 6, 7
			} // Case 4, 6, 7
			else if (typeof arguments[2] === 'object') {
				msg = arguments[0];
				images = arguments[1];
				options = arguments[2];
			} // Case 9 - (string, array, object)
		} // Case 4, 6, 7. 9
		else if (arglen === 2) {
			if (typeof arguments[1] === 'function') {
				images = arguments[0];
				yesCallback = arguments[1];
			} // Case 8  - {array, function}
			else if (typeof arguments[1] === 'object') {
				if (arguments[1] instanceof Array) {
					msg = arguments[0];
					images = arguments[1];
				} // Case 11 - {string, array}
				else {
					images = arguments[0];
					options = arguments[1];
				} // Case 10 - {array, object}
			} // Case 10, 11
		} // Case 8, 10, 11
		else if (arglen === 1) {
			images = arguments[0];
		} // Case 12 - {array}
		else {
			throw new TypeError('dialog.selectImage() - unable to parse arguments (argument count: ' + arglen + ')');
		}

		// Default values for params
		if (typeof yesCallback !== 'function') {
			yesCallback = void 0;
		}
		if (typeof noCallback !== 'function') {
			noCallback = void 0;
		}
		if (typeof msg !== 'string') {
			msg = DEFAULT_SELECT_IMAGE_MESSAGE;
		}
		if (images instanceof Array) {
			if (images.length < 2) {
				throw new TypeError('dialog.selectImage() - images must be an Array of 2 or more values.');
			}
		}
		else {
			throw new TypeError('dialog.selectImage() - images must be an Array.');
		}

		// Options
		if (typeof options !== 'object' || options === null) {
			options = {
				'yesButtonText': DEFAULT_SELECT_IMAGE_YES_BUTTON_TEXT,
				'noButtonText': DEFAULT_SELECT_IMAGE_NO_BUTTON_TEXT,
				'selectedIndex': 0
			}
		}
		if (typeof options['yesButtonText'] !== 'string') {
			options['yesButtonText'] = DEFAULT_SELECT_IMAGE_YES_BUTTON_TEXT;
		}
		if (typeof options['noButtonText'] !== 'string') {
			options['noButtonText'] = DEFAULT_SELECT_IMAGE_NO_BUTTON_TEXT;
		}
		if (typeof options['selectedIndex'] !== 'number' || !isFinite(options['selectedIndex']) || Math.floor(options['selectedIndex']) !== options['selectedIndex']) {
			options['selectedIndex'] = 0;
		}

		// Set texts
		dom_dialog_box.className = 'select-image';
		dom_dialog_box.innerHTML = '<div id="dialog-box-message">' + msg + '</div>';

		// Set text: image list	
		var dom_dialog_box_image_list = document.createElement('div');
		dom_dialog_box_image_list.id = 'dialog-box-image-list';
		dom_dialog_box_image_list.innerHTML = '';
		for (var i=0, len=images.length; i<len; i++) {
			if (typeof images[i] !== 'object') {
				continue;
			}

			dom_dialog_box_image_list.innerHTML += generateImageHTML(images[i], i);
		}
		dom_dialog_box.appendChild(dom_dialog_box_image_list);
		var dom_images = dom_dialog_box_image_list.querySelectorAll('.dialog-box-image');

		// Set text: yes button
		var dom_dialog_box_select_yes = document.createElement('button');
		dom_dialog_box_select_yes.id = 'dialog-box-images-yes-button';
		dom_dialog_box_select_yes.innerHTML = options['yesButtonText'];
		dom_dialog_box.appendChild(dom_dialog_box_select_yes);

		// Set text: no button
		var dom_dialog_box_select_no = document.createElement('button');
		dom_dialog_box_select_no.id = 'dialog-box-images-no-button';
		dom_dialog_box_select_no.innerHTML = options['noButtonText'];
		dom_dialog_box.appendChild(dom_dialog_box_select_no);

		// Display dialog box
		lockScroll();
		dom_dialog_overlay.classList.add('shown');
		dom_dialog_box.scrollTop = 0;

		dom_images[options['selectedIndex']].focus();
		dom_images[options['selectedIndex']].classList.add('selected');

		// Set up Promise, or callbacks if supplied as params
		if (typeof yesCallback === 'undefined') {
			return new Promise(setupHandlers);
		}
		else {
			return setupHandlers(yesCallback, noCallback);
		}

		/**
		 * @param: {object} image
		 *         {number} index
		 * @return: {string} html
		 */
		function generateImageHTML(img, i) {
			var dimensions_html = '';
			var selected_class = '';
			if (img.width && img.height) {
				dimensions_html = '<div class="dialog-box-image-dimensions">' + img.width + ' x ' + img.height + '</div>';
			}
			
			return ''+
			'<div class="dialog-box-image" data-index="' + i + '" tabindex="0">'+
				'<img src="' + img.src + '" alt="' + img.title + '">' + 
				'<div class="dialog-box-image-title">' + img.title + '</div>' +
				dimensions_html +
			'</div>';
		}
		/**
		 * @return {object | null}
		 */
		function getSelectedValue() {
			for (var i=0, len=dom_images.length; i<len; i++) {
				if (dom_images[i].classList.contains('selected')) {
					var index = parseInt(dom_images[i].getAttribute('data-index'));

					console.log(index);
					console.log(images);
					return images[index];
				}
			}

			console.log('No one selected_class');
			return null;
		}

		function setupHandlers(yesCallback, noCallback) {
			function imageClickHandler(e) {
				e.preventDefault();
				e.stopPropagation();

				for (var i=0, len=dom_images.length; i<len; i++) {
					if (dom_images[i] === e.currentTarget) {
						if (dom_images[i].classList.contains('selected')) {

							var index = parseInt(dom_images[i].getAttribute('data-index'));
							closeOverlay();
							yesCallback(images[index]);
							return;
						}
						dom_images[i].classList.add('selected');
					}
					else {
						dom_images[i].classList.remove('selected');
					}
				}
			} //imageClickHandler()

			function confirmYesHandler(e) {
				e.preventDefault();
				e.stopPropagation();
				
				closeOverlay();
				yesCallback(getSelectedValue());
			} //confirmYesHandler()
			
			function confirmNoHandler(e) {
				e.preventDefault();
				e.stopPropagation();
				
				closeOverlay();
				if (noCallback) {
					noCallback(null);
				}
			} //confirmNoHandler()
			
			function keydownHandler(e) {
				var key = (window.event) ? event.keyCode : e.keyCode;
				if (key === 27) { //ESC key
					e.preventDefault();
					
					closeOverlay();
					if (noCallback) {
						noCallback(null);
					}
				}
				if(key === 13) { // ENTER key
					for (var i=0, len=dom_images.length; i<len; i++) {
						if (dom_images[i] === e.target) {

							var value = dom_images[i].getAttribute('data-src');
							closeOverlay();
							yesCallback(value);
						}
					}
					
					return;
				}
				if (key === 32) { // SPACE key
					for (var i=0, len=dom_images.length; i<len; i++) {
						if (dom_images[i] === e.target) {
							if (dom_images[i].classList.contains('selected')) {
								var value = dom_images[i].getAttribute('data-src');
								closeOverlay();
								yesCallback(value);
								return;
							}
							dom_images[i].classList.add('selected');
						}
						else {
							dom_images[i].classList.remove('selected');
						}
					}
				}
			} //keydownHandler()
			
			function focusHandler(e){
				if (!isElementInDialog(e.target)) {
					dom_images[0].focus();
				}
			} //focusHandler()
			
			function closeOverlay() {
				// Clean up bindings
				for (var i=0, len=dom_images.length; i<len; i++) {
					dom_images[i].removeEventListener('click', imageClickHandler);
				}
				dom_dialog_close_button.removeEventListener('click', confirmNoHandler);
				dom_dialog_box_select_yes.removeEventListener('click', confirmYesHandler);
				dom_dialog_box_select_no.removeEventListener('click', confirmNoHandler);
				document.removeEventListener('keydown', keydownHandler);
				document.removeEventListener('focus', focusHandler, true);
				
				// Hide overlay
				dom_dialog_overlay.classList.remove('shown');
				unlockScroll();
			} //closeOverlay()
			
			// Bind image clicks
			for (var i=0, len=dom_images.length; i<len; i++) {
				dom_images[i].addEventListener('click', imageClickHandler);
			}

			// Listen for OK clicks, ESC, ENTER, SPACE key
			dom_dialog_close_button.addEventListener('click', confirmNoHandler);
			dom_dialog_box_select_yes.addEventListener('click', confirmYesHandler);
			dom_dialog_box_select_no.addEventListener('click', confirmNoHandler);
			document.addEventListener('keydown', keydownHandler);
			document.addEventListener('focus', focusHandler, true);
		} //setupHandlers()
	} //selectImage()
	
	/**
	 * @private
	 * Locks scroll position of body, and sets dialog overlay at the current position
	 */
	function lockScroll() {
		if (scrollLocked) {
			return false;
		}
		
		// Remember scroll position
		var scrollPositionX = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
		var scrollPositionY = window.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop;
		document.body.setAttribute('data-scroll-position-x', scrollPositionX);
		document.body.setAttribute('data-scroll-position-y', scrollPositionY);
		// DEBUG
		//console.log('Window pageYOffset: ' + window.pageYOffset + ', Document: ' + document.documentElement.scrollTop + ', Body:'+ document.body.scrollTop + ', Window ScrollY: ' +window.scrollY);
		
		// Don't let window scroll
		prevWindowOnScroll = window.onscroll;
		window.onscroll = function() {
			window.scrollTo(scrollPositionX, scrollPositionY);
		};
		
		// Set dialog overlay to appear at current scroll position
		dom_dialog_overlay.style.left = scrollPositionX+'px';
		dom_dialog_overlay.style.top = scrollPositionY+'px';
		
		// Remember overflow setting
		var currStyle = window.getComputedStyle(document.body, null);
		document.body.setAttribute('data-previous-overflow-x', currStyle.overflowX);
		document.body.setAttribute('data-previous-overflow-y', currStyle.overflowY);
		document.body.style.overflowX = 'hidden';
		document.body.style.overflowY = 'hidden';
		
		scrollLocked = true;
		return true;
	} //lockScroll()
	
	/**
	 * @private
	 * Unlocks scroll position of body
	 */
	function unlockScroll() {
		if (!scrollLocked) {
			return false;
		}
		
		// Recall scroll position
		var scrollPositionX = document.body.getAttribute('data-scroll-position-x');
		var scrollPositionY = document.body.getAttribute('data-scroll-position-y');
		window.scrollTo(scrollPositionX, scrollPositionY);
		window.onscroll = prevWindowOnScroll;
		
		// Set dialog overlay back to 0
		dom_dialog_overlay.style.left = '0';
		dom_dialog_overlay.style.top = '0';
		
		// Recall overflow setting
		var prevOverflowX = document.body.getAttribute('data-previous-overflow-x');
		var prevOverflowY = document.body.getAttribute('data-previous-overflow-y');
		document.body.style.overflowX = prevOverflowX;
		document.body.style.overflowY = prevOverflowY;
		
		scrollLocked = false;
		return true;
	} //unlockScroll()

	/**
	 * @private
	 * Checks if an element is inside the dialog overlay
	 * - used to check if we need to force-remove focus from this element to set focus back on the dialog (e.g. on OK button)
	 */	
	function isElementInDialog(elem) {
		while (elem !== null) {
			elem = elem.parentNode;
			if (elem === dom_dialog_overlay) {
				return true;
			}
		}
		
		return false;
	} //isElementInDialog()
	
	global.dialog = {
		'halt': halt,
		'alert': alert,
		'info': info,
		'confirm': confirm,
		'prompt': prompt,
		'select': select,
		'selectImage': selectImage
	};
})(this || {});
