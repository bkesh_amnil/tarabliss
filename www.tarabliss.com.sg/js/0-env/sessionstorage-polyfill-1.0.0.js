/**
 * Polyfill for sessionStorage, using window.name
 */
(function (window) {
	'use strict';
	
	// Test if we need this polyfill
	try {
		var sessionStorage = window.sessionStorage;
		var test, result;
		
		// Set, get and remove
		test = 'hello';
		sessionStorage.setItem(test, test);
		result = sessionStorage.getItem(test);
		sessionStorage.removeItem(test);
		
		// if we get correct result, we can assume sessionStorage support
		if (result === test) {
			return;
		}
	} // try
	catch (exception) {
		// Exception indicates buggy / non-existent sessionStorage
	}
	
	var data;
	var length = 0;
	
	/**
	 * @private
	 * Inits data and length variables
	 */
	function init() {
		try {
			data = JSON.parse(window.name);
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					length++;
				}
			} // go through all keys
		}
		catch (exception) {
			data = {};
			length = 0;
		}
	} //init()
	
	function getItem(key) {
		if (typeof data === 'undefined') {
			init();
		}
		
		if (typeof data[key] === 'undefined') {
			return null;
		}
		
		return data[key];
	} //getItem()
	
	function setItem(key, value) {
		if (typeof data === 'undefined') {
			init();
		}
		
		// Only add to length if it's a new key
		if (typeof data[key] === 'undefined') {
			length++;
		}
		
		// Add to data
		data[key] = value+''; // "casts" value to a string
		window.name = JSON.stringify(data);
	} //setItem()
	
	function removeItem(key) {
		if (typeof data === 'undefined') {
			init();
		}
		
		if (typeof data[key] === 'undefined') {
			return;
		}

		delete data[key];
		length--;
		window.name = JSON.stringify(data);
	} //removeItem()
	
	function clear() {
		data = {};
		length = 0;
		window.name = JSON.stringify(data);
	} //clear()
	
	function key(i) {
		if (typeof data === 'undefined') {
			init();
		}
		
		var counter = 0;
		for (var key in data) {
			if (data.hasOwnProperty(key)) {
				if (counter == i) return key;
				else counter++;
			}
		} // go through all keys
		return null;
	} //key()
	
	window['sessionStorage'] = {
		'getItem': getItem,
		'setItem': setItem,
		'removeItem': removeItem,
		'length': length,
		'clear': clear,
		'key': key
	}
})(window || {});
