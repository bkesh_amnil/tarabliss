/***
 * Frontend Web App Framework v1.5.0 - Page component
 * =================================
 *
 * This should be the only "global" component.
 * It monitors the page for DOM events on elements (even dynamically loaded ones), and overwrite behaviour:
 * - all links trigger AJAX requests rather than page refreshes
 * - TODO: context menus
 *
 * It also periodically checks the session user with backend, and ensures that pages requiring login will
 * only be shown to logged in users.
 *
 * Before use:
 * - edit settings
 ***/
this.app.controller.startComponent('page', '', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();

	// Settings
	var WAIT_BEFORE_ANNOY_PERIOD = 8000;
	
	// Messages

	// Context
	var has_shown_popup;

	/*****************
	 * Context Logic *
	 *****************/
	function asyncLoadHasShownPopUp() {
		if (typeof has_shown_popup !== 'undefined') {
			return Promise.resolve(has_shown_popup);
		}

		return sandbox.sessionDB.get('has-shown-popup').then(function (data) {
			if (data === null) {
				has_shown_popup = false;
			}
			else {
				has_shown_popup = data;
			}

			return has_shown_popup;
		});
	} // asyncLoadHasShownPopUp()

	function asyncSaveHasShownPopUp() {
		//console.log('DEBUG saving ' + has_shown_popup);
		return sandbox.sessionDB.set('has-shown-popup', has_shown_popup);
	} // asyncSaveHasShownPopUp()

	/******************
	 *  Module Logic  *
	 ******************/
	/*
	function asyncFetchTherapies() {
		return sandbox.userAPI.getTherapies().then(function(data) {
			sandbox.publish('therapies-loaded', data);
		});
	}
	function asyncFetchCategories() {
		return sandbox.userAPI.getCategories().then(function(data) {
			sandbox.publish('categories-loaded', data);
		});
	}
	function asyncFetchSubCategories() {
		return sandbox.userAPI.getSubCategories().then(function(data) {
			sandbox.publish('sub-categories-loaded', data);
		});
	}
	*/
	function startTimerToPopUp() {
		if (has_shown_popup) {
			//console.log('DEBUG has seen popup');
			return;
		}
		
		setTimeout(function() {
			sandbox.showOverlay('newsletter');
			has_shown_popup = true;
			return asyncSaveHasShownPopUp();
		}, WAIT_BEFORE_ANNOY_PERIOD);
	} //startTimerToPopUp()

	/*************
	 * Listeners *
	 *************/
	/*
	function onScroll() {
		var curr_scroll_y = dom_component.scrollTop;
		var velocity = curr_scroll_y - last_scroll_y;
		if (velocity > 0) {
			sandbox.publish('scroll-down', {scrollTop: curr_scroll_y, velocity: velocity});
		} // DOWN
		else if (velocity < 0) {
			sandbox.publish('scroll-up', {scrollTop: curr_scroll_y, velocity: velocity});
		} // UP
		
		last_scroll_y = curr_scroll_y;
	} //onScroll()
	*/

	function onClick(e) {
		var tgt = e.target;
		//console.log('DEBUG click ' + tgt.nodeName);
		//e.preventDefault();

		// Handle internal links
		var dom_a = tgt.closest('a');
		if (dom_a) {
			//console.log('DEBUG: anchor click: ' + dom_a.href);
			if (dom_a.href.startsWith(sandbox.settings.DOMAIN_URL) && dom_a.target === '') {
				e.preventDefault();
				sandbox.navigate(dom_a.href);
			}
		}
	} //onClick()

	/* TODO
	function onContextMenu() {

	} //onContextMenu()
	*/
	
	/***************
	 * Subscribers *
	 ***************/
	/*
	function onLogout() {
		sandbox.navigate(POST_LOGOUT_PAGE);
	}
	*/
	
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Listeners
		sandbox.on(dom_component, 'click', onClick);
		//sandbox.on(dom_component, 'scroll', onScroll);
		//sandbox.on(dom_component, 'contextmenu', onContextMenu);

		// Subscribers
		//sandbox.subscribe('member-logged-out', onLogout);
		
		// Init
		asyncLoadHasShownPopUp()
		.then(startTimerToPopUp);
	}

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	}
	
	return {
		start: start,
		stop: stop
	};
});
