this.app.controller.startComponent('site-header', '', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_nav_links = sandbox.findAll('a');
	var dom_reservations_link = sandbox.findOne('#header-reservations-link');
	
	// Context
	var curr_page;
	
	/*****************
	 * Context Logic *
	 *****************/
	 
	/*************
	 * DOM Logic *
	 *************/
	// Sets nav-link for current page to "current" class
	function activateNavLinks() {
		for (var i=0; i<dom_nav_links.length; i++) {
			var dom_nav_link = dom_nav_links[i];
			
			// Activate or de-activate it accordingly
			if (dom_nav_link.href === sandbox.settings.DOMAIN_URL + '/' + curr_page) {
				dom_nav_link.classList.add('current');
			}
			else {
				dom_nav_link.classList.remove('current');
			}
		} // for each link
	} //activateNavLinks()

	/****************
	 * Module Logic *
	 ****************/
	
	/*************
	 * Listeners *
	 *************/
	function onReservationsLinkClick(e) {
		e.preventDefault();

		var mail_link = e.currentTarget.href;
		sandbox.dialog.confirmTemplate(sandbox.settings.DOMAIN_URL + '/html/templates/dnc.html').then(function() {
			window.location = mail_link;
		});
	}
	
	/***************
	 * Subscribers *
	 ***************/
	function onPageSwitch(pg) {
		curr_page = pg;
		activateNavLinks();
	}
	
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Listeners
		sandbox.on(dom_reservations_link, 'click', onReservationsLinkClick);

		// Subscribers
		sandbox.subscribe('page-switched', onPageSwitch);
		
		// Init
		curr_page = sandbox.getCurrentPage();
		activateNavLinks();
	}
	// Called by controller to stop
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	}
	
	return {
		start: start,
		stop: stop
	};
});
