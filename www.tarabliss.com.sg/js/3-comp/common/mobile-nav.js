﻿this.app.controller.startComponent('mobile-nav', '', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_nav_links = sandbox.findAll('a');
	//var dom_mobile_nav_icon = sandbox.findOne('#mobile-nav-icon');
	
	// Context
	var curr_page;
	
	/*****************
	 * Context Logic *
	 *****************/

	/*************
	 * DOM Logic *
	 *************/
	// Sets nav-link for current page to "current" class
	function activateNavLinks() {
		for (var i=0; i<dom_nav_links.length; i++) {
			var dom_nav_link = dom_nav_links[i];
			
			// Activate or de-activate it accordingly
			if (dom_nav_link.href === sandbox.settings.DOMAIN_URL + '/' + curr_page) {
				dom_nav_link.classList.add('current');
			}
			else {
				dom_nav_link.classList.remove('current');
			}
		} // for each link
	} //activateNavLinks()

	function hide() {
		dom_component.classList.remove('shown');
	}
	function show() {
		dom_component.classList.add('shown');
	}
	function toggle(){
		dom_component.classList.toggle('shown');
	}
	 
	/****************
	 * Module Logic *
	 ****************/
	
	/*************
	 * Listeners *
	 *************/
	function onSwipeLeft() {
		hide();
	}
	function onSwipeRight() {
		show();
	}
	function onTap() {
		toggle();
	}

	/***************
	 * Subscribers *
	 ***************/
	function onPageSwitch(pg) {
		hide();
		curr_page = pg;
		activateNavLinks();
	}
	
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Listeners
		sandbox.on(dom_component, 'swipeleft', onSwipeLeft);
		sandbox.on(dom_component, 'swiperight', onSwipeRight);
		sandbox.on(dom_component, 'tap', onTap);
		
		// Subscribers
		sandbox.subscribe('page-switched', onPageSwitch);
		
		// Init
		sandbox.utils.detect(dom_component, 'swipe tap');
		curr_page = sandbox.getCurrentPage();
		activateNavLinks();
	}
	// Called by controller to stop
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	}
	
	return {
		start: start,
		stop: stop
	};
});
