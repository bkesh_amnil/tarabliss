this.app.controller.startComponent('therapy-links', '', function(sandbox) {
	'use strict';

	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_therapy_links;

	// Context
	var therapy_template;
	var categories;
	var has_init;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchTemplates() {
		var promises = [];
		promises.push(sandbox.utils.ajaxLoad('/html/templates/therapy-link.html').then(function(data) {
			therapy_template = data;
		}));

		return Promise.all(promises);
	}
/*
	function asyncLoadCategories() {
		if (typeof categories !== 'undefined') {
			return Promise.resolve(categories);
		}

		return sandbox.sessionDB.get('categories').then(function (data) {
			if (data === null) {
				throw new Error('No categories in sessionDB');
			}

			categories = data;
			return categories;
		});
	} //asyncLoadCategories()
*/
	function asyncFetchCategories() {
		return sandbox.userAPI.getFeaturedCategories().then(function(data) {
			categories = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'})
			return categories;
		});
	}

	/*************
	 * DOM Logic *
	 *************/
	function populateLinks() {
		var i, len; // loop vars
		var html = '';

		// Generate HTML
		for (i=0,len=categories.length; i<len; i++) {
			html += sandbox.utils.renderTemplate(therapy_template, {
				'id': categories[i]['id'],
				'name': categories[i]['name']
			}).trim();
		}

		dom_component.innerHTML = '<h3>Our Therapies</h3>' + html;

		// Dynamic DOM elements
		dom_therapy_links = sandbox.findAll('a');

		// Listeners
		sandbox.on(dom_therapy_links, 'click', onTherapyLinkClick);
	} //populateLinks()
	
	/****************
	 * Module Logic *
	 ****************/
	function asyncInit() {
		if (has_init) {
			return Promise.resolve();
		}

		return asyncFetchTemplates()
		.then(asyncFetchCategories)
		.then(function() {
			populateLinks();			

			has_init = true;
		}).catch(function(e){
			console.log(e);
		});
	}
	 
	/*************
	 * Listeners *
	 *************/
	function onTherapyLinkClick(e) {
		e.preventDefault();

		var href = e.currentTarget.href;
		var selector_data = {
			'category-id': e.currentTarget.getAttribute('data-category-id'),
			'sub-category-id': null
		};
		return sandbox.sessionDB.set('therapy-selector-data', selector_data).then(function() {
			sandbox.publish('therapy-selector-data-changed', selector_data);
			return sandbox.navigate(href);
		});
	}

	/***************
	 * Subscribers *
	 ***************/
	function onCategoriesLoaded() {
		asyncInit();
	}
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('categories-loaded', onCategoriesLoaded);

		asyncInit();
	} //start()

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});

this.app.controller.startComponent('quick-links', '', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_reservation_link = sandbox.findOne('#reservation-quicklink');

	// Context
	
	/*****************
	 * Context Logic *
	 *****************/

	/*************
	 * DOM Logic *
	 *************/
	
	/****************
	 * Module Logic *
	 ****************/
	 
	/*************
	 * Listeners *
	 *************/
	function onReservationLinkClick(e) {
		e.preventDefault();

		var mail_link = e.currentTarget.href;
		sandbox.dialog.confirmTemplate(sandbox.settings.DOMAIN_URL + '/html/templates/dnc.html', 'I Agree', 'Cancel').then(function() {
			window.location = mail_link;
		});
	}
	
	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Listeners
		sandbox.on(dom_reservation_link, 'click', onReservationLinkClick);

	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});

this.app.controller.startComponent('contact-form', '', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_output = sandbox.findOne('output:last-of-type');

	// Messages
	var INVALID_EMAIL_MESSAGE = 'Invalid email.';
	var INVALID_MOBILE_MESSAGE = 'Invalid mobile.';
	var INVALID_MESSAGE_MESSAGE = 'Invalid message.';
	var WAIT_MESSAGE = 'Please wait...';
	var SUCCESS_MESSAGE = 'Thank you for your message.';
	var DISAGREE_DNC_MESSAGE = 'Your message was not sent.';

	// Settings
	var OUTPUT_SHOW_DURATION = 10000;
	var MIN_CONTACT_MESSAGE_LENGTH = 10; // ensure same as server-side

	// Context
	var output_timer;
	
	/*****************
	 * Context Logic *
	 *****************/

	/*************
	 * DOM Logic *
	 *************/
	function outputMessage(msg) {
		dom_output.innerHTML = msg;
		dom_output.classList.remove('error');
		dom_output.classList.add('shown');

		clearTimeout(output_timer);
		output_timer = setTimeout(clearOutput, OUTPUT_SHOW_DURATION);
	} //outputMessage()

	function outputError(msg) {
		dom_output.innerHTML = msg;
		dom_output.classList.add('error');
		dom_output.classList.add('shown');

		clearTimeout(output_timer);
		output_timer = setTimeout(clearOutput, OUTPUT_SHOW_DURATION);
	} //outputMessage()

	function clearOutput() {
		dom_output.classList.remove('error');
		dom_output.classList.remove('shown');
		dom_output.innerHTML = '';
	}

	function clearForm() {
		dom_component.reset();
	}

	function disableForm() {
		sandbox.utils.disableForm(dom_component);
	}

	function enableForm() {
		sandbox.utils.enableForm(dom_component);
	}

	function isSubmitting() {
		return sandbox.utils.isDisabledForm(dom_component);
	}

	/****************
	 * Module Logic *
	 ****************/
	/**
	 * Outputs error message
	 * @return: {bool}
	 */
	function validateForm() {
		var email = dom_component['email'].value;
		if (!sandbox.utils.isEmail(email)) {
			outputError(INVALID_EMAIL_MESSAGE);
			return false;
		}

		var mobile = dom_component['mobile'].value;
		if (!sandbox.utils.isPhone(mobile) || (!mobile.startsWith('9') && !mobile.startsWith('8'))) {
			outputError(INVALID_MOBILE_MESSAGE);
			return false;
		}

		var message = dom_component['message'].value;
		if (message.trim().length < MIN_CONTACT_MESSAGE_LENGTH) {
			outputError(INVALID_MESSAGE_MESSAGE);
			return false;
		}

		return true;
	} //validateForm()
	 
	/*************
	 * Listeners *
	 *************/
	function onFormSubmit(e) {
		e.preventDefault();

		if (isSubmitting()) {
			return false;
		}

		if (!validateForm()) {
			return false;
		}

		sandbox.dialog.confirmTemplate(sandbox.settings.DOMAIN_URL + '/html/templates/dnc.html', 'I Agree', 'Cancel').then(function() {
			disableForm();
			outputMessage(WAIT_MESSAGE);

			var form_data = new FormData(dom_component);
			return sandbox.userAPI.sendContactMessage(form_data);
		}, function() {
			throw DISAGREE_DNC_MESSAGE;
		}).then(function() {
			clearForm();
			enableForm();
			outputMessage(SUCCESS_MESSAGE);
		}, function(error_msg) {
			enableForm();
			outputError(error_msg);
		});
	} //onFormSubmit()

	/***************
	 * Subscribers *
	 ***************/
	
	
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Listeners
		sandbox.on(dom_component, 'submit', onFormSubmit);
	}
	// Called by controller to stop
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	}
	
	return {
		start: start,
		stop: stop
	};
});

this.app.controller.startComponent('newsletter-form', '', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_output = sandbox.findOne('output:last-of-type');
	
	// Messages
	var INVALID_EMAIL_MESSAGE = 'Invalid email.';
	var WAIT_MESSAGE = 'Please wait...';
	var SUCCESS_MESSAGE = 'Thank you for subscribing.';
	
	// Settings
	var OUTPUT_SHOW_DURATION = 10000;

	// Context
	var output_timer;
	
	/*****************
	 * Context Logic *
	 *****************/

	/*************
	 * DOM Logic *
	 *************/
	function outputMessage(msg) {
		dom_output.innerHTML = msg;
		dom_output.classList.remove('error');
		dom_output.classList.add('shown');

		clearTimeout(output_timer);
		output_timer = setTimeout(clearOutput, OUTPUT_SHOW_DURATION);
	} //outputMessage()

	function outputError(msg) {
		dom_output.innerHTML = msg;
		dom_output.classList.add('error');
		dom_output.classList.add('shown');

		clearTimeout(output_timer);
		output_timer = setTimeout(clearOutput, OUTPUT_SHOW_DURATION);
	} //outputMessage()

	function clearOutput() {
		dom_output.classList.remove('error');
		dom_output.classList.remove('shown');
		dom_output.innerHTML = '';
	}

	function clearForm() {
		dom_component.reset();
	}

	function disableForm() {
		sandbox.utils.disableForm(dom_component);
	}

	function enableForm() {
		sandbox.utils.enableForm(dom_component);
	}

	function isSubmitting() {
		return sandbox.utils.isDisabledForm(dom_component);
	}
	 
	/****************
	 * Module Logic *
	 ****************/
	/**
	 * Outputs error message
	 * @return: {bool}
	 */
	function validateForm() {
		var email = dom_component['email'].value;
		if (!sandbox.utils.isEmail(email)) {
			outputError(INVALID_EMAIL_MESSAGE);
			return false;
		}

		return true;
	} //validateForm()
	 
	/*************
	 * Listeners *
	 *************/
	function onFormSubmit(e) {
		e.preventDefault();

		if (isSubmitting()) {
			return false;
		}
		
		if (!validateForm()) {
			return false;
		}

		disableForm();
		outputMessage(WAIT_MESSAGE);

		var form_data = new FormData(dom_component);
		sandbox.userAPI.subscribeMailList(form_data).then(function() {
			clearForm();
			enableForm();

			outputMessage(SUCCESS_MESSAGE);
		}, function(error_msg) {
			enableForm();
			outputError(error_msg);
		});
	} //onFormSubmit()

	/***************
	 * Subscribers *
	 ***************/
	
	
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Listeners
		sandbox.on(dom_component, 'submit', onFormSubmit);
	}
	// Called by controller to stop
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	}
	
	return {
		start: start,
		stop: stop
	};
});
