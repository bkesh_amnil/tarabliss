this.app.controller.startComponent('selector', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_links = sandbox.findAll('a');

	// Context
	
	/*****************
	 * Context Logic *
	 *****************/

	/*************
	 * DOM Logic *
	 *************/
	function activateLinks() {
		var url = sandbox.settings.DOMAIN_URL + '/' + sandbox.getCurrentPage();
		for (var i=0,len=dom_links.length; i<len; i++) {
			if (dom_links[i].href === url) {
				dom_links[i].classList.add('current');
			}
			else {
				dom_links[i].classList.remove('current');
			}
		}
	} //activateLinks()

	/****************
	 * Module Logic *
	 ****************/
	 
	 /*************
	 * Listeners *
	 *************/
	
	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		activateLinks();
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});

this.app.controller.startComponent('selected-content', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_contact_links;

	// Context
	var job_listing_template;
	var job_listings_template;
	var trait_template;
	var job_listings;
	var has_loaded_context = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchTemplates() {
		var promises = [];
		promises.push(sandbox.utils.ajaxLoad('/html/templates/careers/job-listing.html').then(function(data) {
			job_listing_template = data;
		}));
		promises.push(sandbox.utils.ajaxLoad('/html/templates/careers/job-listings.html').then(function(data) {
			job_listings_template = data;
		}));
		promises.push(sandbox.utils.ajaxLoad('/html/templates/careers/trait.html').then(function(data) {
			trait_template = data;
		}));

		return Promise.all(promises);
	}

	function asyncFetchJobListings() {
		return sandbox.userAPI.getFeaturedJobListings().then(function(data) {
			job_listings = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'})
			return job_listings;
		});
	}

	/*************
	 * DOM Logic *
	 *************/
	function populateJobListings() {
		if (!has_loaded_context) {
			throw new Error('populateJobListings() - cannot proceed before context loaded');
		}

		var i,len; // loop vars
		var j,lenj;

		// Generate HTML
		var html = '';
		for (i=0,len=job_listings.length; i<len; i++) {
			// Generate traits HTML
			var traits = sandbox.utils.csv2array(job_listings[i]['traits']);
			var traits_html = '';
			for (j=0,lenj=traits.length; j<lenj; j++) {
				traits_html += sandbox.utils.renderTemplate(trait_template, {
					trait: traits[j]
				});
			}

			html += sandbox.utils.renderTemplate(job_listing_template, {
				'title': job_listings[i]['title'],
				'responsibilities': job_listings[i]['responsibilities'],
				'traits-html': traits_html,
				'href': 'mailto:careers@tarabliss.com.sg?Subject=' + sandbox.utils.encodeURL(job_listings[i]['title'])
			}).trim();
		}
		dom_component.innerHTML = sandbox.utils.renderTemplate(job_listings_template, {
			'job-listings-html': html
		}).trim();

		// Dynamic elements
		dom_contact_links = sandbox.findAll('.job-listing > a');

		// Listeners
		sandbox.on(dom_contact_links, 'click', onContactLinkClick);

	} //populateJobListings()
	
	/****************
	 * Module Logic *
	 ****************/
	 
	/*************
	 * Listeners *
	 *************/
	function onContactLinkClick(e) {
		e.preventDefault();
		var mail_link = e.currentTarget.href;

		sandbox.dialog.confirmTemplate(sandbox.settings.DOMAIN_URL + '/html/templates/dnc.html', 'I Agree', 'Cancel').then(function() {
			window.location = mail_link;
		});
	}
	
	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		return asyncFetchTemplates()
		.then(asyncFetchJobListings)
		.then(function() {
			has_loaded_context = true;

			populateJobListings();
		}).catch(function(e){
			console.log(e);
		});
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});
