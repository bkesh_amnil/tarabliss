this.app.controller.startComponent('selector', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_links = sandbox.findAll('a');

	// Context
	
	/*****************
	 * Context Logic *
	 *****************/

	/*************
	 * DOM Logic *
	 *************/
	function activateLinks() {
		var url = sandbox.settings.DOMAIN_URL + '/' + sandbox.getCurrentPage();
		for (var i=0,len=dom_links.length; i<len; i++) {
			if (dom_links[i].href.startsWith(url)) {
				dom_links[i].classList.add('current');
			}
			else {
				dom_links[i].classList.remove('current');
			}
		}
	} //activateLinks()

	/****************
	 * Module Logic *
	 ****************/
	 
	 /*************
	 * Listeners *
	 *************/
	
	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		activateLinks();
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});

this.app.controller.startComponent('selected-content', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	
	// Context
	var accolades_template;
	var accolade_template;
	var accolades;
	var has_loaded_context = false;

	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchTemplates() {
		var promises = [];
		promises.push(sandbox.utils.ajaxLoad('/html/templates/accolades/accolade.html').then(function(data) {
			accolade_template = data;
		}));
		promises.push(sandbox.utils.ajaxLoad('/html/templates/accolades/accolades.html').then(function(data) {
			accolades_template = data;
		}));

		return Promise.all(promises);
	}

	function asyncFetchAccolades() {
		return sandbox.userAPI.getFeaturedAccolades().then(function(data) {
			accolades = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'})
			return accolades;
		});
	}

	/*************
	 * DOM Logic *
	 *************/
	function populateAccolades() {
		if (!has_loaded_context) {
			throw new Error('populateAccolades() - cannot proceed before context loaded');
		}

		var i,len; // loop vars

		// Generate HTML
		var html = '';
		for (i=0,len=accolades.length; i<len; i++) {
			html += sandbox.utils.renderTemplate(accolade_template, {
				'title': accolades[i]['title'],
				'description': accolades[i]['description'],
				'img-file': '/img/accolades/' + accolades[i]['id'] + '/thumbnails/main.jpg',
				'img-alt': accolades[i]['title'],
				'href': '/doc/accolades/' + accolades[i]['id'] + '/' + sandbox.utils.str2fileName(accolades[i]['title']) + '.pdf'
			}).trim();
		}

		dom_component.innerHTML = sandbox.utils.renderTemplate(accolades_template, {
			'accolades-html': html
		}).trim();
	} //populateAccolades()
	
	/****************
	 * Module Logic *
	 ****************/
	 
	/*************
	 * Listeners *
	 *************/
	
	
	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		return asyncFetchTemplates()
		.then(asyncFetchAccolades)
		.then(function() {
			has_loaded_context = true;

			populateAccolades();
		}).catch(function(e){
			console.log(e);
		});
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});
