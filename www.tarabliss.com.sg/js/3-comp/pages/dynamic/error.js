this.app.controller.startComponent('error-message', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();

	// Context
	
	/*****************
	 * Context Logic *
	 *****************/

	/*************
	 * DOM Logic *
	 *************/
	 
	/****************
	 * Module Logic *
	 ****************/
	 
	 /*************
	 * Listeners *
	 *************/
	
	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});