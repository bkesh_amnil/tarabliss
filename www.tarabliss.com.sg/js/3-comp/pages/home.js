this.app.controller.startComponent('therapy-categories', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_button_links;

	// Context
	var category_template;
	var categories;
	var has_loaded_context = false;
	var has_init = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchTemplates() {
		var promises = [];
		promises.push(sandbox.utils.ajaxLoad('/html/templates/home/category.html').then(function(data) {
			category_template = data;
		}));

		return Promise.all(promises);
	}

/*
	function asyncLoadCategories() {
		if (typeof categories !== 'undefined') {
			return Promise.resolve(categories);
		}

		return sandbox.sessionDB.get('categories').then(function (data) {
			if (data === null) {
				throw new Error('No categories in sessionDB');
			}

			categories = sandbox.utils.sortArrayBy('displayPriority', data, {
				primer: parseInt,
				order:'desc'
			});
			return categories;
		});
	} //asyncLoadCategories()
*/
	function asyncFetchCategories() {
		return sandbox.userAPI.getFeaturedCategories().then(function(data) {
			categories = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'})
			return categories;
		});
	}

	/*************
	 * DOM Logic *
	 *************/
	function populateCategories() {
		if (!has_loaded_context) {
			throw new Error('populateCategories() - cannot proceed before context loaded');
		}

		var i,len; // loop vars

		// Generate HTML
		var html = '';
		for (i=0,len=categories.length; i<len; i++) {
			html += sandbox.utils.renderTemplate(category_template, {
				'id': categories[i]['id'],
				'name': categories[i]['name']
			}).trim();
		}

		dom_component.innerHTML = html;
		

		// Dynamic DOM elements
		dom_button_links = sandbox.findAll('.button-link');

		// Bind listeners
		sandbox.on(dom_button_links, 'click', onButtonLinkClick);
	} //populateCategories()
	 
	/****************
	 * Module Logic *
	 ****************/
	function asyncInit() {
		if (has_init) {
			return Promise.resolve();
		}

		return asyncFetchTemplates()
		.then(asyncFetchCategories)
		.then(function() {
			has_loaded_context = true;

			populateCategories();

			has_init = true;
		}).catch(function(e){
			console.log(e);
		});
	} //asyncInit()
	 
	/*************
	 * Listeners *
	 *************/
	function onButtonLinkClick(e) {
		e.preventDefault();

		var href = e.currentTarget.href;
		var selector_data = {
			'category-id': e.currentTarget.getAttribute('data-category-id'),
			'sub-category-id': null
		};
		return sandbox.sessionDB.set('therapy-selector-data', selector_data).then(function() {
			sandbox.publish('therapy-selector-data-changed', selector_data);
			sandbox.navigate(href);
		});
	}
	
	/***************
	 * Subscribers *
	 ***************/
	function onCategoriesLoaded() {
		if (has_loaded_context) {
			return;
		}

		asyncInit();
	}
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('categories-loaded', onCategoriesLoaded);

		asyncInit();
	} //start()

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});



  $(document).ready(function(){
      $('#banner-info p').html(' Welcome to TaraBliss – An oasis of tranquility in the heart of the city,<br>the new TaraBliss Spa is designed to soothe  all the stresses of urban living. Step<br> into this  meditative space of calm and discover dramatic skin transformation.');
    
});
