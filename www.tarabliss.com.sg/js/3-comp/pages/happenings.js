var owl;
//this.app.controller.startComponent('carousel-activity', function(sandbox) {
//    function start() {
//        alert("hi");
//        $("#adtext").text("OLA");
//    }
//    
//    function stop() {
//        
//    }
//    
//    return {
//        start: start,
//        stop: stop
//    }
//    
//})



this.app.controller.startComponent('carousel-activity', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_button_links;

	// Context
	var events_template;
	var event_template;
	var eventGalleries;
	var events;
	var has_loaded_context = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchTemplates() {
		var promises = [];
		promises.push(sandbox.utils.ajaxLoad('/html/templates/events/event.html').then(function(data) {
			event_template = data;
		}));
		promises.push(sandbox.utils.ajaxLoad('/html/templates/events/events.html').then(function(data) {
			events_template = data;
		}));

		return Promise.all(promises);
	}

	function asyncFetchGalleries() {
		return sandbox.userAPI.getEventGalleries().then(function(data) {
			eventGalleries = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'asc'});
			return eventGalleries;
		});
	}
	function asyncFetchEvents() {
		return sandbox.userAPI.getEvents(eventGalleries[0]['id']).then(function(data) {
			events = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'});
			return events;
		});
	}

	/*************
	 * DOM Logic *
	 *************/
	function populateEvents() {
		if (!has_loaded_context) {
			throw new Error('populateEvents() - cannot proceed before context loaded');
		}

		var i,len; // loop vars

		// Find therapies on promo
//		var current_events = events.filter(function(v) {
//			return sandbox.utils.isOnPromotion(v);
//		});

		// Generate HTML
		var html = '';
		for (i=0,len=events.length; i<len; i++) {
			// Find category ID of this therapy
//			var current_event_gallery = eventGalleries.find(function(v) {
//				return v['id'] === events[i]['id'];
//			});
//			var current_event_gallery = eventGalleries[i]['id'];

			html += sandbox.utils.renderTemplate(event_template, {
				'title': events[i]['name'],
				'img-file': '/img/events/' + events[i]['id'] + '.png',
				'img-alt': events[i]['title'],
				'description': events[i]['description'],
				'event-date': sandbox.utils.formatDate('d M Y',events[i]['eventTime'])
			}).trim();
		}

		dom_component.innerHTML = sandbox.utils.renderTemplate(events_template, {
			'events-html': html
		}).trim();
                //#bkesh
                var eventGalleryList = '';
                var len = 8;
                if(eventGalleries.length<8){
                    len =eventGalleries.length;
                }
                for (i=0; i<len; i++) {
			eventGalleryList += '<button data-sub-category-id="'+eventGalleries[i]['id']+'" class="sub-category-button">'+eventGalleries[i]['name']+'</button>';
		}
                $('.category-button').html(eventGalleries[0]['name']);
                $('.sub-category-menu').html(eventGalleryList);
                
                
                owl = $('#carousel-activity').owlCarousel({
                       items: 1,
                       loop: true,
                       nav: true
                   });
                   
		// Dynamic DOM elements
//		dom_button_links = sandbox.findAll('.button-link');

		// Bind Listeners
//		sandbox.on(dom_button_links, 'click', onButtonLinkClick);
	} //populatePromotions()
	 
	/****************
	 * Module Logic *
	 ****************/
	/*************
	 * Listeners *
	 *************/
	function onButtonLinkClick(e) {
		e.preventDefault();

		var href = e.currentTarget.href;

		var selector_data = {
			'category-id': e.currentTarget.getAttribute('data-category-id'),
			'sub-category-id': e.currentTarget.getAttribute('data-sub-category-id')
		};
		sandbox.sessionDB.set('therapy-selector-data', selector_data).then(function() {
			sandbox.publish('therapy-selector-data-changed', selector_data);
			sandbox.navigate(href);
		});
	}
	
	/***************
	 * Subscribers *
	 ***************/
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		return asyncFetchTemplates()
		.then(asyncFetchGalleries)
		.then(asyncFetchEvents)
		.then(function() {
			has_loaded_context = true;

			populateEvents();
		}).catch(function(e){
			console.log(e);
		});
	} //start()

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});



  $(document).ready(function(){
//      var server_url = 'http://tara.api.dev/api';
      var server_url = app.settings.SERVER_URL;
//      var server_url = 'http://staging.www.tarabliss.com.sg/api';
      var API_URL_PREFIX = server_url + '/user';
      //#banner slogan
    $('#banner-info p').html('Beyond its skin-transformative face treatments and luxurious body pampering,<br>TaraBliss Spa is also a space dedicated to inner wellbeing.');
    $('.category-button').on('click', function() {
        $('.sub-category-menu').toggleClass('shown');
    });
    
    //#event gallery change load function
    $('body').on('click','.sub-category-button', function() {
        var i,len; // loop vars
        var galleryName = $(this).text();
        var galleryID = $(this).attr('data-sub-category-id');
        $('.category-button').html(galleryName);
        $.ajax({
            type: "GET",
            url: API_URL_PREFIX+'/getEvents',
            data: 'eventGalleryID=' + galleryID,
            dataType:'json',
            success: function(data) {
                console.log(app);
                var events=data['events'];
                var event_html = '';
                for (i=0,len=events.length; i<len; i++) {
                    var eventsData = $('#carousel-activity');
                    var img_file = '/img/events/' + events[i]['id'] + '.png';
                    event_html += '<div class="activity">    <img src="'+ img_file+'" alt="'+events[i]['name'] +'" />    <article>        <h2>'+events[i]['name'] +'</h2>        <span>'+ app.extensions.utils.formatDate('d M Y',events[i]['eventTime'])  +'</span>        <p>'+events[i]['description'] +'</p>    </article></div>';
                    
                    
		}
                eventsData.html(event_html);

    
//                $('#carousel-activity').owlCarousel();
//                $('#carousel-activity').data('owlCarousel').reinit();
                owl.trigger('next.owl.carousel');
                
                owl.trigger('destroy.owl.carousel');
                // After destory, the markup is still not the same with the initial.
                // The differences are:
                //   1. The initial content was wrapped by a 'div.owl-stage-outer';
                //   2. The '.owl-carousel' itself has an '.owl-loaded' class attached;
                //   We have to remove that before the new initialization.
                owl.html(owl.find('.owl-stage-outer').html()).removeClass('owl-loaded');
                owl.owlCarousel({
                       items: 1,
                       loop: true,
                       nav: true
                });

            }
        });

    });
    
    //# hide event list function
       $('body').mouseup(function(e) {
        var container = $(".happening-event-filter");
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            $('.event-list').removeClass('shown');
        }
    });
});


//#bkesh not down


//
//this.app.controller.startComponent('articles', function(sandbox) {
//	'use strict';
//	
//	// DOM elements
//	var dom_component = sandbox.getComponentElement();
//
//	// Context
//	var articles_template;
//	var article_template;
//	var articles;
//	var has_loaded_context = false;
//	
//	/*****************
//	 * Context Logic *
//	 *****************/
//	function asyncFetchTemplates() {
//		var promises = [];
//		promises.push(sandbox.utils.ajaxLoad('/html/templates/whats-new/articles.html').then(function(data) {
//			articles_template = data;
//		}));
//		promises.push(sandbox.utils.ajaxLoad('/html/templates/whats-new/article.html').then(function(data) {
//			article_template = data;
//		}));
//
//		return Promise.all(promises);
//	}
//	function asyncFetchArticles() {
//		return sandbox.userAPI.getFeaturedArticles().then(function(data) {
//			articles = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'});
//			return articles;
//		});
//	}
//
//	/*************
//	 * DOM Logic *
//	 *************/
//	function populateArticles() {
//		if (!has_loaded_context) {
//			throw new Error('populateArticles() - cannot proceed before context loaded');
//		}
//
//		var i,len; // loop vars
//
//		// Generate HTML
//		var html = '';
//		for (i=0,len=articles.length; i<len; i++) {
//			html += sandbox.utils.renderTemplate(article_template, {
//				'title': articles[i]['title'],
//				'excerpt': articles[i]['excerpt'] + '...',
//				'img-file': '/img/articles/' + articles[i]['id'] + '/thumbnails/main.jpg',
//				'img-alt': articles[i]['title'],
//				'href': articles[i]['url']
//			}).trim();
//		}
//
//		dom_component.innerHTML = sandbox.utils.renderTemplate(articles_template, {
//			'articles-html': html
//		}).trim();
//	} //populateArticles()
//	 
//	/****************
//	 * Module Logic *
//	 ****************/
//	
//	 /*************
//	 * Listeners *
//	 *************/
//	
//	/***************
//	 * Subscribers *
//	 ***************/
//	 
//	/**************
//	 * Life Cycle *
//	 **************/
//	function start() {
//		return asyncFetchTemplates()
//		.then(asyncFetchArticles)
//		.then(function() {
//			has_loaded_context = true;
//
//			populateArticles();
//		}).catch(function(e){
//			console.log(e);
//		});
//	} //start()
//	function stop() {
//		sandbox.off();
//		sandbox.unsubscribe();
//	} // stop()
//	
//	return {
//		start: start,
//		stop: stop
//	};
//});
//
//this.app.controller.startComponent('promotions', function(sandbox) {
//	'use strict';
//	
//	// DOM elements
//	var dom_component = sandbox.getComponentElement();
//	var dom_button_links;
//
//	// Context
//	var promotions_template;
//	var promotion_template;
//	var sub_categories;
//	var therapies;
//	var has_loaded_context = false;
//	
//	/*****************
//	 * Context Logic *
//	 *****************/
//	function asyncFetchTemplates() {
//		var promises = [];
//		promises.push(sandbox.utils.ajaxLoad('/html/templates/promotions/promotion.html').then(function(data) {
//			promotion_template = data;
//		}));
//		promises.push(sandbox.utils.ajaxLoad('/html/templates/promotions/promotions.html').then(function(data) {
//			promotions_template = data;
//		}));
//
//		return Promise.all(promises);
//	}
//
//	function asyncFetchSubCategories() {
//		return sandbox.userAPI.getFeaturedSubCategories().then(function(data) {
//			sub_categories = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'});
//			return sub_categories;
//		});
//	}
//	function asyncFetchTherapies() {
//		return sandbox.userAPI.getFeaturedTherapies().then(function(data) {
//			therapies = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'});
//			return therapies;
//		});
//	}
//
//	/*************
//	 * DOM Logic *
//	 *************/
//	function populatePromotions() {
//		if (!has_loaded_context) {
//			throw new Error('populatePromotions() - cannot proceed before context loaded');
//		}
//
//		var i,len; // loop vars
//
//		// Find therapies on promo
//		var current_therapies = therapies.filter(function(v) {
//			return sandbox.utils.isOnPromotion(v);
//		});
//
//		// Generate HTML
//		var html = '';
//		for (i=0,len=current_therapies.length; i<len; i++) {
//			// Find category ID of this therapy
//			var current_sub_category = sub_categories.find(function(v) {
//				return v['id'] === current_therapies[i]['subCategoryID'];
//			});
//			var category_id = current_sub_category['categoryID'];
//
//			html += sandbox.utils.renderTemplate(promotion_template, {
//				'title': current_therapies[i]['name'],
//				'category-id': category_id,
//				'sub-category-id': current_therapies[i]['subCategoryID'],
//				'img-file': '/img/therapies/' + current_therapies[i]['id'] + '/thumbnails/main.jpg',
//				'img-alt': current_therapies[i]['title'],
//				'duration': current_therapies[i]['duration'] + ' Minutes',
//				'promotion-terms': current_therapies[i]['promotionTerms'],
//				'expiry-date': sandbox.utils.formatDate('d M Y',current_therapies[i]['promotionEndTime']),
//				'promotion-price': sandbox.utils.formatPrice(current_therapies[i]['promotionPrice']),
//				'price': sandbox.utils.formatPrice(current_therapies[i]['price'])
//			}).trim();
//		}
//
//		dom_component.innerHTML = sandbox.utils.renderTemplate(promotions_template, {
//			'promotions-html': html
//		}).trim();
//
//		// Dynamic DOM elements
//		dom_button_links = sandbox.findAll('.button-link');
//
//		// Bind Listeners
//		sandbox.on(dom_button_links, 'click', onButtonLinkClick);
//	} //populatePromotions()
//	 
//	/****************
//	 * Module Logic *
//	 ****************/
//	/*************
//	 * Listeners *
//	 *************/
//	function onButtonLinkClick(e) {
//		e.preventDefault();
//
//		var href = e.currentTarget.href;
//
//		var selector_data = {
//			'category-id': e.currentTarget.getAttribute('data-category-id'),
//			'sub-category-id': e.currentTarget.getAttribute('data-sub-category-id')
//		};
//		sandbox.sessionDB.set('therapy-selector-data', selector_data).then(function() {
//			sandbox.publish('therapy-selector-data-changed', selector_data);
//			sandbox.navigate(href);
//		});
//	}
//	
//	/***************
//	 * Subscribers *
//	 ***************/
//	/**************
//	 * Life Cycle *
//	 **************/
//	function start() {
//		return asyncFetchTemplates()
//		.then(asyncFetchSubCategories)
//		.then(asyncFetchTherapies)
//		.then(function() {
//			has_loaded_context = true;
//
//			populatePromotions();
//		}).catch(function(e){
//			console.log(e);
//		});
//	} //start()
//
//	function stop() {
//		sandbox.off();
//		sandbox.unsubscribe();
//	} // stop()
//	
//	return {
//		start: start,
//		stop: stop
//	};
//});



