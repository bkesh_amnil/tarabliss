
  $(document).ready(function(){
      $('#banner-info p').html('Our skin changes every day. That is why each of our face and body treatments <br>can be customised to tackle each and every one of your concerns.');
    
});

this.app.controller.startComponent('selector', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_category_buttons; 
	var dom_sub_category_menus;
	var dom_sub_categories;

	// Context
	var selector_template;
	var selector_category_menu_template;
	var selector_sub_category_button_template;
	var categories;
	var sub_categories;
	var therapy_selector_data;
	var has_loaded_context = false;
	var has_init = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchTemplates() {
		var promises = [];
		promises.push(sandbox.utils.ajaxLoad('/html/templates/therapies/selector.html').then(function(data) {
			selector_template = data;
		}));
		promises.push(sandbox.utils.ajaxLoad('/html/templates/therapies/selector-category-menu.html').then(function(data) {
			selector_category_menu_template = data;
		}));
		promises.push(sandbox.utils.ajaxLoad('/html/templates/therapies/selector-sub-category-button.html').then(function(data) {
			selector_sub_category_button_template = data;
		}));

		return Promise.all(promises);
	} //asyncFetchTemplates()

	function asyncFetchCategories() {
		return sandbox.userAPI.getFeaturedCategories().then(function(data) {
			categories = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'});
			return categories;
		});
	}
	function asyncFetchSubCategories() {
		return sandbox.userAPI.getFeaturedSubCategories().then(function(data) {
			sub_categories = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'});
			return sub_categories;
		});
	}

	function asyncLoadTherapySelectorData() {
		return sandbox.sessionDB.get('therapy-selector-data').then(function(data) {
			if (data === null) {
				therapy_selector_data = {
					'category-id': null,
					'sub-category-id': null
				};
			}
			else {
				therapy_selector_data = data;
			}
			return therapy_selector_data;
		});
	} //asyncLoadTherapySelectorData()

	function asyncSaveTherapySelectorData() {
		return sandbox.sessionDB.set('therapy-selector-data', therapy_selector_data);
	}

	/*************
	 * DOM Logic *
	 *************/
	function populateSelector() {
		if (!has_loaded_context) {
			throw new Error('populateSelector() - cannot proceed before context loaded');
		}

		var i,len; // loop vars
		var j,lenj;

		// Generate HTML of each category menu
		var html = '';
		for (i=0,len=categories.length; i<len; i++) {
			// Get sub cats for this category
			var category_id = categories[i]['id'];
			var category_name = categories[i]['name'];
			var current_sub_categories = sub_categories.filter(function(v) {
				return v['categoryID'] === category_id;
			});

			// Ignore categories without sub categories
			lenj=current_sub_categories.length;
			if (lenj === 0) {
				continue;
			}

			// Generate HTML of sub-menu for this category
			var buttons_html = '';
			for (j=0; j<lenj; j++) {
				buttons_html += sandbox.utils.renderTemplate(selector_sub_category_button_template, {
					'id': current_sub_categories[j]['id'],
					'name': current_sub_categories[j]['name']
				}).trim();
			} // for each sub category
			html += sandbox.utils.renderTemplate(selector_category_menu_template, {
				'id': category_id,
				'name': category_name,
				'sub-category-buttons-html': buttons_html
			}).trim();
		} // for each category

		// Generate selector HTML
		dom_component.innerHTML = sandbox.utils.renderTemplate(selector_template, {
			'menu-html': html
		}).trim();

		// Dynamic DOM elements
		dom_category_buttons = sandbox.findAll('.category-button');
		dom_sub_category_menus = sandbox.findAll('.sub-category-menu');
		dom_sub_categories = sandbox.findAll('.sub-category-button');

		// Bind selector listeners
		sandbox.on(dom_category_buttons, 'click', onCategoryButtonClick);
		sandbox.on(dom_sub_categories, 'click', onSubCategoryClick);
	} //populateSelector()

	// Set current selected sub-category to "current"
	function activateCurrentSubCategory() {
		if (!has_init) {
			throw new Error('activateCurrentSubCategory() - cannot proceed before selector has init.');
		}

		for (var i=0,len=dom_sub_categories.length; i<len; i++) {
			if (dom_sub_categories[i].getAttribute('data-sub-category-id') !== therapy_selector_data['sub-category-id'] ||
				dom_sub_categories[i].parentElement.getAttribute('data-category-id') !== therapy_selector_data['category-id']) {
				dom_sub_categories[i].classList.remove('current');
			}
			else {
				dom_sub_categories[i].classList.add('current');
			}
		}
	} //activateCurrentSubCategory()

	// Open / Close current menu for this category
	function openCurrentSubCategoryMenu() {
		if (!has_init) {
			throw new Error('openCurrentSubCategoryMenu() - cannot proceed before selector has init.');
		}

		for (var i=0,len=dom_sub_category_menus.length; i<len; i++) {
			if (dom_sub_category_menus[i].getAttribute('data-category-id') === therapy_selector_data['category-id']) { 
				dom_sub_category_menus[i].classList.add('shown');
			}
			else {
				dom_sub_category_menus[i].classList.remove('shown');
			}
		}
	} //openCurrentSubCategoryMenu()
	function toggleCurrentSubCategoryMenu() {
		if (!has_init) {
			throw new Error('toggleCurrentSubCategoryMenu() - cannot proceed before selector has init.');
		}

		for (var i=0,len=dom_sub_category_menus.length; i<len; i++) {
			if (dom_sub_category_menus[i].getAttribute('data-category-id') === therapy_selector_data['category-id']) { 
				dom_sub_category_menus[i].classList.toggle('shown');
			}
			else {
				dom_sub_category_menus[i].classList.remove('shown');
			}
		}
	} //toggleCurrentSubCategoryMenu()
	 
	/****************
	 * Module Logic *
	 ****************/
	/**
	 * Ensure selector has valid data. Can only be done after init
	 * @param: {int} index of default category to use if null
	 */
	function processTherapySelectorData(i) {
		if (!has_loaded_context) {
			throw new Error('processTherapySelectorData() - cannot proceed before context loaded');
		}
		if (typeof i !== 'number') {
			i = 0;
		}

		if (therapy_selector_data['category-id'] === null) {
			therapy_selector_data['category-id'] = '' + categories[i]['id']; // store as strings to simulate read from data-* attribute
			//console.debug('processTherapySelectorData() - setting default category-id ' + categories[0]['id']);
		}
		if (therapy_selector_data['sub-category-id'] === null) {
			var tmp = sub_categories.find(function(v) {
				return v['categoryID'] == therapy_selector_data['category-id']; // Note: non-strict comparison
			});
			if (!tmp) {
				console.debug('Cannot find sub categories, trying next category: ' + (i+1));
				therapy_selector_data['category-id'] = null;
				return processTherapySelectorData(i+1);
			}
			therapy_selector_data['sub-category-id'] = '' + tmp['id']; // store as strings to simulate read from data-* attribute
			//console.debug('processTherapySelectorData() - setting default sub-category-id ' + tmp['id']);
		}
	} //processTherapySelectorData()

	function asyncInit() {
		if (has_init) {
			return Promise.resolve();
		}

		return asyncFetchTemplates()
		.then(asyncFetchCategories)
		.then(asyncFetchSubCategories)
		.then(asyncLoadTherapySelectorData)
		.then(function() {
			has_loaded_context = true;

			processTherapySelectorData();
			
			populateSelector();	

			has_init = true;

			// Show based on current selector data
			openCurrentSubCategoryMenu();
			activateCurrentSubCategory();

			return true;
		}).catch(function(e) {
			console.debug(e);
		});
	} // asyncInit()
	 
	 /*************
	 * Listeners *
	 *************/
	function onCategoryButtonClick(e) {
		therapy_selector_data['category-id'] = e.currentTarget.getAttribute('data-category-id');
		toggleCurrentSubCategoryMenu();
	}

	function onSubCategoryClick(e) {
		therapy_selector_data['sub-category-id'] = e.currentTarget.getAttribute('data-sub-category-id');

		asyncSaveTherapySelectorData().then(function(){
			sandbox.publish('therapy-selector-data-changed', therapy_selector_data);
		});
	} //onSubCategoryClick()

	/***************
	 * Subscribers *
	 ***************/
	function onTherapySelectorDataChanged(data) {
		therapy_selector_data = data;

		if (has_loaded_context) {
			processTherapySelectorData();
		}

		if (has_init) {
			// Show based on current selector data
			openCurrentSubCategoryMenu();
			activateCurrentSubCategory();
		}
	}
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('therapy-selector-data-changed', onTherapySelectorDataChanged);

		asyncInit();
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});


this.app.controller.startComponent('selected-content', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_book_now_links;

	// Context
	var template;
	var therapy_template;
	var therapy_promotion_template;
	var categories;
	var sub_categories;
	var therapies;
	var therapy_selector_data;
	var has_loaded_context = false;
	var has_init = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchTemplates() {
		var promises = [];
		promises.push(sandbox.utils.ajaxLoad('/html/templates/therapies/selected-content.html').then(function(data) {
			template = data;
		}));
		promises.push(sandbox.utils.ajaxLoad('/html/templates/therapies/therapy.html').then(function(data) {
			therapy_template = data;
		}));
		promises.push(sandbox.utils.ajaxLoad('/html/templates/therapies/therapy-promotion.html').then(function(data) {
			therapy_promotion_template = data;
		}));

		return Promise.all(promises);
	}

	function asyncFetchCategories() {
		return sandbox.userAPI.getFeaturedCategories().then(function(data) {
			categories = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'});
			return categories;
		});
	}
	function asyncFetchSubCategories() {
		return sandbox.userAPI.getFeaturedSubCategories().then(function(data) {
			sub_categories = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'});
			return sub_categories;
		});
	}
	function asyncFetchTherapies() {
		return sandbox.userAPI.getFeaturedTherapies().then(function(data) {
			therapies = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'});
			return therapies;
		});
	}

	function asyncLoadTherapySelectorData() {
		if (typeof therapy_selector_data !== 'undefined') {
			return Promise.resolve(therapy_selector_data);
		}

		return sandbox.sessionDB.get('therapy-selector-data').then(function(data) {
			if (data === null) {
				therapy_selector_data = {
					'category-id': null,
					'sub-category-id': null
				};
			}
			else {
				therapy_selector_data = data;
			}
			return data;
		});
	} //asyncLoadTherapySelectorData()

	/*************
	 * DOM Logic *
	 *************/
	function populateContent() {
		if (!has_loaded_context) {
			throw new Error('populateContent() - cannot proceed before context loaded');
		}

		var i,len; // loop vars

		// Find the current cat, sub-cat and therapies
		var current_category, current_sub_category, current_therapies;
		current_category = categories.find(function(v) {
			return v['id'] == therapy_selector_data['category-id']; // Note: non-strict comparison
		});
		current_sub_category = sub_categories.find(function(v) {
			return v['id'] == therapy_selector_data['sub-category-id']; // Note: non-strict comparison
		});
		current_therapies = therapies.filter(function(v) {
			return v['subCategoryID'] === current_sub_category['id'];
		});

		// Generate HTML
		var html = '';
		for (i=0,len=current_therapies.length; i<len; i++) {
			// Select template based on whether or not on promo
			if (sandbox.utils.isOnPromotion(current_therapies[i])) {
				html += sandbox.utils.renderTemplate(therapy_promotion_template, {
					'title': current_therapies[i]['name'],
					'description':	current_therapies[i]['description'],
					'benefits':	current_therapies[i]['benefits'],
					'img-file': '/img/therapies/' + current_therapies[i]['id'] + '/thumbnails/main.jpg',
					'img-alt': current_therapies[i]['title'],
					'duration': current_therapies[i]['duration'] + ' Minutes',
					'promotion-terms': current_therapies[i]['promotionTerms'],
					'promotion-price': sandbox.utils.formatPrice(current_therapies[i]['promotionPrice']),
					'price': sandbox.utils.formatPrice(current_therapies[i]['price'])
				}).trim();
			}
			else {
				html += sandbox.utils.renderTemplate(therapy_template, {
					'title': current_therapies[i]['name'],
					'description':	current_therapies[i]['description'],
					'benefits':	current_therapies[i]['benefits'],
					'img-file': '/img/therapies/' + current_therapies[i]['id'] + '/thumbnails/main.jpg',
					'img-alt': current_therapies[i]['title'],
					'duration': current_therapies[i]['duration'] + ' Minutes',
					'price': sandbox.utils.formatPrice(current_therapies[i]['price'])
				}).trim();
			}
		}

		dom_component.innerHTML = sandbox.utils.renderTemplate(template, {
			'category-name': current_category['name'],
			'sub-category-name': current_sub_category['name'],
			'category-description': current_category['description'],
			'therapies-html': html
		}).trim();

		// Dynamic DOM elements
		dom_book_now_links = sandbox.findAll('.therapy > a');
	} //populateContent()
	
	/****************
	 * Module Logic *
	 ****************/
	/**
	 * Ensure selector has valid data.
	 */
	function processTherapySelectorData(i) {
		if (!has_loaded_context) {
			throw new Error('processTherapySelectorData() - cannot proceed before context loaded');
		}
		if (typeof i !== 'number') {
			i = 0;
		}

		if (therapy_selector_data['category-id'] === null) {
			therapy_selector_data['category-id'] = '' + categories[i]['id']; // ensure we store as strings to simulate read from data-* attribute
			console.debug('processTherapySelectorData() - setting default category-id ' + categories[0]['id']);
		}
		if (therapy_selector_data['sub-category-id'] === null) {
			var tmp = sub_categories.find(function(v) {
				return v['categoryID'] == therapy_selector_data['category-id']; // Note: non-strict comparison
			});
			if (!tmp) {
				therapy_selector_data['category-id'] = null;
				return processTherapySelectorData(i+1);
			}
			therapy_selector_data['sub-category-id'] = '' + tmp['id']; // ensure we store as strings to simulate read from data-* attribute
			console.debug('processTherapySelectorData() - setting default sub-category-id ' + tmp['id']);
		}
	} //processTherapySelectorData()

	function scrollToContent() {
		sandbox.utils.animateScrollTo(dom_component, {'offsetY': 60});
	}

	// Called only after content is populated
	function bindListeners() {
		sandbox.on(dom_book_now_links, 'click', onBookNowLinkClick);
	}

	function asyncInit() {
		if (has_init) {
			return Promise.resolve();
		}

		return asyncFetchTemplates()
		.then(asyncFetchCategories)
		.then(asyncFetchSubCategories)
		.then(asyncFetchTherapies)
		.then(asyncLoadTherapySelectorData)
		.then(function() {
			has_loaded_context = true;

			processTherapySelectorData();
			
			populateContent();
			bindListeners();
			scrollToContent();
			
			has_init = true;

			return true;
		}).catch(function(e){
			console.debug(e);
		});
	} //asyncInit()

	/*************
	 * Listeners *
	 *************/
	function onBookNowLinkClick(e) {
		e.preventDefault();
		var mail_link = e.currentTarget.href;

		sandbox.dialog.confirmTemplate(sandbox.settings.DOMAIN_URL + '/html/templates/dnc.html', 'I Agree', 'Cancel').then(function() {
			window.location = mail_link;
		});
	}
	
	/***************
	 * Subscribers *
	 ***************/
	function onTherapySelectorDataChanged(data) {
		therapy_selector_data = data;
		if (has_loaded_context) {
			processTherapySelectorData();

			populateContent();
			bindListeners();
			scrollToContent();
		}
	}
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('therapy-selector-data-changed', onTherapySelectorDataChanged);
		
		asyncInit();
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});
