this.app.controller.startComponent('selector', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_links = sandbox.findAll('a');

	// Context
	
	/*****************
	 * Context Logic *
	 *****************/

	/*************
	 * DOM Logic *
	 *************/
	function activateLinks() {
		var url = sandbox.settings.DOMAIN_URL + '/' + sandbox.getCurrentPage();
		for (var i=0,len=dom_links.length; i<len; i++) {
			if (dom_links[i].href.startsWith(url)) {
				dom_links[i].classList.add('current');
			}
			else {
				dom_links[i].classList.remove('current');
			}
		}
	} //activateLinks()

	/****************
	 * Module Logic *
	 ****************/
	 
	 /*************
	 * Listeners *
	 *************/
	
	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		activateLinks();
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});