
  $(document).ready(function(){
      $('#banner-info p').html('Beyond its skin-transformative face treatments and luxurious body pampering,<br> TaraBliss Spa is also a space dedicated to inner wellbeing.');
    
});

this.app.controller.startComponent('promotions', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_button_links;

	// Context
	var promotions_template;
	var promotion_template;
	var sub_categories;
	var therapies;
	var has_loaded_context = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchTemplates() {
		var promises = [];
		promises.push(sandbox.utils.ajaxLoad('/html/templates/promotions/promotion.html').then(function(data) {
			promotion_template = data;
		}));
		promises.push(sandbox.utils.ajaxLoad('/html/templates/promotions/promotions.html').then(function(data) {
			promotions_template = data;
		}));

		return Promise.all(promises);
	}

	function asyncFetchSubCategories() {
		return sandbox.userAPI.getFeaturedSubCategories().then(function(data) {
			sub_categories = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'});
			return sub_categories;
		});
	}
	function asyncFetchTherapies() {
		return sandbox.userAPI.getFeaturedTherapies().then(function(data) {
			therapies = sandbox.utils.sortArrayBy('displayPriority', data, {primer: parseInt, order:'desc'});
			return therapies;
		});
	}

	/*************
	 * DOM Logic *
	 *************/
	function populatePromotions() {
		if (!has_loaded_context) {
			throw new Error('populatePromotions() - cannot proceed before context loaded');
		}

		var i,len; // loop vars

		// Find therapies on promo
		var current_therapies = therapies.filter(function(v) {
			return sandbox.utils.isOnPromotion(v);
		});


		// Generate HTML
		var html = '';
		for (i=0,len=current_therapies.length; i<len; i++) {
			// Find category ID of this therapy
			var current_sub_category = sub_categories.find(function(v) {
				return v['id'] === current_therapies[i]['subCategoryID'];
			});
			var category_id = current_sub_category['categoryID'];

			html += sandbox.utils.renderTemplate(promotion_template, {
				'title': current_therapies[i]['name'],
				'category-id': category_id,
				'sub-category-id': current_therapies[i]['subCategoryID'],
				'img-file': '/img/therapies/' + current_therapies[i]['id'] + '/thumbnails/main.jpg',
				'img-alt': current_therapies[i]['title'],
				'duration': current_therapies[i]['duration'] + ' Minutes',
				'promotion-terms': current_therapies[i]['promotionTerms'],
				'expiry-date': sandbox.utils.formatDate('d M Y',current_therapies[i]['promotionEndTime']),
				'promotion-price': sandbox.utils.formatPrice(current_therapies[i]['promotionPrice']),
				'price': sandbox.utils.formatPrice(current_therapies[i]['price'])
			}).trim();
		}

		dom_component.innerHTML = sandbox.utils.renderTemplate(promotions_template, {
			'promotions-html': html
		}).trim();

		// Dynamic DOM elements
		dom_button_links = sandbox.findAll('.button-link');

		// Bind Listeners
		sandbox.on(dom_button_links, 'click', onButtonLinkClick);
	} //populatePromotions()
	 
	/****************
	 * Module Logic *
	 ****************/
	function asyncInit() {
		return asyncFetchTemplates()
		.then(asyncFetchSubCategories)
		.then(asyncFetchTherapies)
		.then(function() {
			has_loaded_context = true;

			populatePromotions();
		}).catch(function(e){
			console.log(e);
		});
	} //asyncInit()
	 
	/*************
	 * Listeners *
	 *************/
	function onButtonLinkClick(e) {
		e.preventDefault();

		var href = e.currentTarget.href;

		var selector_data = {
			'category-id': e.currentTarget.getAttribute('data-category-id'),
			'sub-category-id': e.currentTarget.getAttribute('data-sub-category-id')
		};
		sandbox.sessionDB.set('therapy-selector-data', selector_data).then(function() {
			sandbox.publish('therapy-selector-data-changed', selector_data);
			sandbox.navigate(href);
		});
	}
	
	/***************
	 * Subscribers *
	 ***************/
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		asyncInit();
	} //start()

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});
