// Overlays should always be one component belonging to that page
this.app.controller.startComponent('overlay', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_form = sandbox.findOne('#pop-up-newsletter-form');
	var dom_form_output = sandbox.findOne('output:last-of-type');
	
	// Messages
	var INVALID_EMAIL_MESSAGE = 'Invalid email.';
	var WAIT_MESSAGE = 'Please wait...';
	var SUCCESS_MESSAGE = 'Thank you for subscribing.';
	
	// Settings
	var OUTPUT_SHOW_DURATION = 10000;
	var HIDE_OVERLAY_DELAY = 2000;

	// Context
	var output_timer;

	// Context
	var is_submitting = false;
	
	/*****************
	 * Context Logic *
	 *****************/

	/*************
	 * DOM Logic *
	 *************/
	function outputFormMessage(msg) {
		dom_form_output.innerHTML = msg;
		dom_form_output.classList.remove('error');
		dom_form_output.classList.add('shown');

		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	} //outputFormMessage()

	function outputFormError(msg) {
		dom_form_output.innerHTML = msg;
		dom_form_output.classList.add('error');
		dom_form_output.classList.add('shown');

		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	} //outputFormError()

	function clearFormOutput() {
		dom_form_output.classList.remove('error');
		dom_form_output.classList.remove('shown');
		dom_form_output.innerHTML = '';
	}

	function clearForm() {
		dom_form.reset();
	}

	function disableForm() {
		sandbox.utils.disableForm(dom_form);
	}

	function enableForm() {
		sandbox.utils.enableForm(dom_form);
	}

	function isSubmitting() {
		return sandbox.utils.isDisabledForm(dom_form);
	}
	 
	/****************
	 * Module Logic *
	 ****************/
	/**
	 * Outputs error message
	 * @return: {bool}
	 */
	function validateForm() {
		var email = dom_form['email'].value;
		if (!sandbox.utils.isEmail(email)) {
			outputFormError(INVALID_EMAIL_MESSAGE);
			return false;
		}

		return true;
	} //validateForm()

	function hideOverlayAfterDelay() {
		setTimeout(function() {
			sandbox.hideOverlay();
		}, HIDE_OVERLAY_DELAY);
	}

	/*************
	 * Listeners *
	 *************/
	function onFormSubmit(e) {
		e.preventDefault();

		if (is_submitting) {
			return false;
		}
		
		if (!validateForm()) {
			return false;
		}

		is_submitting = true;
		disableForm();
		outputFormMessage(WAIT_MESSAGE);

		// AJAX
		var form_data = new FormData(dom_form);
		sandbox.userAPI.subscribeMailList(form_data).then(function() {
			is_submitting = false;
			clearForm();
			enableForm();
			outputFormMessage(SUCCESS_MESSAGE);

			hideOverlayAfterDelay();
		}, function(error_msg) {
			is_submitting = false;
			enableForm();
			outputFormError(error_msg);
		});
	} //onFormSubmit()

	/***************
	 * Subscribers *
	 ***************/
	
	
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Listeners
		sandbox.on(dom_form, 'submit', onFormSubmit);
	}

	// Called by controller to stop
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	}
	
	return {
		start: start,
		stop: stop
	};
});