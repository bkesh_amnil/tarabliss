<?php
/***
 * Frontend Web App Framework v1.5.0 - Sitemap
 *
 * When executed, produces a sitemap.xml for download.
 * - based on HTML pages found on the server's [html]/[pages] directory
 ***/

define('SITE_URL', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'])?'https://':'http://') . $_SERVER['SERVER_NAME']);
try {
	$pages = array();
	
	// Read all static HTML pages
	$files = glob(__DIR__.'/html/pages/*.html');
	foreach ($files as $file) {
		$pages[] = basename($file, '.html');
	}

	// Read all dynamic pages
	$sub_dirs = glob(__DIR__.'/html/pages/*', GLOB_ONLYDIR);
	foreach ($sub_dirs as $sub_dir) {
		// Ignore [error] dir
		if ($sub_dir === __DIR__.'/html/pages/error') {
			continue;
		}

		$dynamic_name = basename($sub_dir);
		$files = glob($sub_dir.'/*.html');
		foreach ($files as $file) {
			$pages[] = $dynamic_name . '/' . basename($file, '.html');
		}
	}
	
	// Convert each page into an XML string in <url>
	$page_xml = '';
	foreach ($pages as $page) {
		$page_xml .= ''.
			'<url>'."\r\n".
			'	<loc>'.SITE_URL.'/'.$page.'</loc>'."\r\n".
			'	<changefreq>daily</changefreq>'."\r\n".
			'</url>'."\r\n";
	}
	
	// Form complete sitemap.xml
	$sitemap_xml = '<?xml version="1.0" encoding="UTF-8"?>'."\r\n".
	'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"'."\r\n".
	'        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'."\r\n".
	'        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9'."\r\n".
	'              http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'."\r\n".
				$page_xml.
	'</urlset>';
	
	// Save to file
	$file_path = __DIR__.'/private/sitemap.xml';
	if (!file_put_contents($file_path, $sitemap_xml, LOCK_EX)) {
		throw new Exception('Cannot write to: '. $file_path);
	}
	
	// Serve download
	$file_name = basename($file_path);
	header('HTTP/1.1 200 OK');
	header("Content-disposition: attachment; filename=$file_name");
	header("Content-type: application/php");
	readfile($file_path);
	exit(0);
} catch (Exception $e) {
	header('HTTP/1.1 500 Internal Server Error');
	exit(1);
} //unable to open file
