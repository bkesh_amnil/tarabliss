<?php
/***
 * App Framework v1.5.0 - findPage
 *
 * Server-side function to retrieve meta and file names of a page (HTML Escaped / URL encoded)
 ***/

// Settings
define('DEFAULT_PAGE', 'home');
define('DEFAULT_IMAGE', '/ui/logo-FFFFFF.svg');

/***
 * Determine:
 * - $page['title', 'description', 'html', 'css', 'js', 'img']
 * - $page['canonical'] and other optional meta
 * @input: $_GET['page']
 * @return: {array}
 */
function findPage() {
	if (isset($_GET['page'])) {
		$name = $_GET['page'];
	}
	else {
		$_GET['page'] = DEFAULT_PAGE;
		$name = DEFAULT_PAGE;
	}
	
	// Read pages.json
	$json = file_get_contents(__DIR__.'/pages.json');
	$pages = json_decode($json, true);
	if (!isset($pages)) {
		return find500($name);
	}
	
	// Handle static & dynamic pages diff
	$last_slash_index = strpos($name, '/');
	if ($last_slash_index === FALSE) {
		if (isset($pages[$name])) {
			$page = $pages[$name];
		}
		else {
			return find404($name);
		}
	} // static page
	else {
		// Get meta for /*
		$name_before_slash = substr($name, 0, $last_slash_index);
		$dynamic_page_name = $name_before_slash.'/*';
		if (isset($pages[$dynamic_page_name])) {
			$page = $pages[$dynamic_page_name];
		}
		else {
			$page = array(
				'css' => 'dynamic/'.$name_before_slash.'.css',
				'js' => 'dynamic/'.$name_before_slash.'.js',
				'img' => DEFAULT_IMAGE
			);
		}
		
		// Merge with page meta
		if (isset($pages[$name])) {
			$page = array_merge($page, $pages[$name]); // later elements overwrite previous elements
		}
	} // dynamic page
	
	// Derive defaults if necessary
	if (!isset($page['title'])) {
		$page['title'] = $name;
	}
	if (!isset($page['description'])) {
		$page['description'] = $name;
	}
	if (!isset($page['html'])) {
		$page['html'] = $name . '.html';
	}
	if (!isset($page['css'])) {
		$page['css'] = $name . '.css';
	}
	if (!isset($page['js'])) {
		$page['js'] = $name . '.js';
	}
	if (!isset($page['img'])) {
		$page['img'] = DEFAULT_IMAGE;
	}
	if (!isset($page['canonical'])) {
		$page['canonical'] = $name;
	}

	// Escape text
	$page['title'] = htmlspecialchars($page['title']);
	$page['description'] = htmlspecialchars($page['description']);
	//$page['html'] = urlencode($page['html']);
	//$page['css'] = urlencode($page['css']);
	//$page['js'] = urlencode($page['js']);
	//$page['img'] = urlencode($page['img']);
	
	// Handle 404 cases
	if (!is_readable(__DIR__.'/../html/pages/' .$page['html'])) {
		return find404($name. ' [file not found: /html/pages/' .$page['html'].']');
	}
	
	return $page;
} //findPage()

function find500($name) {
	header('HTTP/1.1 500 Internal Server Error');
	return array(
		"title" => "500 Internal Server Error",
		"description" => "Something went wrong",
		"html" => "error/500".'.html',
		"css" => "dynamic/error".'.css',
		"js" => "dynamic/error".'.js',
		"img" => DEFAULT_IMAGE,
		"name" => $name
	);
}
function find404($name) {
	header('HTTP/1.1 404 Not Found');
	return array(
		"title" => "404 Not Found",
		"description" => "What are you looking for?",
		"html" => "error/404".'.html',
		"css" => "dynamic/error".'.css',
		"js" => "dynamic/error".'.js',
		"img" => DEFAULT_IMAGE,
		"name" => $name
	);
}