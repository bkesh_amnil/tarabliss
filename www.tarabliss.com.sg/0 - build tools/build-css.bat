﻿
REM Leave first line empty because of potential UTF-8 BOM
ECHO off
cls

ECHO Cleanup and Directory Prep...
ECHO -----------------------------
IF exist "yui\css" rd /s /q yui\css
mkdir yui\css
ECHO [yui]\[css] recreated
IF exist "OUTPUT\css" rd /s /q OUTPUT\css
mkdir OUTPUT\css
ECHO [OUTPUT]\[css] recreated
ECHO ------------------------
ECHO.

ECHO.
ECHO Minifying Common CSS files
ECHO ------------------------------
cd yui
mkdir css\common
xcopy /q ..\..\css\common css\common
copy css\common\*.css css\common-concat.css >NUL
java -jar yuicompressor-2.4.9a.jar --type css -o "css\m.css" .\css\common-concat.css
xcopy /q css\m.css "..\OUTPUT\css\common\"
cd ..
ECHO -----------------------

ECHO.
ECHO Minifying pages css ...
ECHO -----------------------
cd yui
mkdir css\pages
xcopy /q ..\..\css\pages css\pages
java -jar yuicompressor-2.4.9a.jar --type css -o ".css$:.mincss" .\css\pages\*.css
xcopy /q .\css\pages\*.mincss "..\OUTPUT\css\pages\"
rename "..\OUTPUT\css\pages\*.mincss" *.css
cd ..
ECHO -----------------------

ECHO.
ECHO Minifying dynamic pages css ...
ECHO -------------------------------
cd yui
mkdir css\pages\dynamic
xcopy /q ..\..\css\pages\dynamic css\pages\dynamic
java -jar yuicompressor-2.4.9a.jar --type css -o ".css$:.mincss" .\css\pages\dynamic\*.css
xcopy /q .\css\pages\dynamic\*.mincss "..\OUTPUT\css\pages\dynamic\"
rename "..\OUTPUT\css\pages\dynamic\*.mincss" *.css
cd ..
ECHO -------------------------------

ECHO.
ECHO Minifying overlays css ...
ECHO -----------------------
cd yui
mkdir css\overlays
xcopy /q ..\..\css\overlays css\overlays
java -jar yuicompressor-2.4.9a.jar --type css -o ".css$:.mincss" .\css\overlays\*.css
xcopy /q .\css\overlays\*.mincss "..\OUTPUT\css\overlays\"
rename "..\OUTPUT\css\overlays\*.mincss" *.css
cd ..
ECHO -----------------------

ECHO.
PAUSE
