﻿
REM Leave first line empty because of UTF-8 BOM
ECHO off
cls

ECHO Cleanup and Directory Prep
ECHO --------------------------
IF NOT EXIST "replaceText\replaced" mkdir replaceText\replaced
IF exist "closure\js" rd /s /q closure\js
mkdir closure\js\3-comp\pages\dynamic
ECHO [closure]\[js]\[3-comp]\[paged]\[dynamic] recreated
mkdir closure\js\3-comp\overlays
ECHO [closure]\[js]\[3-comp]\[overlays] recreated
IF exist "OUTPUT\js" rd /s /q OUTPUT\js
mkdir OUTPUT\js
ECHO [OUTPUT]\[js] recreated
ECHO ---------------------
ECHO.

ECHO Creating 0-env.min.js
ECHO ---------------------
cd closure
java -jar compiler.jar --compilation_level WHITESPACE_ONLY --js_output_file=js/0-env.min.js ../../js/0-env/**.js
cd ..
ECHO ---------------------
ECHO.

ECHO Creating 1-lib.min.js
ECHO ---------------------
cd closure
java -jar compiler.jar --compilation_level WHITESPACE_ONLY --js_output_file=js/1-lib.min.js ../../js/1-lib/**.js
cd ..
ECHO ---------------------
ECHO.

ECHO Replacing Text in init.js...
ECHO ----------------------------
cd replaceText
copy  /y ..\..\js\2-core\init.js replaced\init.js
cscript replaceText.vbs replaced\init.js "	///* Development" "	/* Development" >NUL
cscript replaceText.vbs replaced\init.js "	///* Staging" "	/* Staging" >NUL
cscript replaceText.vbs replaced\init.js "	///* Production" "	/* Production" >NUL
cscript replaceText.vbs replaced\init.js "	/* Production" "	///* Production" >NUL
cd ..
ECHO ----------------------------
ECHO.

ECHO Creating 2-core.min.js
ECHO ----------------------
cd closure
java -jar compiler.jar --compilation_level WHITESPACE_ONLY --js_output_file=js/2-core.min.js ../replaceText/replaced/init.js ../../js/2-core/controller.js ../../js/2-core/extensions/**.js ../../js/2-core/sandbox.js ../../js/3-comp/common/**.js
cd ..
ECHO ---------------------

ECHO Compiling 012.js
ECHO --------------
cd closure
java -jar compiler.jar --js_output_file=js/012.js js/0-env.min.js js/1-lib.min.js js/2-core.min.js
xcopy /q js\012.js ..\OUTPUT\js\
cd ..
ECHO -----------------------
ECHO.

ECHO Creating dynamic pages minifed JS...
ECHO ------------------------------------
cd closure
for %%f in (..\..\js\3-comp\pages\dynamic\*.js) do java -jar compiler.jar --js_output_file="js\3-comp\pages\dynamic\%%~nf.js" %%f
xcopy /q js\3-comp\pages\dynamic\*.js "..\OUTPUT\js\3-comp\pages\dynamic\"
cd ..
ECHO ------------------------------------
ECHO.

ECHO Creating pages minifed JS...
ECHO ---------------------------
cd closure
for %%f in (..\..\js\3-comp\pages\*.js) do java -jar compiler.jar --js_output_file="js\3-comp\pages\%%~nf.js" %%f
xcopy /q js\3-comp\pages\*.js "..\OUTPUT\js\3-comp\pages\"
cd ..
ECHO ------------------------------------
ECHO.

ECHO.
ECHO Creating overlays minifed JS...
ECHO ---------------------------
cd closure
for %%f in (..\..\js\3-comp\overlays\*.js) do java -jar compiler.jar --js_output_file="js\3-comp\overlays\%%~nf.js" %%f
xcopy /q js\3-comp\overlays\*.js "..\OUTPUT\js\3-comp\overlays\"
cd ..
ECHO ------------------------------------
ECHO.

PAUSE