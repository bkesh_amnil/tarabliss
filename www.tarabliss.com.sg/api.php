<?php
/***
 * Frontend Web App Framework v1.5.0 - API
 *
 * Backdoor to backend service API.
 * Rather than calling api.tarabliss.com.sg, all API calls to www.tarabliss.com.sg/api/xxx/yyy originate from www.tarabliss.com.sg.
 * This eliminates the need for CORS.
 ***/

	///* Development
	require '../api.tarabliss.com.sg/index.php';
	//*/

	/* Staging
	require '../staging.api.tarabliss.com.sg/index.php';
	//*/

	/* Production
	require '../api.tarabliss.com.sg/index.php';
	//*/