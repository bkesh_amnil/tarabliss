<?php
/***
 * Frontend Web App Framework v1.5.0 - Meta
 *
 * Called by controller.js (AJAX) to determine a page's meta
 ***/

// Populate $page 
require(__DIR__.'/private/findPage.php');
$page = findPage();

// Serve as JSON
header('Content-Type: application/json; charset=utf-8');
echo json_encode($page);