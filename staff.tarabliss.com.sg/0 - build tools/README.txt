﻿Frontend Web App Framework v1.5.0 Build Tools
=================================

The build process should not leave any intermediary/processed files in the source folders at any time.
These files should reside in each build tool's directory/sub-directories only.

Files:
------
  - [closure]/compiler.jar                     - [v20150920](https://github.com/google/closure-compiler/wiki/Binary-Downloads)
  - [eol-converters]/*.exe                     - 4 .exe files here
  - [htmlcompressor]/htmlcompressor-1.5.3.jar
  - [htmlcompressor]/regex.txt
  - [jpegtran]/jpegtran.exe
  - [pngcrush]/pngcrush.exe
  - [jshint]/package.json
  - [jshint]/.jshintignore
  - [replaceText]/replaceText.vbs
  - [yui]/yuicompressor-2.4.9a.jar

Before starting
---------------
Ensure these are installed:

  - npm
  - jshint (installed through npm)
  - java

How to use
----------
**Build HTML**
(build-html.bat, build-html-staging.bat)

  Process
  - Use replaceText on index.php, api.php and all HTML files within [html] folder to comment out files included under other environments.
  - Remove extraneous white-spaces, line-breaks, comments from index.php and all HTML files within [html] folder (*recursive), output to [OUTPUT]/[html] folder.
  - Copy api.php, meta.php, sitemap.php, robots.txt, humans.txt, [private] to [OUTPUT] folder

  Required files:
  - [replaceText]/replaceText.vbs
  - [htmlcompressor]/htmlcompressor-1.5.3.jar
  - [htmlcompressor]/regex.txt                (specifying that php tags should be left untouched)

**Build CSS**
(build-css.bat)

  Process
  - Concatenate all CSS files in [css]/[common] folder, and minify it to [OUTPUT]/[css]/[common]/m.css
  - Minify each CSS file in [css]/[pages] folder to [OUTPUT]/[css]/[pages]
  - Minify each CSS file in [css]/[overlays] folder to [OUTPUT]/[css]/[overlays]
  - Copy all minified CSS files to output folder.

  Required files:
  - [yui]/yuicompressor-2.4.9a.jar


**Lint JS**
(lint-js.bat)

  Process
  - Use JS Hint to check all JS files except files in:
    - [js]/[0-env] folder (polyfills)
    - [js]/[1-lib] folder (libraries)

  Required files:
  - [jshint]/.jshintignore      (files to ignore)
  - [jshint]/package.json       (configuration)
  - Note: JSHint installed through npm

**Build JS**
(build-js.bat, build-js-staging.bat)

  Process
  - Concat & minify all JS files in [js]/[0-env] folder to 0-env.js
  - Concat & minify all JS files in [js]/[1-lib] folder to 1-lib.js
  - ReplaceText on init.js to comment out files included under other environments.
  - Concat & minify all JS files in [js]/[2-app] and [js]/[3-comp]/[common] folder to 2-core.js
  - Compile 0-env.js, 1-lib.js and 2-core.js to 012.js, output to [OUTPUT]/[js]/012.js
  - Compile each JS file in [js]/[3-comp]/[pages]/[dynamic] to [OUTPUT]/[js]/[3-comp]/[pages]/[dynamic].
  - Compile each JS file in [js]/[3-comp]/[pages] to [OUTPUT]/[js]/[3-comp]/[pages].
  - Compile each JS file in [js]/[3-comp]/[overlays] to [OUTPUT]/[js]/[3-comp]/[overlays].

  Required files:
  - [closure]/compiler.jar


**Buiild Assets**
(build-assets.bat)

  Process
  - Copy all [img] to [OUTPUT]/[img]
  - Copy all [font] to [OUTPUT]/[font]
  - Copy all [doc] to [OUTPUT]/[doc]
  - Copy all [data] to [OUTPUT]/[data]
  - Transform all JPGs in [img] (*recursive) to [OUTPUT]/[img]
  - Transform all PNGs in [img] (*recursive) to [OUTPUT]/[img]

  Required files:
  - [jpegtran]/jpegtran.exe
  - [pngcrush]/pngcrush.exe
  
--------------

**Ending Credits**

  - http://www.howtogeek.com/51194/replace-text-in-plain-text-files-from-the-command-line/
  - https://code.google.com/p/htmlcompressor/
  - https://github.com/yui/yuicompressor/releases
  - https://developers.google.com/closure/compiler/
  - http://jshint.com/
