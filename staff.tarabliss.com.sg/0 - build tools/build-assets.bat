﻿
REM Leave first line empty because of UTF-8 BOM
ECHO off
cls

ECHO Cleanup and Directory Prep
ECHO ---------------------------
IF exist "jpegtran\img\" rd /s/q jpegtran\img
mkdir jpegtran\img
ECHO [jpegtran]\[img] recreated
IF exist "pngcrush\img" rd /s /q pngcrush\img
mkdir pngcrush\img
ECHO [pngcrush]\[img] recreated
IF exist "OUTPUT\img" rd /s /q OUTPUT\img
mkdir OUTPUT\img
ECHO [OUTPUT]\[img] recreated
IF exist "OUTPUT\font" rd /s /q OUTPUT\font
mkdir OUTPUT\font
ECHO [OUTPUT]\[font] recreated
IF exist "OUTPUT\doc" rd /s /q OUTPUT\doc
mkdir OUTPUT\doc
ECHO [OUTPUT]\[doc] recreated
IF exist "OUTPUT\data" rd /s /q OUTPUT\data
mkdir OUTPUT\data
ECHO [OUTPUT]\[data] recreated
ECHO ---------------------
ECHO.

ECHO Copying all images...
ECHO ---------------------
xcopy /sy "../img" "OUTPUT/img"
ECHO ---------------------
ECHO.

ECHO Copying all fonts...
ECHO ---------------------
xcopy /sy "../font" "OUTPUT/font"
ECHO ---------------------
ECHO.

ECHO Copying all docs...
ECHO ---------------------
xcopy /sy "../doc" "OUTPUT/doc"
ECHO ---------------------
ECHO.

ECHO Copying all datalists...
ECHO ---------------------
xcopy /sy "../data" "OUTPUT/data"
ECHO ---------------------
ECHO.

ECHO Transforming JPGs...
ECHO ---------------------
cd jpegtran
xcopy /sy ..\..\img\*.jpg img
SETLOCAL EnableDelayedExpansion
FOR /R img %%i IN (*.jpg) DO (
   ECHO Optimizing %%~nxi...
   jpegtran.exe -optimize -progressive -copy none "%%i" "%%i.prog"
   jpegtran.exe -optimize -copy none "%%i" "%%i.base"

   CALL :FileSize "%%i.prog" ProgFileSize
   CALL :FileSize "%%i.base" BaseFileSize
   ECHO Original File Size: %%~zi

   if !ProgFileSize! gtr !BaseFileSize! (
       ECHO - Use Baseline Img: !BaseFilSize!
       DEL "%%i.prog"
       DEL "%%i"
       move /y "%%i.base" "%%ichosen"
   ) else (
       ECHO - Use Progress Img: !ProgFileSize!
       DEL "%%i.base"
       DEL "%%i"
       move /y "%%i.prog" "%%ichosen"
   )
   ECHO.
)
FOR /r %%x in (*.jpgchosen) DO ren "%%x" *.jpg
xcopy /sy img\*.jpg "..\OUTPUT\img"
cd ..
ECHO ---------------------
ECHO.

REM ECHO Crushing favicon PNGs...
REM ECHO ------------------------
REM cd pngcrush
REM for %%f in (..\..\img\favicons\*.png) do pngcrush -reduce -brute %%f "crushed\favicons\%%~nf.png"
REM xcopy /y "crushed\favicons\*.png" "..\OUTPUT\img\favicons\"
REM cd ..
REM ECHO ---------------------
REM ECHO.
REM PAUSE

ECHO Crushing PNGs...
ECHO ----------------
cd pngcrush
xcopy /sy ..\..\img\*.png img
SETLOCAL EnableDelayedExpansion
FOR /R img %%i IN (*.png) DO (
  ECHO Optimizing %%~nxi...
  pngcrush -q -reduce -brute "%%i" "%%icrushed"
  DEL "%%i"
)
FOR /r %%x in (*.pngcrushed) DO ren "%%x" *.png
xcopy /sy img\*.png "..\OUTPUT\img"
cd ..
ECHO ---------------------
ECHO.
GOTO aftersubroutines

:FileSize
SET %~2=%~z1
GOTO :EOF

:aftersubroutines
PAUSE
