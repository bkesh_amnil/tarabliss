/***
 * Frontend Web App Framework v1.5.0 - Init Script
 * =================================
 *
 * Execution:
 * - Feature Detect
 * - External plugins (Facebook, Google)
 *
 * Before use:
 * - paths
 * - default page
 * - googla analytics ID
 * 
 */
(function(app, Modernizr, dialog){
	'use strict';
	
	/*******************
	 * Config settings *
	 *******************/
	app.settings = {
		// Validation
		CODE_LENGTH: 6,
		MIN_PASSWORD_LENGTH: 8,
		MIN_NAME_LENGTH: 2,
		PHONE_LENGTH: 8,

		// Pages
		DEFAULT_PAGE: 'home'
	};

	/* Development
	app.settings.WWW_URL = 'http://localhost:8051';
	app.settings.DOMAIN_URL = 'http://localhost:8053';
	app.settings.SERVER_URL = 'http://localhost:8053/api';
	//*/
	
	/* Staging
	app.settings.WWW_URL = 'http://staging.www.tarabliss.com.sg';
	app.settings.DOMAIN_URL = 'http://staging.staff.tarabliss.com.sg';
	app.settings.SERVER_URL = 'http://staging.staff.tarabliss.com.sg/api';
	//*/
	
	///* Production
	app.settings.WWW_URL = 'https://www.tarabliss.com.sg';
	app.settings.DOMAIN_URL = 'https://staff.tarabliss.com.sg';
	app.settings.SERVER_URL = 'https://staff.tarabliss.com.sg/api';
	//*/

	/*****************
	 * Compatibility *
	 *****************/
	// Check critical features: halt on failure
	if (!Modernizr.cookies) {
		haltWithError('Your browser does not support cookies and will not work properly with our website. Please check your browser settings.');
		return;
	}
	if (!Modernizr.indexeddb && !Modernizr.localstorage && !Modernizr.websqldatabase) {
		haltWithError('Your browser does not support any local storage engines and will not work properly with our website. Please check your browser settings.');
		return;
	}
	
	// Check features: alert user of potential problems
	if (!Modernizr.history) {
		alertWithWarning('Your browser does not support browser history and may not work fully with our website.');
	}
	
	/**
	 * @private
	 * Adds additional markup and standard message, e.g. attention icon, <h2>, <p> etc.
	 */
	function alertWithWarning(msg) {
		var warning_html = ''+
			'<h2><Warning</h2>'+
			'<p>' + msg + '</p>';
		dialog.alert(warning_html);
	}
	function haltWithError(msg) {
		var error_html = ''+
			'<h2 class="error">Incompatible Browser</h2>' +
			'<p>' + msg + '</p>' +
			'<p>If you are using an <strong>outdated</strong> browser, please consider <a href="http://browsehappy.com/">upgrading your browser</a> to improve your experience.</p>' +
			'<p>If you are using a computer with restricted access to application updates, you may wish to explore using <a href="http://portableapps.com/apps/internet">portable browsers</a>.</p>';
		dialog.halt(error_html);
	}
	
	/*****************
	 * External APIs *
	 *****************/
	
})(this.app = this.app || {}, Modernizr, dialog);




