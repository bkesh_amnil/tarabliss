<?php
/***
 * Frontend Web App Framework v1.5.0 - Index
 *
 * Main Application entry point.
 *
 * Remember to set:
 * - Favicons
 * - CSS includes
 * - JS includes
 ***/

require(__DIR__.'/private/findPage.php');
$page = findPage();

define('SITE_URL', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'])?'https://':'http://') . $_SERVER['SERVER_NAME']);
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	
	<title><?php echo $page['title'];?></title>
	<meta name="description" content="<?php echo $page['description'];?>">
	<meta property="og:site_name" content="Example"/>
	<meta property="og:title" content="<?php echo $page['title'];?>" />
	<meta property="og:description" content="<?php echo $page['description'];?>" />
	<meta property="og:image" content="<?php echo SITE_URL.'/img/'.$page['img'];?>" />
	
	<?php if (isset($page['canonical'])) { ?>
	<link rel="canonical" href="<?php echo SITE_URL.'/'.$page['canonical'];?>"/>
	<?php } ?>
	
	<link rel="apple-touch-icon" sizes="60x60" href="/img/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/img/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/img/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/img/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="/img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/img/favicons/favicon-194x194.png" sizes="194x194">
	<link rel="icon" type="image/png" href="/img/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/img/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="/img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/img/favicons/manifest.json">
	<link rel="mask-icon" href="/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/img/favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#9e9450">
	<meta name="msapplication-TileImage" content="/img/favicons/mstile-144x144.png">
	<meta name="msapplication-config" content="/img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#fffdee">
	
	<!-- Development
	<link rel="stylesheet" href="/css/common/reset.css">
	<link rel="stylesheet" href="/css/common/ui-appearance.css">
	<link rel="stylesheet" href="/css/common/ui-layout.css">
	<link rel="stylesheet" href="/css/common/site.css">
	<link rel="stylesheet" href="/css/common/dialog.css">
	<!-- End of Development CSS -->
	
	<!-- Staging
	<link rel="stylesheet" href="/css/common/m.css">
	<!-- End of Staging CSS -->
	
	<!-- Production -->
	<link rel="stylesheet" href="/css/common/m.css">
	<!-- End of Production CSS -->
	
	<link rel="stylesheet" href="/css/pages/<?php echo $page['css']?>" data-page="<?php echo $_GET['page'];?>">
</head>
<body>
<div id="page"><!-- needed to normalise browser scroll element -->
	<section id="site-header">
		<a id="header-logo-link" href="/dashboard">
			<img src="/img/logo-t+a-dashboard.png" alt="Tech+Art Dashboard" />
		</a>
		<nav id="header-user-controls">
			<button type="button" id="header-start-button">Get Started</button>
			<a id="account-link" href="/account">?</a>
			<a id="logout-link" href="/login">Logout</a>
		</nav>
	</section>
	
	<section id="main" data-page="<?php echo $_GET['page'];?>">
		<?php require __DIR__.'/html/pages/'.$page['html'];?>
	</section>
	
	<section id="site-footer">
		<a id="footer-logo-link" href="http://www.techplusart.com" target="_blank">
			<img src="/img/logo-t+a.png" alt="Tech+Art" />
		</a>
		Making stuff simple since 2012.
	</section>
</div><!-- end of page -->
<div id="overlay"></div>
<noscript><p >This site requires JavaScript.</p></noscript>

<!-- Development
<script src="/js/0-env/childNode-polyfill-1.0.js"></script>
<script src="/js/0-env/closest-polyfill-2.0.1.js"></script>
<script src="/js/0-env/details-summary-polyfill-1.0.2.js"></script>
<script src="/js/0-env/dom3-custom-event-0.2.1.js"></script>
<script src="/js/0-env/dom-event-polyfill-1.0.0.js"></script>
<script src="/js/0-env/es6-array-polyfill-1.0.0.js"></script>
<script src="/js/0-env/es6-collections-polyfill-1.0.1.js"></script>
<script src="/js/0-env/es6-math-polyfill-1.0.0.js"></script>
<script src="/js/0-env/es6-number-polyfill-1.0.0.js"></script>
<script src="/js/0-env/es6-object-polyfill-1.0.0.js"></script>
<script src="/js/0-env/es6-string-polyfill-1.0.0.js"></script>
<script src="/js/0-env/fullscreen-1.0.1.js"></script>
<script src="/js/0-env/promise-2.1.0.js"></script>
<script src="/js/0-env/sessionstorage-polyfill-1.0.0.js"></script>
<script src="/js/0-env/setimmediate-1.0.2.js"></script>

<script src="/js/1-lib/modernizr-3.2.0-custom.js"></script>
<script src="/js/1-lib/eve-1.4.3.js"></script>
<script src="/js/1-lib/arya-1.0.2.js"></script>
<script src="/js/1-lib/domo-1.7.0.js"></script>
<script src="/js/1-lib/tove-1.0.3.js"></script>
<script src="/js/1-lib/ajax-1.1.1.js"></script>
<script src="/js/1-lib/tina-1.1.1.js"></script>
<script src="/js/1-lib/fila-1.0.0.js"></script>
<script src="/js/1-lib/GSAP-0global.js"></script>
<script src="/js/1-lib/GSAP-CSSPlugin-1.18.3.min.js"></script>
<script src="/js/1-lib/GSAP-EasePack-1.18.3.min.js"></script>
<script src="/js/1-lib/GSAP-TweenLite-1.18.3.min.js"></script>
<script src="/js/1-lib/localforage-1.4.0.js"></script>
<script src="/js/1-lib/sessionforage-1.0.0.js"></script>
<script src="/js/1-lib/dialog-2.1.1.js"></script>
<script src="/js/1-lib/paginator-1.1.0.js"></script>

<script src="/js/2-core/init.js"></script>
<script src="/js/2-core/controller.js"></script>
<script src="/js/2-core/sandbox.js"></script>
<script src="/js/2-core/extensions/utils.js"></script>
<script src="/js/2-core/extensions/local-db.js"></script>
<script src="/js/2-core/extensions/session-db.js"></script>
<script src="/js/2-core/extensions/dialog.js"></script>
<script src="/js/2-core/extensions/paginator.js"></script>
<script src="/js/2-core/extensions/user-api.js"></script>
<script src="/js/2-core/extensions/staff-api.js"></script>

<script src="/js/3-comp/common/page.js"></script>
<script src="/js/3-comp/common/site-header.js"></script>
<script src="/js/3-comp/common/site-footer.js"></script>
<!-- End of Development JS -->

<!-- Staging
<script src="/js/012.js"></script>
<!-- End of Staging JS -->

<!-- Production -->
<script src="/js/012.js"></script>
<!-- End of Production JS -->

<script src="/js/3-comp/pages/<?php echo $page['js']?>"></script>
</body>





