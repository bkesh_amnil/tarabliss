﻿
REM Leave first line empty because of UTF-8 BOM
ECHO off
cls

ECHO.
cd jshint
ECHO Starting JSHint...
ECHO ----------------------------------
START /b jshint --config package.json ../../js
jshint ../../private/pages.json