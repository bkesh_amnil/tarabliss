﻿
REM Leave first line empty because of potential UTF-8 BOM
ECHO off
cls

ECHO Cleanup and Directory Prep...
ECHO ------------------------
IF NOT EXIST "replaceText\html" mkdir replaceText\html
IF NOT EXIST "OUTPUT" mkdir OUTPUT
del /q OUTPUT\*.*
ECHO [OUTPUT] folder cleaned up
IF EXIST "OUTPUT\html" rd /s /q "OUTPUT\html"
mkdir OUTPUT\html
ECHO [OUTPUT]\[html] folder recreated
ECHO ------------------------
ECHO.

SET HTML_DEV_1="<!-- Development -->"
SET HTML_DEV_2="<!-- Development"
SET HTML_STAG_1="<!-- Staging -->"
SET HTML_STAG_2="<!-- Staging"
SET HTML_PROD_1="<!-- Production -->"
SET HTML_PROD_2="<!-- Production"
SET PHP_DEV_1="	///* Development"
SET PHP_DEV_2="	/* Development"
SET PHP_STAG_1="	///* Development"
SET PHP_STAG_2="	/* Development"
SET PHP_PROD_1="	///* Development"
SET PHP_PROD_2="	/* Development"

ECHO Replacing Text in index.php ...
ECHO -------------------------------
cd replaceText
copy /y ..\..\index.php html\index.php
cscript replaceText.vbs html\index.php %HTML_DEV_1% %HTML_DEV_2% >NUL
cscript replaceText.vbs html\index.php %HTML_STAG_1% %HTML_STAG_2% >NUL
cscript replaceText.vbs html\index.php %HTML_PROD_1% %HTML_PROD_2% >NUL
cscript replaceText.vbs html\index.php %HTML_PROD_2% %HTML_PROD_1% >NUL
cd ..
ECHO ------------------------
ECHO.

ECHO Minifying index.php ...
ECHO -------------------------
cd htmlcompressor
java -jar htmlcompressor-1.5.3.jar --preserve-php -p regex.txt --remove-intertag-spaces --remove-quotes  -o ..\OUTPUT\index.php ..\replaceText\html\index.php
cd ..
ECHO ------------------------
ECHO.

ECHO.
ECHO Replacing Text in HTML pages
ECHO -----------------------------
cd replaceText
xcopy /sy ..\..\html\*.html html
SETLOCAL EnableDelayedExpansion
FOR /R html %%i IN (*.html) DO (
	ECHO processing %%~nxi...
	cscript replaceText.vbs "%%i" !HTML_DEV_1! !HTML_DEV_2! >NUL
	cscript replaceText.vbs "%%i" !HTML_STAG_1! !HTML_STAG_2! >NUL
	cscript replaceText.vbs "%%i" !HTML_PROD_1! !HTML_PROD_2! >NUL
	cscript replaceText.vbs "%%i" !HTML_PROD_2! !HTML_PROD_1! >NUL
)
cd ..
ECHO ------------------------
ECHO.

ECHO.
ECHO Minifyng HTML pages
ECHO ------------------------
cd htmlcompressor
java -jar htmlcompressor-1.5.3.jar --preserve-php -p regex.txt -r --remove-intertag-spaces --remove-quotes  -o ..\OUTPUT\html\ ..\replaceText\html
cd ..
ECHO -----------------------
ECHO.

ECHO Replacing Text in api.php ...
ECHO ------------------------------
cd replaceText
copy /y ..\..\api.php html\api.php
cscript replaceText.vbs html\api.php %PHP_DEV_1% %PHP_DEV_2% >NUL
cscript replaceText.vbs html\api.php %PHP_STAG_1% %PHP_STAG_2% >NUL
cscript replaceText.vbs html\api.php %PHP_PROD_1% %PHP_PROD_2% >NUL
cscript replaceText.vbs html\api.php %PHP_PROD_2% %PHP_PROD_1% >NUL
cd ..
ECHO ------------------------
ECHO.

ECHO.
ECHO Copying other files
ECHO -----------------------------
xcopy /q "replaceText\html\api.php" "OUTPUT\"
xcopy /q "..\meta.php" "OUTPUT\"
xcopy /q "..\sitemap.php" "OUTPUT\"
xcopy /q "..\humans.txt" "OUTPUT\"
xcopy /q "..\robots.txt" "OUTPUT\"
xcopy /q /y "..\private\*" "OUTPUT\private\"
ECHO -----------------------------
ECHO.
PAUSE

