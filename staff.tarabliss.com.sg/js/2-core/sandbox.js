/***
 * Frontend Web App Framework v1.5.0 - Sandbox
 * =================================
 *
 * Manages DOM and events of components. There should be one sandbox per component.
 * - selects DOM elements - findOne(), findAll()
 * - handles DOM-level events - on(), off()
 * - handles component-level events - subscribe(), unsubscribe(), publish()
 ***/
(function(app) {
	'use strict';
	
	/**
	 * @param: {string} name of component
	 * @return: {object} sandbox
	 */
	function createSandbox(component) {
		// Find the component's DOM element
		var dom_component_element = document.querySelector('#'+component);
		if (dom_component_element === null) {
			throw new ReferenceError('app.createSandbox() - #' + component + ' is null');
		}
		
		// Data
		var dom_listeners = []; // each listener is an object literal {element, event, callback}
		var component_listeners = [];  // each listener is an object literal {event, callback}
		
		/**
		 * Gets the HTMLElement of the component
		 */
		function getComponentElement() {
			return dom_component_element;
		} //getComponentElement()
		
		/**
		 * Finds one element (relative to component element).
		 * @param: {string} selector
		 * @return: {HTMLElement | null } element, or null if no matches found.
		 */
		function findOne(selector, dom_from) {
			if (typeof dom_from === 'undefined') {
				dom_from = dom_component_element;
			}
			var x = dom_from.querySelector(selector);
			if (x === null) {
				console.warn('sandbox.findOne(): [' + component + '] ' + selector + ' - not found.');
			}
			return x;
		} //findOne()
		
		/**
		 * Finds elements (relative to component element).
		 * @param: {string} selector
		 * @return: {Array} list of HTMLElements, empty list if no matches found.
		 */
		function findAll(selector, dom_from) {
			if (typeof dom_from === 'undefined') {
				dom_from = dom_component_element;
			}
			var nodelist = dom_from.querySelectorAll(selector);
			var array = Array.from(nodelist);
			if (array.length === 0) {
				console.warn('sandbox.findAll(): [' + component + '] ' + selector + ' - not found.');
			}
			return array;
		} //findAll()
		
		/**
		 * Get all DOM event listeners of an element
		 * @param: {HTMLElement} elemtn
		 * @return: {Array} of object literals {element, event, callback}
		 */
		function getDOMListeners(element) {
			if (typeof element === 'undefined') {
				return dom_listeners;
			}
			
			function compareListenerElement(el) {
				return el.element === element;
			}
			return dom_listeners.filter(compareListenerElement);
		} //getDOMListeners()
		
		/**
		 * Binds callback function to an event on a DOM element, or an array of elements (each will be binded individually).
		 * @param: {HTMLElement | Array of HTMLElements} element,
		 *         {string} events (space separated),
		 *         {function} callback
		 */
		function on(element, events, callback) {
			// Handle Arrays
			if (element instanceof Array) {
				for (i=0, len=element.length; i<len; i++) {
					on(element[i], events, callback);
				}
				return;
			}

			// Process element param
			if (element === null) {
				throw new ReferenceError('sandbox.on() - element is null. (Event: ' + events + ')');
			}
			if (typeof element === 'undefined') {
				throw new ReferenceError('sandbox.on() - element is undefined. (Event: ' + events + ')');
			}
			if (! (element instanceof HTMLElement)) {
				throw new TypeError('sandbox.on() - element is not HTMLElement. ' + typeof element);
			}
			
			// Bind callbacks
			var i, len; //tmp variables for loops
			var events_array = events.split(' ');
			for (i=0, len=events_array.length; i<len; i++) {
				dom_listeners.push({
					element: element,
					event: events_array[i],
					callback: callback
				});
				element.addEventListener(events_array[i], callback);
			} // for each event
		} // on()
		
		/**
		 * Unbinds callback from an event on a DOM element.
		 * - if the same callback function was binded multiple times through on(), they will all be removed.
		 * @param: {HTMLElement | Array of HTMLElements} element - optional. If ommitted, all callbacks for all events of all elements will be removed.
		 *         {string} event - optional. If ommitted, all callbacks for all events will be removed
		 *         {function} callback - optional. If ommitted, all callbacks for the event will be removed.
		 */
		function off(element, event, callback) {
			var i, len; //tmp variables for loops
			
			// Handle Arrays
			if (element instanceof Array) {
				for (i=0, len=element.length; i<len; i++) {
					off(element[i], event, callback);
				}
				return;
			}

			// Handle multiple callbacks
			if (typeof callback === 'undefined') {
				return offAllCallbacks(element, event);
			}

			// Process element param
			if (element === null) {
				throw new ReferenceError('sandbox.off() - element is null. (Event: ' + event + ')');
			}
			if (typeof element === 'undefined') {
				throw new ReferenceError('sandbox.off() - element is undefined. (Event: ' + event + ')');
			}
			if (! (element instanceof HTMLElement)) {
				throw new TypeError('sandbox.off() - element is not HTMLElement. ' + typeof element);
			}
			
			// Remove a specific callback
			for (i=dom_listeners.length-1; i>=0; i--) {
				var listener = dom_listeners[i];
				if (listener.element === element && event === listener.event && callback === listener.callback) {
					element.removeEventListener(event, callback);
					dom_listeners.splice(i,1);
				}
			} // loop through all dom listeners
		} // off()
		
		/**
		 * @private
		 * Unbinds all callbacks from an event on a DOM element.
		 * @param: {HTMLElement} element,
		 *         {string} event,
		 */
		function offAllCallbacks(element, event) {
			if (typeof event === 'undefined') {
				return offAllEvents(element);
			}
			
			// Remove all callbacks for this element event.
			for (var i=dom_listeners.length-1; i>=0; i--) {
				var listener = dom_listeners[i];
				if (listener.element === element && event === listener.event) {
					dom_listeners.splice(i,1);
					listener.element.removeEventListener(listener.event, listener.callback);
				}
			} // loop through all dom listeners
		} // offAllCallbacks()
		
		/**
		 * @private
		 * Removes all dom_listeners made by this element
		 * @param: {HTMLElement} element
		 */
		function offAllEvents(element) {
			if (typeof element === 'undefined') {
				return offAllElements();
			}
			
			// Remove all callbacks for this element (any event)
			for (var i=dom_listeners.length-1; i>=0; i--) {
				var listener = dom_listeners[i];
				if (listener.element === element) {
					dom_listeners.splice(i,1);
					listener.element.removeEventListener(listener.event, listener.callback);
				}
			} // loop through all dom listeners
		} //offAllEvents()
		
		/**
		 * @private
		 * Removes all dom_listeners made by this module
		 */
		function offAllElements() {
			for (var i=dom_listeners.length-1; i>=0; i--) {
				var listener = dom_listeners[i];
				listener.element.removeEventListener(listener.event, listener.callback);
			}
			dom_listeners = [];
		} //offAllElements()
		
		/**
		 * Get all component event listeners
		 * @return: {Array} of object literals {event, callback}
		 */
		function getComponentListeners() {
			return component_listeners;
		}
		
		/**
		 * Adds a listener for a component-level event
		 * @param: {string} event, {function} callback
		 */
		function subscribe(event, callback) {
			// Param check
			if (typeof event !== 'string') {
				throw new TypeError('sandbox.subscribe() - event must be string (' + typeof event +')');
			}
			if (typeof callback !== 'function') {
				throw new TypeError('sandbox.subscribe() - callback must be function (' + typeof callback + ')');
			}
			
			component_listeners.push({
				event: event,
				callback: callback
			});			
		} //subscribe()
		
		/**
		 * Removes all component_listeners for an event / all events.
		 * @param: {string} event - optional. If ommitted, all events will be unsubscribed.
		 */
		function unsubscribe(event) {
			if (typeof event === 'undefined') {
				return unsubscribeAll();
			}
			
			// Param check
			if (typeof event !== 'string') {
				throw new TypeError('sandbox.unsubscribe() - event must be string (' + typeof event +')');
			}
			
			// Remove listeners of event
			component_listeners = component_listeners.filter(function(el){
				return el.event !== event;
			});
		} //unsubscribe()
		
		/**
		 * @private
		 * Removes all component_listeners made by this module
		 */
		function unsubscribeAll() {
			component_listeners = [];
		} // unsubscribeAll()
		
		/**
		 * Publishes an event to other components
		 * @param: {string} event, {anything} data
		 */
		function publish(event, data) {
			app.controller.broadcastComponentEvent(event, data);
		} //publish()
		
		/**
		 * Attempts to switch page / fragment
		 * @param: {string} path - (including hash)
		 *         {bool} remember_history - whether to store this page in browser history. Default: true
		 * @return: {Promise}
		 */
		function navigate(path, remember_history) {
			// Set defaults
			if (typeof path !== 'string') {
				throw new TypeError('sandbox.navigate() - path must be string (' + typeof path +')');
			}
			if (typeof remember_history === 'undefined') {
				remember_history = true;
			}
			
			// Find out target page and fragment
			var page, hash;
			var hashIndex = path.indexOf('#');
			if (hashIndex >= 0) {
				page = path.substring(0, hashIndex);
				hash = path.substring(hashIndex);
			}
			else {
				page = path;
				hash = '';
			}
			
			// Remove any absolute path
			if (page.startsWith(app.settings.DOMAIN_URL+'/')) {
				page = page.substring(app.settings.DOMAIN_URL.length + 1);
			}
			
			// Check if we need to switch pages
			if (page === '' || app.controller.getCurrentPage() === page) {
				// Go to fragment on the same page
				return app.controller.switchFragment(hash);
			} // already on this page
			else {
				return app.controller.switchPage(page, hash, remember_history);
			}
		} //navigate()
		
		var sandbox = {
			// App Settings
			settings: app.settings,

			// Navigation
			navigate: navigate,
			showOverlay: app.controller.showOverlay,
			hideOverlay: app.controller.hideOverlay,
			getCurrentPage: app.controller.getCurrentPage,
			getCurrentFragment: app.controller.getCurrentFragment,
			getCurrentOverlay: app.controller.getCurrentOverlay,
			getURLOrigin: app.controller.getURLOrigin,
			getURLQueryStringValue: app.controller.getURLQueryStringValue,
			
			// DOM selection
			getComponentElement: getComponentElement,
			findOne: findOne,
			findAll: findAll,
			
			// DOM events
			getDOMListeners: getDOMListeners,
			on: on,
			off: off,
			
			// Component events
			getComponentListeners: getComponentListeners,
			subscribe: subscribe,
			unsubscribe: unsubscribe,
			publish: publish
		};
		
		// Each extension should also be exposed by the sandbox (be careful of namespacing)
		for (var key in app.extensions) {
			if (app.extensions.hasOwnProperty(key)) {
				sandbox[key] = app.extensions[key];
			}
		}
		
		return sandbox;
	} //createSandbox()
	
	app.createSandbox = createSandbox;
})(this.app);
