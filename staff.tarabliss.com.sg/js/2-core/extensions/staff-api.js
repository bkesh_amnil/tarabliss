/**
 * Frontend Web App Framework v1.5.0 - App Extension: Staff User API functions
 * =================================
 *
 * Dependencies (libraries):
 *   - Ajax 1.1.1
 *   - Sessionforage 1.0.0
 *
 * Each function returns an ES-6 Promise that resolves/rejects with an object {status, data}.
 * - Our server returns JSON, so all server responses will be converted to JSONs.
 */
(function(extensions, ajax, localforage, sessionforage, SERVER_URL) {
	'use strict';
	
	// Config settings
	var API_URL_PREFIX = SERVER_URL + '/staff';
	
	/******************
	 * Account Access *
	 ******************/
	/**
	 * Logs in the user and stores session user, CSRF token & persistent token
	 * @param: {FormData | object literal} e.g. 
	 *             {email: 'test@exmample.com', password: 'password'}
	 */
	function login(data) {
		return ajaxRequest('POST', '/login', data).then(function(response_data) {
			var handleSessionUser = storeSessionUser(response_data['staff']);
			var handleCSRFToken = storeCSRFToken(response_data['csrf-token']);
			var handlePersistentToken;
			if (response_data['persistent-login-token']) {
				handlePersistentToken = storePersistentLoginCredentials({
					email: response_data['staff'].email,
					token: response_data['persistent-login-token']
				});
			}
			else {
				handlePersistentToken = clearPersistentLoginCredentials();
			}
			
			return Promise.all([handleSessionUser, handleCSRFToken, handlePersistentToken]).then(function() {
				return response_data;
			});
		});
	}

	/**
	 * Logs in the user using the persistent login token stored on the computer.
	 * A new persistent token will be issued.
	 */
	function persistentLogin() {
		return retrievePersistentLoginCredentials().then(function(creds) {
			if (creds === null) {
				throw new Error('No stored persistent login credentials');
			}
			else {
				return ajaxRequest('POST', '/persistentLogin', creds);
			}
		}).then(function(response_data) {
			var handleSessionUser = storeSessionUser(response_data['staff']);
			var handleCSRFToken = storeCSRFToken(response_data['csrf-token']);
			var handlePersistentToken = storePersistentLoginCredentials({
				email: response_data['staff'].email,
				token: response_data['persistent-login-token']
			});
			
			return Promise.all([handleSessionUser, handleCSRFToken, handlePersistentToken]).then(function() {
				return response_data;
			});
		}).catch(function(response_error) {
			return clearPersistentLoginCredentials().then(function() {
				throw response_error;
			});
		});
	} //persistentLogin()

	/**
	 * Logs out the user, removes session user, CSRF token & persistent token from local / session storage
	 */
	function logout() {
		return ajaxRequest('POST', '/logout').then(function(response_data) {
			var handleSessionUser = clearSessionUser();
			var handleCSRFToken = clearCSRFToken();
			var handlePersistentToken = clearPersistentLoginCredentials();

			return Promise.all([handleSessionUser, handleCSRFToken, handlePersistentToken]).then(function() {
				return response_data;
			});
		}, function(response_error){
			throw response_error;
		});
	}
	
	/**
	 * Forgets password of user
	 * @param: {FormData | object literal} e.g. 
	 *                       {email: 'johndoe@gmail.com'}
	 */
	function forgetPassword(data) {
		return ajaxRequest('POST', '/forgetPassword', data);
	}

	/**
	 * Verify email with a 6-digit code (part of registration or reset password process), and stores session user & CSRF token
	 * - Note: User will be considered authenticated (logged in) upon success.
	 * @param: {FormData | object literal} e.g. 
	 *                       {email: 'johndoe@gmail.com', code:"123456"}
	 */
	function verifyEmail(data) {
		return ajaxRequest('POST', '/verifyEmail', data).then(function(response_data) {
			var handleSessionUser = storeSessionUser(response_data['staff']);
			var handleCSRFToken = storeCSRFToken(response_data['csrf-token']);
			var handlePersistentToken = clearPersistentLoginCredentials();
						
			return Promise.all([handleSessionUser, handleCSRFToken, handlePersistentToken]).then(function() {
				return response_data;
			});
		}, function(response_error){
			throw response_error;
		});
	}

	/**
	 * Changes password of session staff user
	 * @param: {FormData | object literal} e.g. 
	 *             {oldPassword: 'password', newPassword: 'newPassw0rD'}
	 */
	function changePassword(data) {
		return ajaxRequest('POST', '/changePassword', data);
	}

	/**
	 * Edits account data of session staff user
	 * @param: {FormData | object literal} e.g. 
	 *             {email: 'test@exmample.com', firstName: 'John', lastName: 'Doe'}
	 */
	function editAccount(data) {
		return ajaxRequest('POST', '/editAccount', data).then(function(response_data) {
			return storeSessionUser(response_data['staff']).then(function() {
				return response_data;
			});
		}, function(response_error){
			throw response_error;
		});
	}

	/***************
	 * Settings *
	 ***************/
	/**
	 * @param: {FormData | object literal} e.g. 
	 *             {contact-email: 'contact@example.com', 'paypal-email': 'money@example.com'}
	 */
	function editSettings(data) {
		return ajaxRequest('POST', '/editSettings', data);
	}

	/***********
	 * Members *
	 ***********/
	function getMemberStats() {
		return ajaxRequest('GET', '/getMemberStats');
	}

	/**
	 * #param: {FormData | object literal} e.g. 
	 *           { conditions: '"[ 
	 *                             [
	 *                               ['emailVerified', '=', true],
	 *                               ['gender',        '=', 'Male']
	 *                             ],
	 *                             [
	 *                               ['emailVerified', '=', true],
	 *                               ['gender',        'IS', null]
	 *                             ]
	 *                           ]"',
	 *                 offset: 0,
	 *                  count: 16,
	 *                   sort: 'createdTime',
	 *                  order: 'ASC'
	 *           }
	 */
	function getMembers(data) {
		return ajaxRequest('GET', '/getMembers', data);
	}

	function getMemberCount(data) {
		return ajaxRequest('GET', '/getMemberCount', data);
	}

	/**
	 * @param: {FormData | object literal} e.g. 
	 *             {name: "Johnny", gender:"Male", phone:"98367452", birth-day:21, birth-month:3, birth-year:1998, email:"john@ex.com", email-verified: "1", password:"acn84uca"}
	 */
	function addMember(data) {
		return ajaxRequest('POST', '/addMember', data);
	}

	/**
	 * @param: {FormData | object literal} e.g. 
	 *             {id: 5, name: "Johnny", gender:"Male", phone:"98367452", birth-day:21, birth-month:3, birth-year:1998 }
	 */
	function editMember(data) {
		return ajaxRequest('POST', '/editMember', data);
	}

	/**
	 * @param: {FormData | object literal} e.g. 
	 *             {id: 5}
	 */
	function disableMember(data) {
		return ajaxRequest('POST', '/disableMember', data);
	}

	/**
	 * @param: {FormData | object literal} e.g. 
	 *             {id: 5, password: "ac4n89#$gqF"}
	 */
	function setMemberPassword(data) {
		return ajaxRequest('POST', '/setMemberPassword', data);
	}

	/**********
	 *  Staffs *
	 **********/
	/**
	 * #param: {FormData | object literal} e.g. 
	 *           { conditions: '"[ 
	 *                             [
	 *                               ['emailVerified', '=', true],
	 *                               ['gender',        '=', 'Male']
	 *                             ],
	 *                             [
	 *                               ['emailVerified', '=', true],
	 *                               ['gender',        'IS', null]
	 *                             ]
	 *                           ]"',
	 *                 offset: 0,
	 *                  count: 16,
	 *                   sort: 'createdTime',
	 *                  order: 'ASC'
	 *           }
	 */
	function getStaffs(data) {
		return ajaxRequest('GET', '/getStaffs', data);
	}
	function getStaffStats() {
		return ajaxRequest('GET', '/getStaffStats');
	}
	function getLastActivity() {
		return ajaxRequest('GET', '/getLastActivity');
	}

	/**
	 * @param: {FormData | object literal} e.g. 
	 *             {name: "Johnny", gender:"Male", phone:"98367452", email:"john@ex.com", email-verified: "1", password:"acn84uca"}
	 */
	function addStaff(data) {
		return ajaxRequest('POST', '/addStaff', data);
	}

	/**
	 * @param: {FormData | object literal} e.g. 
	 *             {id: 5, name: "Johnny", gender:"Male", phone:"98367452"}
	 */
	function editStaff(data) {
		return ajaxRequest('POST', '/editStaff', data);
	}

	/**
	 * @param: {FormData | object literal} e.g. 
	 *             {id: 5}
	 */
	function disableStaff(data) {
		return ajaxRequest('POST', '/disableStaff', data);
	}

	/**
	 * @param: {FormData | object literal} e.g. 
	 *             {id: 5, password: "ac4n89#$gqF"}
	 */
	function setStaffPassword(data) {
		return ajaxRequest('POST', '/setStaffPassword', data);
	}

	/************
	 *  Images  *
	 ************/
	/**
	 * @param: {FormData}
	 *            {domainLabel: "www", fileName: '1.jpg', title:"One", description: 'Nice shot', copyright: 'Copyright John Doe'}
	 */
	function addImage(data) {
		return ajaxRequest('POST', '/addImage', data);
	}

	function getImages() {
		return ajaxRequest('GET', '/getImages');
	}

	function editImage(data) {
		return ajaxRequest('POST', '/editImage', data);
	}
	function removeImage(data) {
		return ajaxRequest('POST', '/removeImage', data);
	}

	/*************
	 *   Pages   *
	 *************/
	/**
	 * @param: {FormData | object literal} e.g. 
	 *             {domain: 'www.example.com', page: 'home'}
	 */
	function getPage(data) {
		return ajaxRequest('GET', '/getPage', data);
	}

	/**
	 * @param: {FormData | object literal} e.g. 
	 *           {  domain: 'www.example.com',
	 *                page: 'home',
	 *          'template': '<h1>Welcome, {{name}}}</h1><p>{{about}}</p>',
	 *              'data': '{"name":"John","about":"Lorem ipsum..."}'
	 *             'title': 'Home Page',
	 *       'description': 'This is home',
	 *               'img': 'home.jpg' (optional)
	 *           }
	 */
	function editPage(data) {
		return ajaxRequest('POST', '/editPage', data);
	}


	/****************************************
	 * @private - local and session storage *
	 ****************************************/
	function storeCSRFToken(token) {
		return localforage.setItem('csrf-token', token);
	}
	function retrieveCSRFToken() {
		return localforage.getItem('csrf-token');
	}
	function clearCSRFToken() {
		return localforage.removeItem('csrf-token');
	}
	function storePersistentLoginCredentials(creds) {
		return localforage.setItem('persistent-login-credentials', creds);
	}
	function retrievePersistentLoginCredentials() {
		return localforage.getItem('persistent-login-credentials');
	}
	function clearPersistentLoginCredentials() {
		return localforage.removeItem('persistent-login-credentials');
	}

	function storeSessionUser(user) {
		return sessionforage.setItem('session-user', user);
	}
	function clearSessionUser() {
		return sessionforage.removeItem('session-user');
	}

	/**
	 * @private
	 * Makes an AJAX request to specified end-point
	 * @param: method (string) - POST / GET,
	 *         endpoint (string) - e.g. '/login',
	 *         data (object) - typically serialized from a form
	 * @return: (Promise) - resolves with response data object, rejects with response Error object
	 */
	function ajaxRequest(method, endpoint, data) {
		if (!endpoint.startsWith('/')) {
			endpoint = '/' + endpoint;
		}
		
		// Prepare request settings
		var settings = {
			method: method,
			url: API_URL_PREFIX + endpoint,
			data: data,
			returnType: 'json'
		};
		
		// Add any CSRF Token to header and make request
		return retrieveCSRFToken().then(function(csrf_token){
			if (csrf_token !== null) {
				settings.headers = {'CSRF-TOKEN': csrf_token};
			}
			
			// Make request and parse result into JSON
			return ajax.request(settings).then(function(result){
				try {
					return JSON.parse(result.responseText);
				}
				catch (ex) {
					//alert('ERROR: (AJAX SUCCESS) - Please take a screenshot.\r\n' + result + '\r\n' + result.responseText + '\r\n' + result.status + '\r\n' + API_URL_PREFIX); // DEBUG
					throw new Error('Unable to parse server response.');
				}
			}, function(result) {
				var result_json;
				try {
					result_json = JSON.parse(result.responseText);
				}
				catch (ex) {
					//alert('ERROR: (AJAX FAIL) - Please take a screenshot.\r\n' + result + '\r\n' + result.responseText + '\r\n' + result.status + '\r\n' + API_URL_PREFIX); // DEBUG
					throw new Error('Unable to parse server response.');
				}
				throw new Error(result_json['error']);
			});
		});
	} //ajaxRequest()

	/*********************************
	 * End of boilerplate for v1.5.0 *
	 *********************************/
	/**
	 * @param: {FormData}
	 *             {offset: 8, count: 8}
	 */
	function getActivities(data) {
		return ajaxRequest('GET', '/getActivities', data);
	}

	/**
	 * @param: {FormData | object literal} e.g. 
	 *              { conditions: '"[ 
	 *                             [
	 *                               ['price', '<', 12000],
	 *                             ],
	 *                             [
	 *                               ['promotionPrice, '<', 12000]
	 *                             ]
	 *                           ]"',
	 *                 offset: 0,
	 *                  count: 16,
	 *                   sort: 'createdTime',
	 *                  order: 'ASC'
	 *           }
	 */
	function getTherapies(data) {
		return ajaxRequest('GET', '/getTherapies', data);
	}

	/**
	 * @param: {FormData}
	 *            {name: "something", description: 'lorem', benefits:"ipsum", duration: 90, price: "38000", promotionPrice: "", promotionStartTime: "", promotionEndTime: ""}
	 */
	function addTherapy(data) {
		return ajaxRequest('POST', '/addTherapy', data);
	}

	/**
	 * @param: {FormData}
	 *            {name: "something", description: 'lorem', benefits:"ipsum", duration: 90, price: "38000", promotionPrice: "", promotionStartTime: "", promotionEndTime: ""}
	 */
	function editTherapy(data) {
		return ajaxRequest('POST', '/editTherapy', data);
	}
        
	function getEventGallery(data) {
		return ajaxRequest('GET', '/getEventGallery', data);
	}
	function addEventGallery(data) {
		return ajaxRequest('POST', '/addEventGallery', data);
	}
	function editEventGallery(data) {
		return ajaxRequest('POST', '/editEventGallery', data);
	}
	function getEvents(data) {
		return ajaxRequest('GET', '/getEvents', data);
	}
	function addEvent(data) {
		return ajaxRequest('POST', '/addEvent', data);
	}
	function editEvent(data) {
		return ajaxRequest('POST', '/editEvent', data);
	}

	function getCategories(data) {
		return ajaxRequest('GET', '/getCategories', data);
	}
	function addCategory(data) {
		return ajaxRequest('POST', '/addCategory', data);
	}
	function editCategory(data) {
		return ajaxRequest('POST', '/editCategory', data);
	}

	function getSubCategories(data) {
		return ajaxRequest('GET', '/getSubCategories', data);
	}
	function addSubCategory(data) {
		return ajaxRequest('POST', '/addSubCategory', data);
	}
	function editSubCategory(data) {
		return ajaxRequest('POST', '/editSubCategory', data);
	}

	function getArticles(data) {
		return ajaxRequest('GET', '/getArticles', data);
	}
	function addArticle(data) {
		return ajaxRequest('POST', '/addArticle', data);
	}
	function editArticle(data) {
		return ajaxRequest('POST', '/editArticle', data);
	}
	function extractArticleData(data) {
		return ajaxRequest('POST', '/extractArticleData', data);
	}

	function getAccolades(data) {
		return ajaxRequest('GET', '/getAccolades', data);
	}
	function addAccolade(data) {
		return ajaxRequest('POST', '/addAccolade', data);
	}
	function editAccolade(data) {
		return ajaxRequest('POST', '/editAccolade', data);
	}

	function getJobListings(data) {
		return ajaxRequest('GET', '/getJobListings', data);
	}
	function addJobListing(data) {
		return ajaxRequest('POST', '/addJobListing', data);
	}
	function editJobListing(data) {
		return ajaxRequest('POST', '/editJobListing', data);
	}
	
	extensions.staffAPI = {
		// account methods
		login: login,
		persistentLogin: persistentLogin,
		logout: logout,
		forgetPassword: forgetPassword,
		verifyEmail: verifyEmail,
		changePassword: changePassword,
		editAccount: editAccount,

		// members
		getMemberStats: getMemberStats,
		getMembers: getMembers,
		getMemberCount: getMemberCount,
		addMember: addMember,
		editMember: editMember,
		disableMember: disableMember,
		setMemberPassword: setMemberPassword,

		// staffs
		getStaffStats: getStaffStats,
		getStaffs: getStaffs,
		getLastActivity: getLastActivity,
		addStaff: addStaff,
		editStaff: editStaff,
		disableStaff: disableStaff,
		setStaffPassword: setStaffPassword,

		// images
		addImage: addImage,
		getImages: getImages,
		editImage: editImage,
		removeImage: removeImage,

		// pages
		getPage: getPage,
		editPage: editPage,

		// app settings
		editSettings: editSettings,

		/*** End of boilerplate for v1.5.0 ***/
		
		// paged activities
		getActivities: getActivities,

		// therapies
		getTherapies: getTherapies,
		addTherapy: addTherapy,
		editTherapy: editTherapy,
		getEventGallery: getEventGallery,
		addEventGallery: addEventGallery,
		editEventGallery: editEventGallery,
		getEvents: getEvents,
		addEvent: addEvent,
		editEvent: editEvent,
		getCategories: getCategories,
		addCategory: addCategory,
		editCategory: editCategory,
		getSubCategories: getSubCategories,
		addSubCategory: addSubCategory,
		editSubCategory: editSubCategory,

		// articles
		getArticles: getArticles,
		addArticle: addArticle,
		editArticle: editArticle,
		extractArticleData: extractArticleData,

		// accolades
		getAccolades: getAccolades,
		addAccolade: addAccolade,
		editAccolade: editAccolade,

		// careers
		getJobListings: getJobListings,
		addJobListing: addJobListing,
		editJobListing: editJobListing
	};
})(app.extensions = app.extensions || {}, this.ajax, this.localforage, this.sessionforage, app.settings.SERVER_URL);
