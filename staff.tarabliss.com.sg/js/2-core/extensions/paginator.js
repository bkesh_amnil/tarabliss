/**
 * Frontend Web App Framework v1.5.0 - App Extension: Paginator
 * =================================
 *
 * Utility to generate pagination buttons.
 *
 * Dependencies (libraries):
 *  - paginator-1.0.1
 */
(function(extensions, paginator) {
	'use strict';

	// Settings
	var MAX_BUTTONS = 5;
	var SHOW_FIRST_LAST = true;
	var SHOW_PREV_NEXT = true;
	var SHOW_PREV_NEXT_5 = false;
	var SHOW_PREV_NEXT_10 = false;

	/**
	 * Generate a list of buttons for a number of pages.
	 * - All generated page buttons have class "page-button"
	 * - The current page will also have class "current"
	 * - A "prev-button" will be prepended to the page buttons.
	 * - A "next-button" will be appended to the page buttons.
	 * @param: {HTMLElement} the element to contain pagination buttons
	 *         {object} options
	 *                 - {number} pageCount
	 *                 - {number} currPage
	 *                 - {number} maxButtons - maximum number of buttons to show (excluding prev, next, first, last) (DEFAULT: 9)
	 *                 - {bool} showFirst    - whether to show a button for first page       (DEFAULT: true)
	 *                 - {bool} showLast     - whether to show a button for last page        (DEFAULT: true)
	 *                 - {bool} showPrev     - whether to show a button for previous page    (DEFAULT: true)
	 *                 - {bool} showNext     - whether to show a button for next page        (DEFAULT: true)
	 *                 - {bool} showPrev5    - whether to show a button for 5 pages back     (DEFAULT: true)
	 *                 - {bool} showNext5    - whether to show a button for 5 pages forward  (DEFAULT: true)
	 *                 - {bool} showPrev10   - whether to show a button for 10 pages back    (DEFAULT: false)
	 *                 - {bool} showNext10   - whether to show a button for 10 pages forward (DEFAULT: false)
	 */
	function init(dom_elem, options) {
		options['maxButtons'] = MAX_BUTTONS;
		options['showFirst'] = SHOW_FIRST_LAST;
		options['showLast'] = SHOW_FIRST_LAST;
		options['showPrev'] =SHOW_PREV_NEXT;
		options['showNext'] =SHOW_PREV_NEXT;
		options['showPrev5'] =SHOW_PREV_NEXT_5;
		options['showNext5'] =SHOW_PREV_NEXT_5;
		options['showPrev10'] =SHOW_PREV_NEXT_10;
		options['showNext10'] =SHOW_PREV_NEXT_10;

		return paginator.init(dom_elem, options);
	}

	extensions.paginator = {
		init: init
	};
})(this.app.extensions = this.app.extensions || {}, this.paginator);
