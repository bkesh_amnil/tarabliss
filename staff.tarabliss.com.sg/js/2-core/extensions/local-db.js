/**
 * Frontend Web App Framework v1.5.0 - App Extension: LocalDB
 * =================================
 *
 * Asynchronous API for client-side storage
 *   - getItem(key)
 *   - setItem(key, value)
 *   - removeItem(key)
 *   - getType()
 *
 * Features:
 *   - Will select the best underlying driver available [IndexedDB, WebSQL, LocalStorage].
 *   - Can store any object type (not just strings).
 *   - Can use EITHER callbacks OR ES6-compliant promises (preferred), depending on whether callback functions are passed in as parameters
 *     Note: if using callbacks, all callbacks are Node-style (error argument first).
 *     Note: if using promises, each function returns an ES6-compliant Promise that resolves with appropriate values.
 *
 * Dependencies (libraries):
 *   - localforage 1.4.0
 *
 * Before use:
 *   - set config settings
 */
(function(extensions, localforage) {
	'use strict';
	
	/**
	 * Returns the underlying driver for storage, one of: [LocalStorage, IndexedDB, WebSQL]
	 * @return: {string}
	 */
	function getType() {
		var driver_type = localforage.driver();
		if (driver_type === localforage.LOCALSTORAGE) {
			return 'LocalStorage';
		}
		else if (driver_type === localforage.INDEXEDDB) {
			return 'IndexedDB';
		}
		else if (driver_type === localforage.WEBSQL) {
			return 'WebSQL';
		}
		else {
			return 'Unknown';
		}
	} //getType()
	
	/**
	 * @return: {Promise} resolves with value
	 */
	function getItem(key) {
		return localforage.getItem(key);
	}
	
	/**
	 * @return: {Promise} resolves with value
	 */
	function setItem(key, value) {
		return localforage.setItem(key, value);
	}
	
	/**
	 * @return: {Promise} resolves with null
	 */
	function removeItem(key) {
		return localforage.removeItem(key);
	}
	
	/* we really dont need to expose these to components
	function clear(callback) {
		return localforage.clear(callback);
	}
	
	function length(callback) {
		return localforage.length(callback);
	}
	
	function keys(callback) {
		return localforage.keys(callback);
	}
	
	function iterate(iterator, callback) {
		return localforage.iterate(iterator, callback);
	}
	*/
	
	/********
	 * Init *
	 ********/
	// init localforage with config settings
	localforage.config({
		//driver: [localforage.LOCALSTORAGE, localforage.WEBSQL, localforage.INDEXEDDB], // bug in FF35 causing IndexedDB to fail? :(
		name: 'Example',        	// may appear during storage limit prompts. Useful to use the name of your app here.
		storeName: 'example_app'	// must be alphanumeric, with underscores allowed. Default: 'keyvaluepairs'
	});
	
	// Return only the part of localforage we need to expose to components
	extensions.localDB = {
		getType: getType,
		getItem: getItem, get: getItem,
		setItem: setItem, set: setItem,
		removeItem: removeItem, remove: removeItem/*,
		clear: clear,
		length: length,
		key: key,
		keys: keys,
		iterate: iterate
		*/
	};
})(app.extensions = app.extensions || {}, this.localforage);
