/**
 * Frontend Web App Framework v1.5.0 - App Extension: Utility functions
 * =================================
 * 
 * Dependencies (libraries):
 *   - Eve 1.3.2
 *   - Arya 1.0.2
 *   - Domo 1.7.0
 *   - Tove 1.0.2
 *   - Ajax 1.1.1
 *   - Tina 1.1.1
 *   - Fila 1.0.0
 *   - TweenLite 1.18.3
 *
 * Required App Settings:
 *   - MIN_NAME_LENGTH
 *   - MIN_PASWORD_LENGTH
 *   - CODE_LENGTH
 *   - PHONE_LENGTH
 *
 * Before use:
 *   - extend with other utils (after boilerplate end)
 */ 	
(function(extensions, settings, eve, arya, domo, tove, ajax, tina, fila, TweenLite) {
	'use strict';
	
	// DOM Elements - global
	var dom_page = document.getElementById('page');
	
	// Settings
	var SCROLL_DURATION = 300; // ought to be same as controller
	
	/**********************
	 * DOM style, content *
	 **********************/
	function appendHTML(elem, html) {
		elem.insertAdjacentHTML('beforeend', html);
	}	
	function prependHTML(elem, html) {
		elem.insertAdjacentHTML('afterbegin', html);
	}
	function createElement(tag) {
		return document.createElement(tag);
	}
	function selectText(elem) {
		return domo.selectText(elem);
	}

	/**
	 * @param: {Node}
	 *         {string}
	 */
	function wrapNode(node, tag_name) {
		return domo.wrapNode(node, tag_name);
	}

	/**
	 * Converts a string containing HTML into DOM Elements
	 * @param: {string}
	 * @return: {array} of HTMLElement
	 */
	function str2dom(str) {
		return domo.str2dom(str);
	}

	/**
	 * Shows or hides input/select/textarea and its label
	 *  - Note: radios and checkboxes NEED to have a <label> immediately after <input>. All other elements have <label> immediately before.
	 *  - Note: needs ".hide" CSS to work
	 * @param: {HTMLInputElement / HTMLTextAreaElement / HTMLSelectElememt} 
	 */ 
	function hideFormElement(dom_elem) {
		var dom_label;
		if (dom_elem.type === 'checkbox' || dom_elem.type === 'radio') {
			dom_label = dom_elem.nextElementSibling;
		} // radios and checkboxes have labels after the input
		else {
			dom_label = dom_elem.previousElementSibling;
		} 

		//dom_elem.disabled = true; // dont disable, else the values wont get POSTed
		dom_elem.classList.add('hide');
		dom_label.classList.add('hide');
	}
	function showFormElement(dom_elem) {
		var dom_label;
		if (dom_elem.type === 'checkbox' || dom_elem.type === 'radio') {
			dom_label = dom_elem.nextElementSibling;
		}
		else {
			dom_label = dom_elem.previousElementSibling;
		}

		dom_elem.disabled = false;
		dom_elem.classList.remove('hide');
		dom_label.classList.remove('hide');
	}

	/**
	 * @param: {HTMLSelectElement}
	 *         {object literal} options in the format of "value": "Text to show"
	 */
	function populateSelect(dom_select, options) {
		return domo.populateSelect(dom_select, options);
	} // populateSelect()

	/**
	 * @param: {HTMLElement} div that is going to contain the checkboxes
	 *         {object literal} options in the format of "value": "Text to show"
	 *         {string} name of the checkboxes
	 */
	function populateCheckboxes(dom_div, options, name) {
		return domo.populateCheckboxes(dom_div, options, name);
	} //populateCheckboxes()

	/**
	 * @param: {HTMLElement} div that is going to contain the options
	 *         {array} of strings
	 */
	function populateDatalist(dom_datalist, options) {
		var html = '';
		for (var i=0,len=options.length; i<len; i++) {
			html += '<option value="'+options[i]+'">';
		}
		dom_datalist.innerHTML = html;
	} //populateDatalist()

	/**
	 * Removes options from a select
	 * @param: {HTMLSelectElement}
	 *         {string} value of the option
	 */
	function removeSelectOption(dom_select, value) {
		for (var i = dom_select.length - 1; i >= 0; --i) {
			if (dom_select[i].value === value) {
				dom_select.removeChild(dom_select[i]);
		    }
		}
	} //removeSelectOption()

	/**
	 * Removes a checkbox from a div of checkboxes
	 *  - Note: checkboxes NEED to have a <label> immediately after <input>.
	 * @param: {HTMLElement} div that is going to contain the checkboxes
	 *         {string} value of the checkbox
	 */
	function removeCheckbox(dom_div, value) {
		var dom_checkboxes = [].slice.call(dom_div.querySelectorAll('input[type=checkbox]'));

		for (var i = dom_checkboxes.length - 1; i >= 0; --i) {

			if (dom_checkboxes[i].value === value) {
				var dom_parent = dom_checkboxes[i].parentNode;	
				var dom_label = dom_checkboxes[i].nextElementSibling;
				dom_parent.removeChild(dom_checkboxes[i]);
				dom_parent.removeChild(dom_label);
		    }
		}
	} //removeCheckbox()

	/**
	 * - makes all <input> and <textarea> in the form READONLY
	 * - disables all options other than the current selected one in all <select>
	 * - disables all submit <button>
	 */
	function disableForm(dom_form) {
		return domo.disableForm(dom_form);
	} //disableForm()

	function enableForm(dom_form) {
		return domo.enableForm(dom_form);
	} //enableForm()

	function isDisabledForm(dom_form) {
		return domo.isDisabledForm(dom_form);
	}

	/**
	 * Removes block elements with no data 
	  * @param: {HTMLElement} 
	 */
	function removeEmptyElementDescendants(dom_root) {
		return domo.removeEmptyElementDescendants(dom_root);
	}

	/**
	 * @param: {Node}
	 */
	function removeNode(node) {
		return domo.removeNode(node);
	}

	/**
	 * Removes all trailing <br> elements
	 */
	function removeBrChildren(dom_root) {
		return domo.removeBrChildren(dom_root);
	}

	/**
	 * Checks for self-closing elements
	 * @param: {HTMLElement}
	 * @return {bool}
	 */
	function isSelfClosingElement(dom_elem) {
		return domo.isSelfClosingElement(dom_elem);
	}

	/**********************
	 * DOM global, window *
	 **********************/
	function getWindowWidth() {
		return window.innerWidth;
	}
	function getWindowHeight() {
		return window.innerHeight;
	}
	function getPageWidth() {
		return dom_page.clientWidth;
	}
	function getPageHeight() {
		return dom_page.clientHeight;
	}
	function getPageScrollX() {
		return dom_page.scrollLeft;
	}
	function getPageScrollY() {
		return dom_page.scrollTop;
	}
	function getPageScrollHeight() {
		return dom_page.scrollHeight;
	}
	function getPageScrollWidth() {
		return dom_page.scrollWidth;
	}
	function lockScroll() {
		return domo.lockScroll(dom_page);
	}
	function unlockScroll() {
		return domo.unlockScroll(dom_page);
	}
	
	function requestFullScreen(elem) {
		if (!(elem instanceof HTMLElement)) {
			elem = dom_page;
		}
		
		return elem.requestFullscreen();
	}

	function exitFullScreen() {
		return document.exitFullscreen();
	}

	function isFullScreen(elem) {
		if (!(elem instanceof HTMLElement)) {
			elem = dom_page;
		}

		return document.fullscreenEnabled && document.fullscreenElement === elem;	
	}

	/**
	 * Opens a client's email app and optionally fills in subject and body
	 */
	function mailto(email, subject, body) {
		// Make URLs
		var mailto_url = 'mailto:' + email;
		if (subject) {
			mailto_url += '?subject=' + eve.encodeURL(subject);
			if (body) {
				mailto_url += '&body=' + eve.encodeURL(body);
			}
		}
		
		// Trigger email app open
		var new_window = window.open(mailto_url, '_blank');
		if (new_window && new_window.open && !new_window.closed) {
			new_window.close();
		}
	} //mailto
	
	/*****************
	 * DOM animation *
	 *****************/
	/**
	 * @param: {HTMLElement} elem
	 *         {number} duration - in ms
	 *         {object} properties - key-value pairs
	 * @return: {Promise}
	 */
	function animate(elem, duration, properties, callback) {
		if (typeof callback === 'function') {
			properties.onComplete = callback;
			return TweenLite.to(elem, duration / 1000, properties);
		}
		else {
			return new Promise(function(resolve){
				properties.onComplete = resolve;
				TweenLite.to(elem, duration / 1000, properties);
			});
		}
	}
	
	/**
	 * @param: {function} callback
	 * @return: {number} id of this animation frame
	 */
	function requestAnimationFrame(x) {
		return window.requestAnimationFrame(x);
	}
	
	/**
	 * @param: {HTMLElement} elem
	 *         {object} options
	 *                  - {number} duration - in ms. DEFAULT: 300
	 *                  - {string} positionX [left, right, center]. DEFAULT: left
	 *                  - {string} positionY [top, bottom, middle]. DEFAULT: top
	 *                  - {number} offsetX (px). DEFAULT: 0
	 *                  - {number} offsetY (px). DEFAULT: 0
	 */
	function animateScrollTo(elem, options) {	
		if (typeof options !== 'object') {
			options = {
				duration: SCROLL_DURATION, 
				positionX: 'left',
				positionY: 'top',
				offsetX: 0,
				offsetY: 0
			};
		}
		if (typeof options.duration !== 'number') {
			options.duration = SCROLL_DURATION;
		}
		if (options.positionX !== 'left' && options.positionX !== 'center' && options.positionX !== 'right') {
			options.positionX = 'left';
		}
		if (options.positionY !== 'top' && options.positionY !== 'middle' && options.positionY !== 'bottom') {
			options.positionY = 'top';
		}
		if (typeof options.offsetX !== 'number') {
			options.offsetX = 0;
		}
		if (typeof options.offsetY !== 'number') {
			options.offsetY = 0;
		}

		// Calculate position to scroll to
		var elem_pos = domo.getPositionOnScreen(elem);
		var target_pos = {
			x: dom_page.scrollLeft + elem_pos.x - options.offsetX,
			y: dom_page.scrollTop + elem_pos.y - options.offsetY
		};
		if (options.positionY === 'middle') {
			target_pos.y -= getPageHeight() / 2 - elem.offsetHeight / 2;
		}
		else if (options.positionY === 'bottom') {
			target_pos.y -= getPageHeight() - elem.offsetHeight;
		}
		if (options.positionX === 'center') {
			target_pos.x -= getPageWidth() / 2 - elem.offsetWidth / 2;
		}
		else if (options.positionX === 'right') {
			target_pos.x -= getPageWidth() - elem.offsetWidth;
		}
		
		// Animate scroll to element
		return new Promise(function(resolve) {
			TweenLite.to(dom_page, options.duration/1000, {
				scrollLeft: target_pos.x,
				scrollTop: target_pos.y,
				onComplete: resolve
			});
		});
	} //animateScrollTo()
	
	/*********************
	 * DOM custom events *
	 *********************/
	function detect(elem, eventType, userOptions, namespace) {
		return tove.detect(elem, eventType, userOptions, namespace);
	}
	function stopDetect(elem, eventType, namespace) {
		return tove.stopDetect(elem, eventType, namespace);
	}
	
	/**********
	 *  AJAX  *
	 **********/
	/**
	 * Laods a file via AJAX. The entire file content is returned as a string in data.
	 * @param: {string}
	 * @return: {{romisea} - resolves/rejects with an object {status, data}.
	 */
	function ajaxLoad(path) {
		return ajax.load(path).then(function(xhr) {
			return xhr.response;
		}, function() {
			throw new Error('cannot load: ' + path);
		});
	} // ajaxLoad()
	
	/****************
	 * Object Utils *
	 ****************/
	/***
	 * Simulates PHP's date() function
	 * @param: {string} format
	 *         {object | number} Date object, or a UNIX timestamp (in ms)
	 * @return {string} formated string
	 ***/
	function formatDate(format, date) {
		return tina.formatDate(format, date);
	}
	
	/****************
	 * String Utils *
	 ****************/
	function escapeHTML(str) {
		return eve.escapeHTML(str);
	}
	function unescapeHTML(str) {
		return eve.unescapeHTML(str);
	}
	function encodeURL(text) {
		return eve.encodeURL(text);
	}
	function decodeURL(str) {
		return eve.decodeURL(str);
	}
	function br2nl(str) {
		return eve.br2nl(str);
	}
	function nl2br(str) {
		return eve.nl2br(str);
	}
	function utf82ascii(str) {
		return eve.utf82ascii(str);
	}
	function csv2array(data, options) {
		return eve.csv2array(data, options);
	}
	function str2fileName(n) {
		return eve.encodeFileName(n);
	}
	function padLeft(str, len, c) {
		return eve.padLeft(str, len, c);
	}
	function padRight(str, len, c) {
		return eve.padRight(str, len, c);
	}

	/**
	 * @param: {string} template containing mustaches
	 *         {obj} key value pairs
	 * @return: {string} rendered text
	 */
	function renderTemplate(template, views) {
		return eve.renderTemplate(template, views);
	}

	/*****************
	 *  Array Utils  *
	 *****************/
	/**
	 * Merges arrays of objects by a property, e.g. id
	 * - Note: objects without the property will not be included in results
	 * - Note: replaces objects with same id with the latest object
	 * @param: {string} prop
	 *         {Array, Array} arrays of objects
	 * @return: {Array} - new array; original arrays are not touched
	 */
	function mergeArraysBy(field, a, b) {
		return arya.mergeBy(field, a, b);
	}
	
	/**
	 * Sorts an array of objects by a certain prop.
	 * - Note: sort is ALWAYS stable
	 * - Note: new array will be returned; original arrays are untouched
	 * @param: {string} prop
	 *         {Array} array of objects
	 *         {object} options
	 *             {string} order - case insensitive (default:'asc') ['asc', 'desc']
	 *             {function} primer (default: null)
	 * @return: {Array}
	 */
	function sortArrayBy(field, array, options) {
		return arya.sortBy(field, array, options);
	}
	
	/**
	 * Finds the object with the min value for a particular property
	 * - Note: objects without prop specified are ignored
	 * - Note: props are compared using relational operators, so it's best to keep type same, or use a primer (parseInt)
	 * @param: {string} prop
	 *         {Array} array of objects
	 *         {object} options
	 *             {function} primer (default: null)
	 * @return: {object} - null if not found
	 */
	function findArrayMin(field, array) {
		return arya.findByMin(field, array);
	}
	
	/**
	 * Finds the object with the max value for a particular property
	 * - Note: objects without prop specified are ignored
	 * - Note: props are compared using relational operators, so it's best to keep type same, or use a primer (parseInt)
	 * @param: {string} prop
	 *         {Array} array of objects
	 *         {object} options
	 *             {function} primer (default: null)
	 * @return: {object} - null if not found
	 */
	function findArrayMax(field, array) {
		return arya.findByMax(field, array);
	}
	
	/**
	 * Returns index of the first object in the array that satisifies the conditions given
	 * - Note: short-circuit return on match, so integrity of the rest of the Array is not checked.
	 * @param: {object} conditions - key-value pairs
	 *         {Array} array - of objects
	 *         {object} options
	 *             {boolean} strict: whether to use === or == (default: true)
	 * @return: {int}
	 */
	function arrayIndexOf(conditions, array, options) {
		return arya.indexOf(conditions, array, options);
	}
	
	/**
	 * Counts the occurrences of objects containing certain key-value pairs in the array
	 * @param: {object} conditions - key-value pairs
	 *         {Array} array - of objects
	 *         {object} options
	 *             {boolean} strict: whether to use === or == (default: true)
	 * @return: {int}
	 */
	function arrayCount(conditions, array, options) {
		return arya.count(conditions, array, options);
	}

	/**
	 * Computes union of multiple Sets (or array-like collections)
	 * - Primarily for Sets. All other objects, e.g. HTMLCollection, will return as Array
	 * - Note: all duplicate values are removed
	 * - Note: non-array-like objects are treated as empty arrays
	 * @param: {Set | object}* - object should be an iterable collection
	 * @return: {Set | Array}
	 */
	function union() {
		return eve.union.apply(null, arguments);
	}

	/**
	 * Computes intersection of multiple Sets (or array-like collections)
	 * - primarily meant for Set
	 * - all other objects, e.g. HTMLCollection, will return as Array
	 * - Note: non-array-like objects are treated as empty arrays
	 * - Note: elements are compared by reference, so {'a':1} !== {'a':1}
	 * @param: {Set | object}* - object should be an iterable collection
	 * @return: {Set | Array}
	 */
	function intersection() {
		return eve.intersection.apply(null, arguments);
	}

	/********************
	 *   Object Utils   *
	 ********************/
	/**
	 * Converts input object to a CSV string
	 * - Note: toString() will be called on all properties / elements,
	 *         null properties/elements will replaced by a string "null"
	 * - Note: object will always result in a single line or double-line CSV, depending on parseHeader option
	 * - Note: 2D arrays with records of consistent, positive length will be result in a multi-line CSV.
	 * @param: {object} - can be object literal, Array, Set
	 * @return: {string}
	 */
	function obj2csv() {
		return eve.obj2csv.apply(null, arguments);
	}

	/******************
	 *   File Utils   *
	 ******************/
	/**
	 * Saves a string into a file for download by the browser
	 * @param: {string} str         - to be stored into fle (DEFAULT: '')
	 *         {string} file_name   - to be served (DEFAULT: 'download.txt')
	 */
	function downloadAsFile(str, file_name) {
		return fila.downloadAsFile(str, file_name);
	} //downloadAsFile()

	/**
	 * Uses File Reader API to read the contents of the file
	 * @param: {HTMLInput}
	 *         {object} options - optional.
	 *                  - {string} encoding [utf-8, base64] (defaults to 'base64')
	 * @return: {Promise} resolves with {string}
	 */
	function readFileInput(dom_input, options) {
		return fila.readFileInput(dom_input, options);
	}
		
	/********************
	 * Input Validation *
	 *******************/
	function isInt(str) {
		return eve.isInt(str);
	}
	function isUInt(str) {
		return eve.isUInt(str);
	}
	function isNumber(str) {
		return eve.isNumber(str);
	}
	function isASCII(str) {
		return eve.isASCII(str);
	}
	function isName(str) {
		return str.length >= settings.MIN_NAME_LENGTH;
	}
	function isEmail(str) {
		return eve.isEmail(str);
	}
	function isPassword(str) {
		return str.length >= settings.MIN_PASSWORD_LENGTH;
	}
	function isOTP(str) {
		var regex = new RegExp('^[0-9]{'+ settings.CODE_LENGTH +'}$');
		return str.match(regex);
	}
	function isPhone(str) {
		return eve.isNumber(str) && str.length === settings.PHONE_LENGTH;
	}
	function isDate(y, m, d) {
		return tina.isDate(y, m-1, d);
	}
	function isDateString(dd_mm_yyyy) {
		var args = dd_mm_yyyy.split('/');
		if (args.length !== 3) {
			return false;
		}
		return tina.isDate(args[2], args[1]-1, args[0]);
	}
	function isURL(str) {
		return eve.isURL(str);
	}
	/*********************************
	 * End of boilerplate for v1.5.0 *
	 *********************************/


	/*****************
	 * Therapy stuff *
	 *****************/
	/**
	 * Checks if a therapy is on promo
	 * @param: {object}
	 * @return: {bool}
	 */
	function isOnPromotion(therapy) {
		if (therapy['promotionPrice'] === null) {
			return false;
		}

		var now = Date.now();
		if (now < therapy['promotionStartTime'] || now > therapy['promotionEndTime']) {
			return false;
		}

		return true;
	}

	/***
	 * Takes a cents value and convert to format like this: $DD,DDD.cc
	 * @param: {string | integer} amount in cents
	 *         {bool} whether to discard the part after the decimal if the cent value is zero, e.g. '$14' instead of '$14.00' (default: false)
	 * @return: {string} "$DD,DDD.cc" or "$DD,DDD". 
	 ***/
	function formatPrice(x, dollarsOnly) {
		if (typeof x !== 'string' && typeof x !== 'number') {
			throw new TypeError('formatPrice() - param is not str or int.');
		}
		if (x%1 !== 0) {
			throw new RangeError('formatPrice() - price value not integer.');
		}
		
		// Check if price is negative
		var neg = false;
		if (x < 0) {
			neg = true;
			x = 0-x;
		}

		// Get absolute, discrete dollars and cents values
		var dollars = Math.abs(parseInt(x/100));
		var cents = Math.abs(parseInt(x%100));

		// Handle dollars only
		var toRet = '';
		while (dollars >= 1000) {
			var remainder = dollars % 1000;
			if (remainder <10) {
				remainder = '00'+ remainder.toString();
			}
			else if (remainder<100) {
				remainder = '0'+remainder.toString();
			}
			
			toRet = ',' + remainder + toRet;
			dollars = parseInt(dollars / 1000);
		}
		toRet = (neg?'-$':'$') + dollars + toRet;

		if (dollarsOnly && cents === 0) {
			return toRet;
		}
		else {
			if (cents < 10) {
				cents = '0' + cents;
			}
			return toRet + '.' + cents;
		}
	} //formatPrice()


	extensions.utils = {
		/* DOM element manipulation */
		appendHTML: appendHTML,
		prependHTML: prependHTML,
		createElement: createElement,
		selectText: selectText,
		wrapNode: wrapNode,
		str2dom: str2dom,
		hideFormElement: hideFormElement,
		showFormElement: showFormElement,
		populateSelect: populateSelect,
		populateCheckboxes: populateCheckboxes,
		populateDatalist: populateDatalist,
		removeSelectOption: removeSelectOption,
		removeCheckbox: removeCheckbox,
		disableForm: disableForm,
		enableForm: enableForm,
		isDisabledForm: isDisabledForm,
		removeEmptyElementDescendants: removeEmptyElementDescendants,
		removeNode: removeNode,
		removeBrChildren: removeBrChildren,
		isSelfClosingElement: isSelfClosingElement,
		
		/* DOM window / viewport */
		getWindowWidth: getWindowWidth,
		getWindowHeight: getWindowHeight,
		getPageWidth: getPageWidth,
		getPageHeight: getPageHeight,
		getPageScrollHeight: getPageScrollHeight,
		getPageScrollWidth: getPageScrollWidth,
		getPageScrollX: getPageScrollX,
		getPageScrollY: getPageScrollY,
		lockScroll: lockScroll,
		unlockScroll: unlockScroll,
		requestFullScreen: requestFullScreen,
		exitFullScreen: exitFullScreen,
		isFullScreen: isFullScreen,
		mailto: mailto,
		
		/* DOM element animations */
		animate: animate,
		requestAnimationFrame: requestAnimationFrame,
		animateScrollTo: animateScrollTo,
		
		/* DOM element custom events */
		detect: detect,
		stopDetect: stopDetect,
		
		/* AJAX */
		ajaxLoad: ajaxLoad,
		
		/* Date utils */
		formatDate: formatDate,
		
		/* String utils */
		escapeHTML: escapeHTML, htmlspecialchars: escapeHTML, 
		unescapeHTML: unescapeHTML, htmlspecialchars_decode: unescapeHTML, 
		encodeURL: encodeURL, urlencode: encodeURL,
		decodeURL: decodeURL, urldecode: decodeURL,
		br2nl: br2nl,
		nl2br: nl2br,
		utf82ascii: utf82ascii,
		csv2array: csv2array,
		str2fileName: str2fileName,
		padLeft: padLeft,
		padRight: padRight,
		renderTemplate: renderTemplate,

		/* Array utils */
		mergeArraysBy: mergeArraysBy,
		sortArrayBy: sortArrayBy,
		findArrayMin: findArrayMin,
		findArrayMax: findArrayMax,
		arrayIndexOf: arrayIndexOf,
		arrayCount: arrayCount,
		union: union,
		intersection: intersection,
		
		/* Object utils */
		obj2csv: obj2csv,

		/* File utils */
		downloadAsFile: downloadAsFile,
		readFileInput: readFileInput,

		/* Input Validation */
		isInt: isInt,
		isUInt: isUInt,
		isNumber: isNumber,
		isASCII: isASCII,
		isName: isName,
		isEmail: isEmail,
		isPassword: isPassword,
		isOTP: isOTP,
		isPhone: isPhone,
		isDate: isDate,
		isDateString: isDateString,
		isURL: isURL,

		/* Price */
		isOnPromotion: isOnPromotion,
		formatPrice: formatPrice
	};
})(app.extensions = app.extensions || {}, app.settings, eve, arya, domo, tove, ajax, tina, fila, greensock.TweenLite);
