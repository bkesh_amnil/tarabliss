/***
 * Frontend Web App Framework v1.5.0 - Controller
 * =================================
 *
 * - manages pages' and modules' life cycles
 * - manages communications between modules
 *
 * Note:
 * - #page will be animated-scrolled during fragment navigation
 * - #main will be swapped out during page switches
 *
 * Before use:
 * - <link> should have "data-page" attribute
 * - <script> should have "data-page" attribute
 * - #main should have "data-page" attribute
 * - #main should be hidden by default, and style class ".shown" to unhide it.
 *
 ****/
(function(app, ajax, domo, TweenLite, Modernizr, ga) {
	'use strict';
	
	/*******************
	 * Config settings *
	 *******************/
	var DOMAIN_URL = app.settings.DOMAIN_URL;
	//var SERVER_URL = app.settings.SERVER_URL;
	var HTML_FOLDER_URL = app.settings.DOMAIN_URL + '/html/';
	var CSS_FOLDER_URL = app.settings.DOMAIN_URL + '/css/';
	var JS_FOLDER_URL = app.settings.DOMAIN_URL + '/js/3-comp/';
	var DEFAULT_PAGE = app.settings.DEFAULT_PAGE;

	// Page switching settings
	var SCROLL_DURATION = 300; // for switchFragment 
	
	// DOM elements
	var dom_page = document.getElementById('page');
	var dom_main = document.getElementById('main');
	var dom_overlay = document.getElementById('overlay');
	
	// Components
	var components = {}; // keys are page names - each is an object literal containing components. Common components are stored as page ''
	var sandboxes = {}; // keys are page names - each is an object literal containing sandboxes
	
	// Pages - keys are page names
	var page_metas = {}; // each page is an object literal containing meta {title, description...}
	
	// Data for page switching
	var curr_page = dom_main.getAttribute('data-page') || DEFAULT_PAGE;
	var curr_fragment = window.location.hash;
	var curr_overlay = dom_overlay.getAttribute('data-overlay') || '';
	
	/**
	 * Loads new page using AJAX
	 * - stops all components on old page
	 * - animates out old page
	 * - fetches new files from server
	 * - removes old CSS / JS
	 * - adds new CSS, HTML, JS to document
	 * - animates in new page
	 * - broadcasts 'page-switched' event
	 * @param: {string} target_page 
	 *         {string} hash - which element on page to scroll to. Default: ''
	 *         {bool} remember_history - whether to store this page in browser history. Default: true
	 * @return: {Promise}
	 */
	function switchPage(target_page, target_hash, remember_history) {
		// Set defaults
		if (typeof target_hash === 'undefined') {
			target_hash = '';
		}
		if (typeof remember_history === 'undefined') {
			remember_history = true;
		}
		
		var page_meta = page_metas[target_page];
		if (typeof page_meta === 'undefined') {
			var ajax_fetch_page_meta = ajax.get({
				url: DOMAIN_URL + '/meta/' + target_page
			});
			return ajax_fetch_page_meta.then(function(data) {
				page_metas[target_page] = JSON.parse(data.responseText); // assume server-side always returns properly formed JSON
				return switchPage(target_page, target_hash, remember_history);
			}, function() {
				var error = new Error('app.controller.switchPage() - Page ' + target_page + ' is undefined in pages.json');
				handleError(error, 'controller.js');
				throw error;
			});
		}
		
		// Set up promise for page exit animation
		var animateout_elapsed = new Promise(function(resolve) {
			function onAnimateOut() {
				dom_main.removeEventListener('transitionend', onAnimateOut); //clean-up
				resolve('transition ended');
			}
			
			dom_main.classList.add('loading');
			if (Modernizr.csstransitions) {	
				dom_main.addEventListener('transitionend', onAnimateOut);
			}
			else {
				resolve('no transition support');
			}
		});
		
		// Set up promises for AJAX fetches
		var target_html_file = HTML_FOLDER_URL + 'pages/' + page_meta.html;
		var target_css_file = CSS_FOLDER_URL + 'pages/' + page_meta.css;
		var target_js_file = JS_FOLDER_URL + 'pages/' + page_meta.js;
		var ajax_fetch_html = ajax.load(target_html_file);
		var ajax_fetch_css = ajax.load(target_css_file);
		var ajax_fetch_js = ajax.load(target_js_file);
		
		// Stop all components on the page first before proceeding
		stopAllComponentsOnPage(curr_page);
		
		// Fetch all in parallel (ideally fetches from browser cache rather than from server)
		return Promise.all([animateout_elapsed, ajax_fetch_html, ajax_fetch_css, ajax_fetch_js]).then(function(data) {
			// Remove old CSS / JS and change curr page name
			domo.removeCSSByData({page: curr_page});
			domo.removeJSByData({page: curr_page});
			curr_page = target_page; // need to set before adding components of the new page as they may be registered under the curr page name.
			
			// Set new HTML / CSS / JS and fade in
			domo.addCSS(target_css_file, {page: target_page});
			dom_main.innerHTML = data[1].responseText;
			dom_main.setAttribute('data-page', target_page);
			dom_main.classList.remove('loading');
			domo.addJS(target_js_file, {page:target_page});
			
			// Set new title and meta
			document.title = page_meta.title;
			domo.setMeta('description', page_meta.description);
			domo.setMeta('image', page_meta.img);
			domo.setMeta('og:title', page_meta.title);
			domo.setMeta('og:description', page_meta.description);
			domo.setMeta('og:image', page_meta.img);
			
			// DEBUG
			//console.log('switching to fragment: ' + target_hash);	
			switchFragment(target_hash, true);
			
			// Browser history and analytics
			if (Modernizr.history && remember_history) {
				rememberBrowserHistory(target_page, target_hash);
			}
			if (ga) {
				ga('send', 'pageview', {
					/*'hitCallback': function() {console.log('DEBUG sent pageview of '+ target_page);},*/
					'page': '/'+target_page,
					'title': '/'+target_page + '(AJAX)'
				});
			}
			
			broadcastComponentEvent('page-switched', curr_page);
			
			return Promise.resolve();
		}, function(data){
			handleError(data.status + ' while switching to ' + target_page + target_hash, 'controller switchPage()');
			
			domo.removeCSSByData({page: curr_page});
			domo.removeJSByData({page: curr_page});

			curr_page = data.status;

			// Set error HTML
			document.title = data.status;
			dom_main.innerHTML = data.responseText;
			dom_main.classList.remove('loading');
			
			broadcastComponentEvent('page-switched', curr_page);
			
			return Promise.resolve(); // resolve because we did switch page... albeit to an error page
		});
	} //switchPage()
	
	/**
	 * Switches to a fragment
	 * @param: {string} hash - If empty string, returns immediately after clearing hash. If only '#' is provided, scrolls to top of page.
	 *         {bool} remember_fragment - whether to update curr_fragment
	 * @return: {Promise}
	 */
	function switchFragment(hash, remember_fragment) {
		// Set defaults
		if (typeof remember_fragment === 'undefined') {
			remember_fragment = false;
		}
		
		if (remember_fragment) {
			curr_fragment = hash;
		}
			
		if (hash === '') {
			dom_page.scrollLeft = 0;
			dom_page.scrollTop = 0;
			return Promise.resolve();
		}
		
		// Find out where to go
		var target_pos;
		if (hash === '#') {
			target_pos = {x:0, y:0};
		} // back to top
		else {
			// Find the element
			var target_element = document.querySelector(hash);
			if (!target_element) {
				target_pos = {x:0, y:0};
			} // no such element: set back to top
			else {
				var tmp = domo.getPositionOnScreen(target_element);
				target_pos = {
					x: tmp.x + dom_page.scrollLeft,
					y: tmp.y + dom_page.scrollTop
				};
			} // element found
		}
			
		// Animate scroll to element
		return new Promise(function(resolve) {
			TweenLite.to(dom_page, SCROLL_DURATION/1000, {
				scrollLeft: target_pos.x,
				scrollTop: target_pos.y,
				onComplete: resolve
			});
		});
	} //switchFragment()
	
	/**
	 * Loads and activates an overlay
	 * @param: {string} overlay - name of overlay
	 * @return: {Promise}
	 */
	function showOverlay(overlay) {
		dom_overlay.classList.add('loading');
		
		// Set up promises for AJAX fetches
		var target_html_file = HTML_FOLDER_URL + 'overlays/' + overlay + '.html';
		var target_css_file = CSS_FOLDER_URL + 'overlays/' + overlay + '.css';
		var target_js_file = JS_FOLDER_URL + 'overlays/' + overlay + '.js';
		var ajax_fetch_html = ajax.load(target_html_file);
		var ajax_fetch_css = ajax.load(target_css_file);
		var ajax_fetch_js = ajax.load(target_js_file);

		return Promise.all([ajax_fetch_html, ajax_fetch_css, ajax_fetch_js]).then(function(data) {
			// Remove old CSS / JS
			//domo.removeCSSByData({type: 'overlay'});
			//domo.removeJSByData({type: 'overlay'});
			curr_overlay = overlay;
			
			// Set new HTML / CSS / JS
			dom_overlay.innerHTML = '<article id="overlay-panel">' + data[0].responseText + '</article>' +
			                        '<button class="close-button" id="overlay-close-button"></button>';
			dom_overlay.setAttribute('data-overlay', overlay);
			domo.addCSS(target_css_file, {page: curr_page, type: 'overlay'});
			domo.addJS(target_js_file, {page: curr_page, type: 'overlay'});
			
			document.addEventListener('keydown', onOverlayKeydown);
			document.getElementById('overlay-close-button').addEventListener('click', onCloseOverlayButtonClick);

			dom_overlay.classList.remove('loading');
			dom_overlay.classList.add('shown');
			return Promise.resolve();
		}, function(data){
			handleError(data.status + ' while showing overlay: ' + overlay, 'controller showOverlay()');
			
			dom_overlay.classList.remove('loading');
		});	
	} //showOverlay()
	
	/**
	 * Hides any showing overlay
	 * @return: {Promise}
	 */
	function hideOverlay() {
		// Set up promise for exit animation
		var animate_out_promise;
		if (!dom_overlay.classList.contains('loading') && !dom_overlay.classList.contains('shown')) {
			 animate_out_promise = Promise.resolve();
		}
		else {
			animate_out_promise = new Promise(function(resolve) {
				function onAnimateOut() {
					dom_overlay.removeEventListener('transitionend', onAnimateOut); //clean-up
					resolve('transition ended');
				}

				document.removeEventListener('keydown', onOverlayKeydown);
				document.getElementById('overlay-close-button').removeEventListener('click', onCloseOverlayButtonClick);
				
				dom_overlay.classList.remove('loading');
				dom_overlay.classList.remove('shown');
				if (Modernizr.csstransitions) {	
					dom_overlay.addEventListener('transitionend', onAnimateOut);
				}
				else {
					resolve('no transition support');
				}
			});
		}
		
		stopComponent('overlay');
		return animate_out_promise.then(function() {
			// Clean up HTML, CSS, JS
			dom_overlay.innerHTML = '';
			domo.removeCSSByData({type: 'overlay'});
			domo.removeJSByData({type: 'overlay'});
		});
	} //hideOverlay()

	/**
	 * Detects ESC keydown on overlays
	 */
	function onOverlayKeydown(e) {
		// Ignore if we're currently in a form element
		var current_node_name = document.activeElement.nodeName;
		if (current_node_name === 'INPUT' || current_node_name === 'TEXTAREA' || current_node_name === 'SELECT') {
			return;
		}

		var key = (window.event) ? event.keyCode : e.keyCode;
		if (key === 27) { //ESC key
			e.preventDefault();
			
			hideOverlay();
		}
	} //onOverlayKeydown()
	/**
	 * Detects close button click on overlays
	 */
	function onCloseOverlayButtonClick(e) {
		e.preventDefault();
		
		hideOverlay();
	} //onCloseOverlayButtonClick()

	
	function getCurrentPage() {
		return curr_page;
	}
	
	function getCurrentFragment() {
		return curr_fragment;
	}
	
	function getCurrentOverlay() {
		return curr_overlay;
	}
	
	function getURLOrigin() {
		return window.location.origin;
	}
	
	/**
	 * Gets a value from the URL query string
	 * - Note: result will be url-decoded, i.e. all %20 will be converted to spaces, + (plus signs) converted to spaces etc.
	 * - Note: if a filed appears multiple times in the query string, the value of last occurrence will be returned.
	 * @param: {string} field
	 * @return: {string | null} value if found, null otherwise
	 */
	function getURLQueryStringValue(field) {
		if (typeof field !== 'string') {
			return null;
		}
		
		// we really don't expect this to be called too many times, so parsing everytime is ok
		var query = window.location.search.substring(1);
		var queries = query.split('&');
		var value = null; // store the last occurence
		for (var i=0; i<queries.length; i++) {
			queries[i] = decodeURIComponent(queries[i].replace(/\+/g, ' '));
			if (queries[i].startsWith(field+'=')) {
				value = queries[i].substr(field.length + 1);
			}
		}
		
		return value;
	} //getQueryStringValue()
	
	/**
	 * Handles component-level events (called by sandbox)
	 * @param: {string} event - name of event,
	 *         {anything} data
	 * @return: void
	 */
	function broadcastComponentEvent(event, data) {
		function compareListenerEvent(el){
			return el.event === event;
		}
		
		// Notify all sandboxes for components that are listening for this event
		for (var page in sandboxes) {
			if (!sandboxes.hasOwnProperty(page)) {
				continue;
			}
			
			for (var name in sandboxes[page]) {
				if (!sandboxes[page].hasOwnProperty(name)) {
					continue;
				}

				// Find listeners for this event
				try {
					var listeners = sandboxes[page][name].getComponentListeners();
					var listeners_to_notify = listeners.filter(compareListenerEvent);
					
					// Notify each of them
					for (var i=0,len=listeners_to_notify.length; i<len; i++) {
						listeners_to_notify[i].callback.call(app.controller, data);
					}
				}
				catch (ex) {
					handleError(ex, '[Page: '+page+'] '+name+' (broadcastComponentEvent: ' +  event + ')' );
				}
			} // for each component's sandbox
		} // for each page's sandboxes
	} //broadcastComponentEvent()
	
	/**
	 * Adds a new component and starts it.
	 * - Any existing component with the same name will be overwritten.
	 * @param: {string} name,
	 *         {string} page - optional (DEFAULT: current page). If empty string '' is passed, will be treated as common component
	 *         {function} creator
	 */
	function startComponent() {
		// Get params
		var name, page, creator;
		if (arguments.length === 2) {
			name = arguments[0];
			page = curr_page;
			creator = arguments[1];
		}
		else if (arguments.length === 3) {
			name = arguments[0];
			page = arguments[1];
			creator = arguments[2];
		}
		else {
			throw new ReferenceError('app.controller.startComponent() - illegal number of params.');
		}
		if (typeof name !== 'string') {
			throw new TypeError('app.controller.startComponent() - param name must be string');
		}
		if (typeof page !== 'string') {
			throw new TypeError('app.controller.startComponent() - page must be string');
		}
		if (typeof creator !== 'function') {
			throw new TypeError('app.controller.startComponent() - creator must be function');
		}
		
		// Create a sandbox for the component
		if (name.substring(0,1) === '#') {
			name = name.substring(1);
		}
		var sandbox = app.createSandbox(name);
		if (typeof sandboxes[page] === 'undefined') {
			sandboxes[page] = {};
		}
		sandboxes[page][name] = sandbox;
		
		// Create the component
		var component = creator(sandbox);
		if (typeof component.start !== 'function') {
			throw new ReferenceError('app.controller.startComponent() - component must have start() function');
		}
		if (typeof component.stop !== 'function') {
			throw new ReferenceError('app.controller.startComponent() - component must have stop() function');
		}
		
		// Pad all methods in component with try-catch block
		function createPaddedMethod(method_name, method) {
			return function() {
				try {return method.apply(this, arguments);}
				catch(ex) {handleError(ex, '[Page: '+page+'] '+name+'.'+method_name+'()');}
			};
		}
		for (var key in component) {
			if (!component.hasOwnProperty(key)) {
				continue;
			}
			
			var method = component[key];
			if (typeof method === 'function') {
				component[key] = createPaddedMethod(key, method);
			}
		}
		
		// Add this component to registry
		if (typeof components[page] === 'undefined') {
			components[page] = {};
		}
		components[page][name] = component;
		
		component.start();
	} //startComponent()
	
	/**
	 * @private
	 * Stops a particular component
	 * - will also remove all listeners and subscribers
	 * @param: {string} name,
	 *         {string} page - optional (DEFAULT: current page). If empty string '' is passed, will be treated as common component
	 */
	function stopComponent(name, page) {
		if (typeof page === 'undefined') {
			page = curr_page;
		}
		
		// Find the component and stop it
		components[page][name].stop();
		delete components[page][name];
		delete sandboxes[page][name];
	} //stopComponent()
	
	/**
	 * @private
	 * Stops all components on a particular page
	 * - will also remove all listeners and subscribers
	 * @param: {string} page
	 */
	function stopAllComponentsOnPage(page) {
		var components_to_stop = components[page];
		for (var name in components_to_stop) {
			if (!components_to_stop.hasOwnProperty(name)) {
				continue;
			}
			
			components_to_stop[name].stop();
			delete sandboxes[page][name];
		}
		
		delete components[page];
	} //stopAllComponentsOnPage()
	
	/**
	 * @private
	 * Pushes a page into browser history
	 */
	function rememberBrowserHistory(target_page, target_hash) {
		var url_to_remember;
		if (target_hash === '') {
			url_to_remember = DOMAIN_URL+'/'+target_page;
		}
		else {
			url_to_remember = DOMAIN_URL+'/'+target_page + target_hash;
		}
		
		try {
			// params: state, title (unused), URL)
			window.history.pushState('nav','URL: '+ url_to_remember, url_to_remember);
		}
		catch (e) {
			console.error('rememberBrowserHistory() - Failed: ' + e);
		}
	} //rememberBrowserHistory()
	
	/**
	 * @private
	 * Handles errors that occur in the any of the system components.
	 * @param: {Error} exception,
	 *         {string} origin - describing which module/method the error originated from
	 * @return: void
	 */
	function handleError(exception, origin) {
		console.error(exception);
		
		// Write error to server
		ajax.post({
			url: DOMAIN_URL + '/api/user/logError',
			data: {'error': 'Web App (' + DOMAIN_URL + ') "' + exception + '" encountered in ' + origin}
		});
	} //handleError()
	
	/***************
	 * Window behaviour
	 ***************/
	// Make browser's Back/Forward buttons AJAX page-switch instead
	window.onpopstate = function(evt) {
		// only proceed if event is caused by Back/Forward buttons - legacy Webkit browsers wrongly fire popstate events on normal pageload
		if (evt.state === null) { // real Back/Forward button clicks should have non-null state)
			//console.log('detected buggy webkit pop');
			return false;
		} //detected webkit pop on load
		
		// Extract page name from URL - see if we can find it directly in URL. If not, try looking in query string. If still noting, fallback to default page
		var page = window.location.pathname.substring(1);
		if (page === '') {
			page = getURLQueryStringValue('page') || '';
		}
		if (page === '') {
			page = DEFAULT_PAGE;
		}
		
		// Extract hash from URL if it's there
		var hash = window.location.hash;

		switchPage(page, hash, false);
	};
	
	// Resize Events
	window.onresize = function() {
		broadcastComponentEvent('window-resized');
	};

	// Fullscreenchange Events
	document.addEventListener('fullscreenchange', function() {
		if (document.fullscreenEnabled && document.fullscreenElement !== null) {
			broadcastComponentEvent('window-entered-fullscreen');
		}
		else {
			broadcastComponentEvent('window-exited-fullscreen');
		}
	});
	
	// Handle landing page - history
	if (Modernizr.history) {
		window.history.pushState('landing','URL: ' + window.location, window.location);
	}
	
	app.controller = {
		// methods for navigation (called by sandbox)
		switchPage: switchPage,
		switchFragment: switchFragment,
		showOverlay: showOverlay,
		hideOverlay: hideOverlay,
		getCurrentPage: getCurrentPage,
		getCurrentFragment: getCurrentFragment,
		getCurrentOverlay: getCurrentOverlay,
		getURLOrigin: getURLOrigin,
		getURLQueryStringValue: getURLQueryStringValue,
		
		// methods for component communications (called by sandbox)
		broadcastComponentEvent: broadcastComponentEvent,
		
		// methods for component life cycle (called by global)
		startComponent: startComponent
	};
})(this.app, this.ajax, this.domo, this.greensock.TweenLite, this.Modernizr, this.ga);
