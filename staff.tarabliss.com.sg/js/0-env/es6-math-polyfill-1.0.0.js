/**
 * Polyfills for ES6 Math functions
 * - acosh(), asinh(), atanh()
 * - cosh(), sinh(), tanh()
 * - cbrt()
 * - clz32()
 * - expm1()
 * - fround()
 * - hypot()
 * - imul()
 * - log10(), log1p(), log2()
 * - sign()
 * - trunc()
 */
Math.acosh = Math.acosh || function(x) {
	return Math.log(x + Math.sqrt(x * x - 1));
};
Math.asinh = Math.asinh || function(x) {
	if (x === -Infinity) {
		return x;
	}
	else {
		return Math.log(x + Math.sqrt(x * x + 1));
	}
};
Math.atanh = Math.atanh || function(x) {
	return Math.log((1+x)/(1-x)) / 2;
};
Math.cosh = Math.cosh || function(x) {
	return (Math.exp(x) + Math.exp(-x)) / 2;
}
Math.sinh = Math.sinh || function(x) {
	var y = Math.exp(x);
	return (y - 1 / y) / 2;
}
Math.tanh = Math.tanh || function(x) {
	if (x === Infinity) {
		return 1;
	}
	else if (x === -Infinity) {
		return -1;
	}
	else {
		var y = Math.exp(2 * x);
		return (y - 1) / (y + 1);
	}
}
Math.cbrt = Math.cbrt || function(x) {
	var y = Math.pow(Math.abs(x), 1/3);
	return x < 0 ? -y : y;
};
Math.clz32 = Math.clz32 || (function () {
	'use strict';

	var table = [32, 31,  0, 16,  0, 30,  3,  0, 15,  0,  0,  0, 29, 10,  2,  0,
	             0,  0, 12, 14, 21,  0, 19,  0,  0, 28,  0, 25,  0,  9,  1,  0,
	             17,  0,  4,   ,  0,  0, 11,  0, 13, 22, 20,  0, 26,  0,  0, 18,
	             5,  0,  0, 23,  0, 27,  0,  6,  0, 24,  7,  0,  8,  0,  0,  0];

	// Adapted from an algorithm in Hacker's Delight, page 103.
	return function (x) {
		// Note that the variables may not necessarily be the same.

		// 1. Let n = ToUint32(x).
		var v = Number(x) >>> 0

		// 2. Let p be the number of leading zero bits in the 32-bit binary representation of n.
		v |= v >>> 1
		v |= v >>> 2
		v |= v >>> 4
		v |= v >>> 8
		v |= v >>> 16
		v = table[Math.imul(v, 0x06EB14F9) >>> 26]

		// Return p.
		return v
	}
})();
Math.expm1 = Math.expm1 || function(x) {
	return Math.exp(x) - 1;
};
Math.fround = Math.fround || function(x) {
	return new Float32Array([x])[0];
};
Math.hypot = Math.hypot || function() {
	var y = 0;
	var length = arguments.length;

	for (var i = 0; i < length; i++) {
		if (arguments[i] === Infinity || arguments[i] === -Infinity) {
			return Infinity;
		}
		y += arguments[i] * arguments[i];
  	}
	return Math.sqrt(y);
};
Math.imul = Math.imul || function(a, b) {
	var ah = (a >>> 16) & 0xffff;
	var al = a & 0xffff;
	var bh = (b >>> 16) & 0xffff;
	var bl = b & 0xffff;
	// the shift by 0 fixes the sign on the high part
	// the final |0 converts the unsigned value into a signed value
	return ((al * bl) + (((ah * bl + al * bh) << 16) >>> 0)|0);
};
Math.log10 = Math.log10 || function(x) {
	return Math.log(x) / Math.LN10;
};
Math.log1p = Math.log1p || function(x) {
	return Math.log(1 + x);
};
Math.log2 = Math.log2 || function(x) {
	return Math.log(x) / Math.LN2;
};
Math.sign = Math.sign || function(x) {
	x = +x; // convert to a number
	if (x === 0 || isNaN(x)) {
		return x;
	}
	return x > 0 ? 1 : -1;
}
Math.trunc = Math.trunc || function(x) {
	return x < 0 ? Math.ceil(x) : Math.floor(x);
}