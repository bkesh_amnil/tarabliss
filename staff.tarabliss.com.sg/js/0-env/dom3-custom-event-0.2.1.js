/**
 * Polyfill for creating CustomEvents on IE9/10/11
 * reference: https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent
 */
(function (root) {
	try {
		new CustomEvent("test");
	}
	catch(e) {
		var CustomEvent = function(event, params) {
			var evt;
			params = params || {bubbles: false, cancelable: false, detail: undefined};
			
			evt = document.createEvent("CustomEvent");
			evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
			return evt;
		};

		CustomEvent.prototype = window.Event.prototype;
		root["CustomEvent"] = CustomEvent;
	}
})(this);