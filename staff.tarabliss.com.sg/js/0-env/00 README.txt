What do I need?

IE8/9
-----
Add everything in these directories, plus all IE 10/11 polyfills.
Even then, probably not sufficient...


IE 10
------
- childNode:        remove(), before(), after(), replaceWith()
- customEvent:      window.CustomEvent
- promise
- es6-array
- es6-number
- es6-string


IE 11
-----
- childNode:        remove(), before(), after(), replaceWith()
- customEvent:      window.CustomEvent
- promise
- es6-array
- es6-number
- es6-string


Firefox 43
----------
- childNode:        remove(), before(), after(), replaceWith()
- setImmediate
- es6-array


Chrome 47 / Opera 30
--------------------
- childNode:        remove(), before(), after(), replaceWith()
- setImmediate
- es6-array


Safari 9
--------
- childNode:        remove(), before(), after(), replaceWith()
- setImmediate
- es6-array
- es6-number


Android Browser Chromium 40
---------------------------
- childNode:        remove(), before(), after(), replaceWith()
- setImmediate
- es6-array
- es6-string


iOS Safari 8.4
--------------
- childNode:        remove(), before(), after(), replaceWith()
- setImmediate
- es6-array
- es6-number




Dependent on specific app requirements
--------------------------------------
- sessionstorage
- dom event          *for IE browsers
- dom3 custom event  *for IE browsers
- es6-math
- es6-object
- es6-collections    *not needed for Firefox