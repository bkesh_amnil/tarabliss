/***
 * Polyfill for creating Event objects on IE9/10/11
 * Background: IE browsers do not support Event constructor :(
 * 
 * reference: https://github.com/Financial-Times/polyfill-service/blob/master/polyfills/Event/polyfill.js
 */
(function(root) {
	if (typeof root.Event === 'function') {
		return;
	}

	try {
		new Event('click');
		return;
	} catch(e) {
		// DEBUG
		//console.log('Event constructor failed');
	}
	
	root.Event = function Event(type, eventInitDict) {
		if (!type) {
			throw new Error('Not enough arguments');
		}
	
		var event = document.createEvent('Event');
		var bubbles = eventInitDict && eventInitDict.bubbles !== undefined ? eventInitDict.bubbles : false;
		var cancelable = eventInitDict && eventInitDict.cancelable !== undefined ? eventInitDict.cancelable : false;
	
		event.initEvent(type, bubbles, cancelable);
		return event;
	};
}(this));
