﻿// https://gist.github.com/yckart/6435861
(function (a, b, c) {
	this[b] =                                 // assign the new function to window.getSelection
	this[b] ||                                // use window.getSelection if available
	a[b] ||                                   // fallback to document.getSelection
	function () {
		return a[c] && a[c].createRange().text; // fallback to document.selection
	};
}(document, 'getSelection', 'selection'));