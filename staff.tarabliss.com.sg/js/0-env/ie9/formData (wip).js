/**
 */
(function(w) {
	if (w.FormData) {
		return;
	}
	
	/**
	 * @constructor
	 */
	function FormData() {
		this.boundary = "--------FormData" + Math.random();
		this._fields = {};
	}
	
	FormData.prototype['append'] = function(key, value) {
		value = '' + value;
		var curr_values = this._fields[key];
		if (typeof curr_values === 'undefined') {
			this._fields[key] = [value];
		}
		else {
			this._fields[key] = curr_values.push(value);
		}
	}
	FormData.prototype['delete'] = function(key) {
		delete this._fields[key];
	}
	
	// TODO: implement get/getAll?
	
	FormData.prototype['toString'] = function() {
		var boundary = this.boundary;
		
		var body = "";
		for (var key in this._fields) {
			if (!this._fields.hasOwnProperty(key)) {
				continue;
			}
			
			var curr_values = this._fields[key];
			
			
			if (field[1].name) {
				var file = field[1];
				body += "Content-Disposition: form-data; name=\""+ field[0] +"\"; filename=\""+ file.name +"\"\r\n";
				body += "Content-Type: "+ file.type +"\r\n\r\n";
				body += file.getAsBinary() + "\r\n";
			} else {
				body += "Content-Disposition: form-data; name=\""+ field[0] +"\";\r\n\r\n";
				body += field[1] + "\r\n";
			}
		}
		this._fields.forEach(function(field) {
			body += "--" + boundary + "\r\n";
			// file upload
			if (field[1].name) {
				var file = field[1];
				body += "Content-Disposition: form-data; name=\""+ field[0] +"\"; filename=\""+ file.name +"\"\r\n";
				body += "Content-Type: "+ file.type +"\r\n\r\n";
				body += file.getAsBinary() + "\r\n";
			} else {
				body += "Content-Disposition: form-data; name=\""+ field[0] +"\";\r\n\r\n";
				body += field[1] + "\r\n";
			}
		});
		body += "--" + boundary +"--";
		return body;
	}
	w.FormData = FormData;
})(window);