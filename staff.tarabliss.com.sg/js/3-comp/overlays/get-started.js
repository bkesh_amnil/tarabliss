this.app.controller.startComponent('overlay', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_forms = sandbox.findAll('form');
	var dom_login_form = sandbox.findOne('#login-form');
	var dom_forget_password_form = sandbox.findOne('#forget-password-form');
	var dom_verify_email_form = sandbox.findOne('#verify-email-form');
	var dom_change_password_form = sandbox.findOne('#change-password-form');

	var dom_form_links = sandbox.findAll('form > a');

	// Setting
	var OUTPUT_SHOW_DURATION = 10000; // in ms
	var DEFAULT_FORM_ID = '#login-form';

	// Context
	var query_string_email;
	var query_string_code;
	var current_form_id;
	var form_strings;       // stores messages used in forms
	var session_staff;      // stores session staff after a successful password reset (after verifyEmail)
	var output_timer;       // for output
	var is_submitting = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function initQueryStringsAndCurrentFormID() {
		query_string_email = sandbox.getURLQueryStringValue('email');
		query_string_code = sandbox.getURLQueryStringValue('code');

		if (query_string_email) {
			if (query_string_code) {
				current_form_id = '#verify-email-form';
			}
			else {
				current_form_id = '#login-form';
			}
			return;
		}
		else {
			current_form_id = DEFAULT_FORM_ID;
		}
	} //initQueryStringsAndCurrentFormID()

	function asyncFetchValidationStrings() {
		if (typeof form_strings !== 'undefined') {
			return Promise.resolve(form_strings);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/data/strings/forms.json').then(function(response_data) {
			form_strings = JSON.parse(response_data);
			return form_strings;
		});
	} //asyncFetchValidationStrings()

	/*************
	 * DOM Logic *
	 *************/
	function showCurrentForm() {
		var dom_form = sandbox.findOne(current_form_id);

		// Go through all forms
		for (var i=0,len=dom_forms.length; i<len; i++) {
			if (dom_forms[i] === dom_form) {
				dom_forms[i].classList.remove('hide');	
			}
			else {
				dom_forms[i].classList.add('hide');
			}
		}
	} // showCurrentForm()

	function enableCurrentFormSubmitButton() {
		var dom_submit_button = sandbox.findOne(current_form_id + ' > button[type="submit"]');
		dom_submit_button.disabled = false;
	} //enableCurrentFormSubmitButton()

	function disableCurrentFormSubmitButton() {
		var dom_submit_button = sandbox.findOne(current_form_id + ' > button[type="submit"]');
		dom_submit_button.disabled = true;
	} //disableCurrentFormSubmitButton()

	function enableForms() {
		for (var i=0,len=dom_forms.length; i<len; i++) {
			sandbox.utils.enableForm(dom_forms[i]);
		}
	} //enableForms()

	function disableForms() {
		for (var i=0,len=dom_forms.length; i<len; i++) {
			sandbox.utils.disableForm(dom_forms[i]);
		}
	} //disableForms()

	function populateEmail(em) {
		dom_login_form['email'].value = em;
		dom_forget_password_form['email'].value = em;
		dom_verify_email_form['email'].value = em;
	}
	function populateCode(code) {
		dom_verify_email_form['code'].value = code;
	}

	function populateOldPassword(pw) {
		dom_change_password_form['old-password'].value = pw;
	}

	function outputError(txt) {
		var dom_output = sandbox.findOne(current_form_id + ' > output');
		
		dom_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_output.classList.add('error');
		dom_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(function() {
			dom_output.classList.remove('shown');
		}, OUTPUT_SHOW_DURATION);
	}
	function outputMessage(txt) {
		var dom_output = sandbox.findOne(current_form_id + ' > output');
		
		dom_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_output.classList.remove('error');
		dom_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(function() {
			dom_output.classList.remove('shown');
		}, OUTPUT_SHOW_DURATION);
	}
	 
	/****************
	 * Module Logic *
	 ****************/
	/**
	 * @param: {bool} output - whether to output error messages (DEFAULT: false)
	 * @return: {bool}
	 */
	function validateCurrentForm(output) {
		if (typeof output !== 'boolean') {
			output = false;
		}

		var email, password, otp, old_password, new_password, retype_password;
		if (current_form_id === '#login-form') {
			email = dom_login_form['email'].value;
			if (!sandbox.utils.isEmail(email)) {
				if (output) {
					outputError(form_strings['INVALID_EMAIL']);
				}
				return false;
			}

			password = dom_login_form['password'].value;
			if (!sandbox.utils.isPassword(password)) {
				if (output) {
					outputError(form_strings['INVALID_PASSWORD']);
				}
				return false;
			}

			return true;
		} // Login Form
		else if (current_form_id === '#forget-password-form') {
			email = dom_forget_password_form['email'].value;
			if (!sandbox.utils.isEmail(email)) {
				if (output) {
					outputError(form_strings['INVALID_EMAIL']);
				}
				return false;
			}

			return true;
		} // Forget Password Form
		else if (current_form_id === '#verify-email-form') {
			email = dom_verify_email_form['email'].value;
			if (!sandbox.utils.isEmail(email)) {
				if (output) {
					outputError(form_strings['INVALID_EMAIL']);
				}
				return false;
			}

			otp = dom_verify_email_form['code'].value;
			if (!sandbox.utils.isOTP(otp)) {
				if (output) {
					outputError(form_strings['INVALID_OTP']);
				}
				return false;
			}

			return true;
		} // Verify Email Form
		else if (current_form_id === '#change-password-form') {
			old_password = dom_change_password_form['old-password'].value;
			if (!sandbox.utils.isPassword(old_password)) {
				if (output) {
					outputError(form_strings['INVALID_PASSWORD']);
				}
				return false;
			}

			new_password = dom_change_password_form['new-password'].value;
			if (!sandbox.utils.isPassword(new_password)) {
				if (output) {
					outputError(form_strings['INVALID_PASSWORD']);
				}
				return false;
			}

			retype_password = dom_change_password_form['retype-password'].value;
			if (new_password !== retype_password) {
				if (output) {
					outputError(form_strings['RETYPE_PASSWORD_MISMATCH']);
				}
				return false;
			}

			return true;
		} // Change Password Form
	} //validateCurrentForm()

	 /************
	 * Listeners *
	 *************/
	function onOverlayClick(e) {
		// Handle clicks on overlay outside of panel - hide overlay
		if (e.target === dom_component) {
			sandbox.hideOverlay();
		}
	}

	function onFormLinkClick(e) {
		e.preventDefault();
		
		// Copy current form's email input value to other forms
		var dom_form = sandbox.findOne(current_form_id);
		var dom_email = dom_form['email'];
		if (dom_email) {
			populateEmail(dom_email.value);
		}
		
		// Switch form and validate immediately
		current_form_id = e.currentTarget.hash;
		if (validateCurrentForm()) {
			enableCurrentFormSubmitButton();
		}
		else {
			disableCurrentFormSubmitButton();
		}
		showCurrentForm();
	}

	function onFormInput() {
		if (validateCurrentForm()) {
			enableCurrentFormSubmitButton();
		}
		else {
			disableCurrentFormSubmitButton();
		}
	}

	function onLoginFormSubmit(e) {
		e.preventDefault();

		// Ensure this is the current form
		if (current_form_id !== '#login-form') {
			return;
		}

		// Validate form
		if (!validateCurrentForm(true)) {
			return;
		}

		// Prevent double submits
		if (is_submitting) {
			return;
		}

		// Disable form
		is_submitting = true;
		disableForms();
		outputMessage(form_strings['PROCESSING']);

		// AJAX
		var form_data = new FormData(dom_login_form);
		return sandbox.staffAPI.login(form_data).then(function(response_data) {
			sandbox.hideOverlay();
			sandbox.publish('staff-logged-in', response_data['staff']);
		}, function(response_error) {
			is_submitting = false;
			enableForms();

			outputError(response_error.message);
		});
	} // onLoginFormSubmit()

	function onForgetPasswordFormSubmit(e) {
		e.preventDefault();

		// Ensure this is the current form
		if (current_form_id !== '#forget-password-form') {
			return;
		}

		// Validate form
		if (!validateCurrentForm(true)) {
			return;
		}

		// Prevent double submits
		if (is_submitting) {
			return;
		}

		// Disable form
		is_submitting = true;
		disableForms();
		outputMessage(form_strings['PROCESSING']);

		// AJAX
		var form_data = new FormData(dom_forget_password_form);
		return sandbox.staffAPI.forgetPassword(form_data).then(function() {
			is_submitting = false;
			enableForms();

			current_form_id = '#verify-email-form';
			showCurrentForm();
		}, function(response_error) {
			is_submitting = false;
			enableForms();

			outputError(response_error.message);
		});
	} //onForgetPasswordFormSubmit()

	function onVerifyEmailFormSubmit(e) {
		e.preventDefault();

		// Ensure this is the current form
		if (current_form_id !== '#verify-email-form') {
			return;
		}

		// Validate form
		if (!validateCurrentForm(true)) {
			return;
		}

		// Prevent double submits
		if (is_submitting) {
			return;
		}

		// Disable form
		is_submitting = true;
		disableForms();
		outputMessage(form_strings['PROCESSING']);

		// AJAX
		var form_data = new FormData(dom_verify_email_form);
		return sandbox.staffAPI.verifyEmail(form_data).then(function(response_data) {
			is_submitting = false;
			enableForms();

			session_staff = response_data['staff'];
			populateOldPassword(response_data['password']);

			current_form_id = '#change-password-form';
			showCurrentForm();
		}, function(response_error) {
			is_submitting = false;
			enableForms();

			outputError(response_error.message);
		});
	} //onVerifyEmailFormSubmit()

	function onChangePasswordFormSubmit(e) {
		e.preventDefault();

		// Ensure this is the current form
		if (current_form_id !== '#change-password-form') {
			return;
		}

		// Validate form
		if (!validateCurrentForm(true)) {
			return;
		}

		// Prevent double submits
		if (is_submitting) {
			return;
		}

		// Disable form
		is_submitting = true;
		disableForms();
		outputMessage(form_strings['PROCESSING']);

		// AJAX
		var form_data = new FormData(dom_change_password_form);
		return sandbox.staffAPI.changePassword(form_data).then(function() {
			sandbox.hideOverlay();
			sandbox.publish('staff-logged-in', session_staff);
			
		}, function(response_error) {
			is_submitting = false;
			enableForms();

			outputError(response_error.message);
		});
	} //onChangePasswordFormSubmit()

	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		asyncFetchValidationStrings().then(function() {
			// Init current form and auto-populate data
			initQueryStringsAndCurrentFormID();
			if (current_form_id === '#verify-email-form') {
				populateEmail(query_string_email);
				populateCode(query_string_code);
			}
			else if (current_form_id === '#login-form') {
				populateEmail(query_string_email);
			}

			// Show and validate immediately
			showCurrentForm();
			if (validateCurrentForm()) {
				enableCurrentFormSubmitButton();
			}
			else {
				disableCurrentFormSubmitButton();
			}

			// Listeners
			sandbox.on(dom_component, 'click', onOverlayClick);
			sandbox.on(dom_form_links, 'click', onFormLinkClick);
			sandbox.on(dom_login_form, 'input', onFormInput);
			sandbox.on(dom_forget_password_form, 'input', onFormInput);
			sandbox.on(dom_verify_email_form, 'input', onFormInput);
			sandbox.on(dom_change_password_form, 'input', onFormInput);
			sandbox.on(dom_login_form, 'submit', onLoginFormSubmit);
			sandbox.on(dom_forget_password_form, 'submit', onForgetPasswordFormSubmit);
			sandbox.on(dom_verify_email_form, 'submit', onVerifyEmailFormSubmit);
			sandbox.on(dom_change_password_form, 'submit', onChangePasswordFormSubmit);
		});	
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});
