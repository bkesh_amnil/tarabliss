this.app.controller.startComponent('site-header', '', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_start_button = sandbox.findOne('#header-start-button');
	var dom_account_link = sandbox.findOne('#account-link');
	var dom_logout_link = sandbox.findOne('#logout-link');
	
	// Context
	var session_user;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncLoadSessionUser() {
		return sandbox.sessionDB.get('session-user').then(function (user) {
			if (user === null) {
				throw new Error('No session user');
			}
			
			session_user = user;
			return session_user;
		});
	} // asyncLoadSessionUser()
	 
	/*************
	 * DOM Logic *
	 *************/
	function showAccountName() {
		dom_account_link.innerHTML = session_user['email'];
	}
	 
	/****************
	 * Module Logic *
	 ****************/
	function asyncInitAccountLink() {
		return asyncLoadSessionUser()
		.then(showAccountName)
		.catch(function(error) {
			console.debug(error);
		});
	}

	/*************
	 * Listeners *
	 *************/
	function onStartButtonClick() {
		sandbox.showOverlay('get-started');
	}

	function onLogoutLinkClick(e) {
		e.preventDefault();
		e.stopPropagation();

		// AJAX logout
		return sandbox.staffAPI.logout().then(function() {
			sandbox.publish('staff-logged-out');
		}).catch(function() {
			sandbox.dialog.alertTemplate(sandbox.settings.DOMAIN_URL + '/html/templates/warning-logout-fail.html');
			sandbox.publish('staff-logged-out'); // even if we fail
		});
	} //onLogoutLinkClick()
	
	/***************
	 * Subscribers *
	 ***************/
	function onStaffLoggedIn() {
		//console.debug('header detected login')
		asyncInitAccountLink();
	}

	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('staff-persistent-logged-in', onStaffLoggedIn);
		sandbox.subscribe('staff-logged-in', onStaffLoggedIn);

		// Listeners
		sandbox.on(dom_start_button, 'click', onStartButtonClick);
		sandbox.on(dom_logout_link, 'click', onLogoutLinkClick);

		asyncInitAccountLink();
	}
	
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	}
	
	return {
		start: start,
		stop: stop
	};
});
