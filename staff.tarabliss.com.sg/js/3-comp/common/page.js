/***
 * Frontend Web App Framework v1.5.0 - Page component
 * =================================
 *
 * This should be the only "global" component.
 * It monitors the page for DOM events on elements (even dynamically loaded ones), and overwrite behaviour:
 * - all links trigger AJAX requests rather than page refreshes
 * - TODO: context menus
 *
 * It also periodically checks the session user with backend, and ensures that pages requiring login will
 * only be shown to logged in users.
 *
 ***/
this.app.controller.startComponent('page', '', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();

	// Settings
	var DEFAULT_USER = {'userType': 'Guest', 'id': 0, 'email': 'guest@user.com'};
	var CHECK_SESSION_PERIOD = 1500000;      // 25 mins
	var LOGIN_FROM_PAGES = ['login'];        // don't stay on this page if we successfully persistent login
	var POST_LOGIN_PAGE = 'dashboard';       // go to this page if we successfully persistent login
	//var LOGOUT_FROM_PAGES = ['dashboard', 'account', 'settings']; // Don't need this. Every page except login is a valid page to logout from.
	var POST_LOGOUT_PAGE = 'login';         // go to this page if we failed persistent login
	
	// Context
	var last_scroll_y;
	var session_user;
	var csrf_token;
	var timer; // for periodic session user checks

	/*****************
	 * Context Logic *
	 *****************/
	function initLastScrollY() {
		last_scroll_y = dom_component.scrollTop;
	}

	function asyncFetchSessionUser() {
		return sandbox.userAPI.getSessionUser().then(function(response_data) {
			session_user = response_data['user'];

			return session_user;
		});
	}

	function asyncLoadCSRFToken() {
		return sandbox.localDB.getItem('csrf-token').then(function(val) {
			csrf_token = val;

			return csrf_token;
		});
	}

	/*************
	 * DOM Logic *
	 *************/
	
	/****************
	 * Module Logic *
	 ****************/
	/**
	 * Check current session user with server, broadcasts either logged in or logged out,
	 */
	function asyncCheckSession() {
		// Schedule next check
		clearTimeout(timer);
		timer = setTimeout(asyncCheckSession, CHECK_SESSION_PERIOD);

		return asyncFetchSessionUser().then(function() {
			if (session_user['userType'] !== 'Staff') {
				throw new Error('Not Staff');
			}

			return asyncLoadCSRFToken();
		}).then(function() {
			if (csrf_token === null) {
				sandbox.dialog.alertTemplate(sandbox.settings.DOMAIN_URL + '/html/templates/warning-null-csrf.html');
				throw new Error('No CSRF Token');
			}

			// Server already has user logged in
			sandbox.publish('staff-persistent-logged-in', session_user);
			
			return session_user;
		}).catch(function() {
			return asyncAttemptPersistentLogin();
		});
	} //asyncCheckSession()

	/**
	 * Attempts to persistent login and broadcasts the result
	 * @return {Promise} - resolves / rejects with session_user object
	 */
	function asyncAttemptPersistentLogin() {
		return sandbox.staffAPI.persistentLogin().then(function(response_data) {
			session_user = response_data['staff'];

			sandbox.publish('staff-persistent-logged-in', session_user);

			return session_user;
		}, function() {
			session_user = DEFAULT_USER;

			sandbox.publish('staff-logged-out');
			
			return session_user;
		});
	} //asyncAttemptPersistentLogin()

	/*************
	 * Listeners *
	 *************/
	function onScroll() {
		var curr_scroll_y = dom_component.scrollTop;
		var velocity = curr_scroll_y - last_scroll_y;
		if (velocity > 0) {
			sandbox.publish('scroll-down', {scrollTop: curr_scroll_y, velocity: velocity});
		} // DOWN
		else if (velocity < 0) {
			sandbox.publish('scroll-up', {scrollTop: curr_scroll_y, velocity: velocity});
		} // UP
		
		last_scroll_y = curr_scroll_y;
	} //onScroll()

	function onClick(e) {
		var tgt = e.target;
		
		// Handle internal links
		var dom_a = tgt.closest('a');
		if (dom_a) {
			//console.debug('anchor click: ' + dom_a.href);
			if (dom_a.href.startsWith(sandbox.settings.DOMAIN_URL) && dom_a.target === '') {
				e.preventDefault();
				sandbox.navigate(dom_a.href);
			}
		}
	} //onClick()

/*
	function onContextMenu() {
		// TODO
	} //onContextMenu()
*/
	
	/***************
	 * Subscribers *
	 ***************/
	function onPageSwitched(pg) {
		if (session_user['userType'] === 'Staff') {
			if (LOGIN_FROM_PAGES.includes(pg)) {
				sandbox.navigate(POST_LOGIN_PAGE);
			} // staff at login page
			else {
				return;
			} // staff at privileged page
		}
		else {
			if (LOGIN_FROM_PAGES.includes(pg)) {
				return;
			} // non-staff at login page
			else {
				sandbox.navigate(POST_LOGOUT_PAGE);
			} // non-staff at privileged page
		}
	}
	function onStaffLoggedIn(user) {
		//console.debug('page detected login');
		session_user = user;
		if (LOGIN_FROM_PAGES.includes(sandbox.getCurrentPage())) {
			sandbox.navigate(POST_LOGIN_PAGE);
		}
	}
	function onStaffLoggedOut() {
		session_user = DEFAULT_USER;
		if (sandbox.getCurrentPage() !== POST_LOGOUT_PAGE) {
			sandbox.navigate(POST_LOGOUT_PAGE);
		}
	}

	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Listeners
		sandbox.on(dom_component, 'scroll', onScroll);
		sandbox.on(dom_component, 'click', onClick);
		//sandbox.on(dom_component, 'contextmenu', onContextMenu); // TODO

		// Subscribers
		sandbox.subscribe('page-switched', onPageSwitched);
		sandbox.subscribe('staff-logged-in', onStaffLoggedIn);
		sandbox.subscribe('staff-persistent-logged-in', onStaffLoggedIn);
		sandbox.subscribe('staff-logged-out', onStaffLoggedOut);

		// Init
		initLastScrollY();
		asyncCheckSession();
	}

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	}
	
	return {
		start: start,
		stop: stop
	};
});