this.app.controller.startComponent('activity-module', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_activity_list = sandbox.findOne('#activity-list');
	var dom_older_log_button = sandbox.findOne('#older-log-button');
	var dom_newer_log_button = sandbox.findOne('#newer-log-button');

	// Settings
	var ACTIVITIES_PER_PAGE = 6;

	// Context
	var session_user;
	var activity_template;
	var activities_to_show;
	var activity_count;
	var display_settings = {
		offset: 0,
		count: ACTIVITIES_PER_PAGE
	};
	var has_loaded_context = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchActivities() {
		return sandbox.staffAPI.getActivities(display_settings).then(function(response_data) {
			activities_to_show = response_data['activities'];
			activity_count = response_data['activity-count'];

			return activities_to_show;
		});
	}

	function asyncLoadSessionUser() {
		return sandbox.sessionDB.get('session-user').then(function (user) {
			if (user === null) {
				throw new Error('No session user');
			}
			
			session_user = user;
			return session_user;
		});
	}

	function asyncFetchTemplates() {
		if (typeof activity_template !== 'undefined') {
			return Promise.resolve(activity_template);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/html/templates/activity.html').then(function(response_data) {
			activity_template = response_data;
			return activity_template;
		});
	}

	/*************
	 * DOM Logic *
	 *************/
	function populateActivites() {
		if (!has_loaded_context) {
			throw new Error('populateActivites() - cannot proceed before context has loaded');
		}

		var i,len;

		// Generate HTML
		var html = '';
		for (i=0,len=activities_to_show.length; i<len; i++) {
			html += sandbox.utils.renderTemplate(activity_template, {
				email: session_user['email'],
				time: sandbox.utils.formatDate('d/m/Y, g:i:sa', activities_to_show[i]['createdTime']),
				action: activities_to_show[i]['action']
			});
		}
		dom_activity_list.innerHTML = html;
	} // populateActivites()

	function setButtons() {
		// More activities to show
		if (activity_count > display_settings['offset'] + display_settings['count']) {
			dom_older_log_button.disabled = false;
		}
		else {
			dom_older_log_button.disabled = true;
		}

		if (display_settings['offset'] > 0) {
			dom_newer_log_button.disabled = false;
		}
		else {
			dom_newer_log_button.disabled = true;
		}
	}
	
	/****************
	 * Module Logic *
	 ****************/
	function asyncRefetchActivities() {
		if (!has_loaded_context) {
			throw new Error('asyncRefetchActivities() - cannot proceed before context has loaded');
		}

		return asyncFetchActivities()
		.then(function() {
			populateActivites();
			setButtons();
		});
	}

	function asyncInit() {
		return asyncFetchActivities()
		.then(function() {
			has_loaded_context = true;

			populateActivites();
			setButtons();

			// Listeners
			sandbox.on(dom_newer_log_button, 'click', onNewerButtonClick);
			sandbox.on(dom_older_log_button, 'click', onOlderButtonClick);
		});
	}
	

	/*************
	 * Listeners *
	 *************/
	function onOlderButtonClick() {
		display_settings['offset'] += ACTIVITIES_PER_PAGE;

		asyncRefetchActivities();
	}

	function onNewerButtonClick() {
		display_settings['offset'] -= ACTIVITIES_PER_PAGE;
		if (display_settings['offset'] < 0) {
			display_settings['offset'] = 0;
		}
		
		asyncRefetchActivities();
	}
	
	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		asyncLoadSessionUser()
		.then(asyncFetchTemplates)
		.then(asyncInit)
		.catch(function(e) {
			console.debug(e);
		});
	} //start()
	
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});
