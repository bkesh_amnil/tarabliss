this.app.controller.startComponent('main', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();

	// Context
	var query_string_email;
	var query_string_code;
	
	/*****************
	 * Context Logic *
	 *****************/
	function initURLQeueryStrings() {
		query_string_email = sandbox.getURLQueryStringValue('email');
		query_string_code = sandbox.getURLQueryStringValue('code');
	}

	/*************
	 * DOM Logic *
	 *************/
	 
	/****************
	 * Module Logic *
	 ****************/
	 
	 /*************
	 * Listeners *
	 *************/
	
	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		initURLQeueryStrings();

		if (query_string_email || query_string_code) {
			sandbox.showOverlay('get-started');
		}
	} //start()

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});
