this.app.controller.startComponent('edit-account-section', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_form = sandbox.findOne('#edit-profile-form');
	var dom_form_output = sandbox.findOne('#edit-profile-form > output');

	// Settings
	var OUTPUT_SHOW_DURATION = 10000; // in ms

	// Context
	var session_user;
	var form_strings;       // stores messages used in forms
	var output_timer;       // for output
	var has_loaded_context = false;
	var is_submitting = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchFormStrings() {
		if (typeof form_strings !== 'undefined') {
			return Promise.resolve(form_strings);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/data/strings/forms.json').then(function(response_data) {
			form_strings = JSON.parse(response_data);
			return form_strings;
		});
	} //asyncFetchFormStrings()

	function asyncLoadUser() {
		if (typeof session_user !== 'undefined') {
			return Promise.resolve(session_user);
		}

		return sandbox.sessionDB.get('session-user').then(function (user) {
			if (user === null) {
				throw new Error('No session user');
			}
			
			session_user = user;
			return session_user;
		});
	} //asyncLoadUser()

	/*************
	 * DOM Logic *
	 *************/
	function populateForm() {
		if (typeof session_user !== 'object') {
			throw new Error('No user to populate form with.');
		}

		// Text fields
		for (var i=0, len = dom_form.elements.length; i<len; i++) {
			var name = dom_form.elements[i].name;
			dom_form.elements[i].value = session_user[name] || '';
		}
	}

	function disableForm() {
		sandbox.utils.disableForm(dom_form);
	}
	function enableForm() {
		sandbox.utils.enableForm(dom_form);
	}

	function outputFormError(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.add('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}
	function outputFormMessage(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.remove('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}

	function clearFormOutput() {
		dom_form_output.classList.remove('error');
		dom_form_output.classList.remove('shown');
		dom_form_output.innerHTML = '';
	}

	function outputFieldError(name, txt) {
		var dom_field_output = sandbox.findOne('output[for='+dom_form[name].id+']');

		dom_field_output.innerHTML = txt;
		dom_field_output.classList.add('error');
		dom_field_output.classList.add('shown');
		sandbox.utils.animateScrollTo(dom_field_output);
	}

	function clearFieldOutputs() {
		var dom_field_outputs = sandbox.findAll('fieldset output');

		for (var i=0,len=dom_field_outputs.length; i<len; i++) {
			dom_field_outputs[i].classList.remove('error');
			dom_field_outputs[i].classList.remove('shown');
			dom_field_outputs[i].innerHTML = '';
		}
	}
	 
	/****************
	 * Module Logic *
	 ****************/
	/**
	 * @param: {bool} output - whether to output error messages (DEFAULT: false)
	 * @return: {bool}
	 */
	function validateForm(output) {
		if (typeof output !== 'boolean') {
			output = false;
		}

		clearFieldOutputs();

		// Name
		var name = dom_form['name'].value;
		if (!sandbox.utils.isName(name)) {
			if (output) {
				outputFieldError('name', form_strings['INVALID_NAME']);
			}
			return false;
		}

		// Phone
		var phone = dom_form['phone'].value;
		if (!sandbox.utils.isPhone(phone)) {
			if (output) {
				outputFieldError('phone', form_strings['INVALID_PHONE']);
			}
			return false;
		}

		return true;
	} // validateForm()
	
	function asyncInitForm() {
		return asyncLoadUser()
		.then(function() {
			has_loaded_context = true;

			populateForm();
			disableForm();
			enableForm();

			// Listneners
			sandbox.on(dom_form, 'submit', onFormSubmit);
			sandbox.on(dom_form, 'reset', onFormReset);
		}).catch(function(e) {
			console.debug(e);
		});
	}
	 /*************
	 * Listeners *
	 *************/
	function onFormSubmit(e) {
		e.preventDefault();

		// Validate form
		if (!validateForm(true)) {
			return;
		}

		// Disable form
		is_submitting = true;
		disableForm();
		outputFormMessage(form_strings['PROCESSING']);

		// AJAX
		var form_data = new FormData(dom_form);
		return sandbox.staffAPI.editAccount(form_data).then(function(response_data) {
			is_submitting = false;
			enableForm();

			session_user = response_data['staff'];
			dom_form.reset();
			outputFormMessage(form_strings['EDIT_SUCCESS']);
		}, function(response_error) {
			is_submitting = false;
			enableForm();

			outputFormError(response_error.message);
		});
	}

	function onFormReset(e) {
		e.preventDefault();

		clearFieldOutputs();
		clearFormOutput();
		populateForm();
	}

	/***************
	 * Subscribers *
	 ***************/
	function onStaffLoggedIn() {
		//console.debug('account detected login')
		asyncInitForm();
	}
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('staff-persistent-logged-in', onStaffLoggedIn);
		sandbox.subscribe('staff-logged-in', onStaffLoggedIn);

		return asyncFetchFormStrings()
		.then(asyncInitForm);
	} //start()

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});

this.app.controller.startComponent('change-password-section', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_form = sandbox.findOne('#change-password-form');
	var dom_form_output = sandbox.findOne('#change-password-form > output');

	// Settings
	var OUTPUT_SHOW_DURATION = 10000; // in ms

	// Context
	var session_user;
	var form_strings;       // stores messages used in forms
	var output_timer;       // for output
	var has_loaded_context = false;
	var is_submitting = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchFormStrings() {
		if (typeof form_strings !== 'undefined') {
			return Promise.resolve(form_strings);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/data/strings/forms.json').then(function(response_data) {
			form_strings = JSON.parse(response_data);
			return form_strings;
		});
	} //asyncFetchFormStrings()

	function asyncLoadUser() {
		if (typeof session_user !== 'undefined') {
			return Promise.resolve(session_user);
		}

		return sandbox.sessionDB.get('session-user').then(function (user) {
			if (user === null) {
				throw new Error('No session user');
			}
			
			session_user = user;
			return session_user;
		});
	} //asyncLoadUser()

	/*************
	 * DOM Logic *
	 *************/
	function disableForm() {
		sandbox.utils.disableForm(dom_form);
	}
	function enableForm() {
		sandbox.utils.enableForm(dom_form);
	}

	function outputFormError(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.add('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}
	function outputFormMessage(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.remove('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}

	function clearFormOutput() {
		dom_form_output.classList.remove('error');
		dom_form_output.classList.remove('shown');
		dom_form_output.innerHTML = '';
	}

	function outputFieldError(name, txt) {
		var dom_field_output = sandbox.findOne('output[for='+dom_form[name].id+']');

		dom_field_output.innerHTML = txt;
		dom_field_output.classList.add('error');
		dom_field_output.classList.add('shown');
		sandbox.utils.animateScrollTo(dom_field_output);
	}

	function clearFieldOutputs() {
		var dom_field_outputs = sandbox.findAll('fieldset output');

		for (var i=0,len=dom_field_outputs.length; i<len; i++) {
			dom_field_outputs[i].classList.remove('error');
			dom_field_outputs[i].classList.remove('shown');
			dom_field_outputs[i].innerHTML = '';
		}
	}
	 
	/****************
	 * Module Logic *
	 ****************/
	/**
	 * @param: {bool} output - whether to output error messages (DEFAULT: false)
	 * @return: {bool}
	 */
	function validateForm(output) {
		if (typeof output !== 'boolean') {
			output = false;
		}

		clearFieldOutputs();

		// Old Password
		var old_pass = dom_form['old-password'].value;
		if (!sandbox.utils.isPassword(old_pass)) {
			if (output) {
				outputFieldError('old-password', form_strings['INVALID_PASSWORD']);
			}
			return false;
		}

		// New Password
		var new_pass = dom_form['new-password'].value;
		if (!sandbox.utils.isPassword(new_pass)) {
			if (output) {
				outputFieldError('new-password', form_strings['INVALID_PASSWORD']);
			}
			return false;
		}

		var retype_pass = dom_form['retype-password'].value;
		if (retype_pass !== new_pass) {
			if (output) {
				outputFieldError('retype-password', form_strings['RETYPE_PASSWORD_MISMATCH']);
			}
			return false;
		}

		return true;
	} // validateForm()

	function asyncInitForm() {
		return asyncLoadUser()
		.then(function() {
			has_loaded_context = true;

			disableForm();
			enableForm();

			// Listneners
			sandbox.on(dom_form, 'submit', onFormSubmit);
			sandbox.on(dom_form, 'reset', onFormReset);
		}).catch(function(e) {
			console.debug(e);
		});
	} // asyncInitForm()
	 
	 /*************
	 * Listeners *
	 *************/
	function onFormSubmit(e) {
		e.preventDefault();

		// Validate form
		if (!validateForm(true)) {
			return;
		}

		// Disable form
		is_submitting = true;
		disableForm();
		outputFormMessage(form_strings['PROCESSING']);

		// AJAX
		var form_data = new FormData(dom_form);
		return sandbox.staffAPI.changePassword(form_data).then(function() {
			is_submitting = false;
			enableForm();

			dom_form.reset();
			outputFormMessage(form_strings['EDIT_SUCCESS']);
		}, function(response_error) {
			is_submitting = false;
			enableForm();

			outputFormError(response_error.message);
		});
	}

	function onFormReset() {
		clearFieldOutputs();
		clearFormOutput();
	}

	/***************
	 * Subscribers *
	 ***************/
	function onStaffLoggedIn() {
		//console.debug('account detected login')
		asyncInitForm();
	}
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('staff-persistent-logged-in', onStaffLoggedIn);
		sandbox.subscribe('staff-logged-in', onStaffLoggedIn);

		return asyncFetchFormStrings()
		.then(asyncInitForm);
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});
