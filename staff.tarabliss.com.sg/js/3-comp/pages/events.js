/*****************
 * Module Header *
 *****************/
this.app.controller.startComponent('events-module-header', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_search_form = sandbox.findOne('#events-search-form');

	// Context
	var galleries;   // for making options
	var search_term;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncLoadSearchTerm() {
		if (typeof search_term !== 'undefined') {
			return Promise.resolve(search_term);
		}

		return sandbox.localDB.get('preference-events-display-settings').then(function(val) {
			if (val) {
				search_term = val['search'];
			}
			else {
				search_term = '';
			}

			return search_term;
		});
	} //asyncLoadSearchTerm()

	function asyncFetchGalleries() {
		return sandbox.staffAPI.getEventGallery().then(function(response_data) {
			galleries = response_data['eventGalleries'];
                       
		});
	}

	/*************
	 * DOM Logic *
	 *************/
	function showSearchTerm() {
		dom_search_form['search'].value = search_term;
	}

	/****************
	 * Module Logic *
	 ****************/
	function makeGalleryOptions() {
		var gallery_options = [];
		for (var i=0, len=galleries.length; i<len; i++) {
			// Find category name
//			var tmp = sandbox.utils.arrayIndexOf({'id': sub_categories[i]['categoryID']}, categories);
			var gallery_name = galleries[i]['name'];

			var option = {};
			option[galleries[i]['id']] = gallery_name;
			gallery_options.push(option);
		}
		return sandbox.sessionDB.set('gallery-options', gallery_options).then(function(data) {
			sandbox.publish('gallery-options-loaded', data);
		});
	} // makeGalleryOptions()

	 /*************
	 * Listeners *
	 *************/
	function onSearchFormSubmit(e) {
		e.preventDefault();

		sandbox.publish('events-search-command', dom_search_form['search'].value);
	}
	
	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		asyncLoadSearchTerm()
		.then(asyncFetchGalleries)
		.then(function() {
			showSearchTerm();
//                 console.log(galleries);
			makeGalleryOptions();

			// Listeners
			sandbox.on(dom_search_form, 'submit', onSearchFormSubmit);
		});
	} //start()

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});

/******************
 *  View Section  *
 ******************/
this.app.controller.startComponent('view-events-section', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_header_pagination = sandbox.findOne('#results-section-header-pagination');
	var dom_footer_pagination = sandbox.findOne('#results-section-footer-pagination');
	var dom_results_per_page_buttons = sandbox.findAll('.results-display-controls > button');
	var dom_results_tbody = sandbox.findOne('#events-tbody');
	var dom_results_sortable_th = sandbox.findAll('#events-table th.sortable');
	var dom_results_edit_buttons;   // dynamic

	// Settings
	var WAIT_FOR_CONTEXT_PERIOD = 100; // in ms
	var DEAFULT_DISPLAY_SETTINGS = {showCount: 10, sort: 'eventGalleryID', order: 'ASC', sort2: 'name', order2: 'ASC', search:''};

	// Context
	var display_settings;
	var result_template;
	var results_section_strings;  // stores messages used in forms
	var events_to_show;        // array of objects
	var total_count;              // total count fitting this search criteria
	var curr_page;
	var has_loaded_context = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncLoadDisplaySettings() {
		if (typeof display_settings !== 'undefined') {
			return Promise.resolve(display_settings);
		}

		return sandbox.localDB.get('preference-events-display-settings').then(function(val) {
			if (val) {
				return val;
			}
			else {
				return sandbox.localDB.set('preference-events-display-settings', DEAFULT_DISPLAY_SETTINGS);
			}
		}).then(function(val) {
			display_settings = val;
			curr_page = 1;

			return display_settings;
		});
	} //asyncLoadDisplaySettings()

	function asyncSaveDisplaySettings() {
		return sandbox.localDB.set('preference-events-display-settings', display_settings);
	} //asyncSaveDisplaySettings()

	function asyncFetchResultsSectionStrings() {
		if (typeof results_section_strings !== 'undefined') {
			return Promise.resolve(results_section_strings);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/data/strings/results-section.json').then(function(response_data) {
			results_section_strings = JSON.parse(response_data);
			return results_section_strings;
		});
	} //asyncFetchResultsSectionStrings()

	function asyncFetchResultTemplate() {
		if (typeof result_template !== 'undefined') {
			return Promise.resolve(result_template);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/html/templates/events/event-result.html').then(function(response_data) {
			result_template = response_data;
			return result_template;
		});
	} //asyncFetchVerifiedMemberTemplate()

	function asyncFetchResults() {
		// Prepare search conditions
		var name_condition = [
			['name', 'LIKE', '%'+display_settings['search']+'%']
		];
		var description_condition = [
			['description', 'LIKE', '%'+display_settings['search']+'%']
		];
		var or_conditions = [name_condition, description_condition];

		// Prepare offset
		var offset;
		if (curr_page === 0) {
			offset = 0;
		}
		else {
			offset = (curr_page - 1) * display_settings['showCount'];
		}

		// AJAX 
		return sandbox.staffAPI.getEvents({
			conditions: JSON.stringify(or_conditions), // 3D array of OR conditions
			offset: offset,
			count: display_settings['showCount'],
			sort: display_settings['sort'],
			order: display_settings['order'],
			sort2: display_settings['sort2'],
			order2: display_settings['order2']
		}).then(function(response_data) {	
			total_count = response_data['count'];
			events_to_show = response_data['events'];

			return events_to_show;
		});
	} //asyncFetchResults()

	/*************
	 * DOM Logic *
	 *************/
	function populateDisplaySettings() {
		if (!has_loaded_context) {
			throw new Error('populateDisplaySettings() - cannot proceed before context has loaded');
		}

		var i, len;
		for (i=0,len=dom_results_per_page_buttons.length; i<len; i++) {
			if (parseInt(dom_results_per_page_buttons[i].getAttribute('data-count')) === display_settings['showCount']) {
				dom_results_per_page_buttons[i].classList.add('current');
			}
			else {
				dom_results_per_page_buttons[i].classList.remove('current');
			}
		}
	} //populateDisplaySettings()

	// Show table header sort according to sort values (in display settings)
	function populateTableHeadersSortSettings() {
		if (!has_loaded_context) {
			throw new Error('populateTableHeadersSortSettings() - cannot proceed before context has loaded');
		}

		for (var i=0,len=dom_results_sortable_th.length; i<len; i++) {
			if (dom_results_sortable_th[i].getAttribute('data-field') === display_settings['sort']) {
				dom_results_sortable_th[i].classList.add('sort-selected');
				if (display_settings['order'] === 'ASC') {
					dom_results_sortable_th[i].classList.remove('desc');
				}
				else {
					dom_results_sortable_th[i].classList.add('desc');
				}
			}
			else {
				dom_results_sortable_th[i].classList.remove('sort-selected');
				dom_results_sortable_th[i].classList.remove('desc');
			}
		}
	} // populateTableHeadersSortSettings()

	function populateTableBody() {
		if (!has_loaded_context) {
			throw new Error('populateTableBody() - cannot proceed before context has loaded');
		}

		var i,len;

		// Generate HTML
		var html = '';
		for (i=0,len=events_to_show.length; i<len; i++) {
                    var day, month, year;
                    var eventDate = events_to_show[i]['eventTime'];
                    if (eventDate) {
                            var start_date = new Date(parseInt(eventDate));
                            day = start_date.getUTCDate();
                            if (day < 10) {
                                    day = '0' + day;
                            }
                            month = start_date.getUTCMonth() + 1; // Note: JS Dates start from 0
                            if (month < 10) {
                                    month = '0' + month;
                            }
                            year = start_date.getUTCFullYear();
                            var eventDateFormatted = day + '/' + month + '/' + year;
                    }

			html += sandbox.utils.renderTemplate(result_template, {
				id: events_to_show[i]['id'],
				name: events_to_show[i]['name'],
				eventDate: eventDateFormatted,
				displayPriority: events_to_show[i]['displayPriority']
			});
		}
		dom_results_tbody.innerHTML = html;

		// Dynamic DOM elements
		dom_results_edit_buttons = sandbox.findAll('#events-tbody .edit-button');

		// Bind Listeners
		sandbox.on(dom_results_edit_buttons, 'click', onEditButtonClick);
	} //populateTableBody()

	function populatePagination() {
		if (!has_loaded_context) {
			throw new Error('populatePagination() - cannot proceed before context has loaded');
		}

		var options = {
			pageCount: Math.ceil(total_count / display_settings['showCount']),
			currPage: curr_page,
			onButtonClick: onPaginationButtonClick,
			prevButtonText: results_section_strings['PREV_BUTTON_TEXT'],
			nextButtonText: results_section_strings['NEXT_BUTTON_TEXT']
		};

		sandbox.paginator.init(dom_header_pagination, options);
		sandbox.paginator.init(dom_footer_pagination, options);
	} //populatePagination()

	/****************
	 * Module Logic *
	 ****************/
	function asyncRefetchResults() {
		if (!has_loaded_context) {
			throw new Error('asyncRefetchResults() - cannot proceed before context has loaded');
		}

		return asyncSaveDisplaySettings()
		.then(asyncFetchResults)
		.then(function() {
			populatePagination();
			populateDisplaySettings();
			populateTableHeadersSortSettings();
			populateTableBody();
		});
	}

	function asyncInit() {
		asyncFetchResults()
		.then(function (){
			has_loaded_context = true;

			populatePagination();
			populateDisplaySettings();
			populateTableHeadersSortSettings();
			populateTableBody();

			// Listeners
			sandbox.on(dom_results_sortable_th, 'click', onSortableHeaderClick);
			sandbox.on(dom_results_per_page_buttons, 'click', onResultsPerPageButtonClick);
		});
	} // asyncInit()

	 /*************
	 * Listeners *
	 *************/
	function onPaginationButtonClick(pg) {
		curr_page = pg;

		return asyncRefetchResults();
	}

	function onResultsPerPageButtonClick(e) {
		e.preventDefault();

		display_settings['showCount'] = parseInt(e.currentTarget.getAttribute('data-count'));
		curr_page = 1;
		return asyncRefetchResults();
	}

	function onSortableHeaderClick(e) {
		if (e.currentTarget.classList.contains('sort-selected')) {
			display_settings['order'] = (display_settings['order'] === 'ASC')? 'DESC' : 'ASC';
		}
		else {
			display_settings['sort2'] = display_settings['sort'];
			display_settings['order2'] = display_settings['order'];
			display_settings['sort'] = e.currentTarget.getAttribute('data-field');
			display_settings['order'] = 'ASC';
		}

		return asyncRefetchResults();
	}

	function onEditButtonClick(e) {
		var id = parseInt(e.currentTarget.getAttribute('data-id'));
		var event = events_to_show.find(function(t) {
			return t['id'] === id;
		});
		sandbox.publish('edit-event-command', event);
	}
	
	/***************
	 * Subscribers *
	 ***************/
	function onEventsSearchCommand(term) {
		if (!has_loaded_context) {
			console.debug('onEventsSearchCommand() - Context not loaded yet. Waiting for ' + WAIT_FOR_CONTEXT_PERIOD + 'ms.');
			return setTimeout(function() {
				onEventsSearchCommand(term);
			}, WAIT_FOR_CONTEXT_PERIOD);
		}

		display_settings['search'] = term;
		return asyncRefetchResults();
	}

	function onEventEdited(v) {
		if (!has_loaded_context) {
			console.debug('onEventEdited() - Context not loaded yet. Waiting for ' + WAIT_FOR_CONTEXT_PERIOD + 'ms.');
			return setTimeout(function() {
				onEventEdited(v);
			}, WAIT_FOR_CONTEXT_PERIOD);
		}

		return asyncRefetchResults();
	}
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('events-search-command', onEventsSearchCommand);
		sandbox.subscribe('event-added', onEventEdited);
		sandbox.subscribe('event-edited', onEventEdited);

		asyncLoadDisplaySettings()
		.then(asyncFetchResultsSectionStrings)
		.then(asyncFetchResultTemplate)
		.then(asyncInit);
	} //start()

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});

/******************
 *  Edit Section  *
 ******************/
this.app.controller.startComponent('edit-event-section', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_form = sandbox.findOne('#edit-event-form');
	var dom_form_output = sandbox.findOne('#edit-event-form > output');
	var dom_preview_img = sandbox.findOne('#edit-event-preview-img');

	// Settings
	var WAIT_FOR_CONTEXT_PERIOD = 100; // in ms
	var OUTPUT_SHOW_DURATION = 10000; // in ms

	// Context
	var gallery_options;
	var event_to_edit;
	var form_strings;       // stores messages used in forms
	var output_timer;       // for output
	var has_loaded_context = false;
	var is_submitting = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchFormStrings() {
		if (typeof form_strings !== 'undefined') {
			return Promise.resolve(form_strings);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/data/strings/forms.json').then(function(response_data) {
			form_strings = JSON.parse(response_data);
			return form_strings;
		});
	} //asyncFetchFormStrings()

	function asyncGalleryOptions() {
		return sandbox.sessionDB.get('gallery-options').then(function(data) {
			if (data === null) {
				throw new Error('No gallery-options in sessionDB');
			}

			gallery_options = data;
		});
	} //asyncGalleryOptions()

	/*************
	 * DOM Logic *
	 *************/
	function showAndScrollToSection() {
		dom_component.classList.add('shown');
		sandbox.navigate('#edit-event-section');
	}
	function hideSectionAndScrollTop() {
		dom_component.classList.remove('shown');
		sandbox.navigate('#');
	}
	function populateSubCategoryOptions() {
		sandbox.utils.populateSelect(dom_form['eventGalleryID'], gallery_options);
	}

	function populateForm() {
		if (typeof event_to_edit !== 'object') {
			throw new Error('No event to populate form with.');
		}

		// Text fields
		for (var i=0, len = dom_form.elements.length; i<len; i++) {
			var name = dom_form.elements[i].name;
			dom_form.elements[i].value = event_to_edit[name] || '';
		}
                

		// Promo Start Date
		var day, month, year;
		if (event_to_edit['eventTime']) {
			var start_date = new Date(parseInt(event_to_edit['eventTime']));
			day = start_date.getUTCDate();
			if (day < 10) {
				day = '0' + day;
			}
			month = start_date.getUTCMonth() + 1; // Note: JS Dates start from 0
			if (month < 10) {
				month = '0' + month;
			}
			year = start_date.getUTCFullYear();
			dom_form['eventDate'].value = day + '/' + month + '/' + year;
		}


		// Img
		var img_src = sandbox.settings.WWW_URL + '/img/events/' + event_to_edit['id'] + '.png';
		dom_preview_img.setAttribute('src', img_src + '?' + Date.now()); // append timestamp to bypass cache
	}

	function disableForm() {
		sandbox.utils.disableForm(dom_form);
	}
	function enableForm() {
		sandbox.utils.enableForm(dom_form);
	}

	function outputFormError(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.add('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}
	function outputFormMessage(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.remove('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}

	function clearFormOutput() {
		dom_form_output.classList.remove('error');
		dom_form_output.classList.remove('shown');
		dom_form_output.innerHTML = '';
	}

	function outputFieldError(name, txt) {
		var dom_field_output = sandbox.findOne('output[for='+dom_form[name].id+']');

		dom_field_output.innerHTML = txt;
		dom_field_output.classList.add('error');
		dom_field_output.classList.add('shown');
		sandbox.utils.animateScrollTo(dom_field_output);
	}

	function clearFieldOutputs() {
		var dom_field_outputs = sandbox.findAll('fieldset output');

		for (var i=0,len=dom_field_outputs.length; i<len; i++) {
			dom_field_outputs[i].classList.remove('error');
			dom_field_outputs[i].classList.remove('shown');
			dom_field_outputs[i].innerHTML = '';
		}
	}

	function clearImgPreview() {
		dom_preview_img.removeAttribute('src');
	}

	function showImgPreview() {
		if (dom_form['img'].files && dom_form['img'].files[0]) {
			sandbox.utils.readFileInput(dom_form['img']).then(function(result) {
				dom_preview_img.setAttribute('src', result);
			});
		}
		else {
			dom_preview_img.removeAttribute('src');
		}
	} // showPreview()

	/****************
	 * Module Logic *
	 ****************/
	/**
	 * @param: {bool} output - whether to output error messages (DEFAULT: false)
	 * @return: {bool}
	 */
	function validateForm(output) {
		if (typeof output !== 'boolean') {
			output = false;
		}

		clearFieldOutputs();

		// Name
		var name = dom_form['name'].value;
		if (!sandbox.utils.isName(name)) {
			if (output) {
				outputFieldError('name', form_strings['INVALID_NAME']);
			}
			return false;
		}

		// Description
		var description = dom_form['description'].value;
		if (description.trim() === '') {
			if (output) {
				outputFieldError('description', form_strings['REQUIRED_FIELD']);
			}
			return false;	
		}
		// Event Date
		var event_date = dom_form['eventDate'].value;
		if (event_date !== '' && !sandbox.utils.isDateString(event_date)) {
			if (output) {
				outputFieldError('eventDate', form_strings['INVALID_DATE']);
			}
			return false;
		}

		// Display Priority
		var display_priority = dom_form['displayPriority'].value;
		if (!sandbox.utils.isUInt(display_priority)) {
			if (output) {
				outputFieldError('displayPriority', form_strings['INTEGER_ONLY']);
			}
			return false;
		}

		return true;
	} //validateForm()

	function asyncInit() {
		asyncGalleryOptions()
		.then(function () {
			has_loaded_context = true;

			populateSubCategoryOptions();

			// Listeners
			sandbox.on(dom_form['eventDate'], 'change', onEventDateChange);
			sandbox.on(dom_form['img'], 'change', onImgChange);
			sandbox.on(dom_form, 'submit', onFormSubmit);
			sandbox.on(dom_form, 'reset', onFormReset);
		}).catch(function(e) {
			console.debug(e);
		});
	}

	 /*************
	 * Listeners *
	 *************/
	function onFormSubmit(e) {
		e.preventDefault();

		// Validate form
		if (!validateForm(true)) {
			return;
		}

		// Disable form
		is_submitting = true;
		disableForm();
		outputFormMessage(form_strings['PROCESSING']);

		// AJAX
		var form_data = new FormData(dom_form);
		return sandbox.staffAPI.editEvent(form_data).then(function(response_data) {
			is_submitting = false;
			enableForm();

			sandbox.publish('event-edited', response_data['event']);

			hideSectionAndScrollTop();
		}, function(response_error) {
			is_submitting = false;
			enableForm();

			outputFormError(response_error.message);
		});
	}

	function onFormReset() {
		clearImgPreview();
		clearFieldOutputs();
		clearFormOutput();
		hideSectionAndScrollTop();
	}

	function onImgChange() {
		showImgPreview();
	}

	function onEventDateChange() {
		var parts = dom_form['eventDate'].value.split('/');
		var date_str = parts[2]+ '-' + parts[1] + '-' + parts[0] + 'T00:00:00Z'; // ISO 8601 format
		var time = Date.parse(date_str);
		dom_form['eventTime'].value = time;
	}

	/***************
	 * Subscribers *
	 ***************/
	function onSubCategoryOptionsLoaded() {
		//console.debug('Rx sub-category-options-loaded');
		if (has_loaded_context) {
			return;
		}

		asyncInit();
	}

	function onEditEventCommand(v) {
		if (!has_loaded_context) {
			console.debug('onEditEventCommand() - Context not loaded yet. Waiting for ' + WAIT_FOR_CONTEXT_PERIOD + 'ms.');
			return setTimeout(function() {
				onEditEventCommand(v);
			}, WAIT_FOR_CONTEXT_PERIOD);
		}

		event_to_edit = v;

		populateForm();
		disableForm();
		enableForm();

		showAndScrollToSection();
	}

	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('gallery-options-loaded', onSubCategoryOptionsLoaded);
		sandbox.subscribe('edit-event-command', onEditEventCommand);

		return asyncFetchFormStrings()
		.then(asyncInit);
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});


/*****************
 *  Add Section  *
 *****************/
this.app.controller.startComponent('add-event-section', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_form = sandbox.findOne('#add-event-form');
	var dom_form_output = sandbox.findOne('#add-event-form > output');
	var dom_preview_img = sandbox.findOne('#add-event-preview-img');

	// Settings
	var OUTPUT_SHOW_DURATION = 10000; // in ms

	// Context
	var gallery_options; //sub_category_options
	var form_strings;       // stores messages used in forms
	var output_timer;       // for output
	var has_loaded_context = false;
	var is_submitting = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchFormStrings() {
		if (typeof form_strings !== 'undefined') {
			return Promise.resolve(form_strings);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/data/strings/forms.json').then(function(response_data) {
			form_strings = JSON.parse(response_data);
			return form_strings;
		});
	} //asyncFetchFormStrings()

	function asyncLoadGalleryOptions() {
		return sandbox.sessionDB.get('gallery-options').then(function(data) {
			if (data === null) {
				throw new Error('No gallery-options in sessionDB');
			}

			gallery_options = data;
		});
	} //asyncLoadGalleryOptions()

	/*************
	 * DOM Logic *
	 *************/
	function scrollToComponent() {
		sandbox.navigate('#add-event-section');
	}

	function populateSubCategoryOptions() {
		sandbox.utils.populateSelect(dom_form['eventGalleryID'], gallery_options);
	}

	function disableForm() {
		sandbox.utils.disableForm(dom_form);
	}
	function enableForm() {
		sandbox.utils.enableForm(dom_form);
	}

	function outputFormError(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.add('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}
	function outputFormMessage(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.remove('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}

	function clearFormOutput() {
		dom_form_output.classList.remove('error');
		dom_form_output.classList.remove('shown');
		dom_form_output.innerHTML = '';
	}

	function outputFieldError(name, txt) {
		var dom_field_output = sandbox.findOne('output[for='+dom_form[name].id+']');

		dom_field_output.innerHTML = txt;
		dom_field_output.classList.add('error');
		dom_field_output.classList.add('shown');
		sandbox.utils.animateScrollTo(dom_field_output);
	}

	function clearFieldOutputs() {
		var dom_field_outputs = sandbox.findAll('fieldset output');

		for (var i=0,len=dom_field_outputs.length; i<len; i++) {
			dom_field_outputs[i].classList.remove('error');
			dom_field_outputs[i].classList.remove('shown');
			dom_field_outputs[i].innerHTML = '';
		}
	}

	function clearImgPreview() {
		dom_preview_img.removeAttribute('src');
	}

	function showImgPreview() {
		if (dom_form['img'].files && dom_form['img'].files[0]) {
			sandbox.utils.readFileInput(dom_form['img']).then(function(result) {
				dom_preview_img.setAttribute('src', result);
			});
		}
		else {
			dom_preview_img.removeAttribute('src');
		}
	} // showPreview()

	/****************
	 * Module Logic *
	 ****************/
	/**
	 * @param: {bool} output - whether to output error messages (DEFAULT: false)
	 * @return: {bool}
	 */
	function validateForm(output) {
		if (typeof output !== 'boolean') {
			output = false;
		}

		clearFieldOutputs();

		// Name
		var name = dom_form['name'].value;
		if (!sandbox.utils.isName(name)) {
			if (output) {
				outputFieldError('name', form_strings['INVALID_NAME']);
			}
			return false;
		}

		// Description
		var description = dom_form['description'].value;
		if (description.trim() === '') {
			if (output) {
				outputFieldError('description', form_strings['REQUIRED_FIELD']);
			}
			return false;	
		}

		// Event Date
		var event_date = dom_form['eventDate'].value;
		if (event_date !== '' && !sandbox.utils.isDateString(event_date)) {
			if (output) {
				outputFieldError('eventDate', form_strings['INVALID_DATE']);
			}
			return false;
		}

		// Display Priority
		var display_priority = dom_form['displayPriority'].value;
		if (!sandbox.utils.isUInt(display_priority)) {
			if (output) {
				outputFieldError('displayPriority', form_strings['INTEGER_ONLY']);
			}
			return false;
		}

		// Img
		if (dom_form['img'].files.length === 0) {
			if (output) {
				outputFieldError('img', form_strings['NO_FILE_UPLOAD']);
			}
			return false;
		}

		return true;
	} //validateForm()

	function asyncInit() {
		asyncLoadGalleryOptions()
		.then(function () {
			has_loaded_context = true;

			populateSubCategoryOptions();

			// Listeners
			sandbox.on(dom_form['eventDate'], 'change', onEventDateChange);
			sandbox.on(dom_form['img'], 'change', onImgChange);
			sandbox.on(dom_form, 'submit', onFormSubmit);
			sandbox.on(dom_form, 'reset', onFormReset);
		}).catch(function(e) {
			console.debug(e);
		});
	}
	 
	 /*************
	 * Listeners *
	 *************/
	function onFormSubmit(e) {
		e.preventDefault();

		// Validate form
		if (!validateForm(true)) {
			return;
		}

		// Disable form
		is_submitting = true;
		disableForm();
		outputFormMessage(form_strings['PROCESSING']);

		// AJAX
		var form_data = new FormData(dom_form);
		return sandbox.staffAPI.addEvent(form_data).then(function(response_data) {
			is_submitting = false;
			enableForm();

			sandbox.publish('event-added', response_data['event']);

			dom_form.reset();
			outputFormMessage(form_strings['ADD_SUCCESS']);
		}, function(response_error) {
			is_submitting = false;
			enableForm();

			outputFormError(response_error.message);
		});
	}
	function onFormReset() {
		clearImgPreview();
		clearFieldOutputs();
		clearFormOutput();
		scrollToComponent();
	}

	function onImgChange() {
		showImgPreview();
	}

	function onEventDateChange() {
		var parts = dom_form['eventDate'].value.split('/');
		var date_str = parts[2]+ '-' + parts[1] + '-' + parts[0] + 'T00:00:00Z'; // ISO 8601 format
		var time = Date.parse(date_str);
		dom_form['eventTime'].value = time;
	}

	/***************
	 * Subscribers *
	 ***************/
	function onSubCategoryOptionsLoaded() {
		//console.debug('Rx sub-category-options-loaded');
		if (has_loaded_context) {
			return;
		}

		asyncInit();
	}

	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('gallery-options-loaded', onSubCategoryOptionsLoaded);

		return asyncFetchFormStrings()
		.then(asyncInit);
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});
