/*****************
 * Module Header *
 *****************/
this.app.controller.startComponent('careers-module-header', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_search_form = sandbox.findOne('#job-listings-search-form');

	// Context
	var search_term;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncLoadSearchTerm() {
		if (typeof search_term !== 'undefined') {
			return Promise.resolve(search_term);
		}

		return sandbox.localDB.get('preference-job-listings-display-settings').then(function(val) {
			if (val) {
				search_term = val['search'];
			}
			else {
				search_term = '';
			}

			return search_term;
		});
	} //asyncLoadSearchTerm()

	/*************
	 * DOM Logic *
	 *************/
	function showSearchTerm() {
		dom_search_form['search'].value = search_term;
	}

	/****************
	 * Module Logic *
	 ****************/
	
	/*************
	 * Listeners *
	 *************/
	function onSearchFormSubmit(e) {
		e.preventDefault();

		sandbox.publish('job-listings-search-command', dom_search_form['search'].value);
	}
	
	/***************
	 * Subscribers *
	 ***************/
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		asyncLoadSearchTerm()
		.then(function() {
			showSearchTerm();
		
			// Listeners
			sandbox.on(dom_search_form, 'submit', onSearchFormSubmit);
		});
	} //start()

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});

/******************
 *  View Section  *
 ******************/
this.app.controller.startComponent('view-job-listings-section', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_header_pagination = sandbox.findOne('#results-section-header-pagination');
	var dom_footer_pagination = sandbox.findOne('#results-section-footer-pagination');
	var dom_results_per_page_buttons = sandbox.findAll('.results-display-controls > button');
	var dom_results_tbody = sandbox.findOne('#job-listings-tbody');
	var dom_results_sortable_th = sandbox.findAll('#job-listings-table th.sortable');
	var dom_results_edit_buttons;   // dynamic

	// Settings
	var WAIT_FOR_CONTEXT_PERIOD = 100; // in ms
	var DEAFULT_DISPLAY_SETTINGS = {showCount: 10, sort: 'displayPriority', order: 'ASC', sort2: 'title', order2: 'ASC', search:''};

	// Context
	var display_settings;
	var result_template;
	var results_section_strings;  // stores messages used in forms
	var job_listings_to_show;     // array of objects
	var total_count;              // total count fitting this search criteria
	var curr_page;
	var has_loaded_context = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncLoadDisplaySettings() {
		if (typeof display_settings !== 'undefined') {
			return Promise.resolve(display_settings);
		}

		return sandbox.localDB.get('preference-job-listings-display-settings').then(function(val) {
			if (val) {
				return val;
			}
			else {
				return sandbox.localDB.set('preference-job-listings-display-settings', DEAFULT_DISPLAY_SETTINGS);
			}
		}).then(function(val) {
			display_settings = val;
			curr_page = 1;

			return display_settings;
		});
	} //asyncLoadDisplaySettings()

	function asyncSaveDisplaySettings() {
		return sandbox.localDB.set('preference-job-listings-display-settings', display_settings);
	} //asyncSaveDisplaySettings()

	function asyncFetchResultsSectionStrings() {
		if (typeof results_section_strings !== 'undefined') {
			return Promise.resolve(results_section_strings);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/data/strings/results-section.json').then(function(response_data) {
			results_section_strings = JSON.parse(response_data);
			return results_section_strings;
		});
	} //asyncFetchResultsSectionStrings()

	function asyncFetchResultTemplate() {
		if (typeof result_template !== 'undefined') {
			return Promise.resolve(result_template);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/html/templates/careers/job-listing-result.html').then(function(response_data) {
			result_template = response_data;
			return result_template;
		});
	} //asyncFetchVerifiedMemberTemplate()

	function asyncFetchResults() {
		// Prepare search conditions
		var name_condition = [
			['title', 'LIKE', '%'+display_settings['search']+'%']
		];
		var responsibilities_condition = [
			['responsibilities', 'LIKE', '%'+display_settings['search']+'%']
		];
		var traits_condition = [
			['traits', 'LIKE', '%'+display_settings['search']+'%']
		];
		var or_conditions = [name_condition, responsibilities_condition, traits_condition];

		// Prepare offset
		var offset;
		if (curr_page === 0) {
			offset = 0;
		}
		else {
			offset = (curr_page - 1) * display_settings['showCount'];
		}

		// AJAX 
		return sandbox.staffAPI.getJobListings({
			conditions: JSON.stringify(or_conditions), // 3D array of OR conditions
			offset: offset,
			count: display_settings['showCount'],
			sort: display_settings['sort'],
			order: display_settings['order'],
			sort2: display_settings['sort2'],
			order2: display_settings['order2']
		}).then(function(response_data) {	
			total_count = response_data['count'];
			job_listings_to_show = response_data['jobListings'];

			return job_listings_to_show;
		});
	} //asyncFetchResults()

	/*************
	 * DOM Logic *
	 *************/
	function populateDisplaySettings() {
		if (!has_loaded_context) {
			throw new Error('populateDisplaySettings() - cannot proceed before context has loaded');
		}

		var i, len;
		for (i=0,len=dom_results_per_page_buttons.length; i<len; i++) {
			if (parseInt(dom_results_per_page_buttons[i].getAttribute('data-count')) === display_settings['showCount']) {
				dom_results_per_page_buttons[i].classList.add('current');
			}
			else {
				dom_results_per_page_buttons[i].classList.remove('current');
			}
		}
	} //populateDisplaySettings()

	// Show table header sort according to sort values (in display settings)
	function populateTableHeadersSortSettings() {
		if (!has_loaded_context) {
			throw new Error('populateTableHeadersSortSettings() - cannot proceed before context has loaded');
		}

		for (var i=0,len=dom_results_sortable_th.length; i<len; i++) {
			if (dom_results_sortable_th[i].getAttribute('data-field') === display_settings['sort']) {
				dom_results_sortable_th[i].classList.add('sort-selected');
				if (display_settings['order'] === 'ASC') {
					dom_results_sortable_th[i].classList.remove('desc');
				}
				else {
					dom_results_sortable_th[i].classList.add('desc');
				}
			}
			else {
				dom_results_sortable_th[i].classList.remove('sort-selected');
				dom_results_sortable_th[i].classList.remove('desc');
			}
		}
	} // populateTableHeadersSortSettings()

	function populateTableBody() {
		if (!has_loaded_context) {
			throw new Error('populateTableBody() - cannot proceed before context has loaded');
		}

		var i,len;

		// Generate HTML
		var html = '';
		for (i=0,len=job_listings_to_show.length; i<len; i++) {
			html += sandbox.utils.renderTemplate(result_template, {
				id: job_listings_to_show[i]['id'],
				title: job_listings_to_show[i]['title'],
				displayPriority: job_listings_to_show[i]['displayPriority']
			});
		}
		dom_results_tbody.innerHTML = html;

		// Dynamic DOM elements
		dom_results_edit_buttons = sandbox.findAll('#job-listings-tbody .edit-button');

		// Bind Listeners
		sandbox.on(dom_results_edit_buttons, 'click', onEditButtonClick);
	} //populateTableBody()

	function populatePagination() {
		if (!has_loaded_context) {
			throw new Error('populatePagination() - cannot proceed before context has loaded');
		}

		var options = {
			pageCount: Math.ceil(total_count / display_settings['showCount']),
			currPage: curr_page,
			onButtonClick: onPaginationButtonClick,
			prevButtonText: results_section_strings['PREV_BUTTON_TEXT'],
			nextButtonText: results_section_strings['NEXT_BUTTON_TEXT']
		};

		sandbox.paginator.init(dom_header_pagination, options);
		sandbox.paginator.init(dom_footer_pagination, options);
	} //populatePagination()

	/****************
	 * Module Logic *
	 ****************/
	function asyncRefetchResults() {
		if (!has_loaded_context) {
			throw new Error('asyncRefetchResults() - cannot proceed before context has loaded');
		}

		return asyncSaveDisplaySettings()
		.then(asyncFetchResults)
		.then(function() {
			populatePagination();
			populateDisplaySettings();
			populateTableHeadersSortSettings();
			populateTableBody();
		});
	}

	function asyncInit() {
		asyncFetchResults()
		.then(function (){
			has_loaded_context = true;

			populatePagination();
			populateDisplaySettings();
			populateTableHeadersSortSettings();
			populateTableBody();

			// Listeners
			sandbox.on(dom_results_sortable_th, 'click', onSortableHeaderClick);
			sandbox.on(dom_results_per_page_buttons, 'click', onResultsPerPageButtonClick);
		});
	} // asyncInit()

	/*************
	 * Listeners *
	 *************/
	function onPaginationButtonClick(pg) {
		curr_page = pg;

		return asyncRefetchResults();
	}

	function onResultsPerPageButtonClick(e) {
		e.preventDefault();

		display_settings['showCount'] = parseInt(e.currentTarget.getAttribute('data-count'));
		curr_page = 1;
		return asyncRefetchResults();
	}

	function onSortableHeaderClick(e) {
		if (e.currentTarget.classList.contains('sort-selected')) {
			display_settings['order'] = (display_settings['order'] === 'ASC')? 'DESC' : 'ASC';
		}
		else {
			display_settings['sort2'] = display_settings['sort'];
			display_settings['order2'] = display_settings['order'];
			display_settings['sort'] = e.currentTarget.getAttribute('data-field');
			display_settings['order'] = 'ASC';
		}

		return asyncRefetchResults();
	}

	function onEditButtonClick(e) {
		var id = parseInt(e.currentTarget.getAttribute('data-id'));
		var job_listing = job_listings_to_show.find(function(t) {
			return t['id'] === id;
		});
		sandbox.publish('edit-job-listing-command', job_listing);
	}
	
	/***************
	 * Subscribers *
	 ***************/
	function onJobListingsSearchCommand(term) {
		if (!has_loaded_context) {
			console.debug('onJobListingsSearchCommand() - Context not loaded yet. Waiting for ' + WAIT_FOR_CONTEXT_PERIOD + 'ms.');
			return setTimeout(function() {
				onJobListingsSearchCommand(term);
			}, WAIT_FOR_CONTEXT_PERIOD);
		}

		display_settings['search'] = term;
		return asyncRefetchResults();
	}

	function onJobListingEdited(v) {
		if (!has_loaded_context) {
			console.debug('onJobListingEdited() - Context not loaded yet. Waiting for ' + WAIT_FOR_CONTEXT_PERIOD + 'ms.');
			return setTimeout(function() {
				onJobListingEdited(v);
			}, WAIT_FOR_CONTEXT_PERIOD);
		}

		return asyncRefetchResults();
	}
	 
	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('job-listings-search-command', onJobListingsSearchCommand);
		sandbox.subscribe('job-listing-added', onJobListingEdited);
		sandbox.subscribe('job-listing-edited', onJobListingEdited);

		asyncLoadDisplaySettings()
		.then(asyncFetchResultsSectionStrings)
		.then(asyncFetchResultTemplate)
		.then(asyncInit);
	} //start()

	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});

/******************
 *  Edit Section  *
 ******************/
this.app.controller.startComponent('edit-job-listing-section', function(sandbox) {
	'use strict';
	
	// DOM elements
	var dom_component = sandbox.getComponentElement();
	var dom_form = sandbox.findOne('#edit-job-listing-form');
	var dom_form_output = sandbox.findOne('#edit-job-listing-form > output');

	// Settings
	var OUTPUT_SHOW_DURATION = 10000; // in ms

	// Context
	var job_listing_to_edit;
	var form_strings;       // stores messages used in forms
	var output_timer;       // for output
	var is_submitting = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchFormStrings() {
		if (typeof form_strings !== 'undefined') {
			return Promise.resolve(form_strings);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/data/strings/forms.json').then(function(response_data) {
			form_strings = JSON.parse(response_data);
			return form_strings;
		});
	} //asyncFetchFormStrings()

	/*************
	 * DOM Logic *
	 *************/
	function showAndScrollToSection() {
		dom_component.classList.add('shown');
		sandbox.navigate('#edit-job-listing-section');
	}
	function hideSectionAndScrollTop() {
		dom_component.classList.remove('shown');
		sandbox.navigate('#');
	}
	
	function populateForm() {
		if (typeof job_listing_to_edit !== 'object') {
			throw new Error('No jobListing to populate form with.');
		}

		// Text fields
		for (var i=0, len = dom_form.elements.length; i<len; i++) {
			var name = dom_form.elements[i].name;
			dom_form.elements[i].value = job_listing_to_edit[name] || '';
		}
	}

	function disableForm() {
		sandbox.utils.disableForm(dom_form);
	}
	function enableForm() {
		sandbox.utils.enableForm(dom_form);
	}

	function outputFormError(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.add('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}
	function outputFormMessage(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.remove('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}

	function clearFormOutput() {
		dom_form_output.classList.remove('error');
		dom_form_output.classList.remove('shown');
		dom_form_output.innerHTML = '';
	}

	function outputFieldError(name, txt) {
		var dom_field_output = sandbox.findOne('output[for='+dom_form[name].id+']');

		dom_field_output.innerHTML = txt;
		dom_field_output.classList.add('error');
		dom_field_output.classList.add('shown');
		sandbox.utils.animateScrollTo(dom_field_output);
	}

	function clearFieldOutputs() {
		var dom_field_outputs = sandbox.findAll('fieldset output');

		for (var i=0,len=dom_field_outputs.length; i<len; i++) {
			dom_field_outputs[i].classList.remove('error');
			dom_field_outputs[i].classList.remove('shown');
			dom_field_outputs[i].innerHTML = '';
		}
	}

	/****************
	 * Module Logic *
	 ****************/
	/**
	 * @param: {bool} output - whether to output error messages (DEFAULT: false)
	 * @return: {bool}
	 */
	function validateForm(output) {
		if (typeof output !== 'boolean') {
			output = false;
		}

		clearFieldOutputs();

		// Title
		var title = dom_form['title'].value;
		if (!sandbox.utils.isName(title)) {
			if (output) {
				outputFieldError('title', form_strings['INVALID_NAME']);
			}
			return false;
		}

		// Responsibilities
		var responsibilities = dom_form['responsibilities'].value;
		if (responsibilities.trim() === '') {
			if (output) {
				outputFieldError('responsibilities', form_strings['REQUIRED_FIELD']);
			}
			return false;	
		}

		// Traits
		var traits = dom_form['traits'].value;
		if (traits.trim() === '') {
			if (output) {
				outputFieldError('traits', form_strings['REQUIRED_FIELD']);
			}
			return false;	
		}

		// Display Priority
		var display_priority = dom_form['displayPriority'].value;
		if (!sandbox.utils.isUInt(display_priority)) {
			if (output) {
				outputFieldError('displayPriority', form_strings['INTEGER_ONLY']);
			}
			return false;
		}

		return true;
	} //validateForm()

	 /*************
	 * Listeners *
	 *************/
	function onFormSubmit(e) {
		e.preventDefault();

		// Validate form
		if (!validateForm(true)) {
			return;
		}

		// Disable form
		is_submitting = true;
		disableForm();
		outputFormMessage(form_strings['PROCESSING']);

		// AJAX
		var form_data = new FormData(dom_form);
		return sandbox.staffAPI.editJobListing(form_data).then(function(response_data) {
			is_submitting = false;
			enableForm();

			sandbox.publish('job-listing-edited', response_data['jobListing']);

			hideSectionAndScrollTop();
		}, function(response_error) {
			is_submitting = false;
			enableForm();

			outputFormError(response_error.message);
		});
	}

	function onFormReset() {
		clearFieldOutputs();
		clearFormOutput();
		hideSectionAndScrollTop();
	}

	/***************
	 * Subscribers *
	 ***************/
	function onEditJobListingCommand(v) {
		job_listing_to_edit = v;

		populateForm();
		disableForm();
		enableForm();

		showAndScrollToSection();
	}

	/**************
	 * Life Cycle *
	 **************/
	function start() {
		// Subscribers
		sandbox.subscribe('edit-job-listing-command', onEditJobListingCommand);

		return asyncFetchFormStrings()
		.then(function(){
			// Listeners
			sandbox.on(dom_form, 'submit', onFormSubmit);
			sandbox.on(dom_form, 'reset', onFormReset);
		});
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});

/*****************
 *  Add Section  *
 *****************/
this.app.controller.startComponent('add-job-listing-section', function(sandbox) {
	'use strict';
	
	// DOM elements
	//var dom_component = sandbox.getComponentElement();
	var dom_form = sandbox.findOne('#add-job-listing-form');
	var dom_form_output = sandbox.findOne('#add-job-listing-form > output');

	// Settings
	var OUTPUT_SHOW_DURATION = 10000; // in ms

	// Context
	var form_strings;       // stores messages used in forms
	var output_timer;       // for output
	var is_submitting = false;
	
	/*****************
	 * Context Logic *
	 *****************/
	function asyncFetchFormStrings() {
		if (typeof form_strings !== 'undefined') {
			return Promise.resolve(form_strings);
		}

		return sandbox.utils.ajaxLoad(sandbox.settings.DOMAIN_URL + '/data/strings/forms.json').then(function(response_data) {
			form_strings = JSON.parse(response_data);
			return form_strings;
		});
	} //asyncFetchFormStrings()

	/*************
	 * DOM Logic *
	 *************/
	function scrollToComponent() {
		sandbox.navigate('#add-job-listing-section');
	}

	function disableForm() {
		sandbox.utils.disableForm(dom_form);
	}
	function enableForm() {
		sandbox.utils.enableForm(dom_form);
	}

	function outputFormError(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.add('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}
	function outputFormMessage(txt) {
		dom_form_output.innerHTML = sandbox.utils.escapeHTML(txt);
		dom_form_output.classList.remove('error');
		dom_form_output.classList.add('shown');

		// Auto-hide some time later
		clearTimeout(output_timer);
		output_timer = setTimeout(clearFormOutput, OUTPUT_SHOW_DURATION);
	}

	function clearFormOutput() {
		dom_form_output.classList.remove('error');
		dom_form_output.classList.remove('shown');
		dom_form_output.innerHTML = '';
	}

	function outputFieldError(name, txt) {
		var dom_field_output = sandbox.findOne('output[for='+dom_form[name].id+']');

		dom_field_output.innerHTML = txt;
		dom_field_output.classList.add('error');
		dom_field_output.classList.add('shown');
		sandbox.utils.animateScrollTo(dom_field_output);
	}

	function clearFieldOutputs() {
		var dom_field_outputs = sandbox.findAll('fieldset output');

		for (var i=0,len=dom_field_outputs.length; i<len; i++) {
			dom_field_outputs[i].classList.remove('error');
			dom_field_outputs[i].classList.remove('shown');
			dom_field_outputs[i].innerHTML = '';
		}
	}

	/****************
	 * Module Logic *
	 ****************/
	/**
	 * @param: {bool} output - whether to output error messages (DEFAULT: false)
	 * @return: {bool}
	 */
	function validateForm(output) {
		if (typeof output !== 'boolean') {
			output = false;
		}

		clearFieldOutputs();

		// Title
		var title = dom_form['title'].value;
		if (!sandbox.utils.isName(title)) {
			if (output) {
				outputFieldError('title', form_strings['INVALID_NAME']);
			}
			return false;
		}

		// Responsibilities
		var responsibilities = dom_form['responsibilities'].value;
		if (responsibilities.trim() === '') {
			if (output) {
				outputFieldError('responsibilities', form_strings['REQUIRED_FIELD']);
			}
			return false;	
		}

		// Traits
		var traits = dom_form['traits'].value;
		if (traits.trim() === '') {
			if (output) {
				outputFieldError('traits', form_strings['REQUIRED_FIELD']);
			}
			return false;	
		}

		// Display Priority
		var display_priority = dom_form['displayPriority'].value;
		if (!sandbox.utils.isUInt(display_priority)) {
			if (output) {
				outputFieldError('displayPriority', form_strings['INTEGER_ONLY']);
			}
			return false;
		}

		return true;
	} //validateForm()

	 /*************
	 * Listeners *
	 *************/
	function onFormSubmit(e) {
		e.preventDefault();

		// Validate form
		if (!validateForm(true)) {
			return;
		}

		// Disable form
		is_submitting = true;
		disableForm();
		outputFormMessage(form_strings['PROCESSING']);

		// AJAX
		var form_data = new FormData(dom_form);
		return sandbox.staffAPI.addJobListing(form_data).then(function(response_data) {
			is_submitting = false;
			enableForm();

			sandbox.publish('job-listing-added', response_data['jobListing']);

			dom_form.reset();
			outputFormMessage(form_strings['ADD_SUCCESS']);
		}, function(response_error) {
			is_submitting = false;
			enableForm();

			outputFormError(response_error.message);
		});
	}

	function onFormReset() {
		clearFieldOutputs();
		clearFormOutput();
		scrollToComponent();
	}

	/***************
	 * Subscribers *
	 ***************/

	/**************
	 * Life Cycle *
	 **************/
	function start() {
		return asyncFetchFormStrings()
		.then(function () {
			// Listeners
			sandbox.on(dom_form, 'submit', onFormSubmit);
			sandbox.on(dom_form, 'reset', onFormReset);
		});
	} //start()
	function stop() {
		sandbox.off();
		sandbox.unsubscribe();
	} // stop()
	
	return {
		start: start,
		stop: stop
	};
});
