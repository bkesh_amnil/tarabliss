-- phpMyAdmin SQL Dump
-- version 4.6.0deb1.wily~ppa.1
-- http://www.phpmyadmin.net
--
-- Host: 10.240.44.227
-- Generation Time: Aug 25, 2016 at 07:38 PM
-- Server version: 10.1.16-MariaDB-1~wily
-- PHP Version: 5.6.23-1+deprecated+dontuse+deb.sury.org~wily+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tarabliss`
--

-- --------------------------------------------------------

--
-- Table structure for table `TB_Accolade`
--

CREATE TABLE `TB_Accolade` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `displayPriority` tinyint(3) UNSIGNED NOT NULL,
  `createdTime` bigint(20) UNSIGNED NOT NULL,
  `lastUpdateTime` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TB_Accolade`
--

INSERT INTO `TB_Accolade` (`id`, `title`, `description`, `displayPriority`, `createdTime`, `lastUpdateTime`) VALUES
(1, 'Elle Beauty Treat List 2016', 'The winner of Best Body Pampering goes to our Grand Luxe Body Treatment. Leave stress at the door with this indulgent body treat!', 5, 1462852192497, 1462852192497),
(2, 'Spa Awards 2015', 'The winner of Best Face-Shaping Treatment goes to our Magic-V Contour and Intensive Hydration Therapy, which also does wonders to smooth lines and wrinkles!', 4, 1462852192497, 1462852192497),
(3, 'Style Best Beauty 2015', 'The winner of Best Beauty Treats goes to our Slim and Contour Tummy Trimming Treatment. Hoping to tone up your mid-second? Consider your wish granted!', 3, 1462852192497, 1462852192497);

-- --------------------------------------------------------

--
-- Table structure for table `TB_Article`
--

CREATE TABLE `TB_Article` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `excerpt` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `displayPriority` tinyint(3) UNSIGNED NOT NULL,
  `createdTime` bigint(20) UNSIGNED NOT NULL,
  `lastUpdateTime` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TB_Article`
--

INSERT INTO `TB_Article` (`id`, `title`, `excerpt`, `url`, `displayPriority`, `createdTime`, `lastUpdateTime`) VALUES
(1, 'Take Your Sunscreen Seriously', 'Many of us know from all the hardcore marketing from sunscreen companies that sunblock is an absolute necessity but', 'https://blog.tarabliss.com.sg/2016/02/04/why-your-should-take-your-sunscreen-seriously/', 99, 1462852194681, 1462861263111),
(2, 'Plagued by Pigmentation?', 'First things first, what is pigmentation? Pigmentation is ageing dark spots usually caused by sun exposure specifically the', 'https://blog.tarabliss.com.sg/2016/03/21/plagued-by-pigmentation/', 98, 1462852194681, 1462852194681),
(3, 'Spray-On Beauty Products?', 'Recently, spray on beauty products are making waves. From spray on nail polish to spray on blush, there is something for', 'https://blog.tarabliss.com.sg/2016/03/24/spray-makeup-yay-nay/', 97, 1462852194681, 1462861250505);

-- --------------------------------------------------------

--
-- Table structure for table `TB_Category`
--

CREATE TABLE `TB_Category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `displayPriority` tinyint(3) UNSIGNED NOT NULL,
  `createdTime` bigint(20) UNSIGNED NOT NULL,
  `lastUpdateTime` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TB_Category`
--

INSERT INTO `TB_Category` (`id`, `name`, `description`, `displayPriority`, `createdTime`, `lastUpdateTime`) VALUES
(1, 'Hi-Tech Aesthetics', 'We believe in the efficacy of modern technology. By combining the latest proven technology with the deeply relaxing touch of the artful hands, the result is pure magic on the skin.', 99, 1462852196652, 1462852196652),
(2, 'Body Pampering', 'Release those knots on the shoulders and get rid of the tensions (and excess water) that’s weighing you down. Our menu of body delights is curated for utter relaxation and innermost revitalisation. Be prepared to float out of the door on cloud nine.   \r\n', 97, 1462852196652, 1469583952626),
(3, 'Skin Deep', 'Defy the clock with our line-up of pampering treatments that will refresh your skin by providing deep hydration and eliminate signs of fine lines and fatigue promoting renewal and radiance from inside out. All treatments include ampoule and deep hydration masks, as well as our signature detox massage.\r\n', 98, 1462852196652, 1469583927005),
(4, 'No More Fuzz', 'Dare to bare with TaraBliss’ semi-permanent hair removal solutions, which utilises highly advanced Dynamic Pulse Light (DPL) and Light and Heat Energy (LHE) lasers for the ultimate solution to an effortlessly silky-smooth body — all day, every day.', 94, 1462852196652, 1469583824013),
(5, 'Lux Indulgence', 'Every significant milestone deserves to be celebrated! From radiance-boosting pre-wedding facials to tummy-tightening post-natal treatments, we care for you at every thrilling step of life’s journey. Plus, our legendary, girly spa parties are the stuff that fond memories are made of.', 96, 1462852196652, 1469583786026),
(6, 'Gentlemen', 'Even men needs a little TLC now and then. Our tempting line-up of men-specific face treatments is designed to deeply-cleanse, nourish and put you in a state of utter comfort and dream-like bliss. \r\n', 95, 1462852196652, 1469584015473);

-- --------------------------------------------------------

--
-- Table structure for table `TB_JobListing`
--

CREATE TABLE `TB_JobListing` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `responsibilities` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `traits` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `displayPriority` tinyint(3) UNSIGNED NOT NULL,
  `createdTime` bigint(20) UNSIGNED NOT NULL,
  `lastUpdateTime` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TB_JobListing`
--

INSERT INTO `TB_JobListing` (`id`, `title`, `responsibilities`, `traits`, `displayPriority`, `createdTime`, `lastUpdateTime`) VALUES
(1, 'Spa Therapist', 'Lorem ipsum...', 'Self-motivated,Enthusiastic,Empathetic,"Willing to work shift hours including nights, weekends & Public Holidays",Able to work alone as well as part of a team.', 1, 1462852198889, 1462852198889);

-- --------------------------------------------------------

--
-- Table structure for table `TB_Member`
--

CREATE TABLE `TB_Member` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDay` tinyint(3) UNSIGNED DEFAULT NULL,
  `birthMonth` tinyint(3) UNSIGNED DEFAULT NULL,
  `birthYear` smallint(5) UNSIGNED DEFAULT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `emailVerified` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `emailVerificationHash` varchar(77) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otpAttemptsRemaining` tinyint(3) UNSIGNED NOT NULL DEFAULT '5',
  `passwordHash` varchar(77) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persistentLoginHashes` varchar(2000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `passwordAttemptsRemaining` tinyint(3) UNSIGNED NOT NULL DEFAULT '10',
  `createdTime` bigint(20) UNSIGNED NOT NULL,
  `lastUpdateTime` bigint(20) UNSIGNED NOT NULL,
  `lastLoginTime` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `TB_Staff`
--

CREATE TABLE `TB_Staff` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDay` tinyint(3) UNSIGNED DEFAULT NULL,
  `birthMonth` tinyint(3) UNSIGNED DEFAULT NULL,
  `birthYear` smallint(5) UNSIGNED DEFAULT NULL,
  `accessLevel` tinyint(3) UNSIGNED NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `emailVerified` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `emailVerificationHash` varchar(77) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otpAttemptsRemaining` tinyint(3) UNSIGNED NOT NULL DEFAULT '5',
  `passwordHash` varchar(77) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persistentLoginHashes` varchar(2000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `passwordAttemptsRemaining` tinyint(3) UNSIGNED NOT NULL DEFAULT '10',
  `createdTime` bigint(20) UNSIGNED NOT NULL,
  `lastUpdateTime` bigint(20) UNSIGNED NOT NULL,
  `lastLoginTime` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TB_Staff`
--

INSERT INTO `TB_Staff` (`id`, `name`, `phone`, `gender`, `birthDay`, `birthMonth`, `birthYear`, `accessLevel`, `email`, `emailVerified`, `emailVerificationHash`, `otpAttemptsRemaining`, `passwordHash`, `persistentLoginHashes`, `passwordAttemptsRemaining`, `createdTime`, `lastUpdateTime`, `lastLoginTime`) VALUES
(1, 'Isen', NULL, NULL, NULL, NULL, NULL, 99, 'isen.majennt@techplusart.com', 1, NULL, 5, 'sha256:1000:feEhYUiWSuBKufmd2NTlfGXuT/8ScNMh:ZLrMzBz6VTv8jraQtBMWCG537/LnZY7q', 'sha256:1000:MDAwMDAwMDAwMDAxNDY4ODI0NTAwMDc0:ypkPpAD6Nh4X92no0KNu1iS9wuNMfAaI,sha256:1000:MDAwMDAwMDAwMDAxNDcwMzA2MDk2OTY1:E05ETMzP9Zkb5tzyuxALRGjfRVHWN2at,sha256:1000:MDAwMDAwMDAwMDAxNDcwMDIwODQzODE3:jPLADGgQ/Wu/rdq8hSFQGlLD71WljukL,sha256:1000:MDAwMDAwMDAwMDAxNDcwMzA1OTIxMzY3:3uy4MbzCuIV9omAeHaFWV5favblOZTcc,sha256:1000:MDAwMDAwMDAwMDAxNDcwOTg4Nzk0MDY2:E2vX+gvUgalY8hziq/BVWcWbFZF7BkSZ,sha256:1000:MDAwMDAwMDAwMDAxNDcwMzA2MTI1MzQz:zkGcn6kpLgb/hznbJZ6bj2hrWd6BY994,sha256:1000:MDAwMDAwMDAwMDAxNDcwMzA2MTI4MTUx:R4qE3PoLT3n1VDMc4EzsNMlcrt8q9QX6,sha256:1000:MDAwMDAwMDAwMDAxNDcwOTg4ODIwMjQ1:B3BC0IeE4Tyld78Poq2ilY6l++5gmyX1,sha256:1000:MDAwMDAwMDAwMDAxNDcwOTg5MTY2NDYx:Ue+KM5PlLmQNXyRAne9uRAN0mkPOspiM,sha256:1000:MDAwMDAwMDAwMDAxNDcxNDI3Mjg1ODI2:vxhqhvcB8xMf/mOV3W6wMbfoDUB5nGyH,sha256:1000:MDAwMDAwMDAwMDAxNDcxNDI3NDAwNjk3:3NHqFCRE01BnQEowA1HqubkSsTERJieV,sha256:1000:MDAwMDAwMDAwMDAxNDcxNDI3NTkyNTI1:Oy2W3cxVmIyd1QJxl066iMHuOCo+r3RV,sha256:1000:MDAwMDAwMDAwMDAxNDcxNTg4MzE0NTEy:UpW166dQ7OY7mu4MczApqG6fTm3AQFJ5', 9, 1462852201147, 1471588428070, 1471588314512),
(2, 'Admin', NULL, NULL, NULL, NULL, NULL, 90, 'admin@tarabliss.com.sg', 1, NULL, 5, 'sha256:1000:tIvRS9LhdAQ2UA97g9vdRUoHT0idN8pg:rzW4bomDTG72mLclUCx47PUgFymBaogZ', 'sha256:1000:MDAwMDAwMDAwMDAxNDY5NjkyMzgyODgz:gT4y8rSxtPwv4Trk1TDb3LroRVKOjOHS,sha256:1000:MDAwMDAwMDAwMDAxNDY5NTc2Mjk4Nzk5:EkGEscfhZoL8VnikOj9lqvaT6q7l7eH2,sha256:1000:MDAwMDAwMDAwMDAxNDY5NjkxNDU3Mjgy:t11YI1kNwBE+uuZyWwojVw/wzBFxMtBx,sha256:1000:MDAwMDAwMDAwMDAxNDcwNjM0ODk5NzI4:CjAOprl4mfO1O0+hcH/5ynVHsZ/3p7I3,sha256:1000:MDAwMDAwMDAwMDAxNDcwODAzNTIwMzQ2:ddDj6SxYocWXAFMt3DfJfhbTMFBKeUz5,sha256:1000:MDAwMDAwMDAwMDAxNDcwODg2MDE0NTY4:riZLIcBdvPNM7Gb2AkJNRpNH7YAgRViR,sha256:1000:MDAwMDAwMDAwMDAxNDcxMzQxNDYyMTkw:4gs1NnOE7ddhIRZZ0mytatvyanAZqZCt,sha256:1000:MDAwMDAwMDAwMDAxNDcxNDI3NDQ2MDQ0:NKfCq1wpRjpVOsbQ7uKoI9Ggb/pXB4f/,sha256:1000:MDAwMDAwMDAwMDAxNDcxNDI3NTcwMzU1:wUbwoPELUr4n2fSm5gP/Iul3XhuINz9l', 10, 1462852201147, 1471427570355, 1471427570355);

-- --------------------------------------------------------

--
-- Table structure for table `TB_SubCategory`
--

CREATE TABLE `TB_SubCategory` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `categoryID` int(10) UNSIGNED NOT NULL,
  `description` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `displayPriority` tinyint(3) UNSIGNED NOT NULL,
  `createdTime` bigint(20) UNSIGNED NOT NULL,
  `lastUpdateTime` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TB_SubCategory`
--

INSERT INTO `TB_SubCategory` (`id`, `name`, `categoryID`, `description`, `displayPriority`, `createdTime`, `lastUpdateTime`) VALUES
(1, 'Purification+', 1, 'Acne Treatment', 99, 1462852204409, 1462852204409),
(2, 'Lightening+', 1, 'Pigmentation Treatment', 98, 1462852204409, 1462852204409),
(3, 'Whitening+', 1, 'Whitening Treatment', 97, 1462852204409, 1462852204409),
(4, 'Firming+', 1, 'Firming & Lifting', 96, 1462852204409, 1462852204409),
(5, 'Detoxification+', 1, 'Detox', 95, 1462852204409, 1462852204409),
(6, 'Undereye+', 1, 'Undereye', 94, 1462852204409, 1462852204409),
(7, 'Firm Up!', 2, 'Body Contouring', 99, 1462852204409, 1469587554960),
(8, 'Deep Cleanse ', 2, 'Body Scrub', 98, 1462852204409, 1469587570457),
(9, 'Muscle Soothe', 2, 'Body Massage', 97, 1462852204409, 1469587590351),
(10, 'Purity Revival', 3, 'Cleansing', 99, 1462852204409, 1462852204409),
(11, 'Utter Rejuvenation', 3, 'Revitalisation', 98, 1462852204409, 1462852204409),
(12, 'Deep Hydration', 3, 'Moisturising', 97, 1462852204409, 1468668867813),
(13, 'Glow', 3, 'Brightening', 96, 1462852204409, 1462852204409),
(14, 'For The Body', 4, 'For The Body', 99, 1462852204409, 1462852204409),
(15, 'For The Face', 4, 'For The Face', 98, 1462852204409, 1462852204409),
(16, 'Face & Eye', 5, 'Face & Eye', 99, 1462852204409, 1462852204409),
(17, 'Head-To-Toe', 5, 'Head To Toe', 98, 1462852204409, 1469588480059),
(18, 'Goodbye Acne!', 6, 'Facial', 99, 1462852204409, 1469588563188),
(19, 'Magic V-Shape Treatment+', 1, 'Steamline Jawline', 92, 1469584928869, 1470637508839),
(20, 'Collagen+', 1, 'Anti-Aging Treatment', 92, 1469585664695, 1469585751940),
(21, 'Méthode Jeanne Piaubert', 1, 'Luxury Art of the French Touch ', 0, 1469586264406, 1470115846764),
(22, 'No More Large Pores!', 6, 'Facial', 98, 1469588602360, 1469588602360),
(23, 'Oil Control!', 6, 'Facial', 97, 1469588624741, 1469588624741),
(24, 'Lymphatic Drainage ', 3, 'Luxury Art of the French Touch ', 99, 1470115493060, 1470115859946);

-- --------------------------------------------------------

--
-- Table structure for table `TB_Therapy`
--

CREATE TABLE `TB_Therapy` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `subCategoryID` int(10) UNSIGNED NOT NULL,
  `description` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `benefits` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `duration` smallint(5) UNSIGNED NOT NULL,
  `price` mediumint(8) UNSIGNED NOT NULL,
  `promotionPrice` mediumint(8) UNSIGNED DEFAULT NULL,
  `promotionStartTime` bigint(20) UNSIGNED DEFAULT NULL,
  `promotionEndTime` bigint(20) UNSIGNED DEFAULT NULL,
  `promotionTerms` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayPriority` tinyint(3) UNSIGNED NOT NULL,
  `createdTime` bigint(20) UNSIGNED NOT NULL,
  `lastUpdateTime` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TB_Therapy`
--

INSERT INTO `TB_Therapy` (`id`, `name`, `subCategoryID`, `description`, `benefits`, `duration`, `price`, `promotionPrice`, `promotionStartTime`, `promotionEndTime`, `promotionTerms`, `displayPriority`, `createdTime`, `lastUpdateTime`) VALUES
(1, 'Acne Treatment: Purification+', 1, 'Purification+ is targeted at problem skin in individuals that are prone to breakouts. It is best for oily & combination skin, clogged pores and severe acne and is suitable for uneven skintone and sensitive skin.                                                                              \r\n\r\nGet to the source of problem skin and inflammations by eradicating acne-causing bacteria deep within the skin with medical-grade light therapy that targets acne, open pores and excess sebum production. An intensive infusion of oxygen using the OxyMed machine is combined with microcurrent technology to soothe inflammation, encourage repair and improve skin tone and sensitivity. ', 'Anti-bacterial\r\n Tighter Pores\r\nLightened Acne Scars\r\nReduce Skin Sensitivity\r\nEven and Radiant Skintone', 90, 38000, NULL, NULL, NULL, '', 99, 1462852206406, 1470363058589),
(2, 'Pigmentation Therapy: Lightening+', 2, 'Lightening+ is targeted at skin with hyper-pigmentation, age spots, acne scars, dull and uneven skin tone. It is best for individuals with dull and uneven skin tone with pigmentation, sun spots or age spots.   \r\n\r\nAesthetic Dynamic Pulse Light (DPL) therapy targets hyper-pigmentation at a deep level of the skin, and effectively rejuvenates skin for a brighter, more even-toned appearance. An intensive infusion of oxygen with OxyMed works synergistically with a hydration mask to plump, revitalise and improve blood circulation.', 'Cell Renewal\r\nLightened Pigments & Acne Scars\r\nIncrease in Collagen & Elastin Production\r\n', 90, 38000, NULL, NULL, 1462060800000, '', 98, 1462852206406, 1470363134160),
(3, 'Whitening Treatment: Whitening+', 3, 'Whitening+ is targeted at achieving a fairer, more even skin tone. It is best for individuals with a dull and uneven complexion. \r\n \r\nSkin is smoothened and deeply cleansed with Diamond Peel therapy to remove dead skin cells while an intensive Vitamin C mask and a hydrating pearl mask penetrate deeply for a brighter and visibly fairer complexion. \r\n', 'Reduction of Fine Lines\r\nRejuvenated and radiant complexion that is plumped and well-hydrated.\r\n', 90, 38000, NULL, NULL, NULL, '', 97, 1462852206406, 1470363151343),
(4, 'Firming Treatment: Firming+', 4, 'Firming+ is targeted at saggy skin, cheeks, fine lines and wrinkles. Also aids in lifting sunken cheeks, droopy eyelids and double chins.\r\n\r\nCutting-edge PROIONIC radiofrequency therapy of 448 kHz wavelength aids in microcirculation and is used on targeted areas of the face to firm, lift and boost cellular regeneration. An oxygen infusion spray is used to tone and revitalize, and encourage deeper penetration of the hydrating ampoule. \r\n', 'Redefined Facial Contours\r\nTightening & Lifting of Saggy Skin\r\nIntense Hydration \r\n', 90, 38000, NULL, NULL, NULL, '', 96, 1462852206406, 1470363167510),
(5, 'Firming Treatment: Magic V-Shape Treatment+', 19, 'V Shape Treatment+ is targeted at women who desire a more streamlined jawline. Individuals with saggy skin, fine lines and wrinkles as well as dull and fatigued skin can also benefit greatly from V Shape Treatment+.\r\n \r\nTighten and lift slackened jowls and saggy cheek with a combination of radiofrequency and vacuum suction. The oxygen infusion therapy encourages deeper penetration of the acacia collagen and lifting ampoule mask, so that the face recovers sharper angles and that coveted V-shape silhouette.\r\n', 'Defined Jawline; Firming Effect with Visible Lift in Sagging Skin\r\nSkin is Brighter & Renewed, and has an Increased Collagen Level', 90, 38000, NULL, NULL, NULL, '', 93, 1462852206406, 1470637447902),
(6, 'Anti-Aging Treatment: Collagen+', 20, 'Collagen+ is targeted at individuals with dull and uneven skin tone, sullen skin lacking moisture and bounce, fine lines and wrinkles sensitive skin. \r\n \r\nGet to the source of dull and lifeless skin with medical-grade laser therapy that boosts collagen production whilst reducing fine lines and wrinkles while improving the skin texture. An intensive infusion of oxygen using the OxyMed machine is combined with a collagen ampoule to soothe and nourish the skin further.', 'Skin Rejuvenation; Skin Tightening\r\nWrinkle Reduction; Collagen Renewal\r\nAcne Clearance', 90, 38000, NULL, NULL, NULL, '', 92, 1462852206406, 1470363301272),
(7, 'Detox Treatment: Detoxification+', 5, 'Detoxification+ is targeted at dull or congested skin experiencing poor blood circulation due to lifestyle factors.\r\n \r\nAdvanced LED microcurrent technology, in conjunction with hot and cold massage therapy and a detoxifying essential oil massage , works to encourage removal of toxins, lymphatic drainage, blood circulation and tighten open pores. \r\n \r\n', 'Improved Blood Circulation\r\nReduction in Puffiness\r\nSkin feels Refreshed, Renewed\r\nSofter & Smoother', 90, 38000, NULL, NULL, NULL, '', 95, 1462852206406, 1470363253270),
(8, 'Eye Treatment: Undereye+', 6, 'Undereye+ is targeted at puffy eye bags, dark circles, fine lines & wrinkles around the eye contour. Pick this treatment to get rid of your ‘designer’ bags. \r\n \r\nA potent combination of LED Microcurrent Technology and oxygen infusion therapies work magic on the delicate eye area, giving it a fresher, smoother, decongested and more radiant appearance. A collagen eye mask keeps dehydration lines at bay. \r\n', 'Reduces Fine Lines\r\nPuffiness & Dark Eye Circles in the undereye area \r\nImprove Blood Circulation', 20, 18000, NULL, NULL, NULL, '', 94, 1462852206406, 1470363244021),
(9, 'Body Contouring: Firm Up! ', 7, 'Firm Up! Is best for individuals with sagging and loose skin, cellulites, stubborn fats and areas that jiggle with simple movements.\r\n \r\nUtilising the advanced radiofrequency technology of the Indiba machine, this comfortable, multi-faceted treatment gives slackened skin tissues a visible lift. It can also target cellulite, stretch marks and stubborn fats to give your body a more streamlined silhouette and a sleeker, smoother appearance.   ', 'Firming & Lifting Effect Visible after One Treatment', 60, 35000, NULL, NULL, NULL, '', 86, 1462852206406, 1470363395179),
(10, 'Body Scrub: Deep Cleanse', 8, 'Deep Cleanse is best for individuals that are too lazy to exfoliate on a frequent basis or has dull skin colour due to a thick layer of dead skin.\r\n \r\nPamper your body by treating it to a deeply relaxing, therapeutic scrub that is guaranteed to leave skin petal-soft and silky-smooth. Choose from three all-natural, luxurious body scrubs: \r\nHIMALAYAN SALT\r\nROSE SALT  \r\nLAVENDER SALT\r\n', 'Glowy, soft and supple skin, that is smooth and well hydrated.\r\n', 20, 8800, NULL, NULL, NULL, '', 85, 1462852206406, 1470363811222),
(11, 'Body Massage: Muscle Soothe', 9, 'Muscle sooth is best for individuals working in a high stress environment with kinks and knots in the shoulders, back and arms. \r\n \r\nEvery body needs a little unwinding from the tensions and stresses of the day. Treat yours with one of our three signature massage therapies, each with your choice of 100 per cent pure essential oils:\r\n\r\nLYMPHATIC: \r\nA gentle massage technique that focuses on acu-points to unblock lymph nodes, promote better circulation and help to reduce water retention and bloatedness.\r\n\r\nRELAX: \r\nA soothing massage that works to recharge tired minds and bodies.\r\n\r\nTENSION RELEASE:   \r\nSpecially created to ease out all the kinks and knots on shoulders and back, this deep-tissue massage loosens tight muscles and releases tension from the body.\r\n\r\n', 'LYMPHATIC:  Blood circulation and relaxed muscles\r\nRELAX: Relaxed muscles and calming effects\r\nTENSION RELEASE: Relaxed muscles, and better posture', 60, 15000, NULL, NULL, NULL, '', 84, 1462852206406, 1469778660441),
(12, 'Purity Revival', 10, 'Purity Revival works best on oily combination skin plagued with whiteheads, blackheads and acne. \r\n \r\nUnclog pores and soothe inflammation with a soothing camomile ampoule mask and an oxygenating ampoule. Skin is deeply cleansed, calmed and impurities are purged for a clearer, smoother visage.  \r\n', 'Supple Skin\r\nTighter Pores Removal of impurities', 90, 22800, NULL, NULL, NULL, '', 90, 1462852206406, 1470363335953),
(13, 'Utter Rejuvenation', 11, 'Utter Rejuvenation works best on dull and uneven and puffy skintone and puffiness caused by water retention.\r\n \r\nA dull, fatigued-looking complexion can put years on your face. Get a fresher outlook on life with this de-stressing facial. A lymphatic massage works to stimulate the detox process, while a Co-Enzyme Q10 ampoule mask and a caviar ampoule adds an inner glow and density to the skin.  \r\n', 'Reduce Puffiness\r\nPromotes Radiance', 60, 22800, NULL, NULL, NULL, '', 89, 1462852206406, 1470363349232),
(14, 'Deep Hydration', 12, 'Deep Hydration is best for individuals with dry, flaking patches, fine lines and wrinkles. \r\n \r\nSkin in need of moisture can appear rough and dull with fine lines. Give your dehydrated or dry skin the deep nourishment it deserves with this intensely hydrating treatment, utilising a grapefruit ampoule mask and a hydrating ampoule. \r\n', 'Deep Hydration\r\nNatural Glow', 60, 22800, NULL, NULL, NULL, '', 88, 1462852206406, 1470363371670),
(15, 'Glow', 13, 'Glow is best for individuals with clogged pores, dull skin and puffy face due to lack of water and sleep.\r\n \r\nModern urban life and daily exposure to pollution equal congested, sensitised and dull-looking skin. Stimulate the release of toxins with a lymphatic massage coupled with a detox essential oil and a skin-brightening ampoule.', 'Visibly Brighter Skin\r\nTighter Pores\r\nReduction in Puffiness', 90, 22800, NULL, NULL, NULL, '', 87, 1462852206406, 1470363384766),
(16, 'Hair Removal: For The Body', 14, 'Utilising advanced Dynamic Pulsed Light (DPL) technology, this non-invasive, utterly comfortable treatment permanently removes 80 to 90 per cent of hair volume and growth in the targeted areas. It is proven to be safe, quick and highly effective, even for large areas like the legs and arms and for sensitive areas like the bikini line.   \r\n\r\nChoose from:\r\nBRAZILIAN    60 minutes/ Price: $280\r\nBIKINI            60 minutes/ Price: $200 \r\nUNDERARM  60 minutes/ Price: $150 \r\nARM               60 minutes/ Price: $180 \r\nHALF LEG      60 minutes/ Price: $200 \r\nFULL LEG       60 minutes/ Price: $250 \r\n', 'Smooth Silky Skin', 60, 15000, NULL, NULL, NULL, '', 78, 1462852206406, 1470363632208),
(17, 'Underarm', 14, 'Utilising advanced Dynamic Pulsed Light (DPL) technology, a broad spectrum light source is a non-invasive and utterly comfortable treatment that permanently removes 80 to 90 per cent of hair volume and minimise growth in the targeted areas. It is proven to be safe, quick and highly effective, even for large areas like the legs and arms and for sensitive areas like the bikini line.', 'Smooth Silky Skin', 60, 15000, NULL, 1459468800000, 1462060800000, 'First session and new customers only. From Mon - Fri, 12noon - 4pm only.', 0, 1462852206406, 1469675425585),
(18, 'Half Leg', 14, 'Utilising advanced Dynamic Pulsed Light (DPL) technology, a broad spectrum light source is a non-invasive and utterly comfortable treatment that permanently removes 80 to 90 per cent of hair volume and minimise growth in the targeted areas. It is proven to be safe, quick and highly effective, even for large areas like the legs and arms and for sensitive areas like the bikini line.', 'Smooth Silky Skin', 60, 20000, NULL, NULL, NULL, '', 0, 1462852206406, 1469675375363),
(19, 'Full Leg', 14, 'Utilising advanced Dynamic Pulsed Light (DPL) technology, a broad spectrum light source is a non-invasive and utterly comfortable treatment that permanently removes 80 to 90 per cent of hair volume and minimise growth in the targeted areas. It is proven to be safe, quick and highly effective, even for large areas like the legs and arms and for sensitive areas like the bikini line.', 'Smooth Silky Skin', 60, 25000, NULL, NULL, NULL, '', 0, 1462852206406, 1469675412891),
(20, 'Arm', 14, 'Utilising advanced Dynamic Pulsed Light (DPL) technology, a broad spectrum light source is a non-invasive and utterly comfortable treatment that permanently removes 80 to 90 per cent of hair volume and minimise growth in the targeted areas. It is proven to be safe, quick and highly effective, even for large areas like the legs and arms and for sensitive areas like the bikini line.', 'Smooth Silky Skin', 60, 18000, NULL, NULL, NULL, '', 0, 1462852206406, 1469675440070),
(21, 'Bikini', 14, 'Utilising advanced Dynamic Pulsed Light (DPL) technology, a broad spectrum light source is a non-invasive and utterly comfortable treatment that permanently removes 80 to 90 per cent of hair volume and minimise growth in the targeted areas. It is proven to be safe, quick and highly effective, even for large areas like the legs and arms and for sensitive areas like the bikini line.', 'Smooth Silky Skin', 60, 20000, NULL, NULL, NULL, '', 0, 1462852206406, 1469675479163),
(22, 'Hair Removal: For The Face ', 15, 'The more sensitive and delicate face area is treated with LHE (Light and Heat Energy) therapy, which gently and effectively targets difficult-to-remove facial hair without side effects or damaging the skin around the hair follicles. \r\n\r\nChoose from: \r\nUPPER LIP	60 minutes/ Price: $110\r\nCHIN               60 minutes/ Price: $110 \r\n', 'Smooth Silky Skin', 60, 11000, NULL, NULL, NULL, '', 77, 1462852206406, 1470363688057),
(23, 'Chin', 15, 'The more sensitive and delicate face area is treated with LHE (Light and Heat Energy) therapy, which gently and effectively targets difficult-to-remove facial hair without side effects or damaging the skin around the hair follicles.', 'Smooth Silky Skin', 60, 11000, NULL, NULL, NULL, '', 0, 1462852206406, 1469675465500),
(24, 'Face & Eye: Opulent Rejuvenescence Treatment', 16, 'Opulent Rejuvenescence is best for fine lines, dull complexion and skin with poor blood circulation that is puffy. \r\n \r\nGetting prepped for a hot date or a gala event? If you need radiance in a hurry, this ultra-luxe treatment will conjure magic on your skin. Enter Indiba Therapy, a non-invasive radiofrequency (RF) based treatment that nourishes and balances your skin’s concentration of ions to increase collagen and elasticity for a firmer and silky smooth visage. Working to improve your blood circulation and lymphatic system as well, you’ll be sure to walk out feeling brand new.\r\n', 'Reduced puffiness\r\nLifting effects\r\nErase fine lines\r\nIntense hydration\r\nPearlised finish\r\n', 90, 48800, NULL, NULL, NULL, '', 83, 1462852206406, 1470363467596),
(25, 'Head-to-Toe: Total Body Rejuvenescence', 17, 'Total Body Rejuvenescence is the perfect head-to-toe pampering for a tired individual in need of some TLC. Common problems like dead skin, dull skin, water retention and puffiness are targeted and corrected.\r\n \r\nFor top-to-the tips of your tippy toes rejuvenation, opt for this all-encompassing pampering treat for your face and body. \r\n\r\nFACE: Your complexion is treated to a glow-revealing Diamond Peel microdermabrasion, an intensive oxygen infusion followed by a double mask treat of vitamin C and hydrating actives. \r\n\r\nBODY: Embark on a detoxifying journey with our signature lymphatic massage, which unblocks lymph nodes to release toxins, promote better blood circulation and reduces water retention for an overall feeling of wellbeing and lightness. \r\n', 'Brighter & Plumped-up Complexion\r\nRelaxed Muscles\r\nSmooth Skin from Top-to-toe\r\n', 150, 68800, NULL, NULL, NULL, '', 82, 1462852206406, 1470363492100),
(26, 'Goodbye Acne!', 18, 'Goodbye Acne is best for men plagued with oily skin, acne, blackheads, whiteheads, milia seeds.\r\n \r\nBid adieu to pesky breakouts and inflammations with this high-tech face treatment that utilises medical-grade Dynamic Pulsed Light (DPL) technology, an intensive oxygen infusion spray and cold microcurrent therapy to soothe, eradicate bacteria and encourage cellular repair. ', 'Balanced Skin\r\nReduced Sebum Production\r\nRemoval of Impurities', 90, 38000, NULL, NULL, NULL, '', 81, 1462852206406, 1470363506344),
(27, 'No More Large Pores!', 22, 'No More Large Pores! are best for men facing issues with large pores, acne scars and ‘strawberry noses’. \r\n \r\nUnclog and tighten open pores with the combination of medical-grade Dynamic Pulsed Light (DPL) technology, thorough manual extraction and a soothing bio-cellulose apple stem cell mask.\r\n \r\n', 'Tighter Pores\r\nLightened Acne Scars\r\n', 90, 38000, NULL, NULL, NULL, '', 80, 1462852206406, 1470363520577),
(28, 'Lymphatic Drainage Face Treatment', 24, 'The sensorial pleasure of skin-on-skin contact is at the centre of the Méthode Jeanne Piaubert patented technique. Precise, specialised and highly effective, the Méthode Jeanne Piaubert massage technique employed in all its face treatments is not only deeply relaxing, but also wields incredible results on the skin. \r\n \r\nMéthode Jeanne Piaubert Lymphatic Drainage Face Treatment\r\n \r\nDehydration, a lack of sleep and an unhealthy lifestyle can result in sluggish micro-circulation and blocked lymph nodes deep within your skin, resulting in water retention, bloating and poor skin tone. \r\n \r\nThe patented Méthode Jeanne Piaubert massage technique is developed to thoroughly drain the lymph nodes on your face, décolleté and neck, while enveloping you in a cocoon of comfort and wellbeing. \r\n', 'Balance is Restored to the Skin\r\nBoost Circulation; Face Regains its Streamlined, Youthful Contours. \r\n', 90, 28800, NULL, NULL, NULL, '', 91, 1469586790587, 1470363320452),
(29, 'Oil Control!', 23, 'Oil Control! Is best for men that have extremely shiny and oily faces due to excessive production of sebum. This treatment also helps with large pores.\r\n \r\nHealthy skin should look radiant, not shiny. Keep the greasies at bay with a dose of medical-grade Dynamic Pulsed Light (DPL) technology and a deep-cleansing clay mask and purifying ampoule. \r\n ', 'Brighter Skin\r\nTighter Pores\r\nReduced Sebum\r\n', 90, 38000, NULL, NULL, NULL, '', 79, 1469589048480, 1470363539598);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `TB_Accolade`
--
ALTER TABLE `TB_Accolade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TB_Article`
--
ALTER TABLE `TB_Article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TB_Category`
--
ALTER TABLE `TB_Category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `TB_JobListing`
--
ALTER TABLE `TB_JobListing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TB_Member`
--
ALTER TABLE `TB_Member`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `emailVerified` (`emailVerified`),
  ADD KEY `otpAttemptsRemaining` (`otpAttemptsRemaining`),
  ADD KEY `passwordAttemptsRemaining` (`passwordAttemptsRemaining`);

--
-- Indexes for table `TB_Staff`
--
ALTER TABLE `TB_Staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `emailVerified` (`emailVerified`),
  ADD KEY `otpAttemptsRemaining` (`otpAttemptsRemaining`),
  ADD KEY `passwordAttemptsRemaining` (`passwordAttemptsRemaining`);

--
-- Indexes for table `TB_SubCategory`
--
ALTER TABLE `TB_SubCategory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `categoryID` (`categoryID`);

--
-- Indexes for table `TB_Therapy`
--
ALTER TABLE `TB_Therapy`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nameSubCat` (`name`,`subCategoryID`),
  ADD KEY `subCategoryID` (`subCategoryID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `TB_Accolade`
--
ALTER TABLE `TB_Accolade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `TB_Article`
--
ALTER TABLE `TB_Article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `TB_Category`
--
ALTER TABLE `TB_Category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `TB_JobListing`
--
ALTER TABLE `TB_JobListing`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `TB_Member`
--
ALTER TABLE `TB_Member`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TB_Staff`
--
ALTER TABLE `TB_Staff`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `TB_SubCategory`
--
ALTER TABLE `TB_SubCategory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `TB_Therapy`
--
ALTER TABLE `TB_Therapy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `TB_SubCategory`
--
ALTER TABLE `TB_SubCategory`
  ADD CONSTRAINT `TB_SubCategory_ibfk_1` FOREIGN KEY (`categoryID`) REFERENCES `TB_Category` (`id`);

--
-- Constraints for table `TB_Therapy`
--
ALTER TABLE `TB_Therapy`
  ADD CONSTRAINT `TB_Therapy_ibfk_1` FOREIGN KEY (`subCategoryID`) REFERENCES `TB_SubCategory` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
